<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
| AUTO-LOADER
| -------------------------------------------------------------------
| This file specifies which systems should be loaded by default.
|
| In order to keep the framework as light-weight as possible only the
| absolute minimal resources are loaded by default. For example,
| the database is not connected to automatically since no assumption
| is made regarding whether you intend to use it.  This file lets
| you globally define which systems you would like loaded with every
| request.
|
| -------------------------------------------------------------------
| Instructions
| -------------------------------------------------------------------
|
| These are the things you can load automatically:
|
| 1. Packages
| 2. Libraries
| 3. Helper files
| 4. Custom config files
| 5. Language files
| 6. Models
|
*/

/*
| -------------------------------------------------------------------
|  Auto-load Packges
| -------------------------------------------------------------------
| Prototype:
|
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
|
*/

$autoload['packages'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| These are the classes located in the system/libraries folder
| or in your application/libraries folder.
|
| Prototype:
|
|	$autoload['libraries'] = array('database', 'session', 'xmlrpc');
*/

$autoload['libraries'] = array(
	'database', 'session', 'form_validation',
	
	'OTable',
	'OKandidat_bahasa', 'OKandidat_dokumen', 'OKandidat_keluarga', 'OKandidat_kesehatan', 'OKandidat_keterampilan',
	'OKandidat_pendidikan', 'OKandidat_pengalaman', 'OKandidat_resume', 'OKandidat',
	
	'OMaster_agama', 'OMaster_bahasa', 'OMaster_bidang_usaha', 'OMaster_gol_darah', 'OMaster_jenis_kelamin',
	'OMaster_kategori', 'OMaster_level_karir', 'OMaster_lokasi_kabupaten', 'OMaster_lokasi_kawasan', 'OMaster_lokasi_kecamatan',
	'OMaster_lokasi_kelurahan', 'OMaster_lokasi_negara', 'OMaster_lokasi_propinsi', 'OMaster_pendidikan', 'OMaster_shdk',
	'OMaster_status_kawin', 'OMaster_waktu_kerja', 'OMaster_status_kerja',
	
	'OLowongan', 'OPerusahaan', 'OPerusahaan_promo',
	
	'OSetting', 'OAdmin', 'OUser', 'OArtikel','OPage','OAd','mpdf'
	);


/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['helper'] = array('url', 'file');
*/

$autoload['helper'] = array('url', 'file', 'form', 'string', 'text', 'inflector', 'common', 'country', 'currency', 'dt', 'image', 'login', 'mail', 'myarray', 'myform', 'notif', 'pagination', 'photo', 'time', 'utility', 'valid');


/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['config'] = array('config1', 'config2');
|
| NOTE: This item is intended for use ONLY if you have created custom
| config files.  Otherwise, leave it blank.
|
*/

$autoload['config'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['language'] = array('lang1', 'lang2');
|
| NOTE: Do not include the "_lang" part of your file.  For example
| "codeigniter_lang.php" would be referenced as array('codeigniter');
|
*/

$autoload['language'] = array();


/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|
|	$autoload['model'] = array('model1', 'model2');
|
*/

$autoload['model'] = array();


/* End of file autoload.php */
/* Location: ./application/config/autoload.php */