<?php
$config['image_sizes'] = array('thumbnail' => '200|100|true',
							   'banner' => '900|300|false',
							   /*
							   // gak perlu di komen bawah iki, pake yg udah aku set global :)
							   'product-thumbnail' => '30|30|true',
							   'artikel' => '93|92|true',
							   'logo_perusahaan' => '60|60|false',
							   'list_kandidat' => '50|50|false',
								*/
							   'avatar' => '200|200|true|true',
							   'square' => '200|200|true',
							   'medium-square' => '400|400|true',
							   'small' => '100|100|false',
							   'medium' => '200|200|false',
							   'large' => '400|400|false',
							   'extra-large' => '600|600|false'
							   );
?>