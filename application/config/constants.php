<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

define('DOMAIN_NAME',							$_SERVER['HTTP_HOST']);

if(stristr($_SERVER['HTTP_HOST'],"rk.dev"))
{
	define('FPATH','');
	define('IS_LOCALHOST', true);
	define('RECAPTCHA_PUBLIC', '6LdHNM8SAAAAAIq3eiyFmunplYrMk6hDgQOVsyZK');
	define('RECAPTCHA_PRIVATE', '6LdHNM8SAAAAADwMd-I9o8IrnWV20UaQKhhHiW89');
}
elseif(stristr($_SERVER['HTTP_HOST'],"localhost"))
{
	define('FPATH','rk.com/');
	define('IS_LOCALHOST', true);
	define('RECAPTCHA_PUBLIC', '6LdHNM8SAAAAAIq3eiyFmunplYrMk6hDgQOVsyZK');
	define('RECAPTCHA_PRIVATE', '6LdHNM8SAAAAADwMd-I9o8IrnWV20UaQKhhHiW89');
}
elseif(stristr($_SERVER['HTTP_HOST'],"develio.us"))
{
	define('FPATH','rumahkandidat/');
	define('IS_LOCALHOST', true);
	define('RECAPTCHA_PUBLIC', '6LdHNM8SAAAAAIq3eiyFmunplYrMk6hDgQOVsyZK');
	define('RECAPTCHA_PRIVATE', '6LdHNM8SAAAAADwMd-I9o8IrnWV20UaQKhhHiW89');
}
else
{
	define('FPATH','');
	define('IS_LOCALHOST', false);
	define('RECAPTCHA_PUBLIC', '6LdSY-8SAAAAAJBSDxkxmsusjLBek5jdp4XqU4Vc');
	define('RECAPTCHA_PRIVATE', '6LdSY-8SAAAAAAotE1YwaT-vnHK16UM9gaC3sfmP');
}


/* End of file constants.php */
/* Location: ./application/config/constants.php */