<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OMaster_lokasi_kabupaten{
    var $CI;
    var $db;
    var $table_name = "master_lokasi_kabupatens";
    var $row;
    var $id;
    
    public function __construct($id, $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        $arr = array("id" => $id);
        	if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }		
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public static function add($arr)
    {
        $CI =& get_instance();
        $CI->db->insert("master_lokasi_kabupatens",$arr);
        return $CI->db->insert_id();
    }
    
    public static function get_list($start,$limit,$orderby = "master_lokasi_propinsi_id ASC, nama ASC",$where = "")
    {
        $CI =& get_instance();

        $q = "SELECT SQL_CALC_FOUND_ROWS master_lokasi_kabupatens.*
                FROM master_lokasi_kabupatens
                ";
        $arr = array();
        if($where != "")
        {
            $q .= " WHERE ".$where;
        }
        if($orderby != "")
        {
            $q .= " ORDER BY ".$orderby;
        }
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $CI->db->query($q,$arr);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }

    public static function get_list_filter($start,$limit,$orderby,$where)
    {
        $CI =& get_instance();
        if(empty($orderby)) $orderby="master_lokasi_propinsis.nama ASC, master_lokasi_kabupatens.nama ASC";

        $q = "SELECT SQL_CALC_FOUND_ROWS master_lokasi_kabupatens.* 
                FROM master_lokasi_kabupatens
                INNER JOIN master_lokasi_propinsis
                    ON master_lokasi_propinsis.id = master_lokasi_kabupatens.master_lokasi_propinsi_id
                ";
        $arr = array();
        if($where != "")
        {
            $q .= " WHERE ".$where;
        }
        if($orderby != "")
        {
            $q .= " ORDER BY ".$orderby;
        }
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $CI->db->query($q,$arr);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public static function search($keyword,$orderby = "")
    {
        $CI =& get_instance();
        $q = "SELECT SQL_CALC_FOUND_ROWS * 
                FROM `master_lokasi_kabupatens`
                WHERE id = ?";
        $arr = array();
        $arr[] = intval($keyword);
        if($orderby != "")
        {
            $q .= " ORDER BY ".@mysql_escape_string($orderby);  
        }
        $res = $CI->db->query($q,$arr);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public static function search_filter($keyword,$start,$limit,$orderby,$where)
    {
        $CI =& get_instance();
        if(empty($orderby)) $orderby="master_lokasi_propinsis.nama ASC, master_lokasi_kabupatens.nama ASC";

        $where_arr = $and_q_arr = NULL;
        if(!empty($keyword))
        {
            $keyword = $CI->db->escape_like_str($keyword);
            $or_q_arr = NULL;
            $or_q_arr[] = "master_lokasi_negaras.nama LIKE ?";
            $or_q_arr[] = "master_lokasi_propinsis.nama LIKE ?";
            $or_q_arr[] = "master_lokasi_kabupatens.nama LIKE ?";
            $where_arr[] = "%".$keyword."%";
            $where_arr[] = "%".$keyword."%";
            $where_arr[] = "%".$keyword."%";
            $and_q_arr[] = "(".implode(" OR ", $or_q_arr).")";
        }
        if($where != "")
        {
            $and_q_arr[] = $where;
        }

        if(count($and_q_arr) > 0)
        {
            $add_sql = " WHERE ".implode(" AND ", $and_q_arr);
        }
        
        $q = "SELECT SQL_CALC_FOUND_ROWS master_lokasi_kabupatens.* 
                FROM master_lokasi_kabupatens
                INNER JOIN master_lokasi_negaras
                    ON master_lokasi_negaras.id = master_lokasi_kabupatens.master_lokasi_negara_id
                INNER JOIN master_lokasi_propinsis
                    ON master_lokasi_propinsis.id = master_lokasi_kabupatens.master_lokasi_propinsi_id
                {$add_sql}
                ";
        if($orderby != "")
        {
            $q .= " ORDER BY ".$orderby;
        }
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $CI->db->query($q,$where_arr);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public static function drop_down_select($name,$selval,$optional = "",$default="",$where="",$readonly)
	{
		$list = OMaster_lokasi_kabupaten::get_list(0, 0, "master_lokasi_propinsi_id ASC, nama ASC", $where);
		
        $O = new OMaster_lokasi_kabupaten();
        foreach($list as $r)
        {
            $O->setup($r);
            $arr[$r->id] = $O->get_nama();
        }
        unset($O);
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
        return $this->db->delete($this->table_name,array("id" => $this->id));		
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $this->db->update($this->table_name,array('photo' => $image_file),array('id' => $this->id));
    }
    
    public function get_photo($type = "medium")
    {
    	$filename = $this->row->photo;
        return base_url()."/_assets/images/uploads/{$type}/".$filename;
    }
    
    public function update_url_title()
    {
    	
    	$new_url_title = url_title($this->row->nama,"-",TRUE);
    	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
        $res = $this->db->query($q,array($new_url_title,$this->id));
        if(!emptyres($res))
        {
        	$new_url_title .= "-".$this->id;
        }
        $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
    }
    
    public function get_link()
    {
    	return site_url("lowongan/listing/lokasi_kabupaten/".$this->row->url_title);
    }
        
    public function get_nama()
    {
    	return $this->row->nama;
    }

}
