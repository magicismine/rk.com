<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OLowongan{
    var $CI;
    var $db;
    var $table_name = "lowongans";
    var $row;
    var $id;
    
    public function __construct($id, $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        $arr = array("id" => $id);
        	if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }		
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public static function add($arr)
    {
        $CI =& get_instance();
        $CI->db->insert("lowongans",$arr);
        return $CI->db->insert_id();
    }
    
    public static function get_list($start,$limit,$orderby = "id DESC",$where = "")
    {
    	$CI =& get_instance();
        if($orderby == "") $orderby = "id DESC";
        if(intval($limit) == 0)
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `lowongans` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby}",array(intval($start),intval($limit))); 
        }
        else
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `lowongans` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby} LIMIT ?, ?",array(intval($start),intval($limit))); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }

    public static function get_list_front($start,$limit,$order_by="lowongans.id DESC",$filter_arr=array())
    {
        $CI =& get_instance();
        if(empty($order_by)) $order_by = 'lowongans.id DESC';
        $where_arr = array('users.status' => 'aktif');
        if(is_array($filter_arr) && count($filter_arr) > 0) $where_arr = array_merge($where_arr, $filter_arr);
        $CI->db->select('SQL_CALC_FOUND_ROWS lowongans.*', FALSE)
                ->from('lowongans')
                ->join('perusahaans', 'perusahaans.id = lowongans.perusahaan_id', 'inner')
                ->join('users', 'users.id = perusahaans.user_id', 'inner')
                ->where($where_arr);
        if( !empty($order_by) ) $CI->db->order_by($order_by);
        $CI->db->group_by('lowongans.id');
        if( !empty($limit) ) $CI->db->limit(intval($limit),intval($start));
        $res = $CI->db->get();
        if(emptyres($res)) return FALSE;
        return $res->result();
    }
    
    public static function get_list_filter($keyword,$start,$limit,$orderby,$where,$filter_arr)
    {
        //kategori,$bidang_usaha,$pendidikan,$level_karir,$waktu_kerja,$propinsi,$kabupaten
        $CI =& get_instance();
        $arr = $and_q = NULL;
        $add_join = $add_where = "";
        if(!empty($keyword))
        {
            // posisi, waktu kerja, level karir, kategori, bidang usaha, lokasi
            $keyword = $CI->db->escape_like_str($keyword);
            $and_q[] = "(
                        lowongans.posisi LIKE ?
                        OR lowongans.deskripsi LIKE ?
                        OR perusahaans.nama LIKE ?
                        OR master_waktu_kerjas.nama LIKE ?
                        OR master_bidang_usahas.nama LIKE ?
                        OR lowongan_level_karirs.level_karir_id IN(SELECT id FROM master_level_karirs WHERE nama LIKE ?)

                        OR master_lokasi_negaras.nama LIKE ?
                        OR master_lokasi_propinsis.nama LIKE ?
                        OR master_lokasi_kabupatens.nama LIKE ?
                        OR master_lokasi_kawasans.nama LIKE ?
                        )";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $arr[] = "%".$keyword."%";
            $filter_kategori = $filter_bidang_usaha = $filter_pendidikan = $filter_level_karirs = $filter_waktu_kerja = $filter_negara = $filter_propinsi = $filter_kabupaten = $filter_kawasan = TRUE;
        }
        if(count($filter_arr) > 0)
        {
            $filter_arr = array_unique($filter_arr);
            foreach($filter_arr as $key => $val)
            {
                if(empty($key) || empty($val)) continue;

                if(stristr($key, "perusahaan"))
                {
                    $and_q[] = "lowongans.perusahaan_id = ?";
                    $arr[] = intval($val);
                    $filter_perusahaan = TRUE;
                    //$add_join .= " INNER JOIN perusahaans ON lowongans.perusahaan_id = perusahaans.id";
                }
                elseif(stristr($key, "kategori"))
                {
                    $and_q[] = "lowongans.kategori_id = ?";
                    $arr[] = intval($val);
                    $filter_kategori = TRUE;
                    $add_join .= " LEFT JOIN master_kategoris ON lowongans.kategori_id = master_kategoris.id";
                }
                elseif(stristr($key, "bidang_usaha"))
                {
                    $and_q[] = "lowongans.bidang_usaha_id = ?";
                    $arr[] = intval($val);
                    $filter_bidang_usaha = TRUE;
                    $add_join .= " LEFT JOIN master_bidang_usahas ON lowongans.bidang_usaha_id = master_bidang_usahas.id";
                }
                elseif(stristr($key, "pendidikan"))
                {
                    $and_q[] = "lowongans.pendidikan_id = ?";
                    $arr[] = intval($val);
                    $filter_pendidikan = TRUE;
                    $add_join .= " LEFT JOIN master_pendidikans ON lowongans.pendidikan_id = master_pendidikans.id";
                }
                elseif(stristr($key, "level_karir"))
                {
                    if(is_array($val) && count($val) > 0)
                    {
                        $has_value = FALSE;
                        foreach($val as $k => $v)
                        {
                            if(!empty($v))
                            {
                                $has_value = TRUE; break;
                            }
                        }
                        if($has_value)
                        {
                            $and_q[] = "lowongan_level_karirs.level_karir_id IN(?)";
                            $arr[] = implode(",",$val);
                        }
                    }
                    elseif(intval($val) > 0)
                    {
                        $and_q[] = "lowongan_level_karirs.level_karir_id IN(?)";
                        $arr[] = $val;
                    }
                    $filter_level_karirs = TRUE;
                    $add_join .= " LEFT JOIN lowongan_level_karirs ON lowongans.id = lowongan_level_karirs.lowongan_id";
                }
                elseif(stristr($key, "waktu_kerja"))
                {
                    $and_q[] = "lowongans.waktu_kerja_id = ?";
                    $arr[] = intval($val);
                    $filter_waktu_kerja = TRUE;
                    $add_join .= " LEFT JOIN master_waktu_kerjas ON lowongans.waktu_kerja_id = master_waktu_kerjas.id";
                }
                elseif(stristr($key, "negara"))
                {
                    $and_q[] = "(lowongans.lokasi_negara_id = ?)";
                    $arr[] = intval($val);
                    $filter_negara = TRUE;
                    $add_join .= " LEFT JOIN master_lokasi_negaras ON lowongans.lokasi_negara_id = master_lokasi_negaras.id";
                }
                elseif(stristr($key, "propinsi"))
                {
                    $and_q[] = "(lowongans.lokasi_propinsi_id = ?)";
                    $arr[] = intval($val);
                    $filter_propinsi = TRUE;
                    $add_join .= " LEFT JOIN master_lokasi_propinsis ON lowongans.lokasi_propinsi_id = master_lokasi_propinsis.id";
                }
                elseif(stristr($key, "kabupaten"))
                {
                    $and_q[] = "(lowongans.lokasi_kabupaten_id = ?)";
                    $arr[] = intval($val);
                    $filter_kabupaten = TRUE;
                    $add_join .= " LEFT JOIN master_lokasi_kabupatens ON lowongans.lokasi_kabupaten_id = master_lokasi_kabupatens.id";
                }
                elseif(stristr($key, "kawasan"))
                {
                    $and_q[] = "(lowongans.lokasi_kawasan_id = ?)";
                    $arr[] = intval($val);
                    $filter_kawasan = TRUE;
                    $add_join .= " LEFT JOIN master_lokasi_kawasans ON lowongans.lokasi_kawasan_id = master_lokasi_kawasans.id";
                }
            }
        }
        if(stristr($orderby, "kategori")) { $filter_kategori = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "bidang_usaha")) { $filter_bidang_usaha = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "pendidikan")) { $filter_pendidikan = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "level_karir")) { $filter_level_karirs = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "waktu_kerja")) { $filter_waktu_kerja = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "negara")) { $filter_negara = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "propinsi")) { $filter_propinsi = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "kabupaten")) { $filter_kabupaten = TRUE; $keyword = TRUE; }
        if(stristr($orderby, "kawasan")) { $filter_kawasan = TRUE; $keyword = TRUE; }
        if($keyword)
        {
            if($filter_kategori) $add_join .= " LEFT JOIN master_kategoris ON lowongans.kategori_id = master_kategoris.id";
            if($filter_bidang_usaha) $add_join .= " LEFT JOIN master_bidang_usahas ON lowongans.bidang_usaha_id = master_bidang_usahas.id";
            if($filter_pendidikan) $add_join .= " LEFT JOIN master_pendidikans ON lowongans.pendidikan_id = master_pendidikans.id";
            //if($filter_level_karirs) $add_join .= " LEFT JOIN lowongan_level_karirs ON lowongans.id = lowongan_level_karirs.lowongan_id";
            if($filter_waktu_kerja) $add_join .= " LEFT JOIN master_waktu_kerjas ON lowongans.waktu_kerja_id = master_waktu_kerjas.id";

            if($filter_negara) $add_join .= " LEFT JOIN master_lokasi_negaras ON lowongans.lokasi_negara_id = master_lokasi_negaras.id";
            if($filter_propinsi) $add_join .= " LEFT JOIN master_lokasi_propinsis ON lowongans.lokasi_propinsi_id = master_lokasi_propinsis.id";
            if($filter_kabupaten) $add_join .= " LEFT JOIN master_lokasi_kabupatens ON lowongans.lokasi_kabupaten_id = master_lokasi_kabupatens.id";
            if($filter_kawasan) $add_join .= " LEFT JOIN master_lokasi_kawasans ON lowongans.lokasi_kawasan_id = master_lokasi_kawasans.id";
        }
        if(count($and_q) > 0) $add_where = " AND ".implode(" AND ", $and_q);

        $q = "SELECT SQL_CALC_FOUND_ROWS lowongans.*
                FROM lowongans
                INNER JOIN perusahaans
                    ON perusahaans.id = lowongans.perusahaan_id
                INNER JOIN users
                    ON users.id = perusahaans.user_id
                {$add_join}
                WHERE users.status = 'aktif'
                AND lowongans.active = 1
                ";
        if($where != "") $q .= " AND ".$where;
        $q .= " {$add_where}";
        $q .= " GROUP BY lowongans.id";
        $q .= " {$add_sql}";
        if($orderby != "") $q .= " ORDER BY ".$orderby;
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $CI->db->query($q, $arr);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }

    public static function search($keyword,$orderby = "")
    {
    	$CI =& get_instance();
        $q = "SELECT SQL_CALC_FOUND_ROWS * 
        		FROM `lowongans`
                WHERE id = ?";
        $arr = array();
        $arr[] = intval($keyword);
                if($orderby != "")
        {
        	$q .= " ORDER BY ".@mysql_escape_string($orderby);
        	$res = $CI->db->query($q,$arr); 
        }
        else
        {
        	$res = $CI->db->query($q,$arr); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
                
    }
    
    public static function drop_down_select($name,$selval,$optional = "",$default="",$readonly)
	{
		$list = OLowongan::get_list(0, 0, "id ASC");
		
		foreach($list as $r)
		{
			$arr[$r->id] = $r->posisi;			
		}
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
        // delete all related
        // self
        $this->delete_level_karirs();
        // other
        $this->delete_lowongan_simpans();
        $this->delete_lowongan_lamars();
        return $this->db->delete($this->table_name,array("id" => $this->id));		
    }

    public function delete_level_karirs()
    {
        $this->db->delete("lowongan_level_karirs", array("lowongan_id" => $this->id));
        return true;
    }

    public function delete_lowongan_simpans()
    {
        $this->db->delete("kandidat_lowongan_simpans", array("lowongan_id" => $this->id));
        return true;
    }

    public function delete_lowongan_lamars()
    {
        $this->db->delete("kandidat_lowongan_lamars", array("lowongan_id" => $this->id));
        return true;
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }

    public function update_hits()
    {
        $this->db->query("UPDATE lowongans SET hits = hits + 1 WHERE id = ?",array($this->id));
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $this->db->update($this->table_name,array('photo' => $image_file),array('id' => $this->id));
    }
    
    public function get_photo($type = "medium")
    {
    	$filename = $this->row->photo;
        return base_url()."/_assets/images/uploads/{$type}/".$filename;
    }
    
    public function update_url_title()
    {
    	
    	$new_url_title = url_title($this->row->posisi,"-",TRUE);
    	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
        $res = $this->db->query($q,array($new_url_title,$this->id));
        if(!emptyres($res))
        {
        	$new_url_title .= "-".$this->id;
        }
        $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
    }
    
    public function get_link()
    {
        return site_url("lowongan/detail/".$this->get_perusahaan()->url_title."/".$this->row->url_title);   
    }

    public function get_nama()
    {
    	return $this->row->posisi;
    }

    public function is_expired()
    {
        if($this->row->dt_expired > date("Y-m-d")) return FALSE;
        else return TRUE;
    }

    public function generate_auto_number()
    {
        $low = get_total_lowongan();
        $total = $low->total;
        $number = $total + 1;
        $ret = str_pad($number, 5, "0", STR_PAD_LEFT); // 00001
        return "14".$ret;
    }

    public function get_current_complete_lokasi_name()
    {
        if($this->row->lokasi_kerja_kantor_flag == 1)
        {
            $perusahaan_r = $this->get_perusahaan();
            $negara_id = $perusahaan_r->lokasi_negara_id;
            $propinsi_id = $perusahaan_r->lokasi_propinsi_id;
            $kabupaten_id = $perusahaan_r->lokasi_kabupaten_id;
            $kawasan_id = $perusahaan_r->lokasi_kawasan_id;
        }
        else
        {
            $negara_id = $this->row->lokasi_negara_id;
            $propinsi_id = $this->row->lokasi_propinsi_id;
            $kabupaten_id = $this->row->lokasi_kabupaten_id;
            $kawasan_id = $this->row->lokasi_kawasan_id;
        }
        $arr = NULL;
        $OTmp = new OMaster_lokasi_negara($negara_id);
        $arr[$negara_id] = $OTmp->get_nama();
        $OTmp = new OMaster_lokasi_propinsi($propinsi_id);
        $arr[$propinsi_id] = $OTmp->get_nama();
        $OTmp = new OMaster_lokasi_kabupaten($kabupaten_id);
        $arr[$kabupaten_id] = $OTmp->get_nama();
        $OTmp = new OMaster_lokasi_kawasan($kawasan_id);
        $arr[$kawasan_id] = $OTmp->get_nama();
        unset($OTmp);
        return $arr;
    }

    public function get_lowongan_sejenis($start = 0, $limit = 10)
    {
        $q = "SELECT SQL_CALC_FOUND_ROWS * FROM lowongans WHERE kategori_id = ?";
        if(intval($limit) > 0) $q .= " LIMIT ?,?";        
        $res = $this->db->query($q,array($this->row->kategori_id,intval($start),intval($limit)));
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }

    public function get_perusahaan()
    {
        $q = "SELECT SQL_CALC_FOUND_ROWS * FROM perusahaans WHERE id = ?";
        $res = $this->db->query($q,array($this->row->perusahaan_id));
        if(emptyres($res)) return FALSE;
        else return $res->row();
    }   

    //get all list kandidat pelamar
    public function get_all_rekomendasi($start=0,$limit=0,$orderby="kandidat_lowongan_lamars.dt_added DESC",$where)
    {
        if($orderby == ""){ $orderby = "kandidat_lowongan_lamars.dt_added DESC";}
        $q = "SELECT SQL_CALC_FOUND_ROWS kandidats.*, kandidat_lowongan_lamars.*, lowongans.posisi
                FROM kandidat_lowongan_lamars
                INNER JOIN kandidats
                    ON kandidat_lowongan_lamars.kandidat_id = kandidats.id
                INNER JOIN lowongans 
                    ON kandidat_lowongan_lamars.lowongan_id = lowongans.id
                WHERE kandidat_lowongan_lamars.lowongan_id = ?
                    AND kandidat_lowongan_lamars.recommendation_flag = 1
                    ".($where != "" ? " AND ".$where : "")."
                ";
        $arr = array($this->id);
        if(trim($orderby) != "")
        {
            $q .= " ORDER BY {$orderby}";
        }
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $this->db->query($q,$arr);
        if(emptyres($res)) return FALSE;
        return $res->result();
    }


    //get_list kandidat pada lowongan tersebut
    public static function get_all_pelamar($start=0,$limit=0,$orderby="kandidat_lowongan_lamars.dt_added DESC",$where)
    {
        $CI =& get_instance();
        if($orderby == ""){ $orderby = "kandidat_lowongan_lamars.dt_added DESC";}
        $q = "SELECT SQL_CALC_FOUND_ROWS kandidats.*,kandidat_lowongan_lamars.*,perusahaans.nama,lowongans.posisi 
                FROM kandidat_lowongan_lamars
                INNER JOIN kandidats
                    ON kandidat_lowongan_lamars.kandidat_id = kandidats.id
                INNER JOIN lowongans
                    ON kandidat_lowongan_lamars.lowongan_id = lowongans.id
                LEFT JOIN perusahaans
                    ON lowongans.perusahaan_id = perusahaans.id
                ".($where != "" ? " WHERE ".$where : "")."
                ";
        $arr = array();
        if(trim($orderby) != "")
        {
            $q .= " ORDER BY {$orderby}";
        }
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $CI->db->query($q,$arr);
        if(emptyres($res)) return FALSE;
        return $res->result();
    }
    public function get_lamarans($start=0,$limit=0,$orderby="kandidat_lowongan_lamars.dt_added DESC",$where)
    {
        if($orderby == ""){ $orderby = "kandidat_lowongan_lamars.dt_added DESC";}
        $q = "SELECT SQL_CALC_FOUND_ROWS kandidats.*,kandidat_lowongan_lamars.*,lowongans.posisi 
                FROM kandidat_lowongan_lamars
                INNER JOIN kandidats
                    ON kandidat_lowongan_lamars.kandidat_id = kandidats.id
                INNER JOIN lowongans
                    ON kandidat_lowongan_lamars.lowongan_id = lowongans.id
                WHERE kandidat_lowongan_lamars.lowongan_id = ? 
                ".($where != "" ? " AND ".$where : "")."
                ";
        $arr = array($this->id);
        if(trim($orderby) != "")
        {
            $q .= " ORDER BY {$orderby}";
        }
        if(intval($limit) > 0)
        {
            $q .= " LIMIT ?, ?";
            $arr[] = intval($start);
            $arr[] = intval($limit);
        }
        $res = $this->db->query($q,$arr);
        if(emptyres($res)) return FALSE;
        return $res->result();
    }

    public function check_rekomendasi($id)
    {
        $arr['lowongan_id'] = intval($this->id);
        $arr['kandidat_id'] = intval($id);
        $arr['recommendation_flag'] = 1;
        $res = $this->db->get_where("kandidat_lowongan_lamars", $arr);
        if(emptyres($res)) return FALSE;
        else return TRUE;
    }

    public function delete_rekomendasi($id)
    {
        if(empty($id)) return;
        $where_arr['lowongan_id'] = intval($this->id);
        $where_arr['kandidat_id'] = intval($id);
        $arr['recommendation_flag'] = 0;
        $arr['dt_recommendation'] = '0000-00-00 00:00:00';
        return $this->db->update("kandidat_lowongan_lamars",$arr,$where_arr);
    }

    public function set_rekomendasi($id)
    {
        if(empty($id) || $this->check_rekomendasi($id)) return FALSE;
        $this->delete_rekomendasi($id);

        $where_arr['lowongan_id'] = intval($this->id);
        $where_arr['kandidat_id'] = intval($id);
        $arr['recommendation_flag'] = 1;
        $arr['dt_recommendation'] = date('Y-m-d H:i:s');
        $res = $this->db->update("kandidat_lowongan_lamars",$arr,$where_arr);
        if($res)
        {
            $OK = new OKandidat($id);
            $OU = new OUser($OK->row->user_id);
            $OL = new OLowongan($this->id);
            $OP = new OPerusahaan($OL->row->perusahaan_id);
            // send an email notif to kandidat
            $to = $OU->row->email;
            $subject = "LAMARAN NOTIF: Anda telah melamar lowongan ".$OL->get_nama()." on ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
            $content = $this->CI->load->view('email/auto_lamar/kandidat', array('OL' => $OL, 'OP' => $OP), TRUE);
            noreply_mail($to,$subject,$content);

            // send an email notif to perusahaan
            $to = $OP->row->email;
            $subject = "KANDIDAT LAMAR NOTIF: ".$OK->get_nama()." telah melamar lowongan ".$OL->get_nama()." ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
            $content = $this->CI->load->view('email/auto_lamar/perusahaan', array('OK' => $OK), TRUE);
            noreply_mail($to,$subject,$content);
            /*
            // send an email notif to admin
            $to = admin_email();
            $subject = "KANDIDAT LAMAR NOTIF: ".$OK->get_nama()." telah melamar lowongan ".$OL->get_nama()." (".$OP->get_nama().") ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
            $content = $this->CI->load->view('email/auto_lamar/perusahaan', array('OK' => $OK), TRUE);
            noreply_mail($to,$subject,$content);
            //*/
            unset($OK,$OU,$OL,$OP);
            return TRUE;
        }
        return FALSE;
    }

    public function delete_terima_rekomendasi($id)
    {
        if(empty($id)) return;
        $where_arr['lowongan_id'] = intval($this->id);
        $where_arr['kandidat_id'] = intval($id);
        $arr['diterima_flag'] = 0;
        $arr['dt_diterima'] = '0000-00-00 00:00:00';
        return $this->db->update("kandidat_lowongan_lamars",$arr,$where_arr);
    }

    public function set_terima_rekomendasi($id)
    {
        if(empty($id) || !$this->check_rekomendasi($id)) return FALSE;
        $this->delete_terima_rekomendasi($id);
        
        $where_arr['lowongan_id'] = intval($this->id);
        $where_arr['kandidat_id'] = intval($id);
        $arr['diterima_flag'] = 1;
        $arr['dt_diterima'] = date('Y-m-d H:i:s');
        $res = $this->db->update("kandidat_lowongan_lamars",$arr,$where_arr);
        if($res) return TRUE;
        return FALSE;
    }
	
	public function get_recom_kandidats()
	{
		// search berdasarkan level, jenis kelamin, kategori, waktu kerja, pendidikan, umur, pengalaman kerja
		$kandidat_ids = NULL;
		$q = "SELECT id FROM kandidats
				WHERE level_karir_id = ?
				AND jenis_kelamin_id = ?
				";
		$res = $this->db->query($q, array(intval($this->row->level_karir_id), intval($this->row->jenis_kelamin_id)));
		if(!emptyres($res))
		{
			foreach($res as $r)
			{
				$kandidat_ids[] = $r->id;
			}
			$q = "SELECT DISTINCT k.id FROM kandidats k
					INNER JOIN kandidat_keterampilans kket
						ON k.id = kket.kandidat_id
					WHERE k.kategori_id = ?
					AND k.id IN(".implode(",",$kandidat_ids).")
					";
			$res = $this->db->query($q, array($this->row->kategori_id));
			if(!emptyres($res))
			{
				$kandidat_ids = NULL;
				foreach($res->result() as $r)
				{
					$kandidat_ids[] = $r->id;
				}
				$q = "SELECT DISTINCT k.id, k.tanggal_lahir FROM kandidats k
						INNER JOIN kandidat_pendidikans kpend
							ON k.id = kpend.kandidat_id
						WHERE k.pendidikan_id = ?
						AND k.id IN(".implode(",",$kandidat_ids).")
						";
				$res = $this->db->query($q, array($this->row->pendidikan_id));
				if(!emptyres($res))
				{
					$kandidat_ids = NULL;
					foreach($res->result() as $r)
					{
						$tgl_lahir = $r->tanggal_lahir;
						$umur_arr = datediff($tgl_lahir);
						$umur = $umur_arr['years'];
						if($umur > $this->row->umur_mulai && $umur < $this->row->umur_sampai)
						{
							$kandidat_ids[] = $r->id;
						}
					}
					if(count($kandidat_ids) > 0)
					{
						$perusahaan_r = $this->get_perusahaan();
						$q = "SELECT SQL_CALC_FOUND_ROWS k.* FROM kandidats k
								INNER JOIN kandidat_pengalamans kpeng
									ON k.id = kpeng.kandidat_id
								WHERE kpeng.bidang_usaha_id = ?
								AND k.id IN(".implode(",",$kandidat_ids).")
								";
						$res = $this->db->query($q, array($perusahaan_r->bidang_usaha_id));
						if(!emptyres($res))
						{
							if(intval($this->row->tahun_pengalaman_kerja) > 0)
							{
								$kandidat_ids = NULL;
								foreach($res->result() as $r)
								{
									$tgl_awal = $r->tanggal_awal;
									$tgl_akhir = $r->tanggal_akhir;
									$umur_arr = datediff($tgl_awal, $tgl_akhir);
									$umur = $umur_arr['years'];
									if($umur >= $this->row->tahun_pengalaman_kerja)
									{
										$kandidat_ids[] = $r->id;
									}
								}
							}
						}
					}
				}
			}
		}
		return $kandidat_ids;
	}

    //level_karirs
    public function get_level_karirs()
    {
        $res = $this->db->get_where('lowongan_level_karirs', array('lowongan_id' => $this->id));
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    public function get_level_karir_arr()
    {
        $this->db->select('master_level_karirs.*');
        $this->db->from('lowongan_level_karirs');
        $this->db->where(array('lowongan_level_karirs.lowongan_id' => $this->id));
        $this->db->join('master_level_karirs', 'master_level_karirs.id = lowongan_level_karirs.level_karir_id', 'inner');
        $res = $this->db->get();
        if(emptyres($res)) return FALSE;

        $list = $res->result();
        
        $arr = NULL;
        foreach($list as $r)
        {
            $arr[$r->id] = $r->nama;
        }
        return $arr;
    }
    public function update_level_karirs($arr)
    {
        $this->db->delete('lowongan_level_karirs', array('lowongan_id' => $this->id));
        $insert_arr = NULL;
        foreach($arr as $key => $val)
        {
            $insert_arr[] = array('lowongan_id' => $this->id, 'level_karir_id' => $val);
        }
        return $this->db->insert_batch('lowongan_level_karirs', $insert_arr);
    }
	
}
