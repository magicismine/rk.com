<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OMaster_jenis_kelamin{
    var $CI;
    var $db;
    var $table_name = "master_jenis_kelamins";
    var $row;
    var $id;
    
    public function __construct($id, $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        $arr = array("id" => $id);
        	if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }		
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public static function add($arr)
    {
        $CI =& get_instance();
        $CI->db->insert("master_jenis_kelamins",$arr);
        return $CI->db->insert_id();
    }
    
    public static function get_list($start,$limit,$orderby = "id DESC",$where = "")
    {
    	$CI =& get_instance();
        if($orderby == "") $orderby = "id DESC";
        if(intval($limit) == 0)
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `master_jenis_kelamins` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby}",array(intval($start),intval($limit))); 
        }
        else
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `master_jenis_kelamins` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby} LIMIT ?, ?",array(intval($start),intval($limit))); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public static function search($keyword,$orderby = "")
    {
    	$CI =& get_instance();
        $q = "SELECT SQL_CALC_FOUND_ROWS * 
        		FROM `master_jenis_kelamins`
                WHERE id = ?";
        $arr = array();
        $arr[] = intval($keyword);
                if($orderby != "")
        {
        	$q .= " ORDER BY ".@mysql_escape_string($orderby);
        	$res = $CI->db->query($q,$arr); 
        }
        else
        {
        	$res = $CI->db->query($q,$arr); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
                
    }
    
    public static function drop_down_select($name,$selval,$optional = "",$default="",$readonly)
	{
		$list = OMaster_jenis_kelamin::get_list(0, 0, "id ASC");
		
		foreach($list as $r)
		{
			$arr[$r->id] = $r->nama;			
		}
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
    public static function radio_select($name,$selval,$optional="",$separator)
	{
		$list = OMaster_jenis_kelamin::get_list(0, 0, "id ASC");
		
		foreach($list as $r)
		{
			$arr[$r->id] = $r->nama;			
		}
		return radios($name,$arr,$selval,$optional,$separator);
	} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
        return $this->db->delete($this->table_name,array("id" => $this->id));		
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $this->db->update($this->table_name,array('photo' => $image_file),array('id' => $this->id));
    }
    
    public function get_photo($type = "medium")
    {
    	$filename = $this->row->photo;
        return base_url()."/_assets/images/uploads/{$type}/".$filename;
    }
    
    public function update_url_title()
    {
    	
    	$new_url_title = url_title($this->row->nama,"-",TRUE);
    	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
        $res = $this->db->query($q,array($new_url_title,$this->id));
        if(!emptyres($res))
        {
        	$new_url_title .= "-".$this->id;
        }
        $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
    }
    
    public function get_link()
    {
    	return $this->row->url_title;
    }
        
	public function get_nama()
    {
        return $this->row->nama;
    }
}
