<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OMaster_kategori{
    var $CI;
    var $db;
    var $table_name = "master_kategoris";
    var $row;
    var $id;
    
    public function __construct($id, $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        $arr = array("id" => $id);
        	if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }		
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public static function add($arr)
    {
        $CI =& get_instance();
        $CI->db->insert("master_kategoris",$arr);
        return $CI->db->insert_id();
    }
    
    public static function get_list($start,$limit,$orderby = "id DESC",$where = "")
    {
    	$CI =& get_instance();
        if($orderby == "") $orderby = "id DESC";
        if(intval($limit) == 0)
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `master_kategoris` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby}",array(intval($start),intval($limit))); 
        }
        else
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `master_kategoris` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby} LIMIT ?, ?",array(intval($start),intval($limit))); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public static function search($keyword,$orderby = "")
    {
    	$CI =& get_instance();
        $q = "SELECT SQL_CALC_FOUND_ROWS * 
        		FROM `master_kategoris`
                WHERE id = ?";
        $arr = array();
        $arr[] = intval($keyword);
                if($orderby != "")
        {
        	$q .= " ORDER BY ".@mysql_escape_string($orderby);
        	$res = $CI->db->query($q,$arr); 
        }
        else
        {
        	$res = $CI->db->query($q,$arr); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
                
    }
    
	public static function get_tree_list_array($parent_id = 0, $prefix = "")
	{
		$CI =& get_instance();
		$arr = array();
		// see if there are children
		$q = "SELECT * FROM master_kategoris WHERE parent_id = ? ORDER BY nama ASC";
		$res = $CI->db->query($q,array($parent_id));
		if(emptyres($res))
		{
			// if there is no children, then return the current node
			$q = "SELECT * FROM master_kategoris WHERE id = ? ORDER BY nama ASC";
			$res = $CI->db->query($q,array($parent_id));
			if(emptyres($res)) return FALSE;
			$r = $res->row();
			if($prefix == "") return array($r->id => $r->nama);
			else return array($r->id => $prefix." > ".$r->nama);
		}
		else
		{
			// if there are children
			// get the current node
			$q = "SELECT * FROM master_kategoris WHERE id = ? ORDER BY nama ASC";
			$tmpres = $CI->db->query($q,array($parent_id));
			$r = $tmpres->row();
			if($r->nama != "") 
			{
				if($prefix == "") $arr[$r->id] = $r->nama;
				else $arr[$r->id] = $prefix." > ".$r->nama;
				$nextprefix = $arr[$r->id];
			}
			
			foreach($res->result() as $row)
			{						
				$arr = array_merge_special($arr,OMaster_kategori::get_tree_list_array($row->id,$nextprefix));
			}
			return $arr;
		}
	}
	
	public static function get_parents($children_id = 0, $prefix = "")
	{
		if(empty($children_id)) return FALSE;
		
		$CI =& get_instance();
		$arr = array();
		// see if there are parent
		$q = "SELECT * FROM master_kategoris WHERE id = ? ORDER BY nama ASC";
		$res = $CI->db->query($q,array($children_id));
		if(!emptyres($res))
		{
			// if there are parent
			// get the current node
			$row = $res->row();
			
			$q = "SELECT * FROM master_kategoris WHERE id = ? ORDER BY nama ASC";
			$tmpres = $CI->db->query($q,array($row->parent_id));
			$r = $tmpres->row();
			if(emptyres($tmpres)) return FALSE;
			if($r->nama != "") 
			{
				$arr = array($r->id => $r->nama);
				//if($prefix == "") $arr = array($r->id => $r->nama);
				//else $arr = array($r->id => $prefix." > ".$r->nama);
				$nextprefix = $arr[$r->id];
				$arr = array_merge_special($arr,OMaster_kategori::get_parents($r->id,$nextprefix));
			}
			return $arr;
		} 
		else return FALSE;
	}

	public static function get_childrens($id = 0, $prefix = "")
	{
		if(empty($id)) return FALSE;
		
		$CI =& get_instance();
		$arr = array();
		// see if there are parent
		//*
		$q = "SELECT * FROM master_kategoris WHERE parent_id = ? ORDER BY nama ASC";
		$res = $CI->db->query($q,array($id));
		if(emptyres($res))
		{
			// if there is no children, then return the current node
			$q = "SELECT * FROM master_kategoris WHERE id = ? ORDER BY nama ASC";
			$res = $CI->db->query($q,array($id));
			if(emptyres($res)) return FALSE;
			$r = $res->row();
			return array($r->id => $r->nama);
			//if($prefix == "") return array($r->id => $r->nama);
			//else return array($r->id => $prefix." > ".$r->nama);
		}
		else
		{
			// if there are parent
			// get the current node
			//*/
			$q = "SELECT * FROM master_kategoris WHERE id = ? ORDER BY nama ASC";
			$tmpres = $CI->db->query($q,array($id));
			$r = $tmpres->row();
			if(emptyres($tmpres)) return FALSE;
			if($r->nama != "") 
			{
				$arr = array($r->id => $r->nama);
				//if($prefix == "") $arr = array($r->id => $r->nama);
				//else $arr = array($r->id => $prefix." > ".$r->nama);
				$nextprefix = $arr[$r->id];
			}
			
			foreach($res->result() as $row)
			{						
				$arr = array_merge_special($arr,OMaster_kategori::get_childrens($row->id,$nextprefix));
			}
			
			return $arr;
		//*
		}
		//*/
	}

    public static function drop_down_select($name,$selval,$optional = "",$default="",$where)
	{
		$list = OMaster_kategori::get_list(0, 0, "id ASC", $where);
		
		$O = new OMaster_kategori();
		$arr = NULL;
		foreach($list as $r)
		{
			$O->setup($r);
			$arr[$r->id] = $O->get_nama();
		}
		unset($O);
		return dropdown($name,$arr,$selval,$optional,$default);
	} 
    
    public static function get_list_arr($start=0,$limit=0,$orderby="nama ASC",$where="")
	{
		$list = OMaster_kategori::get_list($start,$limit,$orderby,$where);
		$arr = NULL;
		if(!$list) return $arr;

		$O = new OMaster_kategori();
		foreach($list as $r)
		{
			$O->setup($r);
			$arr[$r->id] = $O->get_nama();
		}
		unset($O);
		return $arr;
	}

    public static function drop_down_tree_select($name,$selval,$optional = "",$default="",$readonly)
	{
		$list = OMaster_kategori::get_tree_list_array();
		
		foreach($list as $key => $val)
		{
			$arr[$key] = $val;
		}
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
        return $this->db->delete($this->table_name,array("id" => $this->id));		
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $this->db->update($this->table_name,array('photo' => $image_file),array('id' => $this->id));
    }
    
    public function get_photo($type = "medium")
    {
    	$filename = $this->row->photo;
        return base_url()."/_assets/images/uploads/{$type}/".$filename;
    }
    
    public function update_url_title()
    {
    	$new_url_title = url_title($this->row->nama,"-",TRUE);
    	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
        $res = $this->db->query($q,array($new_url_title,$this->id));
        if(!emptyres($res))
        {
        	$new_url_title .= "-".$this->id;
        }
        $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
    }
    
    public function get_link()
    {
    	return site_url("lowongan/listing/kategori/".$this->row->url_title);
    }
        
	public function get_nama()
	{
		return $this->row->nama;
	}	

}
