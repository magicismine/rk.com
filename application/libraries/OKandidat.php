<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OKandidat{
  var $CI;
  var $db;
  var $table_name = "kandidats";
  var $row;
  var $id;
  
  public function __construct($id, $type="id")
  {
      $CI =& get_instance();
      $this->CI = $CI;
      $this->db = $CI->db;
      if(empty($id))
      {
          $this->id = false;
          $this->row = false;
      }
      else
      {
					if($type == "url_title")
					{
					    $arr = array("url_title" => $id);
					}
					elseif($type == "user_id")
					{
					    $arr = array("user_id" => $id);
					}
					else
					{
					    $arr = array("id" => $id);    
					}
          $res = $this->db->get_where($this->table_name,$arr);
          if(emptyres($res)) 
          {
              $this->id = false;
              $this->row = false;
          }
          else
          {
              $this->row = $res->row();
              $this->id = $this->row->id;
          }
      }		
  }
  
  public function setup($row)
  {
      if($row->id != "")
      {
          $this->row = $row;
          $this->id = $row->id;
      }
      else return false;
  }
  
  public static function add($arr)
  {
      $CI =& get_instance();
      $CI->db->insert("kandidats",$arr);
      return $CI->db->insert_id();
  }
  
  public static function get_list($start,$limit,$orderby = "id DESC",$where = "")
  {
  	$CI =& get_instance();
      if($orderby == "") $orderby = "id DESC";
      if(intval($limit) == 0)
      {
      	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `kandidats` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby}",array(intval($start),intval($limit))); 
      }
      else
      {
      	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `kandidats` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby} LIMIT ?, ?",array(intval($start),intval($limit))); 
      }
      if(emptyres($res)) return FALSE;
      else return $res->result();
  }

  public static function get_list_filter($keyword,$start,$limit,$orderby,$where,$filter_arr)
  {
  	//$kategori,$pendidikan,$level_karir,$waktu_kerja,$propinsi,$kabupaten,$umur
  	$CI =& get_instance();
      $arr = $and_q = NULL;
      $add_join = "";
      if(!empty($keyword))
      {
      	$keyword = $CI->db->escape_like_str($keyword);
      	$and_q[] = "(
      				kandidats.nama_depan LIKE ?
      				OR kandidats.nama_akhir LIKE ?
      				OR master_jenis_kelamins.nama LIKE ?
      				OR kandidat_kesehatans.tinggi LIKE ?
      				OR kandidat_keterampilans.kategori_id IN(SELECT id FROM master_kategoris WHERE nama LIKE ?)
      				OR kandidat_pendidikans.pendidikan_id IN(SELECT id FROM master_pendidikans WHERE nama LIKE ?)
      				OR kandidats.asal_propinsi_id IN(SELECT id FROM master_lokasi_propinsis WHERE nama LIKE ?)
      				OR kandidats.skrg_propinsi_id IN(SELECT id FROM master_lokasi_propinsis WHERE nama LIKE ?)
      				OR kandidats.asal_kabupaten_id IN(SELECT id FROM master_lokasi_kabupatens WHERE nama LIKE ?)
      				OR kandidats.skrg_kabupaten_id IN(SELECT id FROM master_lokasi_kabupatens WHERE nama LIKE ?)
      				)";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      	$arr[] = "%".$keyword."%";
      }
      if(count($filter_arr) > 0)
      {
      	$filter_arr = array_unique($filter_arr);
      	foreach($filter_arr as $key => $val)
      	{
      		if(empty($key) || empty($val)) continue;

      		if($key == "kategori_id")
	        {
	        	$and_q[] = "kandidat_keterampilans.kategori_id = ?";
	        	$arr[] = intval($val);
	        	$add_join .= " LEFT JOIN kandidat_keterampilans ON kandidats.id = kandidat_keterampilans.kandidat_id";
	        }
	        if($key == "jenis_kelamin_id")
	        {
	        	$and_q[] = "kandidats.jenis_kelamin_id = ?";
	        	$arr[] = intval($val);
	        	$add_join .= " LEFT JOIN master_jenis_kelamins ON kandidats.jenis_kelamin_id = master_jenis_kelamins.id";
	        }
	        if($key == "tinggi")
	        {
	        	$and_q[] = "kandidat_kesehatans.tinggi = ?";
	        	$arr[] = intval($val);
	        	$add_join .= " LEFT JOIN kandidat_kesehatans ON kandidats.id = kandidat_kesehatans.kandidat_id";
	        }
	        if($key == "pendidikan_id")
	        {
	        	$and_q[] = "kandidat_pendidikans.pendidikan_id = ?";
	        	$arr[] = intval($val);
	        	$add_join .= " LEFT JOIN kandidat_pendidikans ON kandidats.id = kandidat_pendidikans.kandidat_id";
	        }
	        if($key == "level_karir_id")
	        {
	        	$and_q[] = "kandidats.level_karir_id = ?";
	        	$arr[] = intval($val);
	        }
	        if($key == "waktu_kerja_id")
	        {
	        	//$and_q[] = "kandidats.waktu_kerja_id = ?";
	        	//$arr[] = intval($val);
	        }
	        if($key == "lokasi_propinsi_id")
	        {
	        	$and_q[] = "(kandidats.asal_propinsi_id = ? OR kandidats.skrg_propinsi_id = ?)";
	        	$arr[] = intval($val);
	        	$arr[] = intval($val);
	        }
	        if($key == "lokasi_kabupaten_id")
	        {
	        	$and_q[] = "(kandidats.asal_kabupaten_id = ? OR kandidats.skrg_kabupaten_id = ?)";
	        	$arr[] = intval($val);
	        	$arr[] = intval($val);
	        }
	        if($key == "umur")
	        {
	        	//reff >> DATEDIFF(CURRENT_DATE, STR_TO_DATE(t.birthday, '%d-%m-%Y'))/365 AS ageInYears
	        	//reff >> TIMESTAMPDIFF(YEAR,birth,CURDATE()) AS age
	        	$and_q[] = "(TIMESTAMPDIFF(YEAR,kandidats.tanggal_lahir,CURDATE()) = ?)";
	        	$arr[] = intval($val);
	        }
	        if($key == "umur_mulai")
	        {
	        	//reff >> DATEDIFF(CURRENT_DATE, STR_TO_DATE(t.birthday, '%d-%m-%Y'))/365 AS ageInYears
	        	//reff >> TIMESTAMPDIFF(YEAR,birth,CURDATE()) AS age
	        	$and_q[] = "(TIMESTAMPDIFF(YEAR,kandidats.tanggal_lahir,CURDATE()) >= ?)";
	        	$arr[] = intval($val);
	        }
	        if($key == "umur_akhir")
	        {
	        	//reff >> DATEDIFF(CURRENT_DATE, STR_TO_DATE(t.birthday, '%d-%m-%Y'))/365 AS ageInYears
	        	//reff >> TIMESTAMPDIFF(YEAR,birth,CURDATE()) AS age
	        	$and_q[] = "(TIMESTAMPDIFF(YEAR,kandidats.tanggal_lahir,CURDATE()) <= ?)";
	        	$arr[] = intval($val);
	        }
      	}
  	}
		if(!empty($keyword))
		{
    	$add_join = " LEFT JOIN master_jenis_kelamins ON kandidats.jenis_kelamin_id = master_jenis_kelamins.id";
    	$add_join .= " LEFT JOIN kandidat_kesehatans ON kandidats.id = kandidat_kesehatans.kandidat_id";
    	$add_join .= " LEFT JOIN kandidat_keterampilans ON kandidats.id = kandidat_keterampilans.kandidat_id";
    	$add_join .= " LEFT JOIN kandidat_pendidikans ON kandidats.id = kandidat_pendidikans.kandidat_id";
    }
  	if(count($and_q) > 0) $add_where = " AND ".implode(" AND ", $and_q);
      
    $q = "SELECT SQL_CALC_FOUND_ROWS kandidats.*
    		FROM kandidats
    		INNER JOIN users ON users.id = kandidats.user_id
    		{$add_join}
    		WHERE users.status = 'aktif'
    		";
    if($where != "") $q .= " AND ".$where;
    $q .= " {$add_where}";
    $q .= " GROUP BY kandidats.id";
    $q .= " {$add_sql}";
    if($orderby != "") $q .= " ORDER BY ".$orderby;
    if(intval($limit) > 0)
    {
    	$q .= " LIMIT ?, ?";
    	$arr[] = intval($start);
    	$arr[] = intval($limit);
    }
    $res = $CI->db->query($q, $arr);
    if(emptyres($res)) return FALSE;
    else return $res->result();
  }
  
  public static function search($keyword,$orderby = "")
  {
  	$CI =& get_instance();
      $q = "SELECT SQL_CALC_FOUND_ROWS * 
      		FROM `kandidats`
              WHERE id = ? OR nama_depan LIKE ? OR nama_akhir LIKE ?";
      $arr = array();
      $arr[] = intval($keyword);
      $arr[] = "%{$keyword}%";
      $arr[] = "%{$keyword}%";
	if($orderby != "")
      {
      	$q .= " ORDER BY ".@mysql_escape_string($orderby);
      	$res = $CI->db->query($q,$arr); 
      }
      else
      {
      	$res = $CI->db->query($q,$arr); 
      }
      if(emptyres($res)) return FALSE;
      else return $res->result();
              
  }
  
  public static function drop_down_select($name,$selval,$optional = "",$default="",$readonly)
	{
		$list = OKandidat::get_list(0, 0, "id ASC");
		
		$O = new OKandidat();
		$arr = NULL;
		foreach($list as $r)
		{
			$O->setup($r);
			$arr[$r->id] = $O->get_nama_lengkap();			
		}
		unset($O);
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
  public function edit($arr)
  {
  	return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
  }
  
  public function delete()
  {
  	// delete all related
  	// self
  	$this->delete_bahasas();
  	$this->delete_dokumens();
  	$this->delete_keluargas();
  	$this->delete_kesehatans();
  	$this->delete_keterampilans();
  	$this->delete_pendidikans();
  	$this->delete_pengalamans();
  	$this->delete_resume();
  	// other
  	$this->delete_email_alert();
		$this->delete_auto_lamar();
		$this->delete_lowongan_simpans();
		$this->delete_lowongan_lamars();
		$this->delete_perusahaan_lihats();
  	return $this->db->delete($this->table_name,array("id" => $this->id));
  }
  
  public function refresh()
  {
      $res = $this->db->get_where($this->table_name,array("id" => $this->id));
      if(emptyres($res)) 
      {
          $this->id = false;
          $this->row = false;
      }
      else
      {
          $this->row = $res->row();
          $this->id = $this->row->id;
      }
  }
  
  public function update_photo($image_file)
  {
  	$files = auto_resize_photo($image_file);
      return $this->db->update($this->table_name,array('foto' => $image_file),array('id' => $this->id));
  }
  
  public function get_photo($type = "medium")
  {
  	$filename = $this->row->foto;
  	if($filename != "" && is_file(FCPATH."/_assets/images/uploads/{$type}/".$filename))
  	{
  		return base_url()."/_assets/images/uploads/{$type}/".$filename."?v=".date("ymdHi");	
  	}
  	else
  	{
  		return base_url()."/_assets/images/no-image.png";
  	}
      
  }
  
  public function update_url_title()
  {
  	
  	$new_url_title = url_title($this->row->nama_depan,"-",TRUE);
  	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
      $res = $this->db->query($q,array($new_url_title,$this->id));
      if(!emptyres($res))
      {
      	$new_url_title .= "-".$this->id;
      }
      $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
  }
  
  public function get_link()
  {
  	return site_url("user_kandidat/details/".$this->id);
  }
	
	public function get_nama()
	{
		return ucwords($this->row->nama_depan);
	}
	
	public function get_nama_lengkap()
	{
		return ucwords($this->row->nama_depan." ".$this->row->nama_akhir);
	}
	
	public function get_umur()
	{
		if(intval($this->row->tanggal_lahir) == 0) return 0;
		$umur_arr = datediff($this->row->tanggal_lahir);
		return $umur_arr['years'];
	}
        
	public function get_current_lokasi()
	{
		if($this->row->skrg_asal_flag == 1)
        {
        	$kabupaten_id = $this->row->asal_kabupaten_id;
        }
        else
        {
        	$kabupaten_id = $this->row->skrg_kabupaten_id;
        }
		$OTmp = new OMaster_lokasi_kabupaten($kabupaten_id);
		$row = $OTmp->row;
		unset($OTmp);
		return $row;
	}

	public function get_current_complete_lokasi_name()
	{
		if($this->row->skrg_asal_flag == 1)
        {
        	$propinsi_id = $this->row->asal_propinsi_id;
        	$kabupaten_id = $this->row->asal_kabupaten_id;
        	$kawasan_id = $this->row->asal_kawasan_id;
        }
        else
        {
        	$propinsi_id = $this->row->skrg_propinsi_id;
        	$kabupaten_id = $this->row->skrg_kabupaten_id;
        	$kawasan_id = $this->row->skrg_kawasan_id;
        }
        $arr = NULL;
		$OTmp = new OMaster_lokasi_propinsi($propinsi_id);
		$arr[$propinsi_id] = $OTmp->get_nama();
		$OTmp = new OMaster_lokasi_kabupaten($kabupaten_id);
		$arr[$kabupaten_id] = $OTmp->get_nama();
		$OTmp = new OMaster_lokasi_kawasan($kawasan_id);
		$arr[$kawasan_id] = $OTmp->get_nama();
		unset($OTmp);
		return $arr;
	}

	// ->get_resume
	public function get_resume()
	{
		$list = OKandidat_resume::get_list(0,1,"","kandidat_id=".intval($this->id));
		if(!$list) return FALSE;
		return $list[0];
	}
	public function set_resume($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_resume::add($arr);
		return $new_id;
	}
	public function edit_resume($arr)
	{
		$row = $this->get_resume();
		if(!$row) return $this->set_resume($arr);
		$O = new OKandidat_resume();
		$O->setup($row);
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_resume()
	{
		$arr['kandidat_id'] = intval($this->id);
		return $this->db->delete("kandidat_resumes",$arr);
	}
	
	// ->get_kesehatan
	public function get_kesehatan()
	{
		$list = OKandidat_kesehatan::get_list(0,1,"","kandidat_id=".intval($this->id));
		if(!$list) return FALSE;
		return $list[0];
	}
	public function set_kesehatan($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_kesehatan::add($arr);
		return $new_id;
	}
	public function edit_kesehatan($arr)
	{
		$row = $this->get_kesehatan();
		if(!$row) return $this->set_kesehatan($arr);
		$O = new OKandidat_kesehatan();
		$O->setup($row);
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_kesehatans()
	{
		$list = $this->get_kesehatan();
		if(!$list) return;
		$O = new OKandidat_kesehatan;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// ->get_keluarga
	public function get_keluarga($start=0,$limit=0,$order_by='id ASC',$where)
	{
		$list = OKandidat_keluarga::get_list($start,$limit,$order_by,"kandidat_id=".intval($this->id).($where == "" ? "" : " AND {$where}"));
		return $list;
	}
	public function set_keluarga($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_keluarga::add($arr);
		return $new_id;
	}
	public function edit_keluarga($id, $arr)
	{
		$O = new OKandidat_keluarga($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_keluarga($id)
	{
		$O = new OKandidat_keluarga($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		return $O->delete();
	}
	public function delete_keluargas()
	{
		$list = $this->get_keluarga();
		if(!$list) return;
		$O = new OKandidat_keluarga;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// ->get_pendidikan
	public function get_pendidikan($start=0,$limit=0,$order_by='tahun_akhir ASC',$where)
	{
		$list = OKandidat_pendidikan::get_list($start,$limit,$order_by,"kandidat_id=".intval($this->id).($where == "" ? "" : " AND {$where}"));
		return $list;
	}
	public function get_pendidikan_arr($start=0,$limit=0,$order_by='tahun_akhir ASC',$where)
	{
		$list = $this->get_pendidikan($start,$limit,$order_by,$where);
		$OP = new OMaster_pendidikan();
		$arr = NULL;
		foreach($list as $r)
		{
			$OP->setup($r);
			$arr[$r->id] = $OP->get_nama();
		}
		unset($OP);
		return $arr;
	}
	public function get_last_pendidikan()
	{
		$list = $this->get_pendidikan(0,1,"tahun_akhir DESC");
		$r = $list[0];
		return $r;
	}
	public function set_pendidikan($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_pendidikan::add($arr);
		return $new_id;
	}
	public function edit_pendidikan($id, $arr)
	{
		$O = new OKandidat_pendidikan($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_pendidikan($id)
	{
		$O = new OKandidat_pendidikan($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		return $O->delete();
	}
	public function delete_pendidikans()
	{
		$list = $this->get_pendidikan();
		if(!$list) return;
		$O = new OKandidat_pendidikan;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// ->get_pengalaman
	public function get_pengalaman($start=0,$limit=0,$order_by='id ASC',$where)
	{
		$list = OKandidat_pengalaman::get_list($start,$limit,$order_by,"kandidat_id=".intval($this->id).($where == "" ? "" : " AND {$where}"));
		return $list;
	}
	public function set_pengalaman($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_pengalaman::add($arr);
		return $new_id;
	}
	public function edit_pengalaman($id, $arr)
	{
		$O = new OKandidat_pengalaman($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_pengalaman($id)
	{
		$O = new OKandidat_pengalaman($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		return $O->delete();
	}
	public function delete_pengalamans()
	{
		$list = $this->get_pengalaman();
		if(!$list) return;
		$O = new OKandidat_pengalaman;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// ->get_keterampilan
	public function get_keterampilan($start=0,$limit=0,$order_by='id ASC',$where)
	{
		$list = OKandidat_keterampilan::get_list($start,$limit,$order_by,"kandidat_id=".intval($this->id).($where == "" ? "" : " AND {$where}"));
		return $list;
	}
	public function get_all_keterampilan($limit=0)
	{
		$keterampilans = $this->get_keterampilan(0,0);
		$keterampilan_arr = NULL;
		$i=1;
		foreach($keterampilans as $r):
			$OTmp = new OMaster_kategori($r->kategori_id);
			$arr = array($OTmp->get_nama());
			$parents = OMaster_kategori::get_parents($OTmp->id);
			foreach($parents as $key => $val)
			{
				$OTmp_child = new OMaster_kategori($r->key);
				$arr[] = $OTmp_child->get_nama();
				unset($OTmp_child);
			}
			$keterampilan_arr[] = implode(" &gt; ", $arr);
  		unset($OTmp);
  		if( intval($limit) > 0 && $i >= intval($limit) ) break;
  		$i++;
    endforeach;
    return $keterampilan_arr;
	}
	public function get_current_keterampilan()
	{
		$list = $this->get_keterampilan(0,1,"id DESC");
		$r = $list[0];
		return $r;
	}
	public function set_keterampilan($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_keterampilan::add($arr);
		return $new_id;
	}
	public function edit_keterampilan($id, $arr)
	{
		$O = new OKandidat_keterampilan($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_keterampilan($id)
	{
		$O = new OKandidat_keterampilan($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		return $O->delete();
	}
	public function delete_keterampilans()
	{
		$list = $this->get_keterampilan();
		if(!$list) return;
		$O = new OKandidat_keterampilan;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// ->get_bahasa
	public function get_bahasa($start=0,$limit=0,$order_by='id ASC',$where)
	{
		$list = OKandidat_bahasa::get_list($start,$limit,$order_by,"kandidat_id=".intval($this->id).($where == "" ? "" : " AND {$where}"));
		return $list;
	}
	public function set_bahasa($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_bahasa::add($arr);
		return $new_id;
	}
	public function edit_bahasa($id, $arr)
	{
		$O = new OKandidat_bahasa($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_bahasa($id)
	{
		$O = new OKandidat_bahasa($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		return $O->delete();
	}
	public function delete_bahasas()
	{
		$list = $this->get_bahasa();
		if(!$list) return;
		$O = new OKandidat_bahasa;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// ->get_dokumen
	public function get_dokumen($start=0,$limit=0,$order_by='id ASC',$where)
	{
		$list = OKandidat_dokumen::get_list($start,$limit,$order_by,"kandidat_id=".intval($this->id).($where == "" ? "" : " AND {$where}"));
		return $list;
	}
	public function set_dokumens($files,$aktif_flags)
	{
		if(empty($files)) return FALSE;
		$allow_exts = array("docx","doc","pdf","rtf");
		foreach($files['error'] as $key => $val)
		{
			if($files["name"][$key] == "" ||
				$value != UPLOAD_ERR_OK ||
				$files["size"][$key] > 1048576 // 1 M
			) continue;
			
			$filename = $files["name"][$key];
			$ext = end(explode(".",$filename));
			if(!in_array($ext,$allow_exts)) continue;
			
			$filename_arr = explode(".",$filename,2);
			$random = url_title($filename_arr[0], '-', TRUE)."_".time()."_".random_string("alnum", 6);
			$file = $random.".".$ext;
			$upload_dir = FCPATH.'/_assets/files/';
			if(!is_dir($upload_dir)) mkdir($upload_dir, 0777, true);
			
			if (move_uploaded_file($files['tmp_name'][$key], $upload_dir.$file))
			{
				$arr = array();
				$arr['dokumen'] = $file;
				if(count($aktif_flags) > 0) $arr['aktif_flag'] = $aktif_flags[$key];
				$arr['kandidat_id'] = intval($this->id);
				OKandidat_dokumen::add($arr);
			}
		}
		
		return TRUE;
	}
	public function edit_dokumens($files,$aktif_flags)
	{
		if(empty($files)) return FALSE;
		$allow_exts = array("docx","doc","pdf","rtf");
		foreach($files['error'] as $key => $val)
		{
			$O = new OKandidat_dokumen($key);
			// valid?
			if(empty($O->id) || $O->row->kandidat_id != $this->id) continue;
			
			if($files["name"][$key] == "" ||
				$value != UPLOAD_ERR_OK ||
				$files["size"][$key] > 1048576 // 1 M
			) continue;
			
			$filename = $files["name"][$key];
			$ext = end(explode(".",$filename));
			if(!in_array($ext,$allow_exts)) continue;
			
			$filename_arr = explode(".",$filename,2);
			$random = url_title($filename_arr[0], '-', TRUE)."_".time()."_".random_string("alnum", 6);
			$file = $random.".".$ext;
			$upload_dir = FCPATH.'/_assets/files/';
			if(!is_dir($upload_dir)) mkdir($upload_dir, 0777, true);
			
			if (move_uploaded_file($files['tmp_name'][$key], $upload_dir.$file))
			{
				$arr = array();
				$arr['dokumen'] = $file;
				if(count($aktif_flags) > 0) $arr['aktif_flag'] = $aktif_flags[$key];
				$arr['kandidat_id'] = intval($this->id);
				$O->edit($arr);
			}
		}
		return TRUE;
	}
	public function set_dokumen($arr)
	{
		$arr['kandidat_id'] = intval($this->id);
		$new_id = OKandidat_dokumen::add($arr);
		return $new_id;
	}
	public function edit_dokumen($id, $arr)
	{
		$O = new OKandidat_dokumen($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		return $O->edit($arr);
	}
	public function delete_dokumen($id)
	{
		$O = new OKandidat_dokumen($id);
		// valid?
		if(empty($O->id) || $O->row->kandidat_id != $this->id) return FALSE;
		// delete the file
		$upload_dir = FCPATH.'/_assets/files/';
		$file = $O->row->dokumen;
		if( is_file($upload_dir.$file) ) unlink($upload_dir.$file);
		// delete the row of the table
		return $O->delete();
	}
	public function delete_dokumens()
	{
		$list = $this->get_dokumen();
		if(!$list) return;
		$O = new OKandidat_dokumen;
		foreach ($list as $r)
		{
			$O->setup($r);
			$O->delete();
		}
		unset($O);
		return true;
	}
	
	// perusahaan lihat
	public function delete_perusahaan_lihat($id)
	{
		if(empty($id)) return;
		$arr['kandidat_id'] = intval($this->id);
		$arr['perusahaan_id'] = intval($id);
		return $this->db->delete("kandidat_perusahaan_lihats",$arr);
	}
	public function delete_perusahaan_lihats()
	{
		$this->db->delete("kandidat_perusahaan_lihats", array("kandidat_id" => $this->id));
		return true;
	}
	public function set_perusahaan_lihat($id)
	{
		if(empty($id)) return;
		$this->delete_perusahaan_lihat($id);
		$arr['kandidat_id'] = intval($this->id);
		$arr['perusahaan_id'] = intval($id);
		return $this->db->insert("kandidat_perusahaan_lihats",$arr);
	}
	public function get_perusahaan_lihats($start, $limit, $orderby, $where)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS perusahaans.*, kandidat_perusahaan_lihats.* FROM kandidat_perusahaan_lihats
				INNER JOIN perusahaans
				ON kandidat_perusahaan_lihats.perusahaan_id = perusahaans.id
				WHERE kandidat_perusahaan_lihats.kandidat_id = ?";
		$arr = array(intval($this->id));
		if(!empty($where))
		{
			$q .= " AND {$where}";
		}
		if(!empty($orderby))
		{
			$q .= " ORDER BY {$orderby}";
		}
		if(intval($limit) > 0)
		{
			$q .= " LIMIT ?, ?";
			$arr[] = intval($start);
			$arr[] = intval($limit);
		}
		$res = $this->db->query($q, $arr);
		
		if(emptyres($res)) return FALSE;
		return $res->result();
	}

	
	// lowongan yang di lamar
	public function check_lowongan_lamar($id)
	{
		$this->db->select('kandidat_lowongan_lamars.*');
		$this->db->from('kandidat_lowongan_lamars');
		$arr = array(
								'kandidat_lowongan_lamars.kandidat_id' => intval($this->id),
								'kandidat_lowongan_lamars.lowongan_id' => intval($id),
								'lowongans.active' => 1
								);
		$this->db->where($arr);
		$this->db->join('lowongans', 'lowongans.id = kandidat_lowongan_lamars.lowongan_id', 'inner');
		$res = $this->db->get();
		if(emptyres($res)) return FALSE;
		else return TRUE;
	}
	public function is_lowongan_lamar($id)
	{
		$this->db->select('kandidat_lowongan_lamars.*');
		$this->db->from('kandidat_lowongan_lamars');
		$arr = array(
								'kandidat_lowongan_lamars.kandidat_id' => intval($this->id),
								'kandidat_lowongan_lamars.lowongan_id' => intval($id)
								);
		$this->db->where($arr);
		$this->db->join('lowongans', 'lowongans.id = kandidat_lowongan_lamars.lowongan_id', 'inner');
		$res = $this->db->get();
		if(emptyres($res)) return FALSE;
		else return TRUE;
	}
	public function delete_lowongan_lamar($id)
	{
		if(empty($id)) return;
		$arr['kandidat_id'] = intval($this->id);
		$arr['lowongan_id'] = intval($id);
		return $this->db->delete("kandidat_lowongan_lamars",$arr);
	}
	public function delete_lowongan_lamars()
	{
		$this->db->delete("kandidat_lowongan_lamars", array("kandidat_id" => $this->id));
		return true;
	}
	public function set_lowongan_lamar($id)
	{
		if(empty($id) || $this->check_lowongan_lamar($id)) return FALSE;
		$this->delete_lowongan_lamar($id);

		$OK = new OKandidat($this->id);
		$OU = new OUser($this->row->user_id);
		$OL = new OLowongan($id);
		$OP = new OPerusahaan($OL->row->perusahaan_id);
		/*
		// send an email notif to kandidat
		$to = $OU->row->email;
		$subject = "AUTO LAMAR NOTIF: Anda telah melamar lowongan ".$OL->get_nama()." on ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
		$content = $this->CI->load->view('email/auto_lamar/kandidat', array('OL' => $OL, 'OP' => $OP), TRUE);
		noreply_mail($to,$subject,$content);

		// send an email notif to perusahaan
		$to = $OP->row->email;
		$subject = "KANDIDAT LAMAR NOTIF: ".$OK->get_nama()." telah melamar lowongan ".$OL->get_nama()." ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
		$content = $this->CI->load->view('email/auto_lamar/perusahaan', array('OK' => $OK), TRUE);
		noreply_mail($to,$subject,$content);
		//*/
		// send an email notif to admin
		$to = admin_email();
		$subject = "KANDIDAT LAMAR NOTIF: ".$OK->get_nama()." telah melamar lowongan ".$OL->get_nama()." (".$OP->get_nama().") ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
		$content = $this->CI->load->view('email/auto_lamar/perusahaan', array('OK' => $OK), TRUE);
		noreply_mail($to,$subject,$content);
		unset($OK,$OU,$OL,$OP);

		$arr['kandidat_id'] = intval($this->id);
		$arr['lowongan_id'] = intval($id);
		return $this->db->insert("kandidat_lowongan_lamars",$arr);
	}
	public function get_lowongan_lamars($start, $limit, $orderby, $where)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS lowongans.*, kandidat_lowongan_lamars.dt_added as dt
				FROM kandidat_lowongan_lamars
				INNER JOIN lowongans
					ON kandidat_lowongan_lamars.lowongan_id = lowongans.id
				WHERE kandidat_lowongan_lamars.kandidat_id = ?";
		$arr = array(intval($this->id));
		if(!empty($where))
		{
			$q .= " AND {$where}";
		}
		if(!empty($orderby))
		{
			$q .= " ORDER BY {$orderby}";
		}
		if(intval($limit) > 0)
		{
			$q .= " LIMIT ?, ?";
			$arr[] = intval($start);
			$arr[] = intval($limit);
		}
		$res = $this->db->query($q, $arr);
		
		if(emptyres($res)) return FALSE;
		return $res->result();
	}

	// lowongan yang di simpan
	public function check_lowongan_simpan($id)
	{
		$arr['kandidat_id'] = intval($this->id);
		$arr['lowongan_id'] = intval($id);
		$res = $this->db->get_where("kandidat_lowongan_simpans", $arr);
		if(emptyres($res)) return FALSE;
		else return TRUE;
	}
	public function delete_lowongan_simpan($id)
	{
		if(empty($id)) return FALSE;
		$arr['kandidat_id'] = intval($this->id);
		$arr['lowongan_id'] = intval($id);
		return $this->db->delete("kandidat_lowongan_simpans",$arr);
	}
	public function delete_lowongan_simpans()
	{
		$this->db->delete("kandidat_lowongan_simpans", array("kandidat_id" => $this->id));
		return true;
	}
	public function set_lowongan_simpan($id)
	{
		if(empty($id) || $this->check_lowongan_simpan($id)) return FALSE;
		$this->delete_lowongan_simpan($id);
		$arr['kandidat_id'] = intval($this->id);
		$arr['lowongan_id'] = intval($id);
		return $this->db->insert("kandidat_lowongan_simpans",$arr);
	}
	public function get_lowongan_simpans($start, $limit, $orderby, $where)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS lowongans.* FROM kandidat_lowongan_simpans
				INNER JOIN lowongans
					ON kandidat_lowongan_simpans.lowongan_id = lowongans.id
				WHERE kandidat_lowongan_simpans.kandidat_id = ?
				";
		$arr = array(intval($this->id));
		if(!empty($where))
		{
			$q .= " AND {$where}";
		}
		if(!empty($orderby))
		{
			$q .= " ORDER BY {$orderby}";
		}
		if(intval($limit) > 0)
		{
			$q .= " LIMIT ?, ?";
			$arr[] = intval($start);
			$arr[] = intval($limit);
		}
		$res = $this->db->query($q, $arr);
		
		if(emptyres($res)) return FALSE;
		return $res->result();
	}

	// lamar otomatis
	public function delete_auto_lamar()
	{
		$arr['kandidat_id'] = intval($this->id);
		return $this->db->delete("kandidat_auto_lamars",$arr);
	}
	public function set_auto_lamar($arr)
	{
		$this->delete_auto_lamar();
		$arr['kandidat_id'] = intval($this->id);
		return $this->db->insert("kandidat_auto_lamars",$arr);
	}
	public function get_auto_lamar()
	{
		$res = $this->db->get_where('kandidat_auto_lamars',array('kandidat_id' => $this->id));
		if(emptyres($res)) return FALSE;
		return $res->row();
	}
	public function generate_auto_lamars()
	{
		// check the match of today
		$where_arr = array($this->id);
		$q = "SELECT kandidat_auto_lamars.* FROM kandidat_auto_lamars
					INNER JOIN kandidats
						ON kandidats.id = kandidat_auto_lamars.kandidat_id
					WHERE kandidat_auto_lamars.dt_added BETWEEN DATE_SUB(CURDATE(), INTERVAL 3 DAY) AND NOW()
						AND kandidat_auto_lamars.email_alert_active_flag = 1
						AND kandidat_auto_lamars.kandidat_id = ?
					";
		$res = $this->db->query($q,$where_arr);
		if(!emptyres($res))
		{
			$r = $res->row();
			//$res = $res->result();
			//foreach($res as $r)
			//{
				$add_sql = "";
				$add_sql_arr = $where_arr = NULL;
				// fitler from kandidat_auto_lamars
				/*
				waktu_kerja
				status_kerja
				kategori
				sub_kategori/jabatan
				lokasi
				*/
				if(!empty($r->waktu_kerja_ids))
				{
					$add_sql_arr[] = "lowongans.waktu_kerja_id IN(".$r->waktu_kerja_ids.")";
				}
				/*if(!empty($r->status_kerja_ids))
				{
					$add_sql_arr[] = "lowongans.status_kerja_id IN(".$r->status_kerja_ids.")";
				}*/
				if(!empty($r->kategori_ids))
				{
					$add_sql_arr[] = "lowongans.kategori_id IN(".$r->kategori_ids.")";
				}
				if(!empty($r->lokasi_ids))
				{
					$add_sql_arr[] = "lowongans.lokasi_propinsi_id IN(".$r->lokasi_ids.")";
				}
				
				// filter from kandidats
				/*
				level_karir
				umur
				gender
				pendidikan
				*/
				if(!empty($this->row->level_karir_id))
				{
					$add_sql_arr[] = "lowongans.level_karir_id = ?";
					$where_arr[] = intval($this->row->level_karir_id);
				}
				$kandidat_umur = $this->get_umur();
				if(intval($kandidat_umur) > 0)
				{
					$add_sql_arr[] = "(lowongans.umur_mulai >= ? AND lowongans.umur_sampai <= ?)";
					$where_arr[] = intval($kandidat_umur);
					$where_arr[] = intval($kandidat_umur);
				}
				if(!empty($this->row->jenis_kelamin_id))
				{
					$add_sql_arr[] = "lowongans.jenis_kelamin_id = ?";
					$where_arr[] = intval($this->row->jenis_kelamin_id);
				}
				$pendidikan = $this->get_pendidikan();
				if($pendidikan)
				{
					$pendidikan_arr = NULL;
					foreach($pendidikan as $r)
					{
						$pendidikan_arr[] = $r->pendidikan_id;
					}
					$add_sql_arr[] = "lowongans.pendidikan_id IN(".implode(",",$pendidikan_arr).")";
				}

				// generate add sql
				if(count($add_sql_arr) > 0)
				{
					$add_sql = implode(" AND ", $add_sql_arr);
				}
				// check if the lowongans is not expired and match with the filters
				$q = "SELECT lowongans.* FROM lowongans
						WHERE lowongans.dt_expired >= NOW()
						{$add_sql}
						";
				$low_res = $this->db->query($q,$where_arr);
				if(!emptyres($low_res))
				{
					$low_res = $low_res->result();
					foreach($low_res as $low_r)
					{
						// check if the kandidat has been apply lowongan and skip it
						if($this->check_lowongan_lamar($low_r->id)) continue;

						$kandidat_data = (array) $this->row;
						$lowongan_data = (array) $low_r;
						$arr = array(
									'kandidat_id'=> intval($r->kandidat_id),
									'lowongan_id'=> intval($low_r->id),
									'kandidat_data'=> json_encode($kandidat_data),
									'lowongan_data'=> json_encode($lowongan_data)
									);
						// check kandidat_notif_queues exist of today
						$q = "SELECT * FROM kandidat_notif_queues
								WHERE sent_flag = 0
								AND dt_added = NOW()
								AND kandidat = ?
								AND lowongan_id = ?
								";
						$where_arr = array(intval($r->kandidat_id), intval($low_r->id));
						$check = $this->db->query($q, $where_arr);
						if(emptyres($check))
						{
							// add new data
							$this->db->insert('kandidat_notif_queues',$arr);
						}
						else 
						{
							// edit data
							$this->db->update('kandidat_notif_queues',$arr,array('id' => $check->row()->id));
						}
						// endif
					}
					// endforeach
				}
				// endif
			//}
			// endforeach
		}
		// endif
	}
	function set_auto_lamar_mail()
	{
		$q = "SELECT * FROM kandidat_notif_queues WHERE sent_flag = 0 LIMIT 0,500";
		$res = $this->db->query($q,$where_arr);
		if(!emptyres($res))
		{
			$result = $res->result();
			foreach ($result as $r)
			{
				$kandidat_obj = json_decode($r->kandidat_data);
				$lowongan_obj = json_decode($r->lowongan_data);
				$OK = new OKandidat($r->kandidat_id);
				$OU = new OUser($kandidat_obj->user_id);
				$OL = new OLowongan($r->lowongan_id);
				$OP = new OPerusahaan($lowongan_obj->perusahaan_id);
				
				// set lamar
				$OK->set_lowongan_lamar($r->lowongan_id);
				
				/*// send an email notif to kandidat
				$to = $OU->row->email;
				$subject = "AUTO LAMAR NOTIF: Anda telah melamar otomatis lowongan ".$OL->get_nama()." on ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
				$content = $this->CI->load->view('email/auto_lamar/kandidat', array('OL' => $OL, 'OP' => $OP), TRUE);
				if(noreply_mail($to,$subject,$content))
				{
					// update kandidat_notif_queues: set sent_flag and set sent_dt
					echo "<p>SENT to KANDIDAT: ".$OK->get_nama()." (".$OU->row->email.")</p>";
				}

				// send an email notif to perusahaan
				$to = $OP->row->email;
				$subject = "KANDIDAT LAMAR NOTIF: ".$OK->get_nama()." telah melamar otomatis lowongan ".$OL->get_nama()." ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
				$content = $this->CI->load->view('email/auto_lamar/perusahaan', array('OK' => $OK), TRUE);
				if(noreply_mail($to,$subject,$content))
				{
					// update kandidat_notif_queues: set sent_flag and set sent_dt
					echo "<p>SENT to PERUSAHAAN: ".$OP->get_nama()." (".$OP->row->email.")</p>";
				}*/
				unset($OK,$OU,$OL,$OP);
				
				// set sent_flag and sent_date
				$this->db->update('kandidat_notif_queues',
							array('sent_flag'=>1,
									'sent_date'=>date('Y-m-d H:i:s')
									),
							array('id' => $r->id)
							);
			}
		}
	}

	// email alert
	public function delete_email_alert()
	{
		$arr['kandidat_id'] = intval($this->id);
		return $this->db->delete("kandidat_email_alerts",$arr);
	}
	public function set_email_alert($arr)
	{
		$this->delete_email_alert();
		$arr['kandidat_id'] = intval($this->id);
		return $this->db->insert("kandidat_email_alerts",$arr);
	}
	public function get_email_alert()
	{
		$res = $this->db->get_where('kandidat_email_alerts',array('kandidat_id' => $this->id));
		if(emptyres($res)) return FALSE;
		return $res->row();
	}

	public function is_cv_lihat()
	{
		if(empty($this->row->izin_lihat_cv_flag)) return FALSE;
		return TRUE;
	}

	public function is_dokumen_lihat()
	{
		if(empty($this->row->izin_lihat_dokumen_flag)) return FALSE;
		return TRUE;
	}	

	// others
	public function get_others_by_keterampilan($start=0,$limit=0)
	{
		$keterampilans = $this->get_keterampilan(0,0);
		$arr = NULL;
		foreach ($keterampilans as $r)
		{
			$arr[] = $r->kategori_id;
		}
		$this->db->select('SQL_CALC_FOUND_ROWS kandidats.*', FALSE)
						->from('kandidat_keterampilans')
						->join('kandidats', 'kandidats.id = kandidat_keterampilans.kandidat_id', 'inner')
						->where('kandidats.id <>', $this->id)
						->where_in('kandidat_keterampilans.kategori_id', $arr);
		if(intval($limit) > 0)
		{
			$this->db->limit($limit,$start);
		}
		$this->db->order_by('RAND()');
		$this->db->group_by('kandidat_keterampilans.kandidat_id');
		$res = $this->db->get();
		if(emptyres($res)) return FALSE;
		return $res->result();
	}

}
