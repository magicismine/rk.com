<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OPerusahaan{
    var $CI;
    var $db;
    var $table_name = "perusahaans";
    var $row;
    var $id;
    
    public function __construct($id, $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        if($type == "url_title")
          {
          	$arr = array("url_title" => $id);
          }
					elseif($type == "user_id")
          {
          	$arr = array("user_id" => $id);
          }
					else
          {
            $arr = array("id" => $id);    
          }
          $res = $this->db->get_where($this->table_name,$arr);
          if(emptyres($res)) 
          {
            $this->id = false;
            $this->row = false;
          }
          else
          {
            $this->row = $res->row();
            $this->id = $this->row->id;
          }
        }		
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public static function add($arr)
    {
        $CI =& get_instance();
        $CI->db->insert("perusahaans",$arr);
        return $CI->db->insert_id();
    }
    
    public static function get_list($start,$limit,$orderby = "id DESC",$where = "")
    {
    	$CI =& get_instance();
      if($orderby == "") $orderby = "id DESC";
      if(intval($limit) == 0)
      {
      	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `perusahaans` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby}",array(intval($start),intval($limit))); 
      }
      else
      {
      	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `perusahaans` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby} LIMIT ?, ?",array(intval($start),intval($limit))); 
      }
      if(emptyres($res)) return FALSE;
      else return $res->result();
    }

    //get kandidat rekomendasi
    public function get_list_rekomendasi_kandidat_perusahaan($start=0,$limit=0,$orderby="kandidats.id ASC",$lowongan_id)
    {
    	if($orderby == ""){ $orderby = "kandidats.id"; }
      $q = "SELECT SQL_CALC_FOUND_ROWS kandidats.*,kandidat_lowongan_lamars.lowongan_id FROM kandidats LEFT JOIN kandidat_lowongan_lamars ON kandidat_lowongan_lamars.kandidat_id = kandidats.id LEFT JOIN lowongans  ON kandidat_lowongan_lamars.lowongan_id = lowongans.id  WHERE kandidat_lowongan_lamars.lowongan_id = ? AND lowongans.perusahaan_id = ? GROUP BY kandidats.id ORDER BY {$orderby}";
      if(intval($limit) > 0)
			{
				$q .= " LIMIT ?, ?";
				$arr[] = intval($start);
				$arr[] = intval($limit);
			}
      $res = $this->db->query($q,array($lowongan_id,$this->id,$arr));
      return $res->result();
    }
    
    public static function search($keyword,$orderby = "")
    {
    	$CI =& get_instance();
      $q = "SELECT SQL_CALC_FOUND_ROWS * 
      		FROM `perusahaans`
      		WHERE id = ?";
      $arr = array(intval($keyword));
      // others
      $fields = array("nama", "email", "deskripsi");
      foreach ($fields as $field)
      {
      	$q .= " OR `{$field}` LIKE ?";
      	$arr[] = "%".trim($keyword)."%";
      }


      if($orderby != "")
      {
      	$q .= " ORDER BY ".@mysql_escape_string($orderby);
      }
    	$res = $CI->db->query($q,$arr); 
      if(emptyres($res)) return FALSE;
      else return $res->result();            
    }
    
    public static function drop_down_select($name,$selval,$optional = "",$default="",$readonly)
		{
			$list = OPerusahaan::get_list(0, 0, "id ASC");
			
			foreach($list as $r)
			{
				$arr[$r->id] = $r->nama;			
			}
			return dropdown($name,$arr,$selval,$optional,$default,$readonly);
		} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
    	// delete related
    	$this->delete_kandidats();
			$this->delete_promos();
			$this->delete_lowongans();
      return $this->db->delete($this->table_name,array("id" => $this->id));		
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $this->db->update($this->table_name,array('logo' => $image_file),array('id' => $this->id));
    }
    
    public function get_photo($type = "medium")
    {
    	$filename = $this->row->logo;
    	if($filename != "" && is_file(FCPATH."/_assets/images/uploads/{$type}/".$filename))
    	{
    		return base_url()."/_assets/images/uploads/{$type}/".$filename;
    	}
    	else
    	{
    		return base_url()."/_assets/images/no-image-2.png";
    	}
        
    }
    
    public function update_url_title()
    {
    	
    	$new_url_title = url_title($this->row->nama,"-",TRUE);
    	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
        $res = $this->db->query($q,array($new_url_title,$this->id));
        if(!emptyres($res))
        {
        	$new_url_title .= "-".$this->id;
        }
        $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
    }
    
    public function get_link()
    {
    	return site_url("user_perusahaan/details/".$this->row->url_title);
    }
	
    public function get_nama()
    {
    	return $this->row->nama;
    }
	
	// lowongan
	public function add_lowongan($arr)
	{
		if(!is_array($arr) && count($arr) <= 0) return FALSE;

		$arr = array_merge($arr, array('perusahaan_id' => $this->id));
		
		$this->CI->load->library('OLowongan');
		return OLowongan::add($arr);
	}
	public function get_lowongans($start, $limit, $orderby, $add_where)
	{
		$where = "perusahaan_id=".intval($this->id);
		if(!empty($add_where))
		{
			$where .= " AND {$add_where}";
		}
		$this->CI->load->library('OLowongan');
		$res = OLowongan::get_list($start, $limit, $orderby, $where);
		
		return $res;
	}
	public function search_lowongans($keyword, $start, $limit, $orderby, $where, $kategori_id, $lokasi_propinsi_id, $lokasi_kabupaten_id)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM lowongans
				WHERE perusahaan_id = ?";
		$arr = array(intval($this->id));
		if(!empty($kategori_id))
		{
			$kategori_id = intval($kategori_id);
			$q .= " AND kategori_id = ?";
			$arr[] = $kategori_id;
		}
		if(!empty($lokasi_propinsi_id))
		{
			$lokasi_propinsi_id = intval($lokasi_propinsi_id);
			$q .= " AND lokasi_propinsi_id = ?";
			$arr[] = $lokasi_propinsi_id;
		}
		if(!empty($lokasi_kabupaten_id))
		{
			$lokasi_kabupaten_id = intval($lokasi_kabupaten_id);
			$q .= " AND lokasi_kabupaten_id = ?";
			$arr[] = $lokasi_kabupaten_id;
		}
		if(!empty($keyword))
		{
			$keyword = $this->db->escape_like_str($keyword);
			$q .= " AND (posisi LIKE ? OR deskripsi LIKE ?)";
			$arr[] = "%".$keyword."%";
			$arr[] = "%".$keyword."%";
		}
		if(!empty($where))
		{
			$q .= " AND {$where}";
		}
		if(!empty($orderby))
		{
			$q .= " ORDER BY {$orderby}";
		}
		if(intval($limit) > 0)
		{
			$q .= " LIMIT ?, ?";
			$arr[] = intval($start);
			$arr[] = intval($limit);
		}
		$res = $this->db->query($q, $arr);
		
		if(emptyres($res)) return FALSE;
		return $res->result();
	}
	public function get_total_lowongan_aktif()
	{
		$q = "SELECT COUNT(*) as total FROM lowongans WHERE perusahaan_id = ? AND dt_expired > NOW()";
		$arr = array(intval($this->id));
		$res = $this->db->query($q,$arr);
		
		if(emptyres($res)) return FALSE;
		return $res->row()->total;
	}
	
	// CV
	public function check_cv($id, $type="tandai")
	{
		$arr = NULL;
		$arr['perusahaan_id'] = intval($this->id);
		$arr['kandidat_id'] = intval($id);
		if($type == "tandai") $arr['tandai'] = 1;
		if($type == "undang") $arr['undang'] = 1;
		$res = $this->db->get_where("perusahaan_kandidats", $arr);
		if(emptyres($res)) return FALSE;
		else return TRUE;
	}
	public function delete_cv($id, $type="tandai")
	{
		if(empty($id)) return;
		$where_arr['perusahaan_id'] = intval($this->id);
		$where_arr['kandidat_id'] = intval($id);
		if($type == "tandai") $arr['tandai'] = 0;
		if($type == "undang") $arr['undang'] = 0;
		return $this->db->update("perusahaan_kandidats",$arr,$where_arr);
	}
	public function tandai_cv($id)
	{
		if(empty($id)) return;
		$arr['tandai'] = 1;
		if(!$this->check_cv($id)):
			$arr['perusahaan_id'] = intval($this->id);
			$arr['kandidat_id'] = intval($id);
			return $this->db->insert("perusahaan_kandidats",$arr);
		else:
			$where_arr['perusahaan_id'] = intval($this->id);
			$where_arr['kandidat_id'] = intval($id);			
			return $this->db->update("perusahaan_kandidats",$arr,$where_arr);
		endif;
	}
	public function undang_cv($id)
	{
		if(empty($id)) return;
		
		$OK = new OKandidat(intval($id));
		if($OK->row):
		//*
		//Notif Admin
		//$to = $OK->row->email;
		$to = admin_email();
		$subject = $this->get_nama()." mengundang kandidat ".$OK->get_nama()." | ".$_SERVER['HTTP_HOST'];
		//$body = $this->CI->load->view("perusahaan/email/undangan", array("row" => $this->row), TRUE);
		$body = 'Dear Admin,<br />
<br />
'.$this->get_nama().' telah mengundang kandidat '.$OK->get_nama().'.<br />
<br />
Silahkan merekomendasikan kandidat tersebut di salah satu lowongan perusahaan '.$this->get_nama().'.<br />
<br />
Berikut detail informasi tentang:
<br />
- Perusahaan: '.$this->get_link().'
<br />
- Kandidat: '.$OK->get_link().'
<br />
<br />
Best regards,<br />
'.get_setting('sitename');
		noreply_mail($to,$subject,$body);
		//*/
		
		endif;
		unset($OK);
		
		$arr['undang'] = 1;
		if(!$this->check_cv($id)):
			$arr['perusahaan_id'] = intval($this->id);
			$arr['kandidat_id'] = intval($id);
			return $this->db->insert("perusahaan_kandidats",$arr);
		else:
			$where_arr['perusahaan_id'] = intval($this->id);
			$where_arr['kandidat_id'] = intval($id);			
			return $this->db->update("perusahaan_kandidats",$arr,$where_arr);
		endif;
	}
	/*public function get_cvs($start, $limit, $orderby, $where)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS k.* FROM kandidats k
				INNER JOIN perusahaan_kandidats pk
				ON pk.kandidat_id = k.id
				WHERE pk.perusahaan_id = ?";
		$arr = array(intval($this->id));
		if(!empty($where))
		{
			$q .= " AND {$where}";
		}
		$q .= " GROUP BY k.id";
		if(!empty($orderby))
		{
			$q .= " ORDER BY {$orderby}";
		}
		if(intval($limit) > 0)
		{
			$q .= " LIMIT ?, ?";
			$arr[] = intval($start);
			$arr[] = intval($limit);
		}
		$res = $this->db->query($q, $arr);
		
		if(emptyres($res)) return FALSE;
		return $res->result();
	}*/
	public function search_cvs($keyword="", $start=0, $limit=0, $orderby, $where, $params)
	{
		if(empty($orderby)) $orderby = "perusahaan_kandidats.dt_added DESC";
		$q = "SELECT SQL_CALC_FOUND_ROWS kandidats.* FROM perusahaan_kandidats
				INNER JOIN kandidats
					ON perusahaan_kandidats.kandidat_id = kandidats.id
				WHERE perusahaan_kandidats.perusahaan_id = ?
				";
		$arr = array(intval($this->id));
		
		if(!empty($keyword))
		{
			$keyword = $this->db->escape_like_str($keyword);
			$q .= " AND (kandidats.nama_depan LIKE ?)";
			$arr[] = $keyword."%";
		}
		if(count($params) > 0)
		{
			foreach ($params as $key => $val)
			{
				if(empty($val) || is_array($val)) continue;
				# code...
				if(stristr($key, 'pendidikan'))
				{
					$val = intval($val);
					$q .= " AND kandidats.id IN(SELECT kandidat_id FROM kandidat_pendidikans WHERE pendidikan_id = ?)";
					$arr[] = $val;
				}
				if(stristr($key, 'tahun_lahir'))
				{
					$tahun_lahir = intval($tahun_lahir);
					$q .= " AND kandidats.tanggal_lahir = ?";
					$arr[] = intval($val);
				}
				if(stristr($key, 'propinsi'))
				{
					$val = intval($val);
					$q .= " AND (kandidats.asal_propinsi_id = ? OR kandidats.skrg_propinsi_id = ?)";
					$arr[] = $val;
					$arr[] = $val;
				}
				if(stristr($key, 'kabupaten'))
				{
					$val = intval($val);
					$q .= " AND (kandidats.asal_kabupaten_id = ? OR kandidats.skrg_kabupaten_id = ?)";
					$arr[] = $val;
					$arr[] = $val;
				}
			}
		}

		if(!empty($where))
		{
			$q .= " AND {$where}";
		}
		$q .= " GROUP BY kandidats.id";
		if(!empty($orderby))
		{
			$q .= " ORDER BY {$orderby}";
		}
		if(intval($limit) > 0)
		{
			$q .= " LIMIT ?, ?";
			$arr[] = intval($start);
			$arr[] = intval($limit);
		}
		$res = $this->db->query($q, $arr);
		
		if(emptyres($res)) return FALSE;
		return $res->result();
	}

	// PROMO
	public function add_promo($arr)
	{
		if(!is_array($arr) && count($arr) <= 0) return FALSE;

		$arr = array_merge($arr, array('perusahaan_id' => $this->id));
		$OTmp = new OPerusahaan_promo;
		$res = $OTmp->add($arr);
		if($res)
		{
			$OTmp = new OPerusahaan_promo($res);
			$OTmp->update_url_title();
		}
		return $res;
	}
	public function get_promos($start=0, $limit=0, $orderby="", $where="")
	{
		$OTmp = new OPerusahaan_promo;
		$and_where = array("perusahaan_id = ".intval($this->id));
		if(!empty($where))
		{
			$and_where[] = "{$where}";
		}
		$res = $OTmp->get_list($start, $limit, $orderby, implode(" AND ", $and_where));
		
		if(!$res) return FALSE;
		return $res;
	}
	public function delete_promo($id)
	{
		if(empty($id)) return FALSE;
		$arr['perusahaan_id'] = intval($this->id);
		$arr['id'] = intval($id);
		return $this->db->delete("perusahaan_promos",$arr);
	}

}
