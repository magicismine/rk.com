<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OUser{
    var $CI;
    var $db;
    var $table_name = "users";
    var $row;
    var $id;
    
    public function __construct($id, $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        $arr = array("id" => $id);
        	if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }		
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public static function add($arr)
    {
        $CI =& get_instance();
        $CI->db->insert("users",$arr);
        return $CI->db->insert_id();
    }
    
    public static function get_list($start,$limit,$orderby = "dt_added DESC",$where = "")
    {
    	$CI =& get_instance();
        if($orderby == "") $orderby = "dt_added DESC";
        if(intval($limit) == 0)
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `users` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby}",array(intval($start),intval($limit))); 
        }
        else
        {
        	$res = $CI->db->query("SELECT SQL_CALC_FOUND_ROWS * FROM `users` ".($where != "" ? "WHERE {$where} " : "")." ORDER BY {$orderby} LIMIT ?, ?",array(intval($start),intval($limit))); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public static function search($keyword,$orderby = "",$role = "")
    {
    	$CI =& get_instance();
        $q = "SELECT SQL_CALC_FOUND_ROWS * 
        		FROM `users`
                WHERE id = ? OR email LIKE ? AND role = ?";
        $arr = array();
        $arr[] = intval($keyword);
        $arr[] = "%{$keyword}%";
        $arr[] = $role;
                if($orderby != "")
        {
        	$q .= " ORDER BY ".@mysql_escape_string($orderby);
        	$res = $CI->db->query($q,$arr); 
        }
        else
        {
        	$res = $CI->db->query($q,$arr); 
        }
        if(emptyres($res)) return FALSE;
        else return $res->result();
                
    }
    
    public static function drop_down_select($name,$selval,$where="",$optional="",$default="",$readonly)
	{
		$list = OUser::get_list(0, 0, "id ASC", $where);
		
		foreach($list as $r)
		{
			$arr[$r->id] = $r->email;			
		}
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
        return $this->db->delete($this->table_name,array("id" => $this->id));		
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $this->db->update($this->table_name,array('photo' => $image_file),array('id' => $this->id));
    }
    
    public function get_photo($type = "medium")
    {
    	$filename = $this->row->photo;
        return base_url()."/_assets/images/uploads/{$type}/".$filename."?v=".date("ymdHi");
    }
    
    public function update_url_title()
    {
    	
    	$new_url_title = url_title($this->row->nama,"-",TRUE);
    	$q = "SELECT * FROM ".$this->table_name." WHERE url_title = ? AND id <> ?";
        $res = $this->db->query($q,array($new_url_title,$this->id));
        if(!emptyres($res))
        {
        	$new_url_title .= "-".$this->id;
        }
        $this->db->update($this->table_name,array('url_title' => $new_url_title),array('id' => $this->id));
    }
    
    public function get_link()
    {
    	return $this->row->url_title;
    }
        
	public function get_info($role)
	{
		if(empty($role)) $role = $this->row->role;
		if($role == "kandidat")
		{
			$check = $this->db->get_where("kandidats", array("user_id" => $this->id));
		}
		if($role == "perusahaan")
		{
			$check = $this->db->get_where("perusahaans", array("user_id" => $this->id));
		}
		if(emptyres($check)) return FALSE;
		$tmp = $check->row();
		return $tmp;
	}
	
	public function update_info($arr, $role="kandidat")
	{
		if(count($arr) <= 0 || empty($role)) return FALSE;
		$arr["user_id"] = $this->id;
		$check = $this->get_info($role);
		if($role == "kandidat")
		{
			if(!$check)
			{
				$tmp = OKandidat::add($arr);
                $O = new OKandidat($tmp);
                $O->update_url_title();
			}
			else
			{
				$O = new OKandidat();
				$O->setup($check);
				$O->edit($arr);
			}
		}
		if($role == "perusahaan")
		{
			if(!$check)
			{

				$tmp = OPerusahaan::add($arr);
                $O = new OPerusahaan($tmp);
                $O->update_url_title();
			}
			else
			{
				$O = new OPerusahaan();
				$O->setup($check);
				$O->edit($arr);
			}
		}
		return $tmp;
	}
	
	public function get_carts($start = 0, $limit = 20)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM carts WHERE user_id = ?";
		if(intval($limit) > 0) $q .= " LIMIT ?,?";        
		$res = $this->db->query($q,array($this->row->id,intval($start),intval($limit)));
		if(emptyres($res)) return FALSE;
		else return $res->result();
    }

	public function get_orders($start = 0, $limit = 20)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM orders WHERE user_id = ?";
		if(intval($limit) > 0) $q .= " LIMIT ?,?";        
		$res = $this->db->query($q,array($this->row->id,intval($start),intval($limit)));
		if(emptyres($res)) return FALSE;
		else return $res->result();
    }

	public function get_challenge_comments($start = 0, $limit = 20)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM challenge_comments WHERE user_id = ?";
		if(intval($limit) > 0) $q .= " LIMIT ?,?";        
		$res = $this->db->query($q,array($this->row->id,intval($start),intval($limit)));
		if(emptyres($res)) return FALSE;
		else return $res->result();
    }

	public function get_challenges($start = 0, $limit = 20)
	{
		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM challenges WHERE user_id = ?";
		if(intval($limit) > 0) $q .= " LIMIT ?,?";        
		$res = $this->db->query($q,array($this->row->id,intval($start),intval($limit)));
		if(emptyres($res)) return FALSE;
		else return $res->result();
    }	
}
