<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OTable{
    var $CI;
    var $db;
    var $table_name = "";
    var $row;
    var $id;
    
    public function __construct($id="", $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
	        $arr = array("id" => $id);
        	if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }
    }
    
    public function setup($row)
    {
        if($row->id != "")
        {
            $this->row = $row;
            $this->id = $row->id;
        }
        else return false;
    }
    
    public function add($arr)
    {
        $this->db->insert($this->table_name,$arr);
        return $this->db->insert_id();
    }
    
    public function get_list($offset=0,$limit=0,$orderby="id DESC",$where = "")
    {
        if(trim($where) != "") $this->db->where($where,NULL,FALSE);
        if(trim($orderby) != "") $this->db->order_by($orderby);
        if(intval($limit) > 0) $this->db->limit($limit, $offset);
        $this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
        $res = $this->db->get($this->table_name);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public function search($keyword="",$offset=0,$limit=0,$orderby="id DESC",$where = "")
    {
        if(trim($where) != "") $this->db->where($where,NULL,FALSE);
        if(trim($keyword) != "")
        {
            $this->db->like("id", intval($keyword),'none');
            $like_array = array('name' => $keyword);
            $this->db->or_like($like_array);
        }
        if(trim($orderby) != "") $this->db->order_by($orderby);
        if(intval($limit) > 0) $this->db->limit($limit, $offset);
        $this->db->select('SQL_CALC_FOUND_ROWS *', FALSE);
        $res = $this->db->get($this->table_name);
        if(emptyres($res)) return FALSE;
        else return $res->result();
    }
    
    public function drop_down_select($name,$selval,$optional="",$default="",$readonly)
	{
		$list = $this->get_list(0, 0, "name ASC");
		
		foreach($list as $r)
		{
			$arr[$r->id] = $r->name;			
		}
		return dropdown($name,$arr,$selval,$optional,$default,$readonly);
	} 
    
    public function edit($arr)
    {
        return $this->db->update($this->table_name,$arr,array("id" => $this->id));		
    }
    
    public function delete()
    {
        return $this->db->delete($this->table_name,array("id" => $this->id));		
    }
    
    public function refresh()
    {
        $res = $this->db->get_where($this->table_name,array("id" => $this->id));
        if(emptyres($res)) 
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $this->row = $res->row();
            $this->id = $this->row->id;
        }
    }
    
    public function update_photo($image_file)
    {
    	$files = auto_resize_photo($image_file);
        $arr = array('photo' => $image_file);
        $this->edit($arr);
    }
    
    public function get_photo($type = "thumbnail")
    {
    	$filename = $this->row->photo;
        return base_url("_assets/images/uploads/{$type}/{$filename}");
    }
    
    public function update_url_title()
    {
    	
    	$new_url_title = url_title($this->row->name,"-",TRUE);
        $res = $this->get_list(0,1,"","url_title='{$new_url_title}' AND id <> ".intval($this->id));
        if($res)
        {
            $new_url_title .= "-".$this->id;
        }
        $arr = array('url_title' => $new_url_title);
        $this->edit($arr);
    }
    
    public function get_link()
    {
    	return $this->row->url_title;
    }

    public function get_name()
    {
        return $this->row->name;
    }
	
}
