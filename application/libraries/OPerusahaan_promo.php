<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OPerusahaan_promo extends OTable{
    var $CI;
    var $db;
    var $table_name="";
    var $row;
    var $id;
    
    public function __construct($id="", $type="id")
    {
        $CI =& get_instance();
        $this->CI = $CI;
        $this->db = $CI->db;
        $this->table_name = "perusahaan_promos";
        if(empty($id))
        {
            $this->id = false;
            $this->row = false;
        }
        else
        {
            $arr = array("id" => $id);
            if($type != "id") $arr = array("url_title" => $id);
            $res = $this->db->get_where($this->table_name,$arr);
            if(emptyres($res)) 
            {
                $this->id = false;
                $this->row = false;
            }
            else
            {
                $this->row = $res->row();
                $this->id = $this->row->id;
            }
        }
    }

    public function get_link()
    {
        return "promo/kandidat/".$this->row->url_title;
    }

}
