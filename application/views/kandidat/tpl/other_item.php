<?php
$url = $OTmp->get_link();
$photo = $OTmp->get_photo('avatar');
$img = '<img class="lazy" src ="'.$photo.'" data-original="'.$photo.'" width="50" height="50">';
$nama = $OTmp->get_nama_lengkap();
$umur = $OTmp->get_umur();
$keterampilan_arr = $OTmp->get_all_keterampilan(3);
?>
<li>
<label>
	<?php
	echo anchor($url, $img);
	?>
</label>
<label class="ls-desc">
	<?php
	if(($cu->role == "perusahaan" && $O->is_cv_lihat()) || $ca):
		echo anchor($url, "<strong>".$nama." (".$umur.")</strong>");
	endif;
	echo implode(",<br>", $keterampilan_arr);
	?>
</label>
</li>