<?php extract(get_object_vars($O->row)); ?>
<div class="boxWhite"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
	<tr>
	  <th colspan="4"class="title-span">PENDIDIKAN</th>
	</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
		<thead>
		<tr>
			<td width="6%" align="center">#</td>
			<td width="24%" align="center">Tingkat Pendidikan</td>
			<td width="30%">Jurusan/ Program Studi</td>
			<td width="31%">Nama Sekolah/ Universitas</td>
			<td width="9%">Nilai Rata-Rata</td>
		</tr>
		</thead>
	<?php
	$pendidikan = $O->get_pendidikan();
	$i = 1;
	foreach($pendidikan as $r):
		$OTmp = new OMaster_pendidikan($r->pendidikan_id); 
		$pendidikan = $OTmp->get_nama();
		unset($OTmp);
		?>
		<tr>
			<td align="center"><?php echo $i; ?></td>
			<td align="center"><?php echo $pendidikan; ?></td>
			<td><?php echo $r->jurusan; ?></td>
			<td><?php echo $r->nama;?></td>
			<td><?php echo $r->nilai; ?></td>
		</tr>
		<?php
		$i++;
	endforeach;
	?>
	</table>
</div>