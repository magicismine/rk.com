<ul class="list-sidebar kinfo">
	<li class="ls-title">Kandidat Lainnya</li>
	<?php
	$list = $O->get_others_by_keterampilan(0,10);
	$total = get_db_total_rows();

	$total_all = get_total_kandidat() - 1;
	$OTmp = new OKandidat;
	foreach($list as $r):
		$OTmp->setup($r);
		echo $this->load->view($this->vpath.'/tpl/other_item', array('OTmp'=>$OTmp,'O'=>$O,'cu'=>$cu,'ca'=>$ca), TRUE);
	endforeach;
	unset($OTmp);
	?>
	<?php if($total_all > 10): ?>
		<li class="nocl"><?php echo anchor('search/kandidat', 'Lihat Semua ('.($total_all).')') ?></li>
	<?php endif; ?>
	<div class="clr"></div>
</ul>
<br>