<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
<thead>
<tr>
	<td align="center" valign="middle">&nbsp; </td>
	<td valign="middle">Nama dan Keahlian</td>
	<td valign="middle">Jenis Kelamin</td>
	<td valign="middle">Tinggi Badan</td>
	<td align="center" valign="middle">Pendidikan</td>
	<td align="center" valign="middle">Lokasi</td>
	<?php /* ?>
	<td width="7%" align="center" valign="middle">&nbsp; </td>
	<td width="49%" valign="middle">Nama dan Keahlian</td>
	<td width="7%" align="center" valign="middle">Pendidikan</td>
	<td width="20%" align="center" valign="middle">Lokasi</td>
	<td width="17%" align="center" valign="middle">Status</td>
	<?php */ ?>
</tr>
</thead>
<tbody>
<?php

if(!$list):
	
	?>
	<tr>
		<td colspan="6" align="center" valign="middle">Kandidat yang dimaksud tidak ditemukan.</td>
	</tr>
	<?php

else:

	//$current_url = current_url();
	$current_url = "search/kandidat";
	foreach($list as $r)
	{
		$O->setup($r);
		$url = $O->get_link();
		$photo = $O->get_photo('square');
		$img = '<img class="lazy" src ="'.$photo.'" data-original="'.$photo.'" width="50" height="50" alt=""  title="">';
		// jenis_kelamin
		$OTmp_jk = new OMaster_jenis_kelamin($r->jenis_kelamin_id);
		$jenis_kelamin = $OTmp_jk->get_nama();
		$jenis_kelamin_id = $OTmp_jk->row->id;
		unset($OTmp_jk);
		// tinggi_badan
		$kesehatan_r = $O->get_kesehatan();
		$tinggi = intval($kesehatan_r->tinggi);
		// keterampilan
		$keterampilans = $O->get_keterampilan(0,0);
      	$keterampilan_arr = NULL;
      	$i = 0;
		foreach($keterampilans as $r):
			if($i == 3) break;
  		  	$OTmp = new OMaster_kategori($r->kategori_id);
			$arr = array( anchor($current_url."?kategori_id=".$OTmp->id, $OTmp->get_nama()) );
			$parents = OMaster_kategori::get_parents($OTmp->id);
			foreach($parents as $key => $val)
			{
				$OTmp_child = new OMaster_kategori($key);
				$arr[] = anchor($current_url."?kategori_id=".$key, $OTmp_child->get_nama());
        		unset($OTmp_child);
			}
			$keterampilan_arr[] = implode(" &gt; ", $arr);
		  	unset($OTmp);
		  	$i++;
        endforeach;
        $keterampilan_arr = array_unique($keterampilan_arr);
		$keterampilan = implode("<br />", $keterampilan_arr);
		// pendidikan
		$pnddkn_row = $O->get_last_pendidikan();
		$OTmp_pnddkn = new OMaster_pendidikan($pnddkn_row->pendidikan_id);
		$pendidikan = $OTmp_pnddkn->row->nama;
		$pendidikan_id = $OTmp_pnddkn->id;
		unset($OTmp_pnddkn);
		// lokasi
		//$lokasi_row = $O->get_current_lokasi();
		//$lokasi = $lokasi_row->nama;
		$lokasi_arr = $O->get_current_complete_lokasi_name();
		$lokasi_ids = array("lokasi_propinsi_id","lokasi_kabupaten_id","lokasi_kawasan_id");
		$lokasi_link_names = "";
		$i = 0;
		$exclude_arr = array_merge($lokasi_ids, array("page"));
		//var_dump($exclude_arr);
		foreach($lokasi_arr as $id => $val):
			$tmp_lokasi_id[$i] = $id;
			if(!empty($id) && !empty($val))
			{
				$add_params = NULL;
				for ($j=$i; $j >= 0; $j--)
				{ 
					$add_params[] = $lokasi_ids[$j]."=".$tmp_lokasi_id[$j];
				}
				$add_params = array_reverse($add_params);
				//var_dump($add_params);
				//$params = implode("&", $add_params)."&".get_params($_GET, $exclude_arr);
				$params = implode("&", $add_params);
				$lokasi_link_names[] =  anchor($current_url."?".$params, $val);
			}
			$i++;
		endforeach;
		?>
		<tr>
		<td align="right" valign="middle"><?php echo anchor($url, $img); ?></td>
		<td valign="middle">
		<?php
			if($cu->role == "perusahaan"):
				echo anchor($url, '<strong>'.$O->get_nama_lengkap().' ('.$O->get_umur().')</strong><br />');
			endif;
			echo $keterampilan;
		?>
		</td>
		<td align="center" valign="middle">
			<?php echo (!empty($jenis_kelamin) ? anchor($current_url."?jenis_kelamin_id={$jenis_kelamin_id}", $jenis_kelamin) : ""); ?>
		</td>
		<td align="center" valign="middle">
			<?php echo (!empty($tinggi) ? anchor($current_url."?tinggi={$tinggi}", $tinggi."cm") : ""); ?>
		</td>
		<td align="center" valign="middle">
			<?php
			//echo (!empty($pendidikan) ? anchor($current_url."?pendidikan_id={$pendidikan_id}&".get_params($_GET, array("page","pendidikan_id")), $pendidikan) : "");
			echo (!empty($pendidikan) ? anchor($current_url."?pendidikan_id={$pendidikan_id}", $pendidikan) : "");
			?>
		</td>
		<td align="center" valign="middle">
			<?php
			echo implode(' <span class=" f18 forange">&raquo;</span> ', $lokasi_link_names);
			?>
		</td>
		<?php /* ?>
		<td align="center" valign="middle">Pencari Kerja</td>
		<?php */ ?>
		</tr>
		<?php  
	}

endif;

?>
</tbody>
</table>
<?php if($pagination): ?>
<div id="paging_button" align="center">  
<?php 
	echo $pagination; 
?>
</div>
<?php endif; ?>