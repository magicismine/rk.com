<?php
extract(get_object_vars($O->row));
extract(get_object_vars($O->get_kesehatan()));
$OTmp = new OMaster_gol_darah($gol_darah_id);
$gol_darah = $OTmp->get_nama();
unset($OTmp);
?>
<div class="boxWhite"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
      <tr>
          <th colspan="4"class="title-span">KESEHATAN</th>
      </tr>
      </table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
	<tr>
		<td width="25%">Tinggi/ Berat Badan</td>
		<td width="75%"> : <?php echo intval($tinggi); ?> cm/ <?php echo intval($berat); ?> kg</td>
	</tr>
	<tr>
		<td>Golongan Darah </td>
		<td> : <?php echo $gol_darah; ?></td>
	</tr>
	<tr>
		<td>Kacamata </td>
		<td> : <?php echo (empty($kacamata_flag) ? "Tidak" : "Iya"); ?></td>
	</tr>
	<tr>
		<td>Keluhan Kesehatan </td>
		<td> : <?php echo (empty($keluhan_kesehatan) ? $keluhan_kesehatan : "-"); ?></td>
	</tr>
	</table>
</div>  
