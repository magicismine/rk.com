<?php
extract(get_object_vars($O->row));
$keterampilan = $O->get_keterampilan();
if($keterampilan):
?>
<div class="boxWhite"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
	<tr>
	  <th colspan="4"class="title-span">KEAHLIAN KHUSUS</th>
	</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
		<thead>
		<tr>
			<td width="4%" align="center">#</td>
			<td width="63%">Bidang Keahlian </td>
			<td width="9%" align="center">Skala (5)</td>
			<td width="24%">Badan Sertifikasi</td>
		</tr>
		</thead>
	<?php  
	$i = 1;
	foreach($keterampilan as $r):
		$OTmp = new OMaster_kategori($r->kategori_id);
		$kategori = $OTmp->get_nama();
		unset($OTmp);
		?>
		<tr>
			<td align="center"><?php echo $i; ?></td>
			<td><?php echo $kategori; ?></td>
			<td align="center"><?php echo $r->nilai_skala; ?></td>
			<td><?php echo $r->sertifikasi; ?></td>
		</tr>
		<?php  
		$i++;
	endforeach;
	?>
	</table>
</div>
<?php endif; ?>