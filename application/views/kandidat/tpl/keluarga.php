<?php
extract(get_object_vars($O->row));
$OTmp = new OMaster_status_kawin($status_kawin_id);
$pendidikan = $OTmp->get_nama();
unset($OTmp);
?>
<div class="boxWhite"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
	<tr>
	  <th colspan="4"class="title-span">KELUARGA</th>
	</tr>
	</table>
	<p>Status Perkawinan : <?php echo $status_kawin; ?></p>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
		<thead>
		<tr>
			<td width="7%" align="center">#</td>
			<td width="23%">Anggota Keluarga</td>
			<td width="50%">Nama</td>
			<td width="20%">Pendidikan </td>
		</tr>
		</thead>
	<?php  
	$keluarga = $O->get_keluarga();
	$i = 1;
	foreach($keluarga as $r):
		$OTmp = new OMaster_pendidikan($r->pendidikan_id);
		$pendidikan = $OTmp->get_nama();
		unset($OTmp);
		$OTmp = new OMaster_shdk($r->shdk_id);
		$shdk = $OTmp->get_nama();
		unset($OTmp);
		?>
		<tr>
			<td align="center"><?php echo $i; ?></td>
			<td><?php echo $shdk; ?></td>
			<td><?php echo $r->nama; ?></td>
			<td><?php echo $pendidikan; ?></td>
		</tr>
		<?php  
		$i++;
	endforeach;
	?>
	</table>
</div>
