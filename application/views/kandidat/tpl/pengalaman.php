<?php
extract(get_object_vars($O->row));
$list = $O->get_pengalaman();
if($list):
?>
<div class="boxWhite"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
	<tr>
	  <th colspan="4"class="title-span">PENGALAMAN KERJA</th>
	</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
		<thead>
		<tr>
			<td width="15%" align="center">Waktu</td>
			<td width="26%">Perusahaan</td>
			<td width="34%">Bidang Usaha</td>
			<td width="25%">Jabatan</td>
		</tr>
		</thead>
	<?php  
	foreach($list as $r):
		$OTmp = new OMaster_bidang_usaha($r->bidang_usaha_id);
		$bidang_usaha = $OTmp->get_nama();
		unset($OTmp);
		?>
		<tr>
			<td align="center"><?php echo get_only_date($r->tanggal_awal,"year"); ?>-<?php echo get_only_date($r->tanggal_akhir,"year"); ?></td>
			<td><?php echo $r->perusahaan; ?></td>
			<td><?php echo $bidang_usaha; ?></td>
			<td><?php echo $r->jabatan; ?></td>
		</tr>
		<?php  
	endforeach;
	?>
	</table>
</div>
<?php endif; ?>