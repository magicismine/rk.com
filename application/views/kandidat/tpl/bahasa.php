<?php
extract(get_object_vars($O->row));
?>
<div class="boxWhite"> 
	<table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
	<tr>
	  <th colspan="4"class="title-span">Penguasaan Bahasa</th>
	</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
		<thead>
		<tr>
			<td width="4%" align="center">#</td>
			<td width="63%">Bahasa</td>
			<td width="9%" align="center">Skala (5)</td>
			<td width="24%">Badan Sertifikasi</td>
		</tr>
		</thead>
	<?php 
	$i=1;
	$bahasa = $O->get_bahasa();
	foreach($bahasa as $r):
		$OTmp = new OMaster_bahasa($r->bahasa_id);
		$bahasa = $OTmp->get_nama();
		unset($OTmp);
		?>
		<tr>
		<td align="center"><?php echo $i; ?></td>
		<td><?php echo $bahasa; ?></td>
		<td align="center"><?php echo $r->nilai_skala; ?></td>
		<td><?php echo $r->sertifikasi; ?></td>
		</tr>
		<?php 
		$i++;
	endforeach;
	?>
	</table>
</div>