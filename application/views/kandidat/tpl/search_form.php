<?php
$list_kandidat = OKandidat::get_list_filter("",0,0);
$total_kandidat = get_db_total_rows();
extract($_GET);
?>
<div class="advSearchBox">
  <ul class="column">
    <li class="col"><span class="title-advSearchBox">Cari Kandidat</span></li>
    <li class="col JobCount"><strong>Tersedia <?php echo $total_kandidat; ?> Kandidat</strong></li>
    <div class="clr"></div>
  </ul>
  <form action="<?php echo site_url('search/kandidat'); ?>" method="form" class="MainSearch">
    <ul class="column">
      <li class="col">
        <input type="text" style="width:585px" name="keyword" value="<?=$keyword?>" placeholder="Kata Kunci">
      </li>
      <li class="col">
        <?php echo OMaster_kategori::drop_down_select('kategori_id',$kategori_id,'style="width: 210px;"',"Semua keahlian"); ?>
      </li>
      <li class="col">
        <input type="submit" value="Cari" class="btn_blue">
      </li>
      <div class="clr"></div>
      </ul>
      <ul class="column detailSearch <?php if(sizeof($_GET) > 0) echo "dsopen"; ?>">
      
      <li class="col">
        <span id="subkategori_wrap">
        <?php echo OMaster_kategori::drop_down_select('subkategori_id',$subkategori_id,'style="width:300px;"',"Semua Sub Keahlian","parent_id<>0"); ?>
        </span>
      </li>
      <?php /* ?>
      <li class="col"  >
        <select >
        <option>Semua Sub Keahlian</option>
          <option><option> Programmer </option>
          <option> Designer </option>
          <option> Programm Analyst</option></option>
        </select>
      </li>
      <?php */ ?>
      <li class="col">
      <?php echo OMaster_pendidikan::drop_down_select('pendidikan_id',$pendidikan_id,'style="width:165px;"',"Semua Pendidikan"); ?>
      </li>
      <?php //* ?>
      <li class="col">
         <?php echo OMaster_level_karir::drop_down_select('level_karir_id',$level_karir_id,'style="width:160px;"',"Semua Level Karir"); ?>
      </li>
      <?php //*/ ?>
      <?php //* ?>
      <li class="col">
        <?php echo OMaster_waktu_kerja::drop_down_select('waktu_kerja_id',$waktu_kerja_id,'style="width:160px;"',"Semua Waktu"); ?>
      </li>
      <?php //*/ ?>
      <div class="clr"></div>
      <li class="col">
        <?php echo OMaster_lokasi_propinsi::drop_down_select('lokasi_propinsi_id',$lokasi_propinsi_id,'style="width:230px;"',"Semua Propinsi"); ?>
      </li> 
      <li class="col">
        <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>" data-empty="Semua Lokasi Kota dan Kabupaten" data-option="style=width:245px">
          <?php echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id,'style="width:245px;"',"Semua Lokasi Kota dan Kabupaten"); ?>
        </span>
      </li>
      <li class="col">
        <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>" data-empty="Semua Kawasan" data-option="style=width:150px">
          <?php echo OMaster_lokasi_kawasan::drop_down_select("lokasi_kawasan_id",$lokasi_kawasan_id,'style="width:150px;"',"Semua Kawasan"); ?>
        </span>
      </li>
      <li class="col">
        <?php /* ?><input id="date-range1" style="width:160px" value="Waktu Kelahiran" type="text" readonly><?php */ ?>
        <input style="width:80px" maxlength="2" name="umur_mulai" placeholder="Umur Mulai" value="<?php echo $umur_mulai; ?>" type="number" min="18" max="80">
        <input style="width:80px" maxlength="2" name="umur_akhir" placeholder="Umur Akhir" value="<?php echo $umur_akhir; ?>" type="number" min="18" max="80">
      </li>
      <div class="clr"></div>
    </ul>
    <span class="tg-detail ">Pencarian Detail</span>
  </form>
</div>
<br>
<br>
<script>
$(function()
{
  get_kabupaten_ddl("lokasi_");
  $(document).on('change', 'select#lokasi_propinsi_id', function(e){
    get_kabupaten_ddl("lokasi_");
  });
});

function get_kabupaten_ddl(prefix)
{
  var t = $('#'+prefix+'kabupaten_wrap');
  var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': $('#'+prefix+'kabupaten_wrap').data('value'), 'name': prefix+'kabupaten_id', 'default': $('#'+prefix+'kabupaten_wrap').data('empty'), 'option': $('#'+prefix+'kabupaten_wrap').data('option')}
  $.ajax({
    'type': 'POST',
    'async': false,
    url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
    data: p,
    complete: function(xhr, status)
    {
      var ret = xhr.responseText;
      t.html(ret);
    }
  });
}
function get_subcat_ddl(prefix)
{
  var t = $('#'+prefix+'kabupaten_wrap');
  var p = {'category_id': $('#'+prefix+'category_id').val(), 'subcategory_id_id': $('#'+prefix+'category_wrap').data('value'), 'name': prefix+'subcategory_id', 'default': 'Semua Sub Keahlian'}
  $.ajax({
    'type': 'POST',
    'async': false,
    url: '<?php echo site_url("ajax/get_subcategory_ddl"); ?>',
    data: p,
    complete: function(xhr, status)
    {
      var ret = xhr.responseText;
      t.html(ret);
    }
  });
}
</script>