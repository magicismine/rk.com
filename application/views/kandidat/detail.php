<?php
$cu = $this->cu;
$ca = get_current_admin();
extract(get_object_vars($O->row)); 
echo $this->load->view($this->vpath.'/tpl/search_form', array(), TRUE);
?>
<div class="contens">
  <ul class="column fr-wrp ">
    <li class="col frw-lft">
      <ul class="column pinfo">
        <li class="col pinfo-pic-s">
          <img class="lazy" src ="<?php echo $O->get_photo('avatar');  ?>" data-original="<?php echo $O->get_photo('avatar');  ?>" width="50" height="50">
        </li>
        <li class="col pinfo-desc">
          <?php
          if($cu->role == "perusahaan" || $ca)
          {
            echo '<span class="pinfo-name">'.$O->get_nama_lengkap().'</span>';
            echo implode(", ", $O->get_all_keterampilan());
            echo '<br />';
          }
          ?>
          <strong class="forange">  Status : Pencari Kerja </strong>
        </li>
        <li class="col rght">
          <?php
          if($cu->role == "perusahaan")
		      {
            if(!$OP->check_cv($O->id, "tandai")):
				      echo anchor("perusahaan/cv/add/".$O->row->url_title, "Tandai", array("class" => "btn_blue"));
            endif;
            if(!$OP->check_cv($O->id, "undang")):
              echo anchor("perusahaan/cv/undang/".$O->row->url_title, "Undang", array("class" => "btn_Orange"));
            endif;
          }
		      ?>
        </li>
        <div class="clr"></div>
      </ul>
      <br>
      <?php echo print_status(); ?>
       <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
         <tr>
           <td width="25%">Jenis Kelamin</td>
           <td>: 
            <?php  
              $OMJK = new OMaster_jenis_kelamin($O->row->jenis_kelamin_id);
              echo $OMJK->row->nama;
              unset($OMJK);
            ?>
          </td>
           <td width="25%">Nama Panggilan </td>
           <td> : <?php echo $nama_panggilan; ?></td>
         </tr>
         <tr>
           <td>Tempat &amp; Tanggal Lahir </td>
           <td>: <?php echo $O->row->tempat_lahir; ?>/<?=parse_date($O->row->tanggal_lahir,"d F Y","ID","long")?></td>
           <td>Agama </td>
           <td>: 
            <?php  
              $OMA = new OMaster_agama($O->row->agama_id);
              echo $OMA->row->nama;
              unset($OMA);
            ?>
          </td>
         </tr>
         <?php if($cu->role == "perusahaan" || $ca): ?>
         <tr>
           <td class="noborder">&nbsp;</td>
           <td class="noborder">&nbsp;</td>
           <td>Nomer KTP <br></td>
           <td> : <?php echo $O->row->no_ktp?></td>
         </tr>        
         <tr>
           <td colspan="4" class="noborder">&nbsp;</td>
         </tr>
         <tr>
           <td><strong  class="pinfo-sp"> Alamat Asal </strong></td>
           <td>&nbsp;</td>
           <td><strong  class="pinfo-sp"> Alamat Sekarang </strong></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Jalan</td>
           <td>: <?php echo $asal_alamat ?></td>
           <td> Jalan </td>
           <td>: <?php echo ($skrg_alamat == "" ? "-" : $skrg_alamat) ;?></td>
         </tr>
         <tr>
           <td>RT/RW </td>
           <td>: <?php echo $asal_rt; ?>/<?php echo $asal_rw; ?></td>
           <td>RT/RW </td>
           <td>: <?php echo ($skrg_rt == "" ? "-" : $skrg_rt) ;?>/<?php echo ($skrg_rw == "" ? "-" : $skrg_rw) ;?></td>
         </tr>
         <tr>
           <td>Desa/Kelurahan </td>
           <td>: <?php echo $asal_kelurahan; ?></td>
           <td>Desa/Kelurahan </td>
           <td>: <?php echo ($skrg_kelurahan  == "" ? "-" : $skrg_kelurahan) ;?></td>
         </tr>
         <tr>
           <td>Kecamatan</td>
           <td>: <?php echo $asal_kecamatan ?></td>
           <td>Kecamatan </td>
           <td>: <?php echo ($skrg_kecamatan  == "" ? "-" : $skrg_kecamatan) ;?></td>
         </tr>
         <tr>
           <td>Kabupaten</td>
           <td> :
            <?php  
            if($asal_kabupaten_id != "")
            {
              $OMKab = new OMaster_lokasi_kabupaten($asal_kabupaten_id);
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?>
           </td>
           <td>Kabupaten </td>
           <td> : 
            <?php 
            if($skrg_kabupaten_id != "")
            {
              $OMKab = new OMaster_lokasi_kabupaten($skrg_kabupaten_id);
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?>
           </td>
         </tr>
         <tr>
           <td>Provinsi</td>
           <td>: 
            <?php 
            if($asal_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($asal_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?>
           </td>
           <td>Provinsi </td>
           <td>:
            <?php 
            if($skrg_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($skrg_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?>
           </td>
         </tr>

        <?php endif; ?>

        <?php if($cu->role == "perusahaan" || $ca): ?>
         
         <tr>
           <td>Telepon Rumah</td>
           <td>: <?php echo $asal_telepon; ?></td>
           <td>Telepon Rumah</td>
           <td>: <?php echo ($skrg_telepon  == "" ? "-" : $skrg_telepon) ;?></td>
         </tr>
         <tr class="noborder">
           <td  >Nomer HP</td>
           <td>: <?php echo $no_hp; ?> </td>
           <td>Nomer HP </td>
           <td>: <?php echo ($no_hp  == "" ? "-" : $no_hp) ;?></td>
         </tr>

        <?php endif; ?>
         
         <tr>
           <td colspan="4" class="noborder">&nbsp;</td>
         </tr>
       </table>
  <br>
  <?php echo $this->load->view($this->vpath.'/tpl/kesehatan', array('O'=>$O), TRUE); ?>
  
<?php if(($cu->role == "perusahaan" && $O->is_cv_lihat())  || $ca): ?>
  
  <?php echo $this->load->view($this->vpath.'/tpl/keluarga', array('O'=>$O), TRUE); ?>
  <?php echo $this->load->view($this->vpath.'/tpl/pendidikan', array('O'=>$O), TRUE); ?>
  <?php echo $this->load->view($this->vpath.'/tpl/pengalaman', array('O'=>$O), TRUE); ?>

<?php endif; ?>

  <?php echo $this->load->view($this->vpath.'/tpl/keterampilan', array('O'=>$O), TRUE); ?>
  
<?php if(($cu->role == "perusahaan" && $O->is_cv_lihat()) || $ca): ?>

  <?php echo $this->load->view($this->vpath.'/tpl/bahasa', array('O'=>$O), TRUE); ?>
  
<?php endif; ?>

<?php
if($cu->role != "perusahaan" && !$ca):
?>
  <div class="other" style="margin-bottom: 20px;">
  Anda Tidak Dapat Melihat Detail dari <strong class="forange"><?php echo (($cu->role == "perusahaan") ? $O->get_nama_lengkap() : "User ini."); ?></strong><br>
      Detail Hanya dapat di lihat oleh perusahaan yang telah <a href="<?php echo site_url('account/register/perusahaan'); ?>">mendaftar</a> , klik <a href="<?php echo site_url('account/login/perusahaan'); ?>">disini</a> untuk login.
  </div>
<?php
endif;
?>
  

  <li class="col frw-rgt">
    
    <?php echo $this->load->view($this->vpath.'/tpl/other', array('O'=>$O,'ca'=>$ca,'cu'=>$cu), TRUE); ?>

  </li>
  <div class="clr"></div>
</ul>
</div>