<style type="text/css">
.contens.to_pdf h2{
border: 2px solid black; 
text-transform: uppercase;
}
.contens.to_pdf .photo{
text-align: right;
}
.contens.to_pdf .photo img{
border: 1px solid black;
}
table.tblpinfo-dt th{ border: 2px solid black; text-transform: uppercase;}
.title-span{ background: lightBlue;font-weight:bold; text-align: center;}
table.td{ border: 1px solid black; }
</style>
<?php
$cu = $this->cu;
extract(get_object_vars($O->row)); 
?>
<div class="contens to_pdf">
      <h2 align="center">Curriculum Vitae</h2>      
      <div class="photo" align="right">
        <img src="<?php echo $O->get_photo();  ?>" width="113" height="151">
      </div>
      <br>
      <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
      <tr>
          <th colspan="4" class="title-span">PRIBADI</th>
      </tr>
      </table>
       <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
         <tr>
           <td width="25%">Nama </td>
           <td> : <?php echo $O->get_nama_lengkap(); ?></td>
           <td width="25%">Nama Panggilan </td>
           <td> : <?php echo $nama_panggilan; ?></td>
         </tr>
         <tr>
          <td width="25%">Jenis Kelamin</td>
           <td>: 
            <?php  
              $OMJK = new OMaster_jenis_kelamin($O->row->jenis_kelamin_id);
              echo $OMJK->row->nama;
              unset($OMJK);
            ?>
          </td>
           <td>Agama </td>
           <td>: 
            <?php  
              $OMA = new OMaster_agama($O->row->agama_id);
              echo $OMA->row->nama;
              unset($OMA);
            ?>
          </td>
         </tr>
         
         <tr>
           <td>Tempat &amp; Tanggal Lahir </td>
           <td>: <?php echo $O->row->tempat_lahir; ?>/<?=parse_date($O->row->tanggal_lahir,"d F Y","ID","long")?></td>
           <td>Nomer KTP</td>
           <td> : <?php echo $O->row->no_ktp?></td>
         </tr>        
         <tr>
           <td colspan="4" class="noborder">&nbsp;</td>
         </tr>
         <?php //if($cu->role == "perusahaan"): ?>         
         <tr>
           <td><strong  class="pinfo-sp"> Alamat Asal </strong></td>
           <td>&nbsp;</td>
           <td><strong  class="pinfo-sp"> Alamat Sekarang </strong></td>
           <td>&nbsp;</td>
         </tr>
         <tr>
           <td>Jalan</td>
           <td>: <?php echo $asal_alamat ?></td>
           <td> Jalan </td>
           <td>: <?php echo ($skrg_alamat == "" ? "-" : $skrg_alamat) ;?></td>
         </tr>
         <tr>
           <td>RT/RW </td>
           <td>: <?php echo $asal_rt; ?>/<?php echo $asal_rw; ?></td>
           <td>RT/RW </td>
           <td>: <?php echo ($skrg_rt == "" ? "-" : $skrg_rt) ;?>/<?php echo ($skrg_rw == "" ? "-" : $skrg_rw) ;?></td>
         </tr>
         <tr>
           <td>Desa/Kelurahan </td>
           <td>: <?php echo $asal_kelurahan; ?></td>
           <td>Desa/Kelurahan </td>
           <td>: <?php echo ($skrg_kelurahan  == "" ? "-" : $skrg_kelurahan) ;?></td>
         </tr>
         <tr>
           <td>Kecamatan</td>
           <td>: <?php echo $asal_kecamatan ?></td>
           <td>Kecamatan </td>
           <td>: <?php echo ($skrg_kecamatan  == "" ? "-" : $skrg_kecamatan) ;?></td>
         </tr>
         <tr>
           <td>Kabupaten</td>
           <td> :
            <?php  
            if($asal_kabupaten_id != "")
            {
              $OMKab = new OMaster_lokasi_kabupaten($asal_kabupaten_id);
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?>
           </td>
           <td>Kabupaten </td>
           <td> : 
            <?php 
            if($skrg_kabupaten_id != "")
            {
              $OMKab = new OMaster_lokasi_kabupaten($skrg_kabupaten_id);
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?>
           </td>
         </tr>
         <tr>
           <td>Provinsi</td>
           <td>: 
            <?php 
            if($asal_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($asal_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?>
           </td>
           <td>Provinsi </td>
           <td>:
            <?php 
            if($skrg_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($skrg_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?>
           </td>
         </tr>

        <?php //endif; ?>

        <?php //if($cu->role == "perusahaan"): ?>
         
         <tr>
           <td>Telepon Rumah</td>
           <td>: <?php echo $asal_telepon; ?></td>
           <td>Telepon Rumah</td>
           <td>: <?php echo ($skrg_telepon  == "" ? "-" : $skrg_telepon) ;?></td>
         </tr>
         <tr class="noborder">
           <td  >Nomer HP</td>
           <td>: <?php echo $no_hp; ?> </td>
           <td>Nomer HP </td>
           <td>: <?php echo ($no_hp  == "" ? "-" : $no_hp) ;?></td>
         </tr>

        <?php //endif; ?>
         
         <tr>
           <td colspan="4" class="noborder">&nbsp;</td>
         </tr>
       </table>
  <?php echo $this->load->view($this->vpath.'/tpl/kesehatan', array('O'=>$O), TRUE); ?>
  
  <?php //if($cu->role == "perusahaan" && $O->is_cv_lihat()): ?>
  
  <?php echo $this->load->view($this->vpath.'/tpl/keluarga', array('O'=>$O), TRUE); ?>
  <?php echo $this->load->view($this->vpath.'/tpl/pendidikan', array('O'=>$O), TRUE); ?>
  <?php echo $this->load->view($this->vpath.'/tpl/pengalaman', array('O'=>$O), TRUE); ?>

<?php //endif; ?>

  <?php echo $this->load->view($this->vpath.'/tpl/keterampilan', array('O'=>$O), TRUE); ?>
  
<?php //if($cu->role == "perusahaan" && $O->is_cv_lihat()): ?>

  <?php echo $this->load->view($this->vpath.'/tpl/bahasa', array('O'=>$O), TRUE); ?>
  
<?php // endif; ?>

<?php
//if($cu->role != "perusahaan"):  
?>
<?php
//endif;
?>
  <p align="right"><a href="<?=site_url();?>">rumahkandidat.com</a></p>
  <div class="clr"></div>
</div>
<?php /* ?>
<style type="text/css" media="screen">
@import '<?php echo base_url('_assets/css/style.css') ?>';
</style>
<?php */ ?>
