<?php
echo $a;
if(sizeof($_GET) > 0)
{
	extract($_GET);
}
$month_lang_arr = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Aug","Sep","Oct","Nov","Des");
//$limit = intval(date('n'));
$cur_month = intval(date('n'));
$limit = 12;
$tahun_arr = NULL;
if(empty($tahun)) $tahun = date("Y");
$tahun_akhir = date("Y");
if(empty($tahun_awal)) $tahun_awal = $tahun_akhir-2;
for ($i=$tahun_akhir; $i >= $tahun_awal; $i--)
{ 
	$tahun_arr[$i] = $i;
}
?>
<div class="advSearchBox"></div>
<br>

<h2><strong class="f18 forange">KPI RUMAH KANDIDAT TAHUN <?php echo $tahun; ?></strong></h2>
<?php
if(sizeof($_POST) > 0):
	$filter_result_arr = NULL;
	$inc_arr = array('lokasi_negara_id', 'lokasi_propinsi_id', 'lokasi_kabupaten_id', 'lokasi_kawasan_id', 'tahun');
	foreach ($_POST as $key => $value)
	{
		if(empty($value)) continue;
		if(in_array($key, $inc_arr))
		{
			if(substr($key, 0, 7) == "lokasi_")
			{
				if(stristr($key, "negara")) $OTmp = new OMaster_lokasi_negara(intval($value), "id");
				if(stristr($key, "propinsi")) $OTmp = new OMaster_lokasi_propinsi(intval($value), "id");
				if(stristr($key, "kabupaten")) $OTmp = new OMaster_lokasi_kabupaten(intval($value), "id");
				if(stristr($key, "kawasan")) $OTmp = new OMaster_lokasi_kawasan(intval($value), "id");
				if( stristr($key, "negara") == true ||
					stristr($key, "propinsi") == true ||
					stristr($key, "kabupaten") == true ||
					stristr($key, "kawasan") == true
					)
				{
					$value = $OTmp->get_nama();
				}
			}
			$filter_result_arr[] = '<a>'.$value.'</a>';
		}
	}
?>
<strong class=" ">
	Filter <span class=" f18 forange">&raquo;</span> <?php echo implode(' <span class=" f18 forange">&raquo;</span> ', $filter_result_arr); ?>
</strong><br>
<?php
endif;
?>

<table id="performa-table" class="list-table" width="100%">
	<thead>
		<tr rowspan="2">
			<td rowspan="2" align="center" valign="middle" style="vertical-align:middle;">KPI</td>
			<td colspan="<?php echo $limit; ?>" align="center"><?php echo $tahun; ?></td>
		</tr>
		<tr>
			<?php for($i=0; $i<$limit; $i++): ?>
				<td align="center" data-value="<?php echo $month_lang_arr[$i]."".substr($tahun,-2,2); ?>">
					<?php echo strtoupper($month_lang_arr[$i]); ?>
				</td>
			<?php endfor; ?>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td data-value="Angker PEMULA" align="right" valign="middle"><strong>Jumlah angkatan kerja pemula</strong></td>
			<?php
			for($i=1;$i<=$limit;$i++):
				$performa_1 = 0;
				for($j=1;$j<=$i;$j++):
					$performa_1 = $performa_1 + intval($performas[1][$j]);
					if($j > $cur_month) $performa_1 = 0;
				endfor;
			?>
			<td align="center"><?php echo intval($performa_1); ?></td>
			<?php
			endfor;
			?>
		</tr>
		<tr>
			<td data-value="Angker SETELAH PHK" align="right" valign="middle"><strong>Jumlah angkatan kerja akibat PHK</strong></td>
			<?php
			for($i=1;$i<=$limit;$i++):
				$performa_2 = 0;
				for($j=1;$j<=$i;$j++):
					$performa_2 = $performa_2 + intval($performas[2][$j]);
					if($j > $cur_month) $performa_2 = 0;
				endfor;
			?>
			<td align="center"><?php echo intval($performa_2); ?></td>
			<?php
			endfor;
			?>
		</tr>
		<tr>
			<td data-value="Jumlah PERUSAHAAN" align="right" valign="middle"><strong>Jumlah perusahaan yang membuka kesempatan kerja</strong></td>
			<?php
			for($i=1;$i<=$limit;$i++):
				$performa_3 = 0;
				for($j=1;$j<=$i;$j++):
					$performa_3 = $performa_3 + intval($performas[3][$j]);
					if($j > $cur_month) $performa_3 = 0;
				endfor;
			?>
			<td align="center"><?php echo intval($performa_3); ?></td>
			<?php
			endfor;
			?>
		</tr>
		<tr>
			<td data-value="Jumlah POSISI" align="right" valign="middle"><strong>Jumlah posisi pekerjaan yang dibuka</strong></td>
			<?php
			for($i=1;$i<=$limit;$i++):
				$performa_4 = 0;
				for($j=1;$j<=$i;$j++):
					$performa_4 = $performa_4 + intval($performas[4][$j]);
					if($j > $cur_month) $performa_4 = 0;
				endfor;
			?>
			<td align="center"><?php echo intval($performa_4); ?></td>
			<?php
			endfor;
			?>
		</tr>
		<tr>
			<td data-value="KEBUTUHAN Naker" align="right" valign="middle"><strong>Jumlah tenaga kerja yang dibutuhkan</strong></td>
			<?php
			for($i=1;$i<=$limit;$i++):
				$performa_5 = 0;
				for($j=1;$j<=$i;$j++):
					$performa_5 = $performa_5 + intval($performas[5][$j]);
					if($j > $cur_month) $performa_5 = 0;
				endfor;
			?>
			<td align="center"><?php echo intval($performa_5); ?></td>
			<?php
			endfor;
			?>
		</tr>
		<tr>
			<td data-value="PEMENUHAN Naker" align="right" valign="middle"><strong>Jumlah pemenuhan tenaga kerja</strong></td>
			<?php
			for($i=1;$i<=$limit;$i++):
				$performa_6 = 0;
				for($j=1;$j<=$i;$j++):
					$performa_6 = $performa_6 + intval($performas[6][$j]);
					if($j > $cur_month) $performa_6 = 0;
				endfor;
			?>
			<td align="center"><?php echo intval($performa_6); ?></td>
			<?php
			endfor;
			?>
		</tr>
	</tbody>
</table>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript" src="<?php echo base_url('_assets/js/performa.js'); ?>"></script>