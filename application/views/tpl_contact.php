<?php extract($_POST); ?>
<p>Berikut detil pesan yang diterima melalui form Kontak di <?php echo get_setting('sitename');  ?>.</p>
<table>
	<tbody>
		<tr>
			<td>Nama</td><td><?php echo $name; ?></td>
		</tr>
		<tr>
			<td>E-mail</td><td><?php echo $email; ?></td>
		</tr>
		<tr>
			<td>Subyek</td><td><?php echo $subject; ?></td>
		</tr>
		<tr style="vertical-align:top;">
			<td>Pesan</td><td><?php echo nl2br($message); ?></td>
		</tr>
	</tbody>
</table>