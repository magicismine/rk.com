<?php
$table_name = "user_statistic";
$ip			= $this->input->ip_address();
$tanggal	= date("Y-m-d");
$waktu		= time();
$bln		= date("m");
$tgl		= date("j");
$tglk		= intval($tgl)-1;
$tglk		= str_pad($tglk, 2, "0", STR_PAD_LEFT);

$s = $this->db->query("SELECT * FROM {$table_name} WHERE ip='$ip' AND tanggal='$tanggal'");

if(emptyres($s))
{
	$this->db->query("INSERT INTO {$table_name}(ip, tanggal, hits, online) VALUES('$ip','$tanggal','1','$waktu')");
} 
else{
	$this->db->query("UPDATE {$table_name} SET hits=hits+1, online='$waktu' WHERE ip='$ip' AND tanggal='$tanggal'");
}

$kemarin		= $this->db->where( array("tanggal" => date("Y-m-d", strtotime("-1 day")) ) )
							->group_by("ip")
							->select('SQL_CALC_FOUND_ROWS *', FALSE)
							->get($table_name);
$kemarin_total	= get_db_total_rows();

$bulan			= $this->db->like( array("tanggal" => date("Y-m")) )
							->group_by("ip")
							->select('SQL_CALC_FOUND_ROWS *', FALSE)
							->get($table_name);
$bulan_total	= get_db_total_rows();

$tahun			= $this->db->like( array("tanggal" => date("Y")) )
							->group_by("ip")
							->select('SQL_CALC_FOUND_ROWS *', FALSE)
							->get($table_name);
$tahun_total	= get_db_total_rows();

$pengunjung     = $this->db->where( array("tanggal " => $tanggal) )
							->group_by("ip")
							->select('SQL_CALC_FOUND_ROWS *', FALSE)
							->get($table_name);
$pengunjung_total = get_db_total_rows();

$all 		    = $this->db->group_by("ip")
							->select('SQL_CALC_FOUND_ROWS *', FALSE)
							->get($table_name);
$all_total 		= get_db_total_rows();

//$hits             = $this->db->query("SELECT SUM(hits) as hitstoday FROM user_statictic WHERE tanggal='$tanggal' GROUP BY tanggal");
//$hits_today		  = $hits->row()->hitstoday;

//$hits_total       = mysql_result($this->db->query("SELECT SUM(hits) FROM user_statictic"), 0); 

$bataswaktu       = time() - 300;
$online			  = $this->db->where( array("online >" => $bataswaktu) )
								->get($table_name);
$online_total	  = get_db_total_rows();
?>

<div class="statistik-box">
<h4 class="stat-title">Statistik</h4>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td width="36%">Perusahaan</td>
    <td width="64%">:
      <?php
      $perusahaans = OUser::get_list(0,0,"","role='perusahaan' AND status='aktif'");
      echo get_db_total_rows();
      ?>
    </td>
  </tr>
  <tr>
    <td>Kandidat</td>
    <td>:
      <?php
      $kandidats = OUser::get_list(0,0,"","role='kandidat' AND status='aktif'");
      echo get_db_total_rows();
      ?>
    </td>
  </tr>
</table>
<hr class="spacer">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Kunjungan</td>
    <td>Online</td>
    <td align="right">:</td>
    <td align="right"><?php echo $online_total; ?></td>
  </tr>
  <tr>
    <td width="34%">&nbsp;</td>
    <td width="34%">Hari Ini</td>
    <td width="5%" align="right">:</td>
    <td width="27%" align="right"><?php echo $pengunjung_total; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Kemarin</td>
    <td align="right">:</td>
    <td align="right"><?php echo $kemarin_total; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Bulan ini</td>
    <td align="right">:</td>
    <td align="right"><?php echo $bulan_total; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Tahun ini</td>
    <td align="right">:</td>
    <td align="right"><?php echo $tahun_total; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Total</td>
    <td align="right">:</td>
    <td align="right"><?php echo $all_total; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" align="right"><hr class="spacer2"></td>
  </tr>
</table>
</div>
<br>