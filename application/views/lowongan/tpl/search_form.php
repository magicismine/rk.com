<?php 
$list_lowongan = OLowongan::get_list(0,0,"id DESC");
$total_lowongan = get_db_total_rows();
extract($_GET);
?>
<div class="advSearchBox">
  <ul class="column">
    <li class="col"><span class="title-advSearchBox">Cari Pekerjaan</span></li>
    <li class="col JobCount"><strong>Tersedia <?php echo $total_lowongan;?> Pekerjaan</strong></li>
    <div class="clr"></div>
  </ul>
  <form action="<?php echo site_url('search/lowongan'); ?>" method="get" class="MainSearch" >
    <ul class="column">
      <li class="col"  >
        <input type="text" style="width:590px" name="keyword" placeholder="Kata Kunci" value="<?php echo $keyword; ?>">
      </li>
      <li class="col"  >
        <?php echo OMaster_kategori::drop_down_select('kategori_id',$kategori_id,'style="width: 200px;"',"Semua Kategori"); ?>
      </li>
      <li class="col">
        <input type="submit" value="Cari" class="btn_blue">
      </li>
      <div class="clr"></div>
    </ul>
   <!-- <ul class="column detailSearch  dsopen"> default open -->
    <ul class="column detailSearch <?php if(sizeof($_GET) > 0) echo "dsopen"; ?>">
      <li class="col"  >
        <?php echo OMaster_bidang_usaha::drop_down_select('bidang_usaha_id',$bidang_usaha_id,'style="width: 280px;"',"Semua Bidang Usaha"); ?>
      </li>
      <li class="col"  >
        <?php echo OMaster_pendidikan::drop_down_select('pendidikan_id',$pendidikan_id,'style="width: 200px;"',"Semua Pendidikan"); ?>
      </li>
      <li class="col"  >
        <?php echo OMaster_level_karir::drop_down_select('level_karir_id[]',$level_karir_id,'style="width: 160px;"',"Semua Level Karir"); ?>
      </li>
      <li class="col"  >
        <?php echo OMaster_waktu_kerja::drop_down_select('waktu_kerja_id',$waktu_kerja_id,'style="width: 140px;"',"Semua Waktu"); ?>
      </li>
      
      <div class="clr"></div>
       <li class="col"  >
        <?php echo OMaster_lokasi_negara::drop_down_select('lokasi_negara_id',$lokasi_negara_id,'style="width: 200px;"',"Semua Negara"); ?>
      </li>
      <li class="col"  >
        <span id="lokasi_propinsi_wrap" data-value="<?php echo $lokasi_propinsi_id; ?>" data-default="Semua Propinsi" data-option="style='width: 200px'">
        <?php echo OMaster_lokasi_propinsi::drop_down_select('lokasi_propinsi_id',$lokasi_propinsi_id,'style="width: 200px;"',"Semua Propinsi"); ?>
        </span>
      </li>
      <li class="col"  >
        <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>" data-default="Semua Lokasi Kota / Kabupaten" data-option="style='width: 240px'">
          <?php echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id,'style="width: 240px;"',"Semua Lokasi Kota / Kabupaten"); ?>
        </span>
      </li>
      <li class="col"  >
        <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>" data-default="Semua Kawasan" data-option="style='width: 140px'">
          <?php echo OMaster_lokasi_kawasan::drop_down_select("lokasi_kawasan_id",$lokasi_kawasan_id,'style="width: 140px;"',"Semua Kawasan"); ?>
        </span>
      </li>
      <div class="clr"></div>
    </ul>
    <span class="tg-detail ">Pencarian Detail</span>
  </form>
</div>
<br>
<br>