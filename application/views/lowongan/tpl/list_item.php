<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
<thead>
	<tr>
		<td width="8%" align="left" valign="middle">Expired</td>
		<td width="25%" valign="middle">Deskripsi</td>
		<td>Waktu Kerja</td>
		<td>Level Pekerjaan</td>
		<td align="left" valign="middle">Bidang Usaha</td>
		<td align="left" valign="middle">Lokasi</td>
		<?php /* ?>
		<td width="7%" align="center" valign="middle">Expired</td>
		<td width="59%" valign="middle">Deskripsi</td>
		<td width="16%" align="left" valign="middle">Bidang Usaha</td>
		<td width="18%" align="center" valign="middle">Lokasi</td>
		<?php */ ?>
	</tr>
</thead>
<tbody>
<?php 
if(!$list)
{
	?>
	<tr>
		<td colspan="6" align="center" valign="middle">Tidak ada lowongan ditemukan.</td>
	</tr>
	<?php
}
else
{
	$O = new OLowongan();
	//$current_url = current_url();
	$current_url = "search/lowongan";
	foreach($list as $r):
		$O->setup($r);
		$OP = new OPerusahaan($O->row->perusahaan_id);
		// waktu_kerja
		$OTmp = new OMaster_waktu_kerja($O->row->waktu_kerja_id);
		$waktu_kerja = $OTmp->get_nama();
		$waktu_kerja_id = $OTmp->id;
		// level_karir
		$level_karir_arr = $O->get_level_karir_arr();
		$level_karirs = NULL;
		foreach($level_karir_arr as $key => $val)
		{
			$OTmp = new OMaster_level_karir($key);
			$level_karirs[] = anchor($current_url."?level_karir_id[]=".$key, $OTmp->get_nama());
		}
		// kategori
		$OTmp = new OMaster_kategori($O->row->kategori_id);
		$kategori = $OTmp->get_nama();
		$kategori_id = $OTmp->id;
		// bidang_usaha
		$OTmp = new OMaster_bidang_usaha($O->row->bidang_usaha_id);
		$bidang_usaha = $OTmp->get_nama();
		$bidang_usaha_id = $OTmp->id;

		$kategori_bidang_usaha = NULL;
		if(!empty($kategori))
		{
			$kategori_bidang_usaha[] = anchor($current_url."?kategori_id={$kategori_id}", $kategori);
		}
		if(!empty($bidang_usaha))
		{
			$kategori_bidang_usaha[] = anchor($current_url."?bidang_usaha_id={$bidang_usaha_id}", $bidang_usaha);
		}
		//$OMLK = new OMaster_lokasi_kabupaten($O->row->lokasi_kabupaten_id);
		// lokasi
		$lokasi_arr = $O->get_current_complete_lokasi_name();
		$lokasi_ids = array("lokasi_negara_id","lokasi_propinsi_id","lokasi_kabupaten_id","lokasi_kawasan_id");
		$lokasi_link_names = "";
		$i = 0;
		$exclude_arr = array_merge($lokasi_ids, array("page"));
		//var_dump($exclude_arr);
		foreach($lokasi_arr as $id => $val):
			// limit max 3
			if($i>=4) break;
			$tmp_lokasi_id[$i] = $id;
			if(!empty($id) && !empty($val))
			{
				$add_params = NULL;
				for ($j=$i; $j >= 0; $j--)
				{ 
					$add_params[] = $lokasi_ids[$j]."=".$tmp_lokasi_id[$j];
				}
				$add_params = array_reverse($add_params);
				//var_dump($add_params);
				//$params = implode("&", $add_params)."&".get_params($_GET, $exclude_arr);
				$params = implode("&", $add_params);
				$lokasi_link_names[] =  anchor($current_url."?".$params, $val);
			}
			$i++;
		endforeach;
		?>
		<tr>
		<td align="left" valign="middle"><?php echo parse_date($O->row->dt_expired,"d M Y"); ?></td>
		<td valign="middle">
			<?php echo anchor($O->get_link(), $O->row->posisi) ?><br />
			<strong><?php echo anchor($OP->get_link(), $OP->get_nama()) ?></strong>
		</td>
		<td align="left" valign="middle">
			<?php echo (!empty($waktu_kerja) ? anchor($current_url."?waktu_kerja_id={$waktu_kerja_id}", $waktu_kerja) : ""); ?>
		</td>
		<td align="left" valign="middle">
			<?php //echo (!empty($level_karir) ? anchor($current_url."?level_karir_id={$level_karir_id}", $level_karir) : ""); ?>
			<?php echo implode(", ", $level_karirs); ?>
		</td>
		<td align="left" valign="middle">
			<?php echo implode(' <span class=" f18 forange">&raquo;</span> ', $kategori_bidang_usaha); ?>
		<?php
			//echo $OTmp->row->nama;
		?>
		</td>
		<td align="left" valign="middle">
			<?php
			//echo $OMLK->row->nama;
			echo implode(' <span class=" f18 forange">&raquo;</span> ', $lokasi_link_names);
			?>
		</td>
		</tr>
		<?php
		unset($OP,$OTmp,$OMLK);
	endforeach;
	unset($O);
}
?>
</tbody>
</table>

<?php
if($pagination):
?>
<div id="paging_button" align="center">
<?php echo $pagination; ?>
</div>
<?php
endif;
?>