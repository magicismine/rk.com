<?php
extract($_GET);
echo $this->load->view($this->vpath.'/tpl/search_form',NULL,TRUE); 
?>

<strong class="f18 forange">Pencarian Lowongan</strong><br>

<strong class=" ">
  <?php
  $filter_set_arr = NULL;
  if($kategori_id != "")
  { 
    $OTmp = new OMaster_kategori($kategori_id);
    $filter_set_arr[] = 'Kategori '.anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if($bidang_usaha_id != "")
  {
    $OTmp = new OMaster_bidang_usaha($bidang_usaha_id);
    $filter_set_arr[] = 'Bidang Usaha '.anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if($pendidikan_id != "")
  {
    $OTmp = new OMaster_pendidikan($pendidikan_id);
    $filter_set_arr[] = 'Pendidikan '.anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if(count($level_karir_id) > 0)
  {
    $OTmp = new OMaster_level_karir($level_karir_id[0]);
    if($OTmp->row) $filter_set_arr[] = 'Level Karir '.anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if($waktu_kerja_id != "")
  {
    $OTmp = new OMaster_waktu_kerja($waktu_kerja_id);
    $filter_set_arr[] = 'Waktu Kerja '.anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if($lokasi_propinsi_id != "")
  {
    $OTmp = new OMaster_lokasi_propinsi($lokasi_propinsi_id);
    $filter_set_lokasi_arr[] = anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if($lokasi_kabupaten_id != "")
  {
    $OTmp = new OMaster_lokasi_kabupaten($lokasi_kabupaten_id);
    $filter_set_lokasi_arr[] = anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if($lokasi_kawasan_id != "")
  {
    $OTmp = new OMaster_lokasi_kawasan($lokasi_kawasan_id);
    $filter_set_lokasi_arr[] = anchor($OTmp->get_link(), $OTmp->get_nama());
    unset($OTmp);
  }
  if(count($filter_set_lokasi_arr)>0)
  {
    $filter_set_arr[] = 'Lokasi '.implode(', ', $filter_set_lokasi_arr);
  }
  echo implode(' <span class=" f18 forange">&raquo;</span> ', $filter_set_arr)
  ?>
</strong> 

<div align=""> Total <strong><?php echo $total; ?></strong> Daftar Lowongan  </div> 
<br>

<div class="contens">
  <?php echo $this->load->view($this->vpath.'/tpl/list_item', array('list'=>$list,'pagination'=>$pagination), TRUE); ?>
</div>