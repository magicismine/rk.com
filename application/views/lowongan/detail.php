<?php
  echo $this->load->view($this->vpath.'/tpl/search_form',$data);
?>
<div class="contens">
  <ul class="column fr-wrp ">
    <li class="col frw-lft">
       <ul class="column pinfo">
        <li class="col pinfo-pic-s"><img src="<?php echo $OP->get_photo('square'); ?>" width="50" alt="Logo" > </li>
        <li class="col pinfo-desc"><span class="pinfo-name"><?php echo $OP->row->nama; ?></span><?php echo $OP->row->motto; ?></li>
        <div class="clr"></div>
      </ul><br>
      <?php echo print_status(); ?>
      <div title="Deskripsi Perusahaan">
        <!-- <p>We are one of the   biggest investment bank, subsidiary of the   biggest state-owned bank in   Indonesia, currently looking for a future   leader candidate with strong   leadership potential and passion to   always think out of the box. We   challenge you to join our Mandiri   Sekuritas Management Trainee Program   as</p>
        <br>
        <p>Management Trainee<br>
          in which through comprehensive learning program will be   developed to be   our future professional leaders. Graduates of the   program will be   further challenged to build a stronger capital market   industry.</p> -->
          <?php echo closetags($OP->row->deskripsi); ?>
      </div>
      <br>
      <div class=" boxWhite textContent">
        <div style="text-align:center"> <strong class="f30 forange"> <?php echo $O->row->posisi; ?></strong></div>
        <hr class="spacer" />
        <p><strong>Deskripsi dan Detail Persyaratan:</strong></p>
        <?php  
        /*$kualifikasi = explode("\n",$O->row->kualifikasi);
        $total_qualification = count($kualifikasi);
        for($i=0;$i < $total_qualification;$i++)
        {
          echo "<li>".$kualifikasi[$i]."</li>";
        }*/
        echo filter_html_tags($O->row->deskripsi);
        ?>
        <?php 
          $OKandidat = new OKandidat($this->cu->id,"user_id");
          $check_simpan = $OKandidat->check_lowongan_simpan($O->row->id);
          $check_lamar = $OKandidat->check_lowongan_lamar($O->row->id);
          if($this->cu->role == "kandidat" && !$O->is_expired() && ($check_simpan == FALSE || $check_lamar == FALSE)):
        ?>
        <br>
        <div class="grayLight" align="center"> 
          <?php
          $actions = NULL;
          if($check_simpan == FALSE)
          {
            $actions[] = anchor("kandidat/lowongan_disimpan/simpan/{$O->row->url_title}", 'Simpan', array('class' => 'btn_Orange'));
          } 
          if($check_lamar == FALSE)
          {
            $actions[] = anchor("kandidat/lowongan_dilamar/lamar/{$O->row->url_title}", 'KIRIM LAMARAN', array('class' => 'btn_Orange'));
          }
          echo implode(" ", $actions);
          ?>
        </div>
        <?php endif; ?>
        <br>
        <hr class="spacer">
        <div class="lowongan-keterangan"  >
          <ul>
            <li><strong>Kategori</strong> : <?php $OMKat = new OMaster_kategori($O->row->kategori_id);?><a href="<?php echo $OMKat->get_link(); ?>"><?php echo $OMKat->row->nama; unset($OMKat); ?></a></li>
            <li><strong>Bidang Usaha</strong> : <?php $OMBU = new OMaster_bidang_usaha($O->row->bidang_usaha_id);?><a href="<?php echo $OMBU->get_link(); ?>"><?php echo $OMBU->row->nama; unset($OMBU);?></a></li>
            <li><strong>Pendidikan dan Pengalaman</strong> : <?php $OMKat = new OMaster_pendidikan($O->row->pendidikan_id);echo $OMKat->row->nama; unset($OMKat); ?> - <?php echo $O->row->tahun_pengalaman_kerja; ?> tahun </li>
            <li><strong>Batas Waktu Melamar</strong> : <?php echo parse_date($O->row->dt_expired,"d M Y") ?></li>
          </ul>
        </div>
      </div>
      <div class="social-likes" style="margin-top: 10px; text-align: right;width: 645px;">
        <div class="facebook" title="Share link on Facebook">Facebook</div>
        <div class="twitter" title="Share link on Twitter">Twitter</div>
        <div class="plusone" title="Share link on Google+">Google+</div>
        <div class="pinterest" title="Share image on Pinterest" data-media="">Pinterest</div>
      </div>
    </li>
    <li class="col frw-rgt">
    <?php echo $this->load->view($this->vpath.'/tpl/sejenis_box', array('O'=>$O,'OP' => $OP), true); ?>
    <?php echo $this->load->view($this->vpath.'/tpl/other_box', array('O'=>$O,'OP' => $OP), true); ?>
    </li>
    <div class="clr"></div>
  </ul>
</div>