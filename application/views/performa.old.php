<?php
$month_lang_arr = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agst","Sept","Okt","Nop","Des");
$limit = intval(date('n'));
?><h2><strong class="f18 forange">Performa</strong></h2><br>
<table id="performa-table" class="table table-bordered table-striped table-hover" width="100%">
	<thead>
		<tr rowspan="2">
			<th rowspan="2">KPI</th>
			<th colspan="<?php echo $limit; ?>"><?php echo date('Y'); ?></th>
		</tr>
		<tr>
			<?php for($i=0; $i<$limit; $i++): ?>
				<th><?php echo $month_lang_arr[$i]; ?></th>
			<?php endfor; ?>
			<?php /* ?>
			<th>Jan</th>
			<th>Feb</th>
			<th>Mar</th>
			<th>Apr</th>
			<th>Mei</th>
			<th>Jun</th>
			<th>Jul</th>
			<th>Agu</th>
			<th>Sep</th>
			<th>Okt</th>
			<th>Nop</th>
			<th>Des</th>
			<?php */ ?>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td data-value="Angker PEMULA">Jumlah angkatan kerja pemula</td>
			<?php for($i=1;$i<=$limit;$i++): ?>
			<td class="text-center"><?php echo rand(1,100); ?></td>
			<?php endfor; ?>
		</tr>
		<tr>
			<td data-value="Angker SETELAH PHK">Jumlah angkatan kerja akibat PHK</td>
			<?php for($i=1;$i<=$limit;$i++): ?>
			<td class="text-center"><?php echo rand(1,100); ?></td>
			<?php endfor; ?>
		</tr>
		<tr>
			<td data-value="Jumlah PERUSAHAAN">Jumlah perusahaan yang membuka kesempatan kerja</td>
			<?php for($i=1;$i<=$limit;$i++): ?>
			<td class="text-center"><?php echo rand(1,100); ?></td>
			<?php endfor; ?>
		</tr>
		<tr>
			<td data-value="Jumlah POSISI">Jumlah posisi pekerjaan yang dibuka</td>
			<?php for($i=1;$i<=$limit;$i++): ?>
			<td class="text-center"><?php echo rand(1,100); ?></td>
			<?php endfor; ?>
		</tr>
		<tr>
			<td data-value="KEBUTUHAN Naker">Jumlah tenaga kerja yang dibutuhkan</td>
			<?php for($i=1;$i<=$limit;$i++): ?>
			<td class="text-center"><?php echo rand(1,100); ?></td>
			<?php endfor; ?>
		</tr>
		<tr>
			<td data-value="PEMENUHAN Naker">Jumlah pemenuhan tenaga kerja</td>
			<?php for($i=1;$i<=$limit;$i++): ?>
			<td class="text-center"><?php echo rand(1,100); ?></td>
			<?php endfor; ?>
		</tr>
	</tbody>
</table>
<div id="performa-chart" style="width:100%;height:600px;"></div>

<script type="text/javascript">
	
	var values = [];
	var thead_tr = $('#performa-table thead tr');
	var tbody_tr = $('#performa-table tbody tr');
	values[0] = [$('#performa-table thead tr:eq(0) > th:eq(1)').html()];

	//*
	tbody_tr.each(function(){
		values[0].push( $(this).find('td:eq(0)').data('value') );
	});
	var i=1;
	$('#performa-table thead tr:eq(1) > th').each(function(){
		values[i] = [$(this).html()];
		i++;
	});
	var i=1;
	tbody_tr.each(function(){
		var j=0;
		$(this).find('td').each(function(){
			if(j>0)
			{
				values[j].push( parseInt($(this).html()) );
			}
			j++;
		});
		i++;
	});
	//*/
	/*
	$('#performa-table thead tr:eq(1) th').each(function(){
		values[0].push( $(this).html() );
	});
	
	var i=1;
	tbody_tr.each(function(){
		var j=0;
		values[i] = [];
		$(this).find('td').each(function(){
			if(j > 0)
			{
				values[i].push( parseInt($(this).html()) );
			}
			else values[i].push( $(this).data('value') );
			j++;
		});
		i++;
	});
	//*/
	console.log(values);
</script>
<?php //* ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		/*
		var data = google.visualization.arrayToDataTable([
			['Year', 'Sales', 'Expenses'],
			['2004',  1000,      400],
			['2005',  1170,      460],
			['2006',  660,       1$limit0],
			['2007',  10$limit0,      540]
        ]);
		//*/
		var data = google.visualization.arrayToDataTable(values);

		var options = {
			title: 'Performa Rumah Kandidat'
		};

		var chart = new google.visualization.LineChart(document.getElementById('performa-chart'));
		chart.draw(data, options);
	}
</script>
<?php //*/ ?>