<?php
$list_artikel = OArtikel::get_list(0,2,"id DESC");
if($list_artikel):
?><h2 class="f30 artikelBigTitle "><strong>Artikel</strong></h2>
<ul class="column thumbDec-list">
<div class="clr"></div>
<?php  
foreach($list_artikel as $artikel)
{
	$OA->setup($artikel);
?>
<li class="col thumb-list"><a href="<?php echo $OA->get_link() ?>"><img src="<?php echo $OA->get_photo('square');?>" alt="artikel" title="artikel" width="93" /></a></li>
<li class="col dec-list">
	<div class="dec-title"><a href="<?php echo $OA->get_link() ?>"><?php echo $OA->row->nama; ?></a></div>
	<?php echo closetags(word_limiter($OA->row->isi,30)); ?>
</li>
<div class="clr"></div>
<?php  
}
?>
</ul>
<br>
<?php endif; ?>