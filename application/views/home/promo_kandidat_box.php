<div class="promoKandidatBox boxLightOrange">
<h2 class="f30 forange"> Promo Kandidat </h2>
<br>
<hr class="spacer" style="margin-top:-5px;"/>
<?php //* ?>
<p>Alumni Karyawan di beberapa perusahaan siap bergabung dengan perusahaan anggota rumahkandidat, data selengkapnya :</p>
<?php //*/ ?>
<?php //* ?>
<ul class="column thumbDec-list">
	<div class="clr"></div>
	<?php
	$OTmp = new OPerusahaan_promo;
	$perusahaan_promos = $OTmp->get_list(0,2,'id DESC','active=1');
	$perusahaan_promos_total = get_db_total_rows();
	if($perusahaan_promos):
		foreach ($perusahaan_promos as $r)
		{
			$OTmp->setup($r);
			$OPTmp = new OPerusahaan($r->perusahaan_id);
			?>
	<li class="col thumb-list">
		<img src="<?php echo $OPTmp->get_photo('square'); ?>" alt="artikel" title="artikel"/>
	</li>
	<li class="col dec-list">
		<div class="dec-title"><?php echo $r->name; ?></div>
		<?php echo word_limiter(strip_tags($r->description), 20); ?>
		<?php echo "<br />".anchor($OTmp->get_link(), "Selengkapnya &raquo;") ?>
	</li>
	<div class="clr"></div>
			<?php
			unset($OPTmp);
		}
	endif;
	?>
	<?php /* ?>
	<li class="col thumb-list">
		<img src="<?php echo base_url('_assets/img/dummy2.jpg'); ?>" alt="artikel" title="artikel"/>
	</li>
	<li class="col dec-list">
		<div class="dec-title"> Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut   odit aut fugit, sed quia </div>
		Sed ut perspiciatis unde omnis iste natus error sit voluptatem   accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab   illo inventore veritatis et quasi architecto beatae vitae dicta sunt   explicabo.
	</li>
	<div class="clr"></div>
	<?php */ ?>
</ul>
<?php //*/ ?>
<?php //if($perusahaan_promos_total > 2): ?>
<p style="text-align:right;"><?php echo anchor('promo', 'more', '');; ?></p>
<?php //endif; ?>
</div>
<br>