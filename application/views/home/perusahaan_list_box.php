<?php
$perpage = 0;
$list = OPerusahaan::get_list(0,$perpage);
$total = get_db_total_rows();
if($list):
?>
<div class="wrp-companylist grayBox">
<h3 class="f30 listPerusahaanBigTitle "><strong>List Perusahaan</strong></h3>
<div id="ajax-listperusahaan">
  <!-- List Perusahaan -->
</div>
<div id="paging_button" align="center">
  <ul>
    <?php /*if($total_page > 1): ?>
      <li data-link="<?php echo site_url('home/ajax/list_perusahaan');?>">PREV</li>
    <?php endif;*/ ?>
    <?php
    $i = 0; $limit = 15; $total_page = ceil($total/$limit);
    $active = ' class="active"';
    while($i<$total_page):
      $page = $i*$limit;
      ?>
      <li data-link="<?php echo site_url('home/ajax/list_perusahaan/'.$page);?>"<?php if($i==0) echo $active; ?>>
        <?php echo ($i+1); ?>
      </li>
      <?php
      $i++;
    endwhile;
    /*if($total_page > 1):
    ?>
      <li data-link="<?php echo site_url('home/ajax/list_perusahaan/');?>">NEXT</li>
    <?php endif;*/ ?>
    <div class="clr"></div>
  </ul>
</div>
</div>
<?php
endif;
?>