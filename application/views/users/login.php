 <ul class="column">
  <li class="col" style="width:48%; margin-right:2%">
    <div class="wrp-hatOrange ">
      <div  class="hatOrange"> LOGIN<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
      <div class="ho-content grayLight">
        <div class="ho-spacer"></div>
        <?php echo print_status(); ?>
        Fitur yang anda minta hanya diperuntukkan oleh member.<br>
        Silahkan Login terlebih dahulu... <br>
        <form action="<?=site_url('account/login/'.$role)?>" method="post">
          <table width="100%" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td>Email</td>
              <td>:</td>
              <td>
                <input type="text" name="email" id="textfield">
                <br><?=form_error('email');?>
              </td>
            </tr>
            <tr>
              <td>Password</td>
              <td>:</td>
              <td>
                <input type="password" name="password" id="textfield2">
                <br><?=form_error('password');?>
              </td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input type="submit" name="button" id="button" value="Login" class="btn_Orange"></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><a href="<?php echo site_url('account/forgot'); ?>">Lupa Password ? </a></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </li>
  <li class="col" style="width:48%">
    <div class="wrp-hatOrange ">
      <div  class="hatOrange"> BELUM JADI MEMBER ?<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
      <div class="ho-content grayLight">
        <div class="ho-spacer"></div>
        <?php
        $tag_allowed = "<p><br><strong><b><i><em><u><hr>";
        if($role == "kandidat")
        {
          echo strip_tags(get_setting('register_text_kandidat'),$tag_allowed);
          //echo nl2br(get_setting('register_text_kandidat'));
        }
        if($role == "perusahaan")
        {
          echo strip_tags(get_setting('register_text_perusahaan'),$tag_allowed);
          //echo nl2br(get_setting('register_text_perusahaan'));
        }
        ?>
        <?php /* ?>
        <p>Nikmati semua fitur dari Rumah Kandidat.com</p>
        <br>
        <p>Daftar dan nikmati keuntungannya :</p>
        <p>- Unggah CV anda atau buat online CV<br>
          - Cari melalui lowongan dari Perusahaan<br>
          - Terima lowongan pekerjaan melalui email<br>
          - Mengatur area pencari kerja anda sendiri<br>
          <br>
          <br>
        </p>
        <p>Klik link dibawah ini untuk mendaftar</p>
        <?php */ ?><br>
        <a href="<?php echo site_url('account/register/'.$role); ?>" class="btn_Orange">Daftar <?php echo humanize($role); ?></a></div>
    </div>
  </li>
  <div class="clr"></div>
</ul>