<?php
$arr = array(
			'list' => $list,
			'perpage' => $perpage,
			'total' => $total,
			'total_lowongan_aktif' => $total_lowongan_aktif,
			'pagination' => $pagination,
			'curpage' => $this->curpage
			);
//var_dump($arr);
$arr = $arr + array('O' => $O, 'OP' => $OP);
?><h3 class="titleContentRightPanel">Kelola Lowongan</h3>

<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
<br>
<br>

<?php echo anchor($this->curpage."/add", "Pasang Lowongan Baru", array("class" => "btn_Orange"))?>
<br />
<br />

<?php

if(empty($total) && !isset($_GET['keyword'])):

	echo "<p>Anda belum memiliki Daftar Lowongan.</p>";
	
else:

	echo $this->load->view($this->vpath."/tpl_search", NULL, TRUE);
	
	if(empty($total)) echo "<p>Tidak menemukan Lowongan yang disimpan dengan filter tersebut.</p>";
	else
	{
		echo '<div align="right">
		Total <strong>'.intval($total).'</strong> Daftar Lowongan yang pernah dibuat'.($total > $perpage ?
		", ".anchor($this->curpage."/semua", "Lihat Semua") : "").'.</div><br>';
		echo $this->load->view($this->vpath."/tpl_list", $arr, TRUE);
	}

endif;
?>
