<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
  <thead>
  <tr>
    <td width="11%" align="center" valign="middle">Expired </td>
    <td width="72%" valign="middle">Deskripsi</td>
    <td width="17%" align="center" valign="middle">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
    <?php
	$OL = new OLowongan();
	foreach($list as $r):
		$OL->setup($r);
		extract(get_object_vars($r));
		// bidang usaha
		$OTmp = new OMaster_bidang_usaha($bidang_usaha_id);
		$bidang_usaha = $OTmp->get_nama();
		unset($OTmp);
    // perusahaan
    $OTmp = new OPerusahaan($perusahaan_id);
    $perusahaan_url_title = $OTmp->row->url_title;
    unset($OTmp);
    $list_rekomendasi = $OL->get_all_rekomendasi(0,0,"",$OL->id);
    $total_rekomendasi = get_db_total_rows();
	?>
    <tr>
        <td align="right" valign="middle"><?php echo parse_date($dt_expired, "d M Y", "ID", "short"); ?></td>
        <td valign="middle"><?php echo anchor($OL->get_link(), $posisi)?><br />
        <div class="pinf">
          <label><span><?php echo $hits; ?></span> Dilihat </label>
          <label>
            <span><?php echo $total_rekomendasi; ?></span>
            <a href="<?php echo site_url($curpage."/lihat_rekomendasi/".$url_title) ?>">Rekomendasi Kandidat </a>
          </label>
        </div>
        </td>
        <td align="center" valign="middle"><?php echo anchor($curpage."/edit/".$id, "Ubah"); ?> |
        <?php echo anchor($curpage."/delete/".$id, "Hapus"); ?>
        </td>
    </tr>
    <?php
	endforeach;
	unset($OL);
	?>
  </tbody>
</table>
<div id="paging_button" align="center"><?php echo $pagination; ?></div>