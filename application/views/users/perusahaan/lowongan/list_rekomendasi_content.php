<h3 class="titleContentRightPanel">Rekomendasi Kandidat</h3>

<p>Lowongan: <strong><?php echo anchor($OL->get_link(), $OL->get_nama()); ?></strong></p>
<br>
<br>
<?php echo print_status(); ?>
<?php if($list): ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
  <thead>
  <tr>
    <td width="19" align="center" valign="middle"># </td>
    <td colspan="2" valign="middle">Nama Kandidat</td>
    <td width="80" align="center" valign="middle">Pend. Terakhir</td>
    <td width="136" align="center" valign="middle">T.Tinggal</td>
    <td width="74" align="center" valign="middle">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
    <?php  
    $i = 1;
    $OK = new OKandidat;
    foreach($list as $r)
    {
      $OK->setup($r);
      // keterampilan
      $keterampilans = $OK->get_all_keterampilan();
      $keterampilan = implode("<br />", $keterampilans);
      // pendidikan
      $pnddkn_row = $OK->get_last_pendidikan();
      $OTmp_pnddkn = new OMaster_pendidikan($pnddkn_row->pendidikan_id);
      $pendidikan = $OTmp_pnddkn->row->nama;
      unset($OTmp_pnddkn);
      // lokasi
      $lokasi_row = $OK->get_current_lokasi();
      $lokasi = $lokasi_row->nama;
    ?>
    <tr>
      <td align="center" valign="top"><?php echo $i."."; ?></td>
      <td width="60" valign="top">
      	<img class="lazy" src ="<?php echo base_url('_assets/img/dummy6.jpg'); ?>" data-original="<?php echo $OK->get_photo('square'); ?>" width="50" height="50" alt="" title=""/>
      </td>
      <td width="253" valign="top">
        <a href="<?php echo $OK->get_link(); ?>"><strong><?php echo $OK->get_nama_lengkap(); ?> (<?php echo $OK->get_umur(); ?>)</strong></a><br />
        <?php
          echo $keterampilan;
        ?>
        <div class="kand-det-inf"></div></td>
      <td align="center" valign="top">
        <?php
        echo $pendidikan;
        ?>
      </td>
      <td align="center" valign="top">
        <?php
        echo $lokasi;
        ?>
      </td>
      <td align="center" valign="top">
      <?php if(empty($r->diterima_flag)): ?>
        <a href="<?php echo site_url("{$this->curpage}/set_terima_rekomendasi/{$OL->row->url_title}/{$OK->row->url_title}"); ?>" onclick = "return confirm('Yakin menerima kandidat ini?');">Terima</a> |
      <?php else: echo "<strong>Diterima</strong><br>"; ?>
      <?php endif; ?>
        <a href="<?php echo $OK->get_link(); ?>">Lihat</a> |
        <a href="<?php echo site_url("{$this->curpage}/delete_rekomendasi/{$OL->row->url_title}/{$OK->row->url_title}"); ?>" onclick = "return confirm('Yakin ingin menghapus lowongan ini?')">Hapus</a>
      </td>
    </tr>
    <?php  
    $i++;
    }
    ?>
  </tbody>
</table>
<?php else: ?>
  <p>Belum ada kandidat yang melamar lowongan ini. >> <a href="<?php echo site_url($this->curpage); ?>">Kembali</a></p>
<?php endif; ?>