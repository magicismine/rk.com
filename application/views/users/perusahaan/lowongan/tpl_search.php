<?php
extract($_GET);
?><div class="grayBox  dt-search-wrp">
<h3 class="dsw-title">Pencarian</h3>
<form action="" method="get">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
   <tr>
     <td valign="middle">Kategori</td>
     <td valign="middle">: <?php echo OMaster_kategori::drop_down_select("kategori_id", $kategori_id, "", "Semua Kategori"); ?>
     </td>
     <td valign="middle">Lokasi</td>
     <td valign="middle">: <?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id", $lokasi_propinsi_id, "", "Semua Lokasi"); ?></td>
   </tr>
   <tr>
     <td valign="middle">Kata Kunci</td>
     <td valign="middle">:
       <input type="text" name="keyword" id="keyword" value="<?php echo $keyword; ?>"></td>
     <td valign="middle">Kota</td>
     <td valign="middle">:
     <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>">
    <select name="lokasi_kabupaten_id">
        <option>Semua Kota</option>
    </select>
    </span>
     <?php //echo OMaster_lokasi_kabupaten("lokasi_kabupaten_id", $lokasi_kabupaten_id, "", "Semua Kota")?></td>
   </tr>
   <tr>
     <td valign="middle">&nbsp;</td>
     <td valign="middle">&nbsp;</td>
     <td valign="middle">&nbsp;</td>
     <td align="right" valign="middle"><input type="submit" value="Cari" class="btn_blue"></td>
   </tr>
 </table>
</form>
</div>
<br>
<script>
$(function()
{
	get_kabupaten_ddl("lokasi_");
	$(document).on('change', 'select#lokasi_propinsi_id', function(e){
		get_kabupaten_ddl("lokasi_");
	});
});

function get_kabupaten_ddl(prefix)
{
	var t = $('#'+prefix+'kabupaten_wrap');
	var prop = $('#'+prefix+'propinsi_id').val();
	var kab = $('#'+prefix+'kabupaten_wrap').data('value');
	var p = {'propinsi_id': prop, 'kabupaten_id': kab, 'name': prefix+'kabupaten_id'/*, 'default': '-- Pilih --'*/}
	if(prop == "") t.html('Pilih Propinsi dahulu.');
	else
	{
		$.ajax({
			'type': 'POST',
			'async': false,
			url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
			data: p,
			complete: function(xhr, status)
			{
				var ret = xhr.responseText;
				t.html(ret);
			}
		});
	}
}
</script>
