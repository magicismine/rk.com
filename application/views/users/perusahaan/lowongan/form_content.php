<?php
session_start();
$_SESSION['KCFINDER']=array();
$_SESSION['KCFINDER']['disabled'] = false;
$_SESSION['KCFINDER']['uploadURL'] = "../tinymcpuk/uploads";
$_SESSION['KCFINDER']['uploadDir'] = "";

if($row)
{
  extract(get_object_vars($row));
  if(!empty($OL->id))
  {
    $level_karir_arr = $OL->get_level_karir_arr();
    if(is_array($level_karir_arr)) $level_karir_ids = array_keys($level_karir_arr);
  }
}
extract($_POST);
?><h3 class="titleContentRightPanel">Pasang Lowongan Baru</h3>
Isi Form di bawah ini dan mengiklankan lowongan pekerjaan Anda ke ribuan pencari kerja mencari pekerjaan sekarang!
<hr class="spacer">
<?php
echo print_error(validation_errors());
?>
<form class="frmInput" id="lowongan-form" action="" method="post">
  <div class="boxLightOrange"> <strong>Kontak Detail</strong>
    <hr class="spacer">
    Silakan masukkan nama kontak sehingga pengisi lowongan mengetahui mengontak kepada siapa ketika megisi 
    lowongan pekerjaan ini.
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="25%">Nama Kontak</td>
        <td width="2%">:</td>
        <td width="73%"><input type="text" name="kontak_nama" value="<?=$kontak_nama?>" required /></td>
      </tr>
      <tr>
        <td>Email Address:</td>
        <td>:</td>
        <td><input type="email" style="width: 80%; border: 1px solid #e1cda2; padding: 5px; border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px;" name="kontak_email" value="<?=$kontak_email?>" required /></td>
      </tr>
      <tr>
        <td>Telephone</td>
        <td>:</td>
        <td><input type="text" name="kontak_telp" value="<?=$kontak_telp?>" required /></td>
      </tr>
    </table>
  </div>
  <br>
  <div class="grayBox  dt-search-wrp">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><strong>Informasi Pekerjaan
          <hr class="spacer">
          </strong></td>
      </tr>
      <tr>
        <td width="25%">Kode Referensi</td>
        <td width="2%">:</td>
        <td width="73%"><input type="text" name="kode" value="<?=(empty($kode) ? OLowongan::generate_auto_number() : $kode)?>" placeholder="autonumber" readonly /></td>
      </tr>
      <?php /*?><tr>
        <td>Judul</td>
        <td>:</td>
        <td>
            <?php
            echo form_input("judul", $judul, 'required');
            ?>
        </td>
      </tr><?php */?>
      <tr>
        <td>Level Pekerjaan</td>
        <td>:</td>
        <td>
            <?php
            echo OMaster_level_karir::drop_down_select("level_karir_id[]", $level_karir_ids, 'class="chosen-select" multiple data-placeholder="Select Multiple"', '');
            ?>
        </td>
      </tr>
      <tr>
        <td>Kategori Pekerjaan</td>
        <td>:</td>
        <td>
			<?php
            echo OMaster_kategori::drop_down_select("kategori_id", $kategori_id);
			?>
		</td>
      </tr>
      <tr>
        <td>Posisi</td>
        <td>:</td>
        <td>
            <input type="text" name="posisi" value="<?=$posisi?>" required /> 
        </td>
      </tr>
      <tr>
        <td>Jumlah Tenaga Kerja</td>
        <td>:</td>
        <td>
            <input type="number" name="jumlah_tenaga_kerja" class="wd50 text" size="2" min="1" value="<?=$jumlah_tenaga_kerja?>" required /> 
        </td>
      </tr>
      <tr>
        <td>Waktu Kerja</td>
        <td>:</td>
    		<td>
        <?php
        echo OMaster_waktu_kerja::drop_down_select("waktu_kerja_id", $waktu_kerja_id);
        ?>
    		</td>
      </tr>
      <tr>
        <td>Jenjang Pendidikan</td>
        <td>:</td>
        <td>
    		<?php
    			echo OMaster_pendidikan::drop_down_select("pendidikan_id", $pendidikan_id);
    		?>
        </td>
      </tr>
      <tr>
        <td> Umur</td>
        <td>:</td>
        <td>
        <input type="number" name="umur_mulai" value="<?=$umur_mulai?>" size="2" maxlength="2" min="<?php echo get_umur_min(); ?>" max="<?php echo get_umur_max(); ?>" class="wd50 text" required /> Sampai
        <input type="number" name="umur_sampai" value="<?=$umur_sampai?>" size="2" maxlength="2" min="<?php echo get_umur_min(); ?>" max="<?php echo get_umur_max(); ?>" class="wd50 text" required />
		</td>
      </tr>
      <tr>
        <td>Pengalaman Kerja (tahun)</td>
        <td>:</td>
        <td><input type="number" name="tahun_pengalaman_kerja" class="wd50 text" size="2" maxlength="2" min="<?php echo get_pengalaman_min(); ?>" max="<?php echo get_pengalaman_max(); ?>" value="<?=$tahun_pengalaman_kerja?>" /></td>
      </tr>
      <tr>
        <td>Gender</td>
        <td>:</td>
        <td><?php
            echo OMaster_jenis_kelamin::radio_select("jenis_kelamin_id", (empty($jenis_kelamin_id) ? 1 : intval($jenis_kelamin_id)), 'class="fixed"',' &nbsp; ');
            ?>
        </td>
      </tr>
      <tr>
        <td>Gaji yang ditawarkan</td>
        <td>:</td>
        <td>
          <label for="gaji_nego_flag">
          <?php
          echo form_checkbox("gaji_nego_flag",1,(empty($gaji_nego_flag) ? FALSE : TRUE),'id="gaji_nego_flag" class="fixed" data-target="#gaji_wrap"');
          ?>&nbsp; Negosiasi</label><br />
          <div id="gaji_wrap">
          Rp. <?=form_input("gaji", (empty($gaji) ? 5000000 : $gaji), 'class="wd100 currency"')?>
          <?php /* ?>
          <input type="text" name="gaji" id="gaji" class="currency fixed" value="<?=(empty($gaji) ? 5000000 : $gaji)?>" />
          <?php */ ?>
          </div>
        </td>
      </tr>
      <tr>
        <td>Batas Waktu Melamar</td>
        <td>:</td>
        <td><?=form_input("dt_expired", get_date_lang($dt_expired, "ID"), 'class="'.($this->uri->segment(3) == "add" ? 'datepicker_future' : 'datepicker').' wd100" required')?></td>
      </tr>
      <tr>
        <td colspan="3">
      </tr>
      <tr>
        <td colspan="3"><strong>Lokasi Kerja</strong>
          <hr class="spacer"/>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>
        <label>
        <?php
        echo form_checkbox("lokasi_kerja_kantor_flag",1,(empty($lokasi_kerja_kantor_flag) ? FALSE : TRUE),'id="lokasi_kerja_kantor_flag" class="fixed" data-target="#lokasi-table"');
		?>&nbsp; Sama Seperti Alamat Kantor Pusat
        </label>
        </td>
      </tr>
    </table>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="lokasi-table">
      <tr>
        <td width="25%">Alamat</td>
        <td width="2%">:</td>
        <td width="73%"><?php echo form_textarea("lokasi_alamat", $lokasi_alamat, 'class="mceNoEditor"')?></td>
      </tr>
      <tr>
        <td>Negara</td>
        <td>:</td>
        <td><?php echo OMaster_lokasi_negara::drop_down_select("lokasi_negara_id", $lokasi_negara_id)?></td>
      </tr>
      <tr>
        <td>Propinsi</td>
        <td>:</td>
        <td><?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id", $lokasi_propinsi_id)?></td>
      </tr>
      <tr>
        <td>Kota/ Kabupaten</td>
        <td>:</td>
        <td>
          <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>">
            Silahkan memilih Propinsi.
          </span>
          <?php //echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id", $lokasi_kabupaten_id)?>
        </td>
      </tr>
      <tr>
        <td>Kawasan</td>
        <td>:</td>
        <td>
          <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>">
            Silahkan memilih Kabupaten.
          </span>
          <?php
          //echo form_input("lokasi_kawasan", $lokasi_kawasan);
          ?>
        </td>
      </tr>
    </table>
  </div>
  <br>
  <strong>Deskripsi dan Detail Persyaratan </strong>
  <hr class="spacer">
  <?php echo form_textarea("deskripsi", $deskripsi, 'style="height:300px;width:100%;box-sizing:border-box;"'); ?>
<?php /*?><fieldset style="border: 1px solid black; padding: 10px; font-size: 10px;margin-top: 10px;">
<strong>Masukkan list kualifikasi dengan cara perbaris:</strong><br>
Contohnya seperti dibawah ini:<br>
Apple<br>
Orange
</fieldset><?php */?>
  <br>
  <label><input name="term" type="checkbox" value="1">
  Saya telah membaca, memahami, dan menyetujui seluruh <a href="#">persyaratan dan ketentuan</a> yang berlaku</label>
  <br>
  <br>
  <div align="right">
    <input type="button"  class="btn_Gray" value="Cancel" onclick="location.href='<?php echo site_url($this->curpage); ?>';" >
    <input type="submit"  class="btn_Orange" value="Simpan">
  </div>
</form>
<script src="<?=base_url("_assets/js/jquery.number.js")?>"></script>
<script src="<?=base_url("_assets/tinymcpuk/jscripts/tiny_mce/tiny_mce.js")?>"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
	editor_deselector : "mceNoEditor",
	theme : "simple"
});
</script>
<?php /*?><script src="<?=base_url("_assets/tinymcpuk/jscripts/tiny_mce/my_tiny_mce.js")?>"></script><?php */?>
<script>
$(function()
{
	get_kabupaten_ddl("lokasi_");
  /*
	$(document).on('change', 'select#lokasi_propinsi_id', function(e){
		get_kabupaten_ddl("lokasi_");
	});
  $(document).on('change', 'select#lokasi_kabupaten_id', function(e){
    get_kawasan_ddl("lokasi_");
  });
	*/

	$(document).on('submit', '#lowongan-form', function(e){
		e.stopPropagation();
		if($('input[name=term]').is(':checked') == false)
		{
			alert('Silahkan centang bahwa anda telah membaca, memahami, dan menyetujui seluruh persyaratan dan ketentuan yang berlaku.');
			e.preventDefault();
		}
		else
		{
			return true;
		}
	});
	
	set_checkbox_flag('#lokasi_kerja_kantor_flag');
	$(document).on('click', '#lokasi_kerja_kantor_flag', function ()
	{
		set_checkbox_flag( $(this) );
	});
	
	set_checkbox_flag('#gaji_nego_flag');
	$(document).on('click', '#gaji_nego_flag', function ()
	{
		set_checkbox_flag( $(this) );
	});

	// Set up the number formatting.
	$('.currency').number( true, 0 );
	// Get the value of the number for the demo.
	$('.currency').on('blur',function()
	{
		var t = $(this);
		t.next().remove();
		t.parent().append('<input type="hidden" name="'+t.attr('name')+'" value="'+t.val()+'" />');
	});
});

</script>