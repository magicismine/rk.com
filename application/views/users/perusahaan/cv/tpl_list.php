<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
<thead>
    <tr>
        <td width="19" align="center" valign="middle"># </td>
        <td colspan="2" valign="middle">Nama Kandidat</td>
        <td width="80" align="center" valign="middle">Pend. Terahkir</td>
        <td width="136" align="center" valign="middle">T.Tinggal</td>
        <td width="74" align="center" valign="middle">&nbsp;</td>
    </tr>
</thead>
<tbody>
<?php
$i=1;
$OTmp = new OKandidat();
foreach($list as $r):
    $OTmp->setup($r);
    extract(get_object_vars($r));
	// keterampilan
    $keterampilans = $OTmp->get_all_keterampilan();
    $keterampilan = implode("<br />", $keterampilans);
	/*$ket_row = $OTmp->get_current_keterampilan();
	$OTmp_kat = new OMaster_kategori($ket_row->kategori_id);
	$keterampilan = $OTmp_kat->row->nama;
	unset($OTmp_kat);*/
	// pendidikan
	$pnddkn_row = $OTmp->get_last_pendidikan();
	$OTmp_pnddkn = new OMaster_pendidikan($pnddkn_row->pendidikan_id);
	$pendidikan = $OTmp_pnddkn->row->nama;
	unset($OTmp_pnddkn);
	// lokasi
	$lokasi_row = $OTmp->get_current_lokasi();
	$lokasi = $lokasi_row->nama;
?>
<tr>
    <td align="center" valign="top"><?php echo $i; ?></td>
    <td width="60" valign="top">
    	<img class="lazy" src ="<?php echo base_url("_assets/img/gray.gif"); ?>" data-original="<?php echo $OTmp->get_photo("square"); ?>" width="50" height="50" alt="" title=""/>
	</td>
    <td width="253" valign="top">
		<?php echo anchor($OTmp->get_link(), '<strong>'.$OTmp->get_nama_lengkap().'</strong> ('.$OTmp->get_umur().')')?><br>
        <?php echo $keterampilan; ?>
    	<div class="kand-det-inf"></div>
    </td>
    <td align="center" valign="top"><?php echo $pendidikan; ?></td>
    <td align="center" valign="top"><?php echo $lokasi; ?></td>
    <td align="center" valign="top">
		<?php //echo anchor($this->curpage."/view/".$url_title, "Lihat")." |"; ?>
        <?php echo anchor($OTmp->get_link(), "Lihat")." |"; ?>
        <?php echo anchor($this->curpage."/delete/".$url_title, "Hapus"); ?>      
        <?php /*?><a href="index-detailkandidat.html">Lihat</a> | <a href="#">Hapus</a><?php */?>
    </td>
</tr>
<?php
	$i++;
endforeach;
unset($OTmp);
?>
</tbody>
</table>
<div id="paging_button" align="center"><?php echo $pagination; ?></div>