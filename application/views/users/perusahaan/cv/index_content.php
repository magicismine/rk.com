<?php
$arr = array(
			'list' => $list,
			'perpage' => $perpage,
			'total' => $total,
			'pagination' => $pagination,
			'O' => $O
			);
?><h3 class="titleContentRightPanel">CV Yang Disimpan</h3>

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries . <br>


<br><br>
<?php

if(empty($total) && !isset($_GET['keyword'])):

	echo print_status();
	echo "<p>Anda tidak memiliki CV yang disimpan. Silahkan mencari CV kandidat di menu Cari Kandidat.</p>";
	
else:

	echo $this->load->view($this->vpath."/tpl_search", NULL, TRUE);
	echo print_status();
	if(empty($total)) echo "<p>Tidak menemukan CV yang disimpan dengan filter tersebut.</p>";
	else
	{
		echo '<div align="right">
		Total <strong>'.intval($total).'</strong> CV Kandidat yang disimpan'.($total > $perpage ?
		", ".anchor($this->curpage."?viewall", "Lihat Semua") : "").'.</div><br>';
		echo $this->load->view($this->vpath."/tpl_list", $arr, TRUE);
	}

endif;
?>
