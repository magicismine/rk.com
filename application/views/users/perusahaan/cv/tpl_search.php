<?php
extract($_GET);
?><div class="grayBox  dt-search-wrp">
    <h3 class="dsw-title">Pencarian</h3>
    <form action="" method="get">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
         <td valign="middle">Kata Kunci</td>
         <td valign="middle">:
           <input type="text" name="keyword" id="keyword" value="<?=$keyword?>" /></td>
         <td valign="middle">Lokasi</td>
         <td valign="middle">:
         <?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id", $lokasi_propinsi_id, "", "Semua Lokasi"); ?>
         </td>
       </tr>
       <tr>
         <td valign="middle">Pendidikan</td>
         <td valign="middle">:
         <?php echo OMaster_pendidikan::drop_down_select("pendidikan_id", $pendidikan_id, "", "Semua Level Pendidikan"); ?>
         </td>
         <td valign="middle">Kota</td>
         <td valign="middle">:
         <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>"><?php echo "Silahkan memilih lokasi terlebih dahulu."; ?></span>
         </td>
       </tr>
       <tr>
         <td valign="middle">Tahun Lahir</td>
         <td valign="middle">: 
			 <?php
			 $start_dt = date("Y",strtotime("-55 year"));
			 $end_dt = date("Y",strtotime("-15 year"));
			 echo year_custom_ddl( "tahun_lahir", $tahun_lahir, $start_dt, $end_dt, "", "Semua Tahun" );
			 ?>
         </td>
         <td valign="middle">&nbsp;</td>
         <td align="right" valign="middle"><input type="submit" value="Cari" class="btn_blue"></td>
       </tr>
     </table>
    </form>
</div>
<br>
<script>
$(function()
{
	get_kabupaten_ddl("lokasi_");
	$(document).on('change', 'select#lokasi_propinsi_id', function(e){
		get_kabupaten_ddl("lokasi_");
	});
});

function get_kabupaten_ddl(prefix)
{
	var t = $('#'+prefix+'kabupaten_wrap');
	var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': $('#'+prefix+'kabupaten_wrap').data('value'), 'name': prefix+'kabupaten_id'/*, 'default': '-- Pilih --'*/}
	$.ajax({
		'type': 'POST',
		'async': false,
		url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
		data: p,
		complete: function(xhr, status)
		{
			var ret = xhr.responseText;
			t.html(ret);
		}
	});
}
</script>
