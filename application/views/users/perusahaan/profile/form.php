<?php
extract(get_object_vars($row));
extract($_POST);
?><div class="content">
<h1 class="titleContentPage">PERUSAHAAN</h1>
<ul class="column">
	<?php echo $this->load->view($this->vpath."/tpl_sidenav", NULL, TRUE); ?>

<li class="col rightpanel">
<h3 class="titleContentRightPanel">Profile Perusahaan</h3> 
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p><br />
<div class="grayBox  dt-search-wrp">
<?php
echo print_error(validation_errors());
?>
<form action="" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td colspan="3"><span class="f14 forange">Detail Perusahaan</span><br>
    Silakan masukkan nama perusahaan Anda, alamat dan contact detail. pencari kerja yang berpotensi mungkin menggunakan informasi ini ketika mengisi lamaran pekerjaan.</td>
</tr>
<tr>
  <td width="27%"><p>Nama Perusahaan </p></td>
  <td width="44%"><?php echo form_input("nama",$nama); ?></td>
  <td width="29%">&nbsp;</td>
</tr>
<tr>
  <td>Email Perusahaan</td>
  <td><?php echo form_input("email",$email); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Jumlah Karyawan</td>
  <td><select name="jumlah_karyawan" id="jumlah_karyawan">
    <option>&lt; 100</option>
    <option>100 &gt; 500</option>
    <option>500 &gt; 1000</option>
    <option>&gt; 1000</option>
  </select></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Jenis Bidang Usaha</td>
  <td><?php echo OMaster_bidang_usaha::drop_down_select("bidang_usaha_id", $bidang_usaha_id); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Website</td>
  <td><?php echo form_input("website",$website,'placeholder="http://"'); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Motto </td>
  <td><?php echo form_input("motto",$motto); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td style="vertical-align:top;">Logo </td>
  <td colspan="2">
    <?php
    if($O->id != FALSE)
    {
        echo "<img src='".$O->get_photo("medium")."' />";
    }
    ?>
    <div style="display:block;" id="last-photo"></div>
    <div style="display:block;" id="preview-photo"></div>
    <div id="swfupload-control-photo" class="clear">
        <p>Upload maximum <span id="total_photos">1</span> image file(jpg, png, gif) and having maximum size of 1 MB</p>
        <input type="button" id="button-photo" />
        <p id="queuestatus-photo" ></p>
        <ol id="log-photo"></ol>
        <span class="set"></span> 
    </div>  
  </td>
</tr>
<tr>
  <td style="vertical-align:top;">Deskripsi</td>
  <td colspan="2"><?php echo form_textarea("deskripsi",$deskripsi,'style="width:100%; height:130px"'); ?></td>
  </tr>
<tr>
  <td colspan="3"><hr class="line"></td>
</tr>
<tr>
  <td colspan="3"><span class="f14 forange">Contact Detail</span><br>
    Silakan detail alamat kantor pusat perusahaan anda.</td>
</tr>
<tr>
  <td>Nama</td>
  <td><?php echo form_input("kontak_nama",$kontak_nama); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Email</td>
  <td><?php echo form_input("kontak_email",$kontak_email); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>No Telp</td>
  <td><?php echo form_input("kontak_telp",$kontak_telp); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="3"><hr class="line"></td>
</tr>
<tr>
  <td colspan="3"><span class="f14 forange">Alamat</span><br>
    Isilah nama perusahaan anda sesuai dengan izin domisili </td>
</tr>
<tr>
  <td>Alamat</td>
  <td><?php echo form_input("lokasi_alamat",$lokasi_alamat); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Negara</td>
  <td><?php echo OMaster_lokasi_negara::drop_down_select("lokasi_negara_id",$lokasi_negara_id); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Propinsi</td>
  <td>
    <span id="lokasi_propinsi_wrap" data-value="<?php echo $lokasi_propinsi_id; ?>" data-default="Pilih Propinsi">
      <?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id",$lokasi_propinsi_id);?>
    </span>
  </td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Kota/Kabupaten</td>
  <td>
    <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>" data-default="Pilih Kota/ Kabupaten">
      <?php echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id); ?>
    </span>
  </td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Kawasan</td>
  <td>
    <?php //echo form_input("lokasi_kawasan",$lokasi_kawasan); ?>
    <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>" data-default="Pilih Kawasan">
      <?php echo OMaster_lokasi_kawasan::drop_down_select("lokasi_kawasan_id",$lokasi_kawasan_id); ?>
    </span>
  </td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Kecamatan</td>
  <td><?php echo form_input("lokasi_kecamatan",$lokasi_kecamatan); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>Desa/ Kelurahaan</td>
  <td><?php echo form_input("lokasi_kelurahan",$lokasi_kelurahan); ?></td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td colspan="3"><hr class="line"></td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td colspan="2" align="right">
  	<input name="button2" type="button" class="btn_Gray" id="button2" value="Batal" onclick="location.href='<?php echo site_url($this->curpage)?>';" />
    <input name="button" type="submit" class="btn_Orange" id="button" value="Submit" />
  </td>
</tr>
</table>
</form>
</div>
</li>

	<div class="clr"></div>
</ul>
</div>

<script type="text/javascript" src="<?=base_url("_assets/js/swfupload/swfupload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/jquery.swfupload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/jquery.swfupload.init.js")?>"></script>
<script type="text/javascript">
$(function(){
  var total_photos = $('#total_photos').text();
  jquery_upload_image("<?php echo base_url();?>","-photo",".set","image[]","#preview-photo","1 MB",total_photos);
  
  var total_photos_over = $('#total_photos_over').text();
  jquery_upload_image("<?php echo base_url()?>","-photo-over",".set_over","image_over[]","#preview-photo-over","1 MB",total_photos_over);
}); 
</script>
<style type="text/css" >
  #SWFUpload_Console { display:none; position:absolute; z-index:1000; bottom:0; right:10px; }
  #swfupload-control-photo p
  { margin:10px 5px; font-size:0.9em; }
  #log-photo
  { margin:0; padding:0; /*width:500px;*/}
  #log-photo li
  { list-style-position:inside; margin:2px; border:1px solid #ccc; padding:10px; font-size:12px; font-family:Arial, Helvetica, sans-serif; color:#333; background:#fff; position:relative;}
  #log-photo li .progressbar
  { border:1px solid #333; height:5px; background:#fff; }
  #log-photo li .progress
  { background:#999; width:0%; height:5px; }
  #log-photo li p
  { margin:0; line-height:18px; }
  #log-photo li.success
  { border:1px solid #339933; background:#ccf9b9; }
  #log-photo li span.cancel
  { position:absolute; top:5px; right:5px; width:20px; height:20px; background:url('<?=base_url("_assets/js/swfupload/cancel.png")?>') no-repeat; cursor:pointer; }
  
  .form_style .full { width:98%; }
  .error{color:#FF0000}
  
  .tbl_separate{}
  .tbl_separate tr{ vertical-align:top; }
  .tbl_separate tr td{ padding:5px; vertical-align:top; }
  
  .image_list{ list-style:none; float:left; }
  .image_list li{ float:left; margin: 0 10px 10px 0; }
  .form_right label input { vertical-align:top; }
  .form_right label img { vertical-align:top; }
</style>