<?php
extract(get_object_vars($row));
$OTmp = new OMaster_bidang_usaha($bidang_usaha_id);
$bidang_usaha = $OTmp->row->nama;
unset($OTmp);
$OTmp = new OMaster_lokasi_propinsi($lokasi_propinsi_id);
$propinsi = $OTmp->row->nama;
unset($OTmp);
$OTmp = new OMaster_lokasi_negara($lokasi_negara_id);
$negara = $OTmp->row->nama;
unset($OTmp);

?><div class="content">
<h1 class="titleContentPage">PERUSAHAAN</h1>
<ul class="column">
	<?php echo $this->load->view($this->vpath."/tpl_sidenav", NULL, TRUE)?>
	<li class="col rightpanel">
    <h3 class="titleContentRightPanel">Profile Perusahaan</h3><div >
    <?php echo print_status(); ?>
      <ul class="column">
        <li class="rght">
		  <?php echo anchor($this->curpage."/edit", "Edit Profil", array("class" => "btnedit")); ?>
		  <?php //=anchor($this->curpage."/edit", "Save as Pdf", array("class" => "btnpdf"))?>
        </li>
        <div class="clr"></div>
      </ul>
    </div>
    <ul class="column pinfo">
      <li class="col pinfo-pic-s"><img src="<?php echo $O->get_photo('square'); ?>" width="50" height="50"> </li>
      <li class="col pinfo-desc"><span class="pinfo-name"><?php echo $nama; ?></span><?php echo $email; ?></li>
       <div class="clr"></div>
    </ul>
    <span class="pinfo-sk"><?php echo $motto; ?></span>
    <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
      <?php /*?><tr class="noborder">
        <td height="30">&nbsp;</td>
        <td width="28%">&nbsp;</td>
        <td colspan="2"><div class="csc">
<span style="width:50%">Profile Completed 50%</span>
</div></td>
      </tr><?php */?>
      <tr>
        <td>Alamat</td>
        <td colspan="3"> : <?php echo $lokasi_alamat; ?></td>
      </tr>
      <tr>
        <td width="25%">Bidang Usaha</td>
        <td colspan="3">: <?php echo $bidang_usaha; ?></td>
      </tr>
      <tr>
        <td>Lokasi</td>
        <td colspan="3">: <?php echo $lokasi_kawasan; ?></td>
      </tr>
      <tr>
        <td>Propinsi</td>
        <td colspan="3"> : <?php echo $propinsi; ?></td>
      </tr>
      <tr>
        <td>Negara</td>
        <td colspan="3">: <?php echo $negara; ?></td>
      </tr>
      <tr>
        <td>Jumlah Karyawan</td>
        <td colspan="3"> : <?php echo $jumlah_karyawan; ?> Orang</td>
      </tr>
      <tr>
        <td colspan="4" class="noborder">&nbsp;</td>
      </tr>
      <tr>
        <td><strong  class="pinfo-sp">Contact Detail</strong></td>
        <td>&nbsp;</td>
        <td width="23%"><strong  class="pinfo-sp">Statistik</strong></td>
        <td width="24%">&nbsp;</td>
      </tr>
      <tr>
        <td>Nama</td>
        <td>: <?php echo $kontak_nama; ?></td>
        <td>Daftar Sejak</td>
        <td>: <?php echo parse_date($dt_added, "d-M-Y", "ID", "short"); ?></td>
      </tr>
      <tr>
        <td>Email</td>
        <td>: <?php echo $kontak_email; ?></td>
        <td>Masa Berlaku Keanggotaan</td>
        <td>: <?php echo "-"; ?></td>
      </tr>
      <tr>
        <td>Telepon</td>
        <td>: <?php echo $kontak_telp; ?></td>
        <td>Iklan Tayang</td>
        <td>: <?php echo "-"; ?></td>
      </tr>
      <tr class="noborder">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>Total Iklan</td>
        <td>: <?php echo "-"; ?></td>
      </tr>
    </table>
    <br>
    <div class="grayBox"> <span class='gb-title'>Deskripsi</span>
         <?php echo $deskripsi; ?>
	</div>
    <br>
     
     
     
  </li>
  <div class="clr"></div>
</ul>
</div>