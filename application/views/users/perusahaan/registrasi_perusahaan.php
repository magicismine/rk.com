<?php extract($_POST); ?>
<div class="content">
  <h1 class="titleContentPage">Daftar Sebagai perusahaan pemberi kerja</h1>
  <?php 
  if(validation_errors() != ""): ?>
  <div class="validation_error">
    <?php echo validation_errors(); ?>
  </div>
  <?php endif; ?>
  <form enctype="multipart/form-data" class="registerForm" method="post" action="<?php echo site_url('account/register/perusahaan') ?>">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><span class="f14 forange">Login Detail</span><br>Silakan masukkan alamat email Anda dan kemudian pilih password. Anda akan membutuhkan informasi ini setiap Anda masuk pada akun Anda.
        </td>
      </tr>
      <tr>
        <td width="27%">Email Address </td>
        <td width="44%">
          <input type="email" name="email" id="textfield3" value="<?php echo $email; ?>" required>
          <br>
          <?php echo form_error('email'); ?>
        </td>
        <td width="29%">&nbsp;</td>
      </tr>
      <tr>
        <td>Password</td>
        <td>
          <input type="password" name="password" id="textfield" required>
          <br>
          <?php echo form_error('password'); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Confirm Password</td>
        <td>
          <input type="password" name="conf_password" id="textfield2"  required>
          <br>
          <?php echo form_error('conf_password'); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3"><hr class="line"></td>
      </tr>
      <tr>
        <td colspan="3"><span class="f14 forange">Detail Perusahaan</span><br>
        Silakan masukkan nama perusahaan Anda, alamat dan contact detail. pencari kerja yang berpotensi mungkin menggunakan informasi ini ketika mengisi lamaran pekerjaan.</td>
      </tr>
      <tr>
        <td><p>Nama Perusahaan </p></td>
        <td>
          <input type="text" name="nama" id="textfield4" value="<?php echo $nama; ?>"  required>
          <br>
          <?php echo form_error('nama'); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Jenis Bidang Usaha</td>
        <td>
          <?php echo OMaster_bidang_usaha::drop_down_select("bidang_usaha_id",$bidang_usaha_id);?>                
          <br/>
          <?php echo form_error('bidang_usaha_id');?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Nama Kontak</td>
        <td><input type="text" name="kontak_nama" id="textfield6"  value="<?php echo $kontak_nama;?>" required></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Email Kontak</td>
        <td><input type="email" name="kontak_email" id="textfield10" value="<?php echo $kontak_nama;?>"  required></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3"><hr class="line"></td>
      </tr>
      <tr>
        <td colspan="3"><span class="f14 forange">Alamat</span><br>
       Isilah nama perusahaan anda sesuai dengan izin domisili :
        </td>
      </tr>
      <tr>
        <td>Alamat</td>
        <td colspan="2"><textarea name="lokasi_alamat" id="textfield7" required><?php echo $lokasi_alamat;?></textarea></td>
      </tr>
      <tr>
        <td>Negara</td>
        <td>
          <?php echo OMaster_lokasi_negara::drop_down_select("lokasi_negara_id",$lokasi_negara_id);?>                
          <br/>
          <?php echo form_error('lokasi_negara_id');?>
      </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Propinsi</td>
        <td>
          <span id="lokasi_propinsi_wrap" data-value="<?php echo $lokasi_propinsi_id; ?>" data-default="Pilih Propinsi"><?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id",$lokasi_propinsi_id);?></span>
          <br/>
          <?php echo form_error('lokasi_propinsi_id');?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Kota/ Kabupaten</td>
        <td>
          <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>" data-default="Pilih Kota / Kabupaten" ><?php echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id); ?></span>
          <br/>
          <?php echo form_error('lokasi_kabupaten_id');?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Kawasan</td>
        <td>
          <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>" data-default="Pilih Kawasan" ><?php echo OMaster_lokasi_kawasan::drop_down_select("lokasi_kawasan_id",$lokasi_kawasan_id); ?></span>
          <br/>
          <?php echo form_error('lokasi_kawasan_id');?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Desa/ Kelurahan</td>
        <td>        
          <input type="text" name="lokasi_kelurahan" value="<?php echo $lokasi_kelurahan;?>">
          <br/>
          <?php echo form_error('lokasi_kelurahan');?>
      </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Kecamatan</td>
        <td>        
          <input type="text" name="lokasi_kecamatan" value="<?php echo $lokasi_kecamatan;?>" required>
          <br/>
          <?php echo form_error('lokasi_kecamatan');?>
      </td>
        <td>&nbsp;</td>
      </tr>
      <?php /* ?>
      <tr>
        <td>Kawasan</td>
        <td>
          <input type="text" name="lokasi_kawasan" value="<?php echo $lokasi_kawasan;?>" required>             
          <br/>
          <?php echo form_error('lokasi_kawasan');?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <?php */ ?>
      <tr>
        <td colspan="3"><hr class="line"></td>
      </tr>
      <tr>
        <td colspan="3"><span class="forange f14">Spam Prevention</span><br>
        Silakan masukkan kode yang muncul pada gambar:</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">
          <?php /* ?>
          <img class="lazy" src ="img/gray.gif" data-original="img/captcha.jpg" alt=""  >
          <?php */ ?>
          <?php echo $captcha; ?>
          <?php echo form_error('recaptcha_response_field'); ?>
          <?php echo ($error_code == "" ? "" : "<p class='text-red'>{$error_code}</p>"); ?>
        </td>
      </tr>
      <?php /* ?>
      <tr>
        <td>Kode Gambar</td>
        <td><input type="text" name="spam_kode" id="spam_kode"></td>
        <td>&nbsp;</td>
      </tr>
      <?php */ ?>
      <tr>
        <td colspan="3">
        <input type="checkbox" name="term" id="term" value="1" <?php if($term == 1){ echo "checked='checked'";} ?>>
        Silakan Cek pada Kotak untuk mengindakasi bahwa Anda menerima<a href="#"> syarat &amp; kondisi</a> website ini.<br>
        <?php echo form_error('term'); ?>
        <?php //* ?>
        <input type="checkbox" name="newsletter_flag" id="newsletter_flag" value="1" <?php if($newsletter_flag == 1){ echo "checked='checked'";} ?>>
        Silakan tandai kotak ini jika Anda menghendaki untuk menerima berita terakhir dan informasi melalui email.
        <?php //*/ ?>
      </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2" align="right">
          <input name="reset" type="reset" class="btn_Gray" id="button2" value="Batal">
        <input name="button" type="submit" class="btn_Orange" id="button" value="Daftar"></td>
      </tr>
    </table>
  </form>
</div>