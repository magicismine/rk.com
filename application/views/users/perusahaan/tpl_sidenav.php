<?php
$uri = $this->uri->segment(2);
$cururi = $this->uri->segment(1);
?><li class="col leftpanel">
<div class="wrp-hatOrange ">
  <div  class="hatOrange"> Menu's<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
  <div class="ho-content grayDark">
    <div class="ho-spacer"></div>
    <ul class="leftmenu">
      <li<?php echo (($uri == "" || $uri == "home") ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi, "Halaman Utama"); ?></li>
      <li<?php echo ($uri == "profile" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/profile", "Profile Perusahaan"); ?></li> 
      <?php /*?><li<?php echo ($uri == "status" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/status", "Status Keanggotaan"); ?></li> <?php */?>
      <li<?php echo ($uri == "lowongan" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/lowongan", "Kelola Lowongan"); ?></li> 
      <li<?php echo ($uri == "cv" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/cv", "CV Yang Disimpan"); ?></li>
      <li<?php echo ($uri == "promo" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/promo", "Promo Kandidat"); ?></li>
      <li<?php echo ($uri == "search" ? ' class="lm-active"' : ""); ?>><?php echo anchor("search/kandidat", "Cari Kandidat"); ?></li> 
      <li><?php echo anchor($cururi."/home/logout", "Keluar"); ?></li>
    </ul>
  </div>
</div>
<br />
<?php  
  //$OAd = new OAd(6);
  $OAd = new OAd;
  $list_ads = OAd::get_list(0,0,"id ASC","active_flag = 1 AND photo <> '' AND position = 'user_bottom' AND (show_on = 'both' OR show_on = 'perusahaan')");
  $total_ads = get_db_total_rows();
  if($total_ads > 0)
  {
?>
  <?php  
  foreach($list_ads as $ads)
  {
    $OAd->setup($ads);
    $photo = $OAd->get_photo_original();
  ?>
  <a href="<?php echo $OAd->row->url; ?>">
    <img class="lazy" src ="<?php echo $OAd->get_photo_original();?>" data-original="<?php echo $OAd->get_photo_original();?>" alt="<?php echo $OAd->row->nama; ?>" title="<?php echo $OAd->row->nama; ?>" 
      width="<?php echo $OAd->row->width; ?>" height="<?php echo $OAd->row->height; ?>" style="max-width:276px;" />
  </a>
  <?php } ?>
<?php }else{  ?>
  <img class="lazy" src ="<?php echo base_url(); ?>_assets/img/gray.gif" data-original="<?php echo base_url(); ?>_assets/img/dummy7.jpg"> 
<?php } ?>
</li>