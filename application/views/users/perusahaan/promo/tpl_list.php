<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
<thead>
    <tr>
        <td width="5%" align="center" valign="middle"># </td>
        <td valign="middle">Judul</td>
        <td width="20%" align="center" valign="middle">&nbsp;</td>
    </tr>
</thead>
<tbody>
<?php
$i=1;
$OTmp = new OPerusahaan_promo;
foreach($list as $r):
    $OTmp->setup($r);
    extract(get_object_vars($r));
?>
<tr>
    <td align="center" valign="top"><?php echo $i; ?></td>
    <td align="" valign="top"><?php echo $name; ?></td>
    <td align="center" valign="top">
        <?php $action_arr = NULL; ?>
        <?php $action_arr[] = anchor($this->curpage."/edit/".$url_title, "Edit"); ?>
        <?php $action_arr[] = anchor($this->curpage."/delete/".$url_title, "Hapus", 'onclick="return confirm(\'Are you sure?\');"'); ?>
        <?php echo implode(" | ", $action_arr) ?>
        <?php /*?><a href="index-detailkandidat.html">Lihat</a> | <a href="#">Hapus</a><?php */?>
    </td>
</tr>
<?php
	$i++;
endforeach;
unset($OTmp);
?>
</tbody>
</table>
<div id="paging_button" align="center"><?php echo $pagination; ?></div>