<div class="content">
  <h1 class="titleContentPage">Kandidat</h1>
  <ul class="column">
	<?php echo $this->load->view($this->vpath.'/tpl_sidenav',NULL,TRUE);?>
    <li class="col rightpanel">        
      <h3 class="titleContentRightPanel">Pengaturan Akun</h3>
      <?php echo print_status(); ?>
      <p>Silakan masukan alamat email Anda dan pilih password. Anda akan membutuhkan informasi ini saat anda menggunakan akun user Anda.</p><br>
    <br>
    <div class="grayBox  dt-search-wrp">
      <h3 class="dsw-title">Akun Login Anda</h3>
      <form action="" method="post">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="middle" width="25%">Email</td>
            <td valign="middle">:
              <input name="email" type="text" id="textfield" value="<?php echo $OUser->row->email; ?>">
              <br>
              <?php echo form_error('email'); ?>
            </td>
            </tr>
          <tr>
            <td valign="middle">Confirm Email</td>
            <td valign="middle">:
              <input name="conf_email" type="text" id="textfield2" value="">
              <br>
              <?php echo form_error('conf_email'); ?>
            </td>
            </tr>
          <tr>
            <td valign="middle">Password</td>
            <td valign="middle">:
              <input name="password" type="password" id="textfield3" value=""><br>
              <?php echo form_error('password'); ?>
            </td>
            </tr>
          <tr>
            <td valign="middle">Confirm Password</td>
            <td valign="middle">:
              <input name="conf_password" type="password" id="textfield4" value=""><br>
              <?php echo form_error('conf_password'); ?>
            </td>
            </tr>
        </table>
        <hr class="spacer" />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td colspan="2"><span class="dsw-title">Setting Keamanan</span></td>
            </tr>
          <tr>
            <td width="37%"><p>Izinkan Perusahaan melihat online CV anda</p></td>
            <td width="63%">:
              <input type="radio" name="izin_lihat_cv_flag" id="radio" value="1" <?php if($OKandidat->row->izin_lihat_cv_flag == 1){ ?> checked="checked" <?php } ?>/>
              Ya
              <input type="radio" name="izin_lihat_cv_flag" id="radio2" value="0" <?php if($OKandidat->row->izin_lihat_cv_flag == 0){ ?> checked="checked" <?php } ?> />
              Tidak </td>
          </tr>
          <tr>
            <td>Izinkan Perusahaan melihat dokumen yang anda unggah</td>
            <td>:
              <input type="radio" name="izin_lihat_dokumen_flag" id="radio3" value="1" <?php if($OKandidat->row->izin_lihat_dokumen_flag == 1){ ?> checked="checked" <?php } ?>/>
              Ya
              <input type="radio" name="izin_lihat_dokumen_flag" id="radio4" value="0" <?php if($OKandidat->row->izin_lihat_dokumen_flag == 0){ ?> checked="checked" <?php } ?> />
              Tidak </td>
          </tr>
          <tr>
            <td>Terima Pemberitahuan Lewat Email </td>
            <td>:
              <input type="radio" name="newsletter_flag" id="radio5" value="1" <?php if($OKandidat->row->newsletter_flag == 1){ ?> checked="checked" <?php } ?>/>
              Ya
              <input type="radio" name="newsletter_flag" id="radio6" value="0" <?php if($OKandidat->row->newsletter_flag == 0){ ?> checked="checked" <?php } ?> />
              Tidak </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type="reset" name="reset" id="button2" value="Cancel"  class="btn_Gray"/>
              <input type="submit" name="button" id="button" value="Simpan"  class="btn_Orange"/></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </form>
    </div>          
    </li>
    <div class="clr"></div>
  </ul>
</div>