 <form enctype="multipart/form-data" class="frmInput" method="post" action="">
 
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3">Silahkan isi Form dibawah ini sesuai dengan Identitas anda, setelah selesai klik tombol Simpan </td>
    </tr>
  <tr>
    <td colspan="3">
      <table class="table table-condensed table-hover table-autoclone" id="pendidikan-table" style="width:100%;">
          <thead>
          <tr>
              <th width="20%">Tingkat Pendidikan</th>
              <th style="width:26%;text-align:center;">Waktu</th>
              <th>Nama Sekolah/ Perguruan Tinggi</th>
              <th>Jurusan/ Program Studi</th>
              <th>Nilai Rata-rata</th>
              <th></th>
          </tr>
          </thead>
          <tbody>
        <?php
          $edits = $O->get_pendidikan();
          foreach($edits as $r):
              extract(get_object_vars($r));
          ?>
          <tr>
              <td><?php
              echo OMaster_pendidikan::drop_down_select('edit_pendidikan_id['.$id.']',$pendidikan_id);
              ?></td>
              <td><?php
              $start_year = date("Y",strtotime("-80 year"));
              $end_year = date("Y");
              echo year_ddl('edit_pendidikan_tahun_awal['.$id.']',$tahun_awal,$start_year,$end_year,"style='width:50px'");
              echo " - ";
              echo year_ddl('edit_pendidikan_tahun_akhir['.$id.']',$tahun_akhir,$start_year,$end_year,"style='width:50px'");
              ?></td>
              <td><?php
              echo form_input('edit_pendidikan_nama['.$id.']',$nama,'size="60" class=""');
              ?></td>
              <td><?php
              echo form_input('edit_pendidikan_jurusan['.$id.']',$jurusan,'size="40" class=""');
              ?></td>
              <td><?php
              echo form_input('edit_pendidikan_nilai['.$id.']',$nilai,'size="5" maxlength="5" class="fixed"');
              ?></td>
              <td>
              <?php
              echo form_checkbox('edit_pendidikan_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
              ?>
              </td>
          </tr>
          <?php
          endforeach;
          ?>
          <tr>
              <td><?php
              echo OMaster_pendidikan::drop_down_select('pendidikan_id[]','');
              ?></td>
              <td><?php
          $start_year = date("Y",strtotime("-80 year"));
          $end_year = date("Y");
              echo year_ddl('pendidikan_tahun_awal[]',(intval($end_year)-3),$start_year,$end_year,"style='width:50px'");
          echo " - ";
          echo year_ddl('pendidikan_tahun_akhir[]',$end_year,$start_year,$end_year,"style='width:50px'");
              ?></td>
              <td><?php
              echo form_input('pendidikan_nama[]','','size="60" class=""');
              ?></td>
              <td><?php
              echo form_input('pendidikan_jurusan[]','','size="40" class=""');
              ?></td>
              <td><?php
              echo form_input('pendidikan_nilai[]','','size="5" maxlength="5" class="fixed"');
              ?></td>
              <td><a href="javascript:void(0)" class="btn_Red rmv_clone remove_row">X</a></td>
          </tr>
          </tbody>
          <tfoot>
              <tr>
                  <td colspan="6" align="right"><a href="javascript:void(0)" class="btn_blue rmv_clone add_row">+ Tambah </a></td>
              </tr>
          </tfoot>
      </table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <input type="reset" name="reset" id="button2" value="Cancel"  class="btn_Gray"/>      
      <input type="submit" name="submit_pendidikan" id="button" value="Simpan"  class="btn_Orange"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 </table>

 
 </form>