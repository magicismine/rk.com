<?php 
extract(get_object_vars($O->row));
?>
 <form enctype="multipart/form-data" class="frmInput" method="post">
 
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">Silahkan isi Form dibawah ini sesuai dengan Identitas anda, setelah selesai klik tombol Simpan </td>
    </tr>
  <tr>
    <td width="92px">&nbsp;</td>
    <td  >&nbsp;</td>
  </tr>
  <tr>
    <td>Nama Depan</td>
    <td>:
      <input type="text" name="nama_depan" id="textfield" value="<?php echo $nama_depan; ?>"></td>
  </tr>
  <tr>
    <td>Nama Belakang</td>
    <td>:
      <input type="text" name="nama_akhir" id="textfield" value="<?php echo $nama_akhir; ?>"></td>
  </tr>
  <tr>
    <td>Nama Panggilan</td>
    <td>:
      <input type="text" name="nama_panggilan" id="textfield2" value="<?php echo $nama_panggilan; ?>" ></td>
    </tr>
  <tr>
    <td>Agama</td>
    <td>:
      <?php echo OMaster_agama::drop_down_select('agama_id',$agama_id); ?>
    </td>
  </tr>
  <tr>
    <td>Nomer KTP </td>
    <td>:
      <input type="text" name="no_ktp" id="textfield4" value="<?php echo $no_ktp; ?>"></td>
    </tr>
  <tr>
    <td>Jenis Kelamin  </td>
    <td>: 
      <?php echo OMaster_jenis_kelamin::drop_down_select('jenis_kelamin_id',$jenis_kelamin_id); ?>
</td>
    </tr>
  <tr>
    <td> Tempat &amp; Tanggal Lahir</td>
    <td>:
      <input type="text" name="tempat_lahir" id="textfield5"  class="wd100" value="<?php echo $tempat_lahir; ?>" /> 
      -  
      <input type="text" name="tanggal_lahir" class="datepicker tgl_lahir" value="<?php echo get_date_lang($tanggal_lahir,"ID"); ?>">
    </td>
    </tr>
  <tr>
    <td>Upload Foto</td>
    <td>: 
        <?php
        if($O->id != FALSE)
        {
            echo "<img src='".$O->get_photo()."' />";
        }
        ?>
        <div style="display:block;" id="last-photo"></div>
        <div style="display:block;" id="preview-photo"></div>
        <div id="swfupload-control-photo" class="clear">
            <p>Upload maximum <span id="total_photos">1</span> image file(jpg, png, gif) and having maximum size of 1 MB</p>
            <input type="button" id="button-photo" />
            <p id="queuestatus-photo" ></p>
            <ol id="log-photo"></ol>
            <span class="set"></span> 
        </div>  
    </td>
  </tr>
  <tr>
    <td><strong> Alamat Asal </strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td> Jalan </td>
    <td>:
      <textarea name="asal_alamat" cols="60" rows="1" id="textfield6"><?php echo $asal_alamat; ?></textarea></td>
    </tr>
  <tr>
    <td> Provinsi </td>
    <td>:
      <?php echo OMaster_lokasi_propinsi::drop_down_select('asal_propinsi_id',$asal_propinsi_id); ?>
    </td>
  </tr>
  <tr>
    <td> Kabupaten /Kota </td>
    <td>:
      <span id="asal_kabupaten_wrap" data-value="<?php echo $asal_kabupaten_id?>" data-default="Pilih Kota/ Kabupaten"></span><?php //echo OMaster_lokasi_kabupaten::drop_down_select('asal_kabupaten_id',$asal_kabupaten_id); ?>
    </td>
  </tr>
  <tr>
    <td> Kecamatan </td>
    <td>:
      <input type="text" name="asal_kecamatan" value="<?php echo $asal_kecamatan; ?>" id="textfield10">
    </td>
    </tr>
  
  <tr>
    <td> Desa/Kelurahan </td>
    <td>:
      <input type="text" name="asal_kelurahan" value="<?php echo $asal_kelurahan; ?>" id="textfield10">
  </tr>
  <tr>
    <td> Telepon Rumah </td>
    <td>:
      <input type="text" name="asal_telepon" value="<?php echo $asal_telepon; ?>" id="textfield10">
  </tr>
  <tr>
    <td> No Handphone</td>
    <td>:
      <input type="text" name="no_hp" value="<?php echo $no_hp; ?>" id="textfield10">
  </tr>
  <tr>
    <td> RT/RW </td>
    <td>:
      <input name="asal_rt" type="text" value="<?php echo $asal_rt; ?>" id="textfield19" size="3" maxlength="3" class="wd30" />
      -
      <input name="asal_rw" type="text" value="<?php echo $asal_rw; ?>" id="textfield20" size="3" maxlength="3" class="wd30" /></td>
  </tr>
  <tr>
    <td colspan="2"><strong> Alamat Sekarang</strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>:
      <label for="skrg_asal_flag">
        <?php /* echo form_checkbox("skrg_asal_flag",1,(!empty($skrg_asal_flag) ? TRUE : FALSE),'class="fixed" id="skrg_asal_flag" data-target="#skrg_table"') */?>
        <input type="checkbox" name="skrg_asal_flag" value="1" class="fixed" id="skrg_asal_flag" data-target="#skrg_table" <?php if($skrg_asal_flag == 1){ echo "checked='checked'";} ?>>
        &nbsp;
        Sama dengan alamat asal.
      </label>  
  </td>
  </tr>
  </table>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="skrg_table">
    <tr>
      <td width="92px"> Jalan </td>
      <td>:
        <textarea name="skrg_alamat" cols="60" rows="2" id="textfield32"><?php echo $skrg_alamat; ?></textarea></td>
    </tr>
    <tr>
      <td> RT/RW </td>
      <td>:
        <input name="skrg_rt" value="<?php echo $skrg_rt; ?>"  type="text" id="textfield30" size="3" class="wd30">
        -
        <input name="skrg_rw" value="<?php echo $skrg_rw; ?>"  type="text" id="textfield31" size="3" class="wd30"></td>
    </tr>
    <tr>
      <td> Provinsi </td>
      <td>:
        <?php echo OMaster_lokasi_propinsi::drop_down_select('skrg_propinsi_id',$skrg_propinsi_id); ?>
      </td>
    </tr>
    <tr>
      <td> Kabupaten </td>
      <td>:
        <span id="skrg_kabupaten_wrap" data-value="<?php echo $skrg_kabupaten_id?>" data-default="Pilih Kota/ Kabupaten"></span><?php //echo OMaster_lokasi_kabupaten::drop_down_select('skrg_kabupaten_id',$skrg_kabupaten_id); ?></span>
    </td>
    <tr>
      <td> Kecamatan </td>
      <td>:
        <input type="text" name="skrg_kecamatan" id="textfield28" value="<?php echo $skrg_kecamatan; ?>"></td>
    </tr>
    </tr>
    <tr>
      <td> Desa/Kelurahan </td>
      <td>:
        <input type="text" name="skrg_kelurahan" id="textfield29" value="<?php echo $skrg_kelurahan; ?>"></td>
    </tr>
    <tr>
      <td> Telepon Rumah </td>
      <td>:
        <input type="text" name="skrg_telepon" id="textfield25" value="<?php echo $skrg_telepon; ?>"></td>
    </tr>
  </table>
  <table width="100%">
  <tr>
    <td width="25%">&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="reset" name="button2" id="button2" value="Cancel"  class="btn_Gray"/>      
      <input type="submit" name="submit_identitas" id="button" value="Simpan"  class="btn_Orange"/>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 </table>
 </form>
<style type="text/css" media="screen">
  .tgl_lahir{ width: 51% !important;}  
</style>

<script type="text/javascript" src="<?=base_url("_assets/js/swfupload/swfupload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/jquery.swfupload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/jquery.swfupload.init.js")?>"></script>
<script type="text/javascript">
$(function()
{
	var total_photos = $('#total_photos').text();
	jquery_upload_image("<?php echo base_url();?>","-photo",".set","image[]","#preview-photo","1 MB",total_photos);
	
	var total_photos_over = $('#total_photos_over').text();
	jquery_upload_image("<?php echo base_url()?>","-photo-over",".set_over","image_over[]","#preview-photo-over","1 MB",total_photos_over);
  
	get_kabupaten_ddl("asal_");
	get_kabupaten_ddl("skrg_");
	
	set_checkbox_flag('#skrg_asal_flag');
});
</script>
<style type="text/css" >
  #SWFUpload_Console { display:none; position:absolute; z-index:1000; bottom:0; right:10px; }
  #swfupload-control-photo p
  { margin:10px 5px; font-size:0.9em; }
  #log-photo
  { margin:0; padding:0; width:500px;}
  #log-photo li
  { list-style-position:inside; margin:2px; border:1px solid #ccc; padding:10px; font-size:12px; font-family:Arial, Helvetica, sans-serif; color:#333; background:#fff; position:relative;}
  #log-photo li .progressbar
  { border:1px solid #333; height:5px; background:#fff; }
  #log-photo li .progress
  { background:#999; width:0%; height:5px; }
  #log-photo li p
  { margin:0; line-height:18px; }
  #log-photo li.success
  { border:1px solid #339933; background:#ccf9b9; }
  #log-photo li span.cancel
  { position:absolute; top:5px; right:5px; width:20px; height:20px; background:url('<?=base_url("_assets/js/swfupload/cancel.png")?>') no-repeat; cursor:pointer; }
  
  .form_style .full { width:98%; }
  .error{color:#FF0000}
  
  .tbl_separate{}
  .tbl_separate tr{ vertical-align:top; }
  .tbl_separate tr td{ padding:5px; vertical-align:top; }
  
  .image_list{ list-style:none; float:left; }
  .image_list li{ float:left; margin: 0 10px 10px 0; }
  .form_right label input { vertical-align:top; }
  .form_right label img { vertical-align:top; }
</style>