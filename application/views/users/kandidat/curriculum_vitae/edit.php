<div class="content">
  <h1 class="titleContentPage">Kandidat</h1>
  <ul class="column">
	<?php echo $this->load->view($this->vpath.'/tpl_sidenav',NULL,TRUE);?>
    <li class="col rightpanel"><h3 class="titleContentRightPanel">Curriculum Vitae</h3>Buatlah CV anda dengan mengisi Form, mengupload Foto dan CV Word di bawah ini. Anda akan mudah ditemukan perusahaan sehingga anda akan cepat mendapatkan pekerjaan karena sesuai kualifikasi anda.
    <br>
      <div id="editcv-wrp">
        <ul class="column  tab-editcv">
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_identitas')?>" data-target="#identitas">Identitas</a></li>
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_kesehatan')?>" data-target="#kesehatan">Kesehatan</a></li>
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_keluarga')?>" data-target="#keluarga">Keluarga</a></li>
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_pendidikan')?>" data-target="#pendidikan">Pendidikan</a></li>
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_pengalaman_kerja')?>" data-target="#pengalamankerja">Pengalaman Kerja</a></li>
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_keterampilan')?>" data-target="#keterampilan">Keterampilan</a></li>
          <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_bahasa')?>" data-target="#bahasa">Bahasa</a></li>
           <li class="col"><a href="<?php echo site_url('kandidat/curriculum_vitae/ajax/get_dokumen')?>" data-target="#uploadcv">Dokumen</a></li>
          <div class="clr"></div>
        </ul>
        <div id="identitas" class="boxLightOrange"></div>
        <div id="kesehatan" class="boxLightOrange"></div>
        <div id="keluarga" class="boxLightOrange"></div>
        <div id="pendidikan" class="boxLightOrange"></div>
        <div id="pengalamankerja" class="boxLightOrange"></div>
        <div id="keterampilan" class="boxLightOrange"></div>
        <div id="uploadcv" class="boxLightOrange"></div>
        <div id="bahasa" class="boxLightOrange"></div>
      </div>
    </li>
    <div class="clr"></div>
  </ul>
</div>
<script>
$(function()
{
  get_kabupaten_ddl("asal_");
  get_kabupaten_ddl("skrg_");
  $('select#asal_propinsi_id').live('change', function(){
    get_kabupaten_ddl("asal_");
  });
  $('select#skrg_propinsi_id').live('change', function(){
    get_kabupaten_ddl("skrg_");
  });

  set_checkbox_flag('#skrg_asal_flag');
  $('#skrg_asal_flag').live('click', function ()
  {
    set_checkbox_flag( $(this) );
  });
    
  $('#keluarga-table .add_row').live('click', function(e)
  {
    TABLEROW.init('#keluarga-table');
    TABLEROW.add();
  });
  $('#keluarga-table .remove_row').live('click', function(e)
  {
    TABLEROW.init('#keluarga-table');
    TABLEROW.remove($(this));
  });
  
  $('#pendidikan-table .add_row').live('click', function(e)
  {
    TABLEROW.init('#pendidikan-table');
    TABLEROW.add();
  });
  $('#pendidikan-table .remove_row').live('click', function(e)
  {
    TABLEROW.init('#pendidikan-table');
    TABLEROW.remove($(this));
  });
  
  $('#pengalaman-table .add_row').live('click', function(e)
  {
    TABLEROW.init('#pengalaman-table');
    TABLEROW.add();
  });
  $('#pengalaman-table .remove_row').live('click', function(e)
  {
    TABLEROW.init('#pengalaman-table');
    TABLEROW.remove($(this));
  });

  $('#keterampilan-table .add_row').live('click', function(e)
  {
    TABLEROW.init('#keterampilan-table');
    TABLEROW.add();
  });
  $('#keterampilan-table .remove_row').live('click', function(e)
  {
    TABLEROW.init('#keterampilan-table');
    TABLEROW.remove($(this));
  });

  $('#bahasa-table .add_row').live('click', function(e)
  {
    TABLEROW.init('#bahasa-table');
    TABLEROW.add();
  });
  $('#bahasa-table .remove_row').live('click', function(e)
  {
    TABLEROW.init('#bahasa-table');
    TABLEROW.remove($(this));
  });

  $('#dokumen-table .add_row').live('click', function(e)
  {
    TABLEROW.init('#dokumen-table');
    TABLEROW.add();
  });
  $('#dokumen-table .remove_row').live('click', function(e)
  {
    TABLEROW.init('#dokumen-table');
    TABLEROW.remove($(this));
  });
  
});

function set_checkbox_flag(sel)
{
  var t = $(sel);
  var trgt = t.data('target');
  if(t.is(':checked') == true)
  {
    $(trgt).css('opacity', 0.4);
    $(trgt).find('input, select, textarea').attr('disabled', 'disabled');
  }
  else
  {
    $(trgt).css('opacity', 1);
    $(trgt).find('input, select, textarea').removeAttr('disabled');
  }
}
/*function get_kabupaten_ddl(prefix)
{
  var t = $('#'+prefix+'kabupaten_wrap');
  var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': $('#'+prefix+'kabupaten_wrap').data('value'), 'name': prefix+'kabupaten_id', 'default': '-- Pilih --'}
  $.ajax({
    'type': 'POST',
    'async': false,
    url: '<?=site_url("ajax/get_kabupaten_ddl")?>',
    data: p,
    complete: function(xhr, status)
    {
      var ret = xhr.responseText;
      t.html(ret);
    }
  });
}*/
</script>