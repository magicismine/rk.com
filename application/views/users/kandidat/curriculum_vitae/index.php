<?php 
extract(get_object_vars($O->row)); 
extract(get_object_vars($O->get_kesehatan()));

$OMJK = new OMaster_jenis_kelamin($O->row->jenis_kelamin_id);
  ($OMJK->id != "" ? ($a[0] = 1) : ($a[0] = 0));
($O->row->nama_panggilan != "" ? ($a[1] = 1) : ($a[1] = 0));

($O->row->tempat_lahir != "" ? ($a[2] = 1) : ($a[2] = 0));

($O->row->tanggal_lahir != "" ? ($a[3] = 1) : ($a[3] = 0));

$OMA = new OMaster_agama($O->row->agama_id);
($OMA->id != "" ? ($a[4] = 1) : ($a[4] = 0));

($O->row->no_ktp != "" ? ($a[5] = 1) : ($a[5] = 0));

($asal_alamat != "" ? ($a[6] = 1) : ($a[6] = 0));

($skrg_alamat != "" ? ($a[7] = 1) : ($a[7] = 0));

($asal_rt != "" ? ($a[8] = 1) : ($a[8] = 0));

($asal_rw != "" ? ($a[9] = 1) : ($a[9] = 0));

($asal_kelurahan != "" ? ($a[10] = 1) : ($a[10] = 0));

($skrg_kelurahan != "" ? ($a[11] = 1) : ($a[11] = 0));

($asal_kecamatan != "" ? ($a[12] = 1) : ($a[12] = 0));

($skrg_kecamatan != "" ? ($a[13] = 1) : ($a[13] = 0));

($asal_kabupaten_id != "" ? ($a[14] = 1) : ($a[14] = 0));

($skrg_kabupaten_id != "" ? ($a[15] = 1) : ($a[15] = 0));

($asal_propinsi_id != "" ? ($a[16] = 1) : ($a[16] = 0));

($skrg_propinsi_id != "" ? ($a[17] = 1) : ($a[17] = 0));

($asal_telepon != "" ? ($a[18] = 1) : ($a[18] = 0));

($skrg_telepon != "" ? ($a[19] = 1) : ($a[19] = 0));

($asal_hp != "" ? ($a[20] = 1) : ($a[20] = 0));

($skrg_hp != "" ? ($a[21] = 1) : ($a[21] = 0));

($tinggi != "" ? ($a[22] = 1) : ($a[22] = 0));

($berat != "" ? ($a[23] = 1) : ($a[23] = 0));

($gol_darah_id != "" ? ($a[24] = 1) : ($a[24] = 0));

($kacamata_flag != "" ? ($a[25] = 1) : ($a[25] = 0));

($keluhan_kesehatan != "" ? ($a[26] = 1) : ($a[26] = 0));

($status_kawin_id != "" ? ($a[27] = 1) : ($a[27] = 0));

$keluarga = $O->get_keluarga();
($keluarga != "" ? ($a[28] = 1) : ($a[28] = 0));

$pendidikan = $O->get_pendidikan();
($pendidikan != "" ? ($a[29] = 1) : ($a[29] = 0));
//var_dump($pendidikan);

$pengalaman = $O->get_pengalaman();
($pengalaman != "" ? ($a[30] = 1) : ($a[30] = 0));

$keterampilan = $O->get_keterampilan();
($keterampilan != "" ? ($a[31] = 1) : ($a[31] = 0));

$bahasa = $O->get_bahasa();
($bahasa != "" ? ($a[32] = 1) : ($a[32] = 0));

($skrg_rt != "" ? ($a[33] = 1) : ($a[33] = 0));

($skrg_rw != "" ? ($a[34] = 1) : ($a[34] = 0));

$OMKab = new OMaster_lokasi_kabupaten($asal_kabupaten_id);
  ($OMKab->id  != "" ? ($a[35] = 1) : ($a[35] = 0));

$OMKab1 = new OMaster_lokasi_kabupaten($skrg_kabupaten_id);
  ($OMKab1->id  != "" ? ($a[36] = 1) : ($a[36] = 0));

($O->row->nama_depan  != "" ? ($a[37] = 1) : ($a[37] = 0));

($O->row->foto  != "" ? ($a[38] = 1) : ($a[38] = 0));

$total_completed = count($a);

$total_terisi = 0;

foreach($a as $val)
{
  $total_terisi += $val;
}

$prosentase = intval(($total_terisi / $total_completed) * 100);
?>
<div class="content">
  <h1 class="titleContentPage">Kandidat</h1>
  <ul class="column">
	<?php echo $this->load->view($this->vpath.'/tpl_sidenav',NULL,TRUE);?>
    <li class="col rightpanel">
      <h3 class="titleContentRightPanel">Curriculum Vitae</h3>
      <ul class="column pinfo">
        <li class="col pinfo-pic-s"><img src="<?php echo $O->get_photo('square');  ?>" width="50" height="50"> </li>
        <li class="col pinfo-desc"><span class="pinfo-name"><?php echo $O->row->nama_depan." ".$O->row->nama_akhir; ?></span> <?php echo $this->cu->email; ?></li>
        <li class="col rght"><a href="<?php echo site_url('kandidat/curriculum_vitae/edit')?>" class="btnedit">Edit CV</a> <a href="<?php echo site_url('kandidat/export/to_pdf');?>" target="_blank" class="btnpdf">Save as Pdf</a></li>
        <div class="clr"></div>
      </ul>
      <!-- <span class="pinfo-sk">IT - Software / Network > Programmer</span> -->
      <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
        <?php ///* ?>
        <tr class="noborder">
          <td height="30">&nbsp;</td>
          <td>&nbsp;</td>
          <td colspan="2">
            <div class="csc"> 
              <span style="width:<?php echo $prosentase; ?>%">CV Completed <?php echo $prosentase; ?>%</span>               
            </div>
          </td>
        </tr>
        <?php //*/ ?>
        <tr>
          <td width="25%">Jenis Kelamin</td>
          <td width="">: 
            <?php  
              echo $OMJK->row->nama;
              unset($OMJK);
            ?>
          </td>
          <td width="25%">Nama Panggilan </td>
          <td width=""> : <?php echo $O->row->nama_panggilan; ?></td>
        </tr>
        <tr>
          <td>Tempat &amp; Tanggal Lahir </td>
          <td>: <?php echo $O->row->tempat_lahir; ?>/<?=parse_date($O->row->tanggal_lahir,"d F Y","ID","long")?></td>
          <td>Agama </td>
          <td>:  
              <?php  
              echo $OMA->row->nama;
              unset($OMA);
            ?>
          </td>
        </tr>
        <tr>
          <td class="noborder">&nbsp;</td>
          <td class="noborder">&nbsp;</td>
          <td>Nomer KTP <br></td>
          <td> : <?php echo $O->row->no_ktp;?></td>
        </tr>
        <tr>
          <td colspan="4" class="noborder">&nbsp;</td>
        </tr>
        <tr>
          <td><strong  class="pinfo-sp"> Alamat Asal </strong></td>
          <td>&nbsp;</td>
          <td><strong  class="pinfo-sp"> Alamat Sekarang </strong></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Jalan</td>
          <td>: <?php echo $asal_alamat; ?></td>
          <td> Jalan </td>
          <td>: <?php echo ($skrg_alamat == "" ? "-" : $skrg_alamat); ?></td>
        </tr>
        <tr>
          <td>RT/RW </td>
          <td>: <?php echo $asal_rt; ?>/<?php echo $asal_rw; ?></td>
          <td>RT/RW </td>
          <td>: <?php echo ($skrg_rt == "" ? "-" : $skrg_rt); ?>/<?php echo ($skrg_rw == "" ? "-" : $skrg_rw); ?></td>
        </tr>
        <tr>
          <td>Desa/Kelurahan </td>
          <td>: <?php echo $asal_kelurahan; ?></td>
          <td>Desa/Kelurahan </td>
          <td>: <?php echo ($skrg_kelurahan  == "" ? "-" : $skrg_kelurahan); ?></td>
        </tr>
        <tr>
          <td>Kecamatan</td>
          <td>: <?php echo $asal_kecamatan ?></td>
          <td>Kecamatan </td>
          <td>: 
            <?php echo ($skrg_kecamatan  == "" ? "-" : $skrg_kecamatan); ?></td>
        </tr>
        <tr>
          <td>Kabupaten</td>
          <td> : 
            <?php 
            if($asal_kabupaten_id != "")
            {
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?>
          </td>
          <td>Kabupaten </td>
          <td> : 
            <?php 
            if($skrg_kabupaten_id != "")
            {
              
              echo $OMKab1->row->nama;
              unset($OMKab1);
            }else
            {
              echo "-";
            }
            ?>
          </td>
        </tr>
        <tr>
          <td>Provinsi</td>
          <td>: 
            <?php 
            if($asal_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($asal_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?>
          </td>
          <td>Provinsi </td>
          <td>: 
            <?php 
            if($skrg_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($skrg_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?>
          </td>
        </tr>
        <tr>
          <td>Telepon Rumah</td>
          <td>: <?php echo $asal_telepon; ?></td>
          <td>Telepon Rumah</td>
          <td>: <?php echo ($skrg_telepon  == "" ? "-" : $skrg_telepon); ?></td>
        </tr>
        <tr class="noborder">
          <td  >Nomer HP</td>
          <td>: <?php echo $no_hp ?> </td>
          <td>Nomer HP </td>
          <td>: <?php echo ($skrg_hp  == "" ? "-" : $skrg_hp); ?> </td>
        </tr>
      </table>
      <br>
      <div class="grayBox"> <span class='gb-title'>Kesehatan</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%">Tinggi/Berat Badan </td>
            <td width="75%"> : <?php echo $tinggi; ?> cm/<?php echo $berat; ?> kg</td>
          </tr>
          <tr>
            <td>Golongan Darah </td>
            <td>: <?php echo $gol_darah = new OMaster_gol_darah($gol_darah_id); echo $gol_darah->row->nama;unset($gol_darah); ?></td>
          </tr>
          <tr>
            <td>Kacamata </td>
            <td> : <?php echo (empty($kacamata_flag) ? "Tidak" : "Iya"); ?></td>
          </tr>
          <tr>
            <td>Keluhan Kesehatan </td>
            <td> : <?php echo ($keluhan_kesehatan != "" ? $keluhan_kesehatan : "-"); ?></td>
          </tr>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Keluarga</span> Status Perkawinan  : <?php echo $status_kawin = new OMaster_status_kawin($status_kawin_id); echo $status_kawin->row->nama; unset($status_kawin); ?><br>
        <br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="7%" align="center">#</td>
              <td width="23%">Anggota Keluarga</td>
              <td width="50%">Nama</td>
              <td width="20%">Pendidikan </td>
            </tr>
          </thead>
          <?php  
          //$keluarga = $O->get_keluarga();
          $i = 1;
          foreach($keluarga as $kel)
          {
          ?>
          <tr>
            <td align="center"><?php echo $i; ?></td>
            <td><?php $shdk = new OMaster_shdk($kel->shdk_id); echo $shdk->row->nama; unset($shdk); ?></td>
            <td><?php echo $kel->nama; ?></td>
            <td><?php $master_pendidikan = new OMaster_pendidikan($kel->pendidikan_id); echo $master_pendidikan->row->nama; unset($master_pendidikan); ?></td>
          </tr>
          <?php  
          $i++;
          }
          ?>
        
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Pendidikan</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="6%" align="center">#</td>
              <td width="24%" align="center">Tingkat Pendidikan</td>
              <td width="41%">Jurusan/ Program Studi</td>
              <td width="20%">Nama Sekolah/ Universitas</td>
              <td width="9%">Nilai Rata-Rata</td>
            </tr>
          </thead>
          <?php  
          //$pendidikan = $O->get_pendidikan();
          $i = 1;
          foreach($pendidikan as $pend)
          {
          ?>
          <tr>
            <td align="center"><?php echo $i; ?></td>
            <td align="center"><?php $master_pendidikan = new OMaster_pendidikan($pend->pendidikan_id); echo $master_pendidikan->row->nama; unset($master_pendidikan); ?></td>
            <td><?php echo $pend->jurusan; ?></td>
            <td><?php echo $pend->nama;?></td>
            <td><?php echo $pend->nilai; ?></td>
          </tr>
          <?php  
          $i++;
          }
          ?>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Pengalaman Kerja</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="15%" align="center">Waktu</td>
              <td width="26%">Perusahaan</td>
              <td width="28%">Bidang Usaha</td>
              <td width="19%">Jabatan</td>
              <!-- <td width="12%">Salary</td> -->
            </tr>
          </thead>
          <?php  
          //$pengalaman = $O->get_pengalaman();
          foreach($pengalaman as $peng)
          {
          ?>
          <tr>
            <td align="center"><?php echo get_only_date($peng->tanggal_awal,"year"); ?>-<?php echo get_only_date($peng->tanggal_akhir,"year"); ?></td>
            <td><?php echo $peng->perusahaan; ?></td>
            <td><?php $bidang_usaha = new OMaster_bidang_usaha($peng->bidang_usaha_id); echo $bidang_usaha->row->nama; unset($bidang_usaha);?></td>
            <td><?php echo $peng->jabatan; ?></td>
            <!-- <td align="right">Rp. 500.000</td> -->
          </tr>
          <?php  
          }
          ?>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Keahlian Khusus</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="4%" align="center">#</td>
              <td width="62%">Bidang Keahlian </td>
              <td width="10%" align="center">Skala (5)</td>
              <td width="24%">Badan Sertifikasi</td>
            </tr>
          </thead>
          <?php  
          //$keterampilan = $O->get_keterampilan();
          $i=1;
          foreach($keterampilan as $ket)
          {
          ?>
          <tr>
            <td align="center"><?php echo $i; ?></td>
            <td><?php echo $kategori = new OMaster_kategori($ket->kategori_id); echo $kategori->row->nama; unset($kategori); ?></td>
            <td align="center"><?php echo $ket->nilai_skala; ?></td>
            <td><?php echo $ket->sertifikasi; ?></td>
          </tr>
          <?php  
          $i++;
          }
          ?>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Penguasaan Bahasa</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="4%" align="center">#</td>
              <td width="62%">Bahasa</td>
              <td width="10%" align="center">Skala (5)</td>
              <td width="24%">Badan Sertifikasi</td>
            </tr>
          </thead>
          <?php 
          $i=1;
          //$bahasa = $O->get_bahasa();
          foreach($bahasa as $bhs){
          ?>
          <tr>
            <td align="center"><?php echo $i; ?></td>
            <td><?php $master_bahasa = new OMaster_bahasa($bhs->bahasa_id); echo $master_bahasa->row->nama; unset($master_bahasa);?></td>
            <td align="center"><?php echo $bhs->nilai_skala; ?></td>
            <td><?php echo $bhs->sertifikasi; ?></td>
          </tr>
          <?php 
          $i++;
          } 
          ?>
        </table>
      </div>
      <br>
    </li>
    <div class="clr"></div>
  </ul>
</div>