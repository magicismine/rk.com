<?php 
extract(get_object_vars($O->row)); 
?>

 <form enctype="multipart/form-data" class="frmInput" action="" method="post">
 
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">Silahkan isi Form dibawah ini sesuai dengan Identitas anda, setelah selesai klik tombol Simpan </td>
    </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td  >&nbsp;</td>
  </tr>
  <tr>
    <td>Status Pernikahan</td>
    <td>:
      <?php echo OMaster_status_kawin::radios('status_kawin_id',$status_kawin_id,"onchange='update_status_kawin()'","&nbsp;") ?>
    </td>
  </tr>
  <tr>
    <td colspan="2"><hr  class="spacer"/>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><strong>Anggota Keluarga Serumah</strong></td>
          <td width="28%">&nbsp;</td>
          <td width="24%">&nbsp;</td>
          <td width="15%">&nbsp;</td>
          <td width="2%">&nbsp;</td>
        </tr>
        <tr>
          <td width="31%">Nama Pasangan Suami/Istri</td>
          <td colspan="2">:
          <input type="text" name="nama_pasangan" id="nama_pasangan" class="nama_pasangan" value="<?php echo $nama_pasangan; ?>" /></td>
          <td colspan="2">&nbsp;</td>
          </tr>
      </table>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table-autoclone" id="keluarga-table">
        <thead>
        <tr>
          <th>Nama</th>
          <th>Jenis Kelamin</th>
          <th>Hubungan</th>
          <th>Pendidikan</th>
          <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>        
        <?php
        $edits = $O->get_keluarga();
        foreach($edits as $r):
        extract(get_object_vars($r));
        ?>
        <tr>
            <td><?php
            echo form_input('edit_keluarga_nama['.$id.']',$nama,'class="fixed"');
            ?></td>
            <td><?php
            echo OMaster_jenis_kelamin::drop_down_select('edit_keluarga_jenis_kelamin_id['.$id.']',$jenis_kelamin_id);
            ?></td>
            <td><?php
            echo OMaster_shdk::drop_down_select('edit_keluarga_shdk_id['.$id.']',$shdk_id);
            ?></td>
            <td><?php
            echo OMaster_pendidikan::drop_down_select('edit_keluarga_pendidikan_id['.$id.']',$pendidikan_id);
            ?></td>
            <td>
            <?php
            echo form_checkbox('edit_keluarga_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
            ?>
            </td>
        </tr>
        <?php
        endforeach;
        ?>
        <tr>
            <td><?php
            echo form_input('keluarga_nama[]',$keluarga_nama,'class="fixed"');
            ?></td>
            <td><?php
            echo OMaster_jenis_kelamin::drop_down_select('keluarga_jenis_kelamin_id[]',$keluarga_jenis_kelamin_id);
            ?></td>
            <td><?php
            echo OMaster_shdk::drop_down_select('keluarga_shdk_id[]',$keluarga_shdk_id);
            ?></td>
            <td><?php
            echo OMaster_pendidikan::drop_down_select('keluarga_pendidikan_id[]',$keluarga_pendidikan_id);
            ?></td>
            <td align="center"><a href="javascript:void(0)" class="btn_Red rmv_clone remove_row">X</a></td>
        </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="5" style="text-align:right;"><a href="javascript:void(0)" class="btn_blue rmv_clone add_row">+ Tambah </a></td>
          </tr>
        </tfoot>
      </table>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <input type="submit" name="reset" id="button2" value="Cancel"  class="btn_Gray"/>      
      <input type="submit" name="submit_keluarga" id="button" value="Simpan"  class="btn_Orange"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 </table>

 
 </form>
 <script>
    /*var selval = $('input[name=status_kawin_id]:checked').val();
    var status_kawin = function () {
    if (selval == 2) {
        $('#nama_pasangan').prop('disabled', false);

    }
    else {
        $('#nama_pasangan').prop('disabled', 'disabled');
    }
  };
  $(status_kawin);
  $("input[name=status_kawin_id]").change(status_kawin);*/

  function update_status_kawin()
  {
    $('#form-add').find('input[type="text"], input[type="email"],select').attr('disabled','disabled');
    $('#').attr('disabled','disabled');
    var selval = $('input[name=status_kawin_id]:checked').val();
    if (selval == 2) {
          $('#nama_pasangan').prop('disabled', false);
           $('#nama_pasangan').val("");
      }
      else {
          $('#nama_pasangan').prop('disabled', 'disabled');
          $('#nama_pasangan').val("");
      }
  }
</script>