 <form enctype="multipart/form-data" class="frmInput" method="post">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">Silahkan isi Form dibawah ini sesuai dengan Identitas anda, setelah selesai klik tombol Simpan </td>
  </tr>
  <tr>
    <td colspan="2">
      <?php
      $arr = array("" => "-- Pilih --", "1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5);
      ?>
      <table class="table table-condensed table-hover table-autoclone" id="keterampilan-table">
          <thead>
          <tr>
              <th>Kategori Bidang Keahlian</th>
              <th>Nilai Keahlian (skala 5)</th>
              <th>Badan Sertifikasi</th>
              <th></th>
          </tr>
          </thead>
          <tbody>
        <?php
          $edits = $O->get_keterampilan();
          foreach($edits as $r):
              extract(get_object_vars($r));
          ?>
          <tr>
              <td><?php
              echo OMaster_kategori::drop_down_tree_select('edit_keterampilan_kategori_id['.$id.']',$kategori_id,'','-- Pilih --');
              ?></td>
              <td><?php
              echo form_dropdown('edit_keterampilan_nilai_skala['.$id.']',$arr,$nilai_skala,"class='fixed'"," &nbsp; ");
              ?></td>
              <td><?php
              echo form_input('edit_keterampilan_sertifikasi['.$id.']',$sertifikasi,'size="60"');
              ?></td>
              <td>
              <?php
              echo form_checkbox('edit_keterampilan_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
              ?>
              </td>
          </tr>
          <?php
          endforeach;
          ?>
          <tr>
              <td><?php
              echo OMaster_kategori::drop_down_select('keterampilan_kategori_id[]','','','-- Pilih --');
              ?></td>
              <td><?php
              echo form_dropdown('keterampilan_nilai_skala[]',$arr,'',"class='fixed'"," &nbsp; ");
              ?></td>
              <td><?php
              echo form_input('keterampilan_sertifikasi[]','','size="60"');
              ?></td>
              <td><a href="javascript:void(0)" class="btn_Red rmv_clone remove_row">X</a></td>
          </tr>
          </tbody>
          <tfoot>
          <tr>
              <td colspan="5" class="text-right" align="right">
                <a href="javascript:void(0)" class="btn_blue rmv_clone add_row">+ Tambah </a>
              </td>
          </tr>
          </tfoot>
      </table>
    </td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <input type="reset" name="button2" id="button2" value="Cancel"  class="btn_Gray"/>      
      <input type="submit" name="submit_keterampilan" id="button" value="Simpan"  class="btn_Orange"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 </table>

 
 </form>