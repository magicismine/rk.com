 <form enctype="multipart/form-data" class="frmInput" method="post">
 
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">Silakan upload dokumen pendukung Anda. Setiap dokumen Anda akan disampaikan kepada perusahaan yang berpotensi ketika anda mengisi 
      lowongan pekerjaan. Dokumen yang anda upload  dapat menggunakan format berikut: .doc, .docx, .pdf, .rtf</td>
    </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td  >&nbsp;</td>
  </tr>
  <tr>
    <td>Upload Dokumen Pendukung</td>
    <td>:
      <?php
        echo form_upload('dokumen[]','','');
      ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input type="reset" name="button2" id="button2" value="Cancel"  class="btn_Gray"/>
      <input type="submit" name="submit_dokumen" id="button" value="Simpan"  class="btn_Orange"/></td>
  </tr>
  <tr>
    <td colspan="2"><hr  class="spacer"/>
      <table width="100%" border="0" cellspacing="0" cellpadding="0" >
        <tr>
          <td width="62%"><strong>Daftar Dokumen</strong></td>
          <td width="8%">&nbsp;</td>
          <td width="15%">&nbsp;</td>
          <td width="15%">&nbsp;</td>
          </tr>
        <tr>
          <td>Nama File</td>
          <td align="center">Type</td>          
          <td>&nbsp;</td>
        </tr>
        <?php  
        $edits = $O->get_dokumen();
        foreach($edits as $r):
          extract(get_object_vars($r));
          $extension = end(explode(".",$dokumen));
        ?>      
        <tr>
          <td><a href="<?php echo base_url('_assets/files/'.$dokumen);?>"> -> <?php echo $dokumen; ?> </a></td>
          <td align="center"><?php echo $extension; ?></td>
          <td>
            <?php if($aktif_flag == 1){ ?>
              <a href="<?php echo site_url($this->curpage."/set_dokumen/0/".intval($id)); ?>" class="btn_blue">AKTIF </a>
            <?php }else{ ?>
              <a href="<?php echo site_url($this->curpage."/set_dokumen/1/".intval($id)); ?>" class="btn_Gray">AKTIFKAN </a>
            <?php } ?>
          </td>
        </tr>
        <?php
          endforeach;
        
        ?>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td align="right"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 </table>

 
 </form>