 <form enctype="multipart/form-data" class="frmInput" method="post">
 
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="2">Silahkan isi Form dibawah ini sesuai dengan Identitas anda, setelah selesai klik tombol Simpan </td>
    </tr>
  <tr>
    <td width="25%">&nbsp;</td>
    <td  >&nbsp;</td>
  </tr>
  <td colspan="2">
  <table class="table table-condensed table-hover table-autoclone" id="pengalaman-table">
    <thead>
    <tr>
        <th width="30%">Waktu</th>
        <th>Perusahaan</th>
        <th>Bidang Usaha</th>
        <th>Jabatan</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
  <?php
    $edits = $O->get_pengalaman();
    foreach($edits as $r):
        extract(get_object_vars($r));
    ?>
    <tr>
        <td><?php
    echo form_input('edit_pengalaman_tanggal_awal['.$id.']',get_date_lang($tanggal_awal,"ID"),'size="10" maxlength="10" class="datepicker_dob" style="width:56px"');
    echo " - ";
    echo form_input('edit_pengalaman_tanggal_akhir['.$id.']',get_date_lang($tanggal_akhir,"ID"),'size="10" maxlength="10" class="datepicker_dob" style="width:56px"');
        ?></td>
        <td><?php
        echo form_input('edit_pengalaman_perusahaan['.$id.']',$perusahaan,'size="50" class=""');
        ?></td>
        <td><?php
        echo OMaster_bidang_usaha::drop_down_select('edit_pengalaman_bidang_usaha_id['.$id.']',$bidang_usaha_id);
        ?></td>
        <td><?php
        echo form_input('edit_pengalaman_jabatan['.$id.']',$jabatan,'size="40" class="fixed"');
        ?></td>
        <td>
        <?php
        echo form_checkbox('edit_pengalaman_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
        ?>
        </td>
    </tr>
    <?php
    endforeach;
    ?>
    <tr>
        <td><?php
    
    echo form_input('pengalaman_tanggal_awal[]','','size="10" maxlength="10" class="datepicker_dob" style="width:56px"');
    echo " - ";
    echo form_input('pengalaman_tanggal_akhir[]','','size="10" maxlength="10" class="datepicker_dob" style="width:56px"');
        ?></td>
        <td><?php
        echo form_input('pengalaman_perusahaan[]','','size="50" class=""');
        ?></td>
        <td><?php
        echo OMaster_bidang_usaha::drop_down_select('pengalaman_bidang_usaha_id[]','','','-- Pilih --');
        ?></td>
        <td><?php
        echo form_input('pengalaman_jabatan[]','','size="40" class="fixed"');
        ?></td>
        <td><a href="javascript:void(0)" class="btn_Red rmv_clone remove_row">X</a></td>
    </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-right" align="right">
              <a href="javascript:void(0)" class="btn_blue rmv_clone add_row">+ Tambah </a>
            </td>
        </tr>
    </tfoot>
</table>
  </td>
  <td></td>
</tr>
 <tr>
    
    <td colspan="3" align="center"><input type="reset" name="button2" id="button2" value="Cancel"  class="btn_Gray"/>      <input type="submit" name="submit_pengalaman_kerja" id="button" value="Simpan"  class="btn_Orange"/></td><td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
 </table>

 
 </form>