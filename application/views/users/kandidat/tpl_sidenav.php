<?php
$uri = $this->uri->segment(2);
$cururi = $this->uri->segment(1);
?><li class="col leftpanel">
<div class="wrp-hatOrange ">
  <div  class="hatOrange"> Menu's<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
  <div class="ho-content grayDark">
    <div class="ho-spacer"></div>
    <ul class="leftmenu">
      <li<?php echo ($uri == "" || $uri == "home" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi, "Halaman Utama"); ?></li>
      <li<?php echo ($uri == "resume" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/resume", "Surat Lamaran"); ?></li> 
      <li<?php echo ($uri == "curriculum_vitae" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/curriculum_vitae", "Curriculum Vitae"); ?></li> 
      <li<?php echo ($uri == "melamar_otomatis" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/melamar_otomatis", "Melamar Otomatis"); ?></li> 
      <li<?php echo ($uri == "lowongan_terbaru" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/lowongan_terbaru", "Lowongan Terbaru"); ?></li> 
      <li<?php echo ($uri == "lowongan_dilamar" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/lowongan_dilamar", "Lowongan Dilamar"); ?></li> 
      <li<?php echo ($uri == "lowongan_disimpan" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/lowongan_disimpan", "Lowongan Disimpan"); ?></li>
      <?php /* ?>
      <li<?php echo ($uri == "email_alert" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/email_alert", "Email Alert"); ?></li>
      <?php */ ?>
      <li<?php echo ($uri == "akun" ? ' class="lm-active"' : ""); ?>><?php echo anchor($cururi."/akun", "Pengaturan Akun"); ?></li>
      <li><?php echo anchor($cururi."/home/logout", "Keluar"); ?></li>
    </ul>
  </div>
</div>
<br>
<?php  
  //$OAd = new OAd(6);
  $OAd = new OAd;
  $list_ads = OAd::get_list(0,0,"id ASC","active_flag = 1 AND photo <> '' AND position = 'user_sidebar_left' AND (show_on = 'both' OR show_on = 'kandidat')");
  $total_ads = get_db_total_rows();
  if($total_ads > 0)
  {
?>
  <?php  
  foreach($list_ads as $ads)
  {
    $OAd->setup($ads);
    $photo = $OAd->get_photo_original();
  ?>
  <a href="<?php echo $OAd->row->url; ?>">
    <img class="lazy" src ="<?php echo $OAd->get_photo_original();?>" data-original="<?php echo $OAd->get_photo_original();?>" alt="<?php echo $OAd->row->nama; ?>" title="<?php echo $OAd->row->nama; ?>" 
      width="<?php echo $OAd->row->width; ?>" height="<?php echo $OAd->row->height; ?>" style="max-width:276px;" />
  </a>
  <?php } ?>
<?php }else{  ?>
  <img class="lazy" src ="<?php echo base_url(); ?>_assets/img/gray.gif" data-original="<?php echo base_url(); ?>_assets/img/dummy7.jpg"> 
<?php } ?>
</li>
