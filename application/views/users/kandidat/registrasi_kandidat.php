<?php extract($_POST); ?>
<div class="content">
  <h1 class="titleContentPage">Daftar Sebagai Pencari Kerja</h1>
  <?php echo ($error_code == "" ? "" : "<p class='text-red'>{$error_code}</p>"); ?>
  <?php 
  if(validation_errors() != ""): ?>
  <div class="validation_error">
    <?php echo validation_errors(); ?>
  </div>
  <?php endif; ?>
  <form enctype="multipart/form-data" class="registerForm" method="post" action="<?php echo site_url('account/register/kandidat') ?>">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="3"><span class="f14 forange">Login Detail</span><br>Silakan masukkan alamat email Anda dan kemudian pilih password. Anda akan membutuhkan informasi ini setiap Anda masuk pada akun Anda.
        </td>
      </tr>
      <tr>
        <td width="27%">Email Address </td>
        <td width="44%">
          <input type="email" name="email" id="textfield3" value="<?php echo set_value('email'); ?>" required>
          <br>
          <?php echo form_error('email'); ?>
        </td>
        <td width="29%">&nbsp;</td>
      </tr>
      <tr>
        <td>Password</td>
        <td>
          <input type="password" name="password" id="password" required >
          <br>
          <?php echo form_error('password'); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Confirm Password</td>
        <td>
          <input type="password" name="conf_password" id="conf_password" required >
          <br>
          <?php echo form_error('conf_password'); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="3"><hr class="line"></td>
      </tr>
      <tr>
        <td>Nama Depan</td>
        <td>
          <input type="text" name="nama_depan" id="textfield4" value="<?php echo set_value('nama_depan'); ?>" required>
          <br>
          <?php echo form_error('nama_depan'); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Nama Akhir</td>
        <td><input type="text" name="nama_akhir" value="<?php echo $nama_akhir?>" id="textfield5"></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>Pengalaman Bekerja</td>
        <td>
          <?php echo OMaster_level_karir::drop_down_select('pengalaman_id',$pengalaman_id); ?>
        </td>
        <td>&nbsp;</td>
      </tr>
      <?php //* ?>
      <tr>
        <td colspan="3"><hr class="line"></td>
      </tr>
      
      <tr>
        <td colspan="3"><input type="checkbox" name="newsletter_flag" value="1" id="checkbox" <?php if($newsletter_flag == 1){ echo "checked='checked'";} ?>>
        Silakan tandai kotak ini jika Anda menghendaki untuk menerima berita terakhir dan informasi melalui email.</td>
      </tr>
      <?php //*/ ?>
      <tr>
        <td colspan="3"><hr class="line"></td>
      </tr>
      <?php //* ?>
      <tr>
        <td colspan="3"><span class="f14 forange">Spam Prevention</span><br>
        Silakan masukkan kode yang muncul pada gambar:</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">
          <?php echo $captcha; ?>
          <?php echo form_error('recaptcha_response_field'); ?>
          <?php echo ($error_code == "" ? "" : "<p class='text-red'>{$error_code}</p>"); ?>
        </td>
      </tr>
      <?php /* ?>
      <tr>
        <td>Kode Gambar</td>
        <td><input type="text" name="captcha" id="textfield9"></td>
        <td>&nbsp;</td>
      </tr>
      <?php //*/ ?>
      <tr>
        <td colspan="3">
          <label>
            <input type="checkbox" name="term" id="term" value="1" <?php if($term == 1){ echo "checked='checked'";} ?>>
            Silakan Cek pada Kotak untuk mengindakasi bahwa Anda menerima syarat &amp; kondisi website ini.
            <br /><?php echo form_error('term'); ?>
          </label>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2" align="right">
          <input name="button2" type="reset" class="btn_Gray" id="button2" value="Batal">
          <input name="button" type="submit" class="btn_Orange" id="button" value="Daftar"></td>
      </tr>
    </table>
  </form>
</div>