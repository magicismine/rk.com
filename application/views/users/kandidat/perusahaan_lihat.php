<div class="content">
<h1 class="titleContentPage">Kandidat</h1>
<ul class="column">
	<?php echo $this->load->view($this->vpath."/tpl_sidenav", NULL, TRUE)?>
	<li class="col rightpanel">

	<div align="right">
        <strong><?php echo $total_perusahaan; ?></strong> Perusahaan telah melihat CV Online anda.<br>
	</div><br>

    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
    <thead>
    <tr>
        <td width="8%" align="center" valign="middle">Expired </td>
        <td width="49%" valign="middle">Deskripsi</td>
        <td width="25%" align="left" valign="middle">Bidang Usaha</td>
        <td width="18%" align="center" valign="middle">Lokasi</td>
    </tr>
    </thead>
    <tbody>
    <?php
	$OL = new OLowongan();
	foreach($low_list as $r):
		$OL->setup($r);
		extract(get_object_vars($r));
		$OTmp = new OMaster_bidang_usaha($bidang_usaha_id);
		$bidang_usaha = $OTmp->row->nama;
		unset($OTmp);
	?>
    <tr>
        <td align="right" valign="middle"><?php echo parse_date($dt_expired, "d M Y", "ID", "short"); ?></td>
        <td valign="middle"><?php echo anchor($OL->get_link(), $nama)?><br>
        <div class="pinf"><label><span><?php echo $hits; ?></span> Dilihat </label> <label><span><?php echo $OL->get_recom_kandidats(); ?></span> Rekomendasi Kandidat </label></div>
        </td>
        <td align="left" valign="middle"><?php echo $bidang_usaha; ?></td>
        <td align="center" valign="middle"><?php echo $lokasi_kawasan; ?></td>
    </tr>
    <?php
	endforeach;
	unset($OL);
	?>
    </tbody>
    </table>
    
    <div id="paging_button" align="center">
    <?php echo $pagination; ?>
    <?php /*?><ul>
    <li>PREV</li>
    <li class="active">1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
    <li>...</li>
    <li>323</li>
    <li>NEXT</li>
    <div class="clr"></div>
    </ul><?php */?>
    </div>
    </li>
    <div class="clr"></div>
</ul>
</div>
