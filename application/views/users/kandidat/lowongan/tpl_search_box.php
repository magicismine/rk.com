<?php
extract($_GET);
?>
<form action="" method="get" class="MainSearch" >
<ul class="column">
  <li class="col"  >
    <input type="text" style="width:385px" name="keywords" placeholder="Kata Kunci" />
  </li>
  <li class="col"  >
    <input id="date-range1" style="width:150px" name="date" placeholder="Date" class="datepicker" type="text" />
  </li>
  <div class="clr"></div>
</ul>
<!-- <ul class="column detailSearch  dsopen"> default open -->
<ul class="column detailSearch ">
  <li class="col"  >
    <?php echo OMaster_Kategori::drop_down_select("kategori_id",$kategori_id,'style="width:270px"','Semua Kategori Pekerjaan')?>
  </li>
  <li class="col"  >
    <?php echo OMaster_bidang_usaha::drop_down_select("bidang_usaha_id",$bidang_usaha_id,'style="width:265px"','Semua Bidang Usaha')?>
  </li>
  <li class="col"  >
    <?php echo OMaster_lokasi_negara::drop_down_select("lokasi_negara_id",$lokasi_negara_id,'style="width:150px"','Semua Negara')?>
  </li>
  <li class="col"  >
    <?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id",$lokasi_propinsi_id,'style="width:150px"','Semua Propinsi')?>
  </li>
  <li class="col"  >
    <div id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>">
		<?php echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id,'style="width:210px"','Semua Lokasi Kota dan Kabupaten')?>
    </div>
  </li>
  <?php /*?><li class="col"  >
    <?php echo OMaster_lokasi_kawasan::drop_down_select("kawasan_id",$kawasan_id,'style="width:150px"','Semua Kawasan')?>
  </li><?php */?>
  <li class="col"  >
    <?php echo OMaster_level_karir::drop_down_select("level_karir_id",$level_karir_id,'style="width:150px"','Semua Level Karir')?>
  </li>
  <li class="col"  >
    <?php echo OMaster_waktu_kerja::drop_down_select("waktu_kerja_id",$waktu_kerja_id,'style="width:150px"','Semua Waktu Kerja')?>
  </li>
  <div class="clr"></div>
</ul>
<div align="right"> <span class="tg-detail ">Pencarian Detail</span>
  <input type="submit" value="Cari" class="btn_blue" style="margin-left:10px">
</div>
</form>
<script>
$(function()
{
	get_kabupaten_ddl("lokasi_");
	$(document).on('change', 'select#lokasi_propinsi_id', function(e){
		get_kabupaten_ddl("lokasi_");
	});
});

function get_kabupaten_ddl(prefix)
{
	var t = $('#'+prefix+'kabupaten_wrap');
	var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': $('#'+prefix+'kabupaten_wrap').data('value'), 'name': prefix+'kabupaten_id'/*, 'default': '-- Pilih --'*/}
	$.ajax({
		'type': 'POST',
		'async': false,
		url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
		data: p,
		complete: function(xhr, status)
		{
			var ret = xhr.responseText;
			t.html(ret);
		}
	});
}
</script>
