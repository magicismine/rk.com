<h3 class="titleContentRightPanel">Lowongan Dilamar</h3>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries . <br>
<br>
<div class="grayBox  dt-search-wrp">
  <h3 class="dsw-title">Pencarian Lowongan yang Dilamar</h3>
  <?php echo $this->load->view($this->vpath.'/lowongan/tpl_search_box',NULL,TRUE); ?>
</div>
<br>
<div align="right"> Tersedia <strong><?php echo $total?></strong> lowongan sesuai dengan keahlian anda.<!-- , <a href="#">Lihat Semua</a> --><br>
</div>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
  <thead>
    <tr>
      <td width="8%" align="center" valign="middle">Tanggal</td>
      <td width="41%" valign="middle">Deskripsi</td>
      <td width="20%" align="left" valign="middle">Bidang Usaha</td>
      <td width="14%" align="center" valign="middle">Lokasi</td>
    </tr>
  </thead>
  <tbody>
  <?php
	$O = new OLowongan();
	foreach($list as $r):
		$O->setup($r);
		extract(get_object_vars($r));
		$OP = new OPerusahaan($perusahaan_id);
		$OBU = new OMaster_bidang_usaha($bidang_usaha_id);
    $OMLKab = new OMaster_lokasi_kabupaten($lokasi_kabupaten_id);
	?>
    <tr>
      <td align="right" valign="middle"><?php echo parse_date($dt,"d M Y","ID","short")?></td>
      <td valign="middle"><a href="<?php echo $O->get_link(); ?>"><?php echo $posisi; ?></a></br>
        <strong><?php echo anchor($OP->get_link(), $OP->row->nama); ?></strong></td>
      <td align="left" valign="middle"><?php echo $OBU->row->nama; ?></td>
      <td align="center" valign="middle"><?php echo $OMLKab->get_nama(); ?></td>
    </tr>
    <?php
		unset($OP,$OBU);
    endforeach;
	unset($O);
	?>
  </tbody>
</table>
<div id="paging_button" align="center"><?php echo $pagination; ?></div>