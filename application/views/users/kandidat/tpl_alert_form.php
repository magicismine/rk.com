<?php
extract(get_object_vars($O->get_auto_lamar()));
?><div class="grayBox  dt-search-wrp">
  <h3 class="dsw-title">Keriteria</h3>
  <form action="" method="post">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="middle" width="25%"> Pilih Kategori<br>
          <span class="pinf">(Max 4)</span></td>
        <td valign="middle">
          <?php
          $tmp_arr = OMaster_kategori::get_list_arr(0,0,"nama ASC","parent_id=0");
          echo form_dropdown('kategori[]',$tmp_arr,explode(",",$kategori_ids),'data-placeholder="Pilih Kategori..." multiple class="chosen-select-max4"');
          ?>
          <?php /* ?>
          <div class="checkbox-wrap" max-check="4">
          <ul>
            <li>
            <?php
            echo checkboxes("kategori[]",$tmp_arr,$kategori,"","</li><li>");
            ?>
            </li>
          </ul>
          </div>
          <?php //*/ ?>
        </td>
      </tr>
    <?php
    $tmp_arr = OMaster_kategori::get_list_arr(0,0,"nama ASC","parent_id<>0");
    if(count($tmp_arr) > 0):
    ?>
      <tr>
        <td valign="middle">Pilih Jabatan<br>
          <span class="pinf">(Max 4)</span></td>
        <td valign="middle">
          <?php
          echo form_dropdown('jabatan[]',$tmp_arr,explode(",",$jabatan_ids),'data-placeholder="Pilih Jabatan..." multiple class="chosen-select-max4"');
          ?>
          <?php /* ?>
          <div class="checkbox-wrap" max-check="4">
          <ul>
            <?php
            echo checkboxes("jabatan[]",$tmp_arr,$jabatan,"","</li><li>");
            ?>
          </ul>
          </div>
          <?php //*/ ?>
        </td>
      </tr>
    <?php
    endif;
    ?>
      <tr>
        <td valign="middle">Pilih Lokasi<br>
          <span class="pinf">(Max 2)</span></td>
        <td valign="middle">
          <?php
          $tmp_arr = OMaster_lokasi_propinsi::get_list_arr(0,0,"nama ASC");
          echo form_dropdown('lokasi[]',$tmp_arr,explode(",",$lokasi_ids),'data-placeholder="Pilih Lokasi..." multiple class="chosen-select-max2"');
          ?>
          <?php /* ?>
          <div class="checkbox-wrap" max-check="2">
          <ul>
            <?php
            echo checkboxes("lokasi[]",$tmp_arr,$lokasi,"","</li><li>");
            ?>
          </ul>
          </div>
          <?php //*/ ?>
        </td>
      </tr>
      <tr>
        <td valign="middle">Jam Kerja</td>
        <td valign="middle">
            <?php
            $tmp_arr = OMaster_waktu_kerja::get_list_arr();
            echo checkboxes("waktu_kerja[]",$tmp_arr,explode(",",$waktu_kerja_ids),""," &nbsp; ");
            ?>
        </td>
      </tr>
      <tr>
        <td valign="middle">Status Kerja</td>
        <td valign="middle">
            <?php
            $tmp_arr = OMaster_status_kerja::get_list_arr();
            echo checkboxes("status_kerja[]",$tmp_arr,explode(",",$status_kerja_ids),""," &nbsp; ");
            ?>
        </td>
      </tr>
      <tr>
        <td valign="middle">Aktifkan Email Alert</td>
        <td valign="middle">
          <?php
          $tmp_arr = array('0'=>'Tidak','1'=>'Ya');
          echo radios('email_alert_active_flag',$tmp_arr,$email_alert_active_flag,'','&nbsp; &nbsp;');
          ?>
        </td>
      </tr>
      <tr>
        <td valign="middle">&nbsp;</td>
        <td valign="middle"><input type="submit" name="button" id="button" value="Simpan" class="btn_Orange" /></td>
      </tr>
      <tr>
        <td valign="middle">&nbsp;</td>
        <td valign="middle">&nbsp;</td>
      </tr>
      <tr>
        <td valign="middle">&nbsp;</td>
        <td valign="middle">&nbsp;</td>
      </tr>
    </table>
  </form>
</div>         
<script type="text/javascript">
$(function(){
  var config = {
    '.chosen-select-max4' : {no_results_text:'Oops, nothing found!',width:"95%",max_selected_options:4},
    '.chosen-select-max2' : {no_results_text:'Oops, nothing found!',width:"95%",max_selected_options:2},
  }
  for (var selector in config) {
    $(selector).chosen(config[selector]); 
  }
});
</script>