<div class="content">
  <h1 class="titleContentPage">Kandidat</h1>
  <ul class="column">
	<?php echo $this->load->view($this->vpath.'/tpl_sidenav',NULL,TRUE);?>
    <li class="col rightpanel">
      <h3 class="titleContentRightPanel">Surat Lamaran</h3>
      At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. <br>
      <br>
      <br>
      <form class="frmInput resume_kandidat" method="post" action="">
        <div class="boxLightOrange"> <strong>Informasi</strong>
          <hr class="spacer">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="25%">Judul Resume</td>
              <td width="2%">:</td>
              <td width="73%">
                <input type="text" name="judul" id="judul" value="<?php echo $O->row->judul; ?>" required>
                <br>
                <?php echo form_error('judul'); ?>
              </td>
            </tr>
          </table>
        </div>
        <br>
        <textarea name="isi" cols="120" rows="20"><?php echo $O->row->isi; ?></textarea>
        <br>
        <?php echo form_error('isi'); ?>
        <br>
        <div align="right">
          <input type="reset"  class="btn_Gray" value="Cancel">
          <input type="submit"  class="btn_Orange" value="Simpan">
        </div>
      </form>
    </li>
    <div class="clr"></div>
  </ul>
</div>
<script type="text/javascript" src="<?=base_url("_assets/tinymcpuk/jscripts/tiny_mce/tiny_mce.js")?>"></script>
<?php /* ?><script type="text/javascript" src="<?=base_url("_assets/tinymcpuk/jscripts/tiny_mce/my_tiny_mce.js")?>"></script><?php */ ?>
<script type="text/javascript">
tinyMCE.init({
  mode : "textareas",
  editor_deselector : "mceNoEditor",
  theme : "simple"
});
</script>