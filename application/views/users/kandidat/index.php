<div class="content">
<h1 class="titleContentPage">Kandidat</h1>
<ul class="column">
	<?php echo $this->load->view($this->vpath.'/tpl_sidenav',NULL,TRUE);?>
    <li class="col rightpanel">
    <h3 class="titleContentRightPanel">Selamat Datang <strong><?php $user = new OKandidat($cu->id,"user_id"); echo $user->row->nama_depan;  ?></strong></h3>
    Nikmati fitur - fitur yang telah kami sediakan. <br>
    <br>
    <br>
	<?php
$menus = array(
	'editcv' =>
		array('kandidat/resume', '<span class="sc-title">Surat Lamaran</span>eaque ipsa quae ab illo inventore veritatis et'),
	'lihatcv' =>
		array('kandidat/curriculum_vitae', '<span class="sc-title">Curriculum Vitae</span>eaque ipsa quae ab illo inventore veritatis et'),
	'low_terbaru' =>
		array('kandidat/lowongan_terbaru', '<span class="sc-title">Lowongan Terbaru</span>eaque ipsa quae ab illo inventore veritatis et'),
	'lamarotomatis' =>
		array('kandidat/melamar_otomatis', '<span class="sc-title">Melamar Otomatis</span>eaque ipsa quae ab illo inventore veritatis et'),
	'lowongandilamar' =>
		array('kandidat/lowongan_dilamar', '<span class="sc-title">Lowongan Dilamar</span>eaque ipsa quae ab illo inventore veritatis et'),
	'lowongandisimpan' =>
		array('kandidat/lowongan_disimpan', '<span class="sc-title">Lowongan Disimpan</span>eaque ipsa quae ab illo inventore veritatis et'),
	//'emailalert' =>
		//array('kandidat/email_alert', '<span class="sc-title">Email Alert</span>eaque ipsa quae ab illo inventore veritatis et'),
	'profile' =>
		array('kandidat/akun', '<span class="sc-title">Pengaturan Akun</span>eaque ipsa quae ab illo inventore veritatis et')
);
	?>
	<ul class="column shortcutlist">
	<?php
	$total_menus = count($menus);
	$i = 1;
	foreach($menus as $key => $val):
		list($k,$v) = $val;
	?>
	  <li class="col"> <img class="lazy" src ="<?php echo base_url('_assets/img/gray.gif'); ?>" data-original="<?php echo base_url('_assets/img/icon/'.$key.'.gif'); ?>" />
		<div class="sc-desc"><?php echo anchor($k, $v)?></div>
	  </li>
	<?php
		if($i%4==0 && $i<$total_menus)
		{
			echo '<div class="clr"></div>';
		}
		$i++;
	endforeach;
	?>
	  <div class="clr"></div>
	</ul><br>

	<div align="right">
		<strong><?php echo $total_perusahaan; ?></strong> Perusahaan telah menandai CV Online anda, <?php echo anchor($this->curpage.'/perusahaan_lihat', "Lihat Semua")?><br>
	</div><br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
  <thead>
  <tr>
    <td width="8%" align="center" valign="middle">Expired </td>
    <td width="49%" valign="middle">Deskripsi</td>
    <td width="25%" align="left" valign="middle">Bidang Usaha</td>
    <td width="18%" align="center" valign="middle">Lokasi</td>
    </tr>
  </thead>
  <tbody>
  <?php
  foreach($list_perusahaan as $r):
  	extract(get_object_vars($r));
	$OP = new OPerusahaan($perusahaan_id);
	$OBU = new OBidang_usaha($bidang_usaha_id);
	$OMLKab = new OBidang_usaha($master_lokasi_kabupaten_id);
  ?>
  <tr>
    <td align="right" valign="middle"><?php echo parse_date($dt_expired,"d M Y","ID","short"); ?></td>
    <td valign="middle">
      <strong><?php echo anchor($OP->get_link(), $OP->row->nama); ?></strong></td>
    <td align="left" valign="middle"><?php echo $OBU->row->nama; ?></td>
    <td align="center" valign="middle"><?php echo $OMLKab->row->nama; ?></td>
  </tr>
  <?php
  	unset($OP,$OBU,$OMLKab);
  endforeach;
  ?>
  </tbody>
</table>
<?php
if($pagination):
?>
<div id="paging_button" align="center"><?php echo $pagination; ?></div>
<?php
endif;
?>
<br>
	</li>
    <div class="clr"></div>
</ul>
</div>
<br />
<?php  
  //$OAd = new OAd(6);
  $OAd = new OAd;
  $list_ads = OAd::get_list(0,0,"id ASC","active_flag = 1 AND photo <> '' AND position = 'user_bottom' AND (show_on = 'both' OR show_on = 'kandidat')");
  $total_ads = get_db_total_rows();
  if($total_ads > 0)
  {
?>
  <?php  
  foreach($list_ads as $ads)
  {
    $OAd->setup($ads);
    $photo = $OAd->get_photo_original();
  ?>
  <a href="<?php echo $OAd->row->url; ?>">
    <img class="lazy" src ="<?php echo $OAd->get_photo_original();?>" data-original="<?php echo $OAd->get_photo_original();?>" alt="<?php echo $OAd->row->nama; ?>" title="<?php echo $OAd->row->nama; ?>" 
      width="<?php echo $OAd->row->width; ?>" height="<?php echo $OAd->row->height; ?>"/>
  </a>
  <?php } ?>
<?php }else{  ?>
  <img class="lazy" src ="<?php echo base_url(); ?>_assets/img/gray.gif" data-original="<?php echo base_url(); ?>_assets/img/dummy6.jpg"> 
<?php } ?>
