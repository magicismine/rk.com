<div class="content">
  <h1 class="titleContentPage">Kandidat</h1>
  <ul class="column">
    <li class="col leftpanel">
      <div class="wrp-hatOrange ">
        <div  class="hatOrange"> Menu's<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
        <div class="ho-content grayDark">
          <div class="ho-spacer"></div>
            <?php echo $this->load->view($this->themes."/".'tpl_side_kandidat',array('current' => $current),TRUE); ?>
        </div>
      </div>
      <br>
    </li>
    <li class="col rightpanel">
      <h3 class="titleContentRightPanel">Curriculum Vitae</h3>
      <ul class="column pinfo">
        <li class="col pinfo-pic-s"><img src="<?php base_url() ?>assets/img/dummy8.jpg" width="50" height="50"> </li>
        <li class="col pinfo-desc"><span class="pinfo-name">Steve Pance</span>pance.steve@gmail.com</li>
        <li class="col rght"><a href="<?php echo site_url('kandidat/curriculum_vitae/edit')?>" class="btnedit">Edit CV</a> <a href="<?php echo site_url('export/to_pdf');?>" class="btnpdf">Save as Pdf</a></li>
        <div class="clr"></div>
      </ul>
      <span class="pinfo-sk">IT - Software / Network > Programmer</span>
      <table width="100%" border="0" cellspacing="0" cellpadding="0"  class="tblpinfo-dt">
        <tr class="noborder">
          <td height="30">&nbsp;</td>
          <td>&nbsp;</td>
          <td colspan="2"><div class="csc"> <span style="width:80%">CV Completed 80%</span> </div></td>
        </tr>
        <tr>
          <td width="25%">Jenis Kelamin</td>
          <td width="">: Laki-Laki</td>
          <td width="25%">Nama Panggilan </td>
          <td width=""> : Steve</td>
        </tr>
        <tr>
          <td>Tempat &amp; Tanggal Lahir </td>
          <td>: Jakarta/02 Maret 1982</td>
          <td>Agama </td>
          <td>: Islam</td>
        </tr>
        <tr>
          <td class="noborder">&nbsp;</td>
          <td class="noborder">&nbsp;</td>
          <td>Nomer KTP <br></td>
          <td> : 12.17876.5876.002</td>
        </tr>
        <tr>
          <td colspan="4" class="noborder">&nbsp;</td>
        </tr>
        <tr>
          <td><strong  class="pinfo-sp"> Alamat Asal </strong></td>
          <td>&nbsp;</td>
          <td><strong  class="pinfo-sp"> Alamat Sekarang </strong></td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Jalan</td>
          <td>: Pancoran Raya 2</td>
          <td> Jalan </td>
          <td>: Pancoran Raya 2</td>
        </tr>
        <tr>
          <td>RT/RW </td>
          <td>: 002/003</td>
          <td>RT/RW </td>
          <td>: 002/003</td>
        </tr>
        <tr>
          <td>Desa/Kelurahan </td>
          <td>: Pancoran</td>
          <td>Desa/Kelurahan </td>
          <td>: Pancoran</td>
        </tr>
        <tr>
          <td>Kecamatan</td>
          <td>: Pancoran</td>
          <td>Kecamatan </td>
          <td>: Pancoran</td>
        </tr>
        <tr>
          <td>Kabupaten</td>
          <td> : Pancoran</td>
          <td>Kabupaten </td>
          <td> : Pancoran</td>
        </tr>
        <tr>
          <td>Provinsi</td>
          <td>: DKI Jakarta</td>
          <td>Provinsi </td>
          <td>: DKI Jakarta</td>
        </tr>
        <tr>
          <td>Telepon Rumah</td>
          <td>: 021 87654323</td>
          <td>Telepon Rumah</td>
          <td>: 021 87654323</td>
        </tr>
        <tr class="noborder">
          <td  >Nomer HP</td>
          <td>: 8234567889 </td>
          <td>Nomer HP </td>
          <td>: 8234567889 </td>
        </tr>
      </table>
      <br>
      <div class="grayBox"> <span class='gb-title'>Kesehatan</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="25%">Tinggi/Berat Badan </td>
            <td width="75%"> : 179 Cm</td>
          </tr>
          <tr>
            <td>Golongan Darah </td>
            <td>: O</td>
          </tr>
          <tr>
            <td>Kacamata </td>
            <td> : Tidak</td>
          </tr>
          <tr>
            <td>Keluhan Kesehatan </td>
            <td> : Tidak Ada</td>
          </tr>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Keluarga</span> Status Perkawinan  : Menikah<br>
        <br>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="7%" align="center">#</td>
              <td width="23%">Anggota Keluarga</td>
              <td width="41%">Nama</td>
              <td width="20%">Pendidikan </td>
              <td width="9%">Usia</td>
            </tr>
          </thead>
          <tr>
            <td align="center">1</td>
            <td> Istri</td>
            <td>Della Sephani</td>
            <td>D3</td>
            <td>29</td>
          </tr>
          <tr>
            <td align="center">2</td>
            <td>Anak</td>
            <td>Budi Santoso</td>
            <td>SD</td>
            <td>12</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Pendidikan</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="6%" align="center">#</td>
              <td width="24%" align="center">Tingkat Pendidikan</td>
              <td width="41%">Jurusan/ Program Studi</td>
              <td width="20%">Nama Sekolah/ Universitas</td>
              <td width="9%">Nilai Rata-Rata</td>
            </tr>
          </thead>
          <tr>
            <td align="center">1</td>
            <td align="center">SMU</td>
            <td>IPA</td>
            <td>AL-Azhar</td>
            <td>8.2</td>
          </tr>
          <tr>
            <td align="center">2</td>
            <td align="center">D3</td>
            <td>Elektronika Komputer</td>
            <td>IPB</td>
            <td>3.3</td>
          </tr>
          <tr>
            <td align="center">3</td>
            <td align="center">S1</td>
            <td>Ilmu Komputer</td>
            <td>IPB</td>
            <td>5.0</td>
          </tr>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Pengalaman Kerja</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="15%" align="center">Waktu</td>
              <td width="26%">Perusahaan</td>
              <td width="28%">Bidang Usaha</td>
              <td width="19%">Jabatan</td>
              <td width="12%">Salary</td>
            </tr>
          </thead>
          <tr>
            <td align="center">2006-2008</td>
            <td>PT. Maju Selamanya</td>
            <td>Oil and Gas</td>
            <td>Programmer</td>
            <td align="right">Rp. 300.000</td>
          </tr>
          <tr>
            <td align="center">2008-2009</td>
            <td>PT. Sejahtera Sentosa</td>
            <td>Manufaktur</td>
            <td>Designer</td>
            <td align="right">Rp. 500.000</td>
          </tr>
          <tr>
            <td align="center">2009-2013</td>
            <td>PT. Makmur Seadanya</td>
            <td>Information Technology</td>
            <td>Programmer</td>
            <td align="right">Rp. 1.000.000</td>
          </tr>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Keahlian Khusus</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="4%" align="center">#</td>
              <td width="62%">Bidang Keahlian </td>
              <td width="10%" align="center">Skala (5)</td>
              <td width="24%">Badan Sertifikasi</td>
            </tr>
          </thead>
          <tr>
            <td align="center">1</td>
            <td>IT - Software / Network &gt; Programmer</td>
            <td align="center">4</td>
            <td>Kreatif Agency</td>
          </tr>
          <tr>
            <td align="center">2</td>
            <td>IT - Software / Network &gt; Programmer</td>
            <td align="center">2</td>
            <td>Kreatif Agency</td>
          </tr>
        </table>
      </div>
      <br>
      <div class="grayBox"> <span class='gb-title'>Penguasaan Bahasa</span>
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
          <thead>
            <tr>
              <td width="4%" align="center">#</td>
              <td width="62%">Bahasa</td>
              <td width="10%" align="center">Skala (5)</td>
              <td width="24%">Badan Sertifikasi</td>
            </tr>
          </thead>
          <tr>
            <td align="center">1</td>
            <td>English</td>
            <td align="center">5</td>
            <td>Kreatif Agency</td>
          </tr>
          <tr>
            <td align="center">2</td>
            <td>Mandarin</td>
            <td align="center">2</td>
            <td>Kreatif Agency</td>
          </tr>
        </table>
      </div>
    </li>
    <div class="clr"></div>
  </ul>
</div>