<ul style="padding:0;margin:0;">
	<li style="float:left;list-style:none;margin-right:10px;">
		<img src="<?php echo $OP->get_photo('square'); ?>" width="50" alt="Logo" />
	</li>
	<li style="float:left;list-style:none;">
		<span style="font-size: 24px;color: #f60;display: block;"><?php echo $OP->get_nama(); ?></span>
		<?php echo $OP->row->motto; ?>
	</li>
	<div class="clear:both"></div>
</ul>
<br />
<div title="Deskripsi Perusahaan">
	<?php $OP->row->deskripsi; ?>
</div>
<br />
<div style="background:#fff;border:1px solid #dfdfdf;padding:20px;line-height:2;border-radius:4px;">
	<h2 style="text-align:center;font-size:30px;font-weight:bold;color:#f60;"><?php echo $OL->get_nama(); ?></h2>
	<hr />
	<?php echo $OL->row->deskripsi; ?>
	<br />
	<hr />
	<div class="lowongan-keterangan">
		<ul style="padding:0;margin:0;">
			<li style="list-style:none;"><strong>Kategori</strong> : 
				<?php
				$OMKat = new OMaster_kategori($OL->row->kategori_id);
				echo anchor($OMKat->get_link(), $OMKat->get_nama());
				unset($OMKat);
				?>
			</li>
            <li style="list-style:none;"><strong>Bidang Usaha</strong> : 
            	<?php
            	$OMBU = new OMaster_bidang_usaha($OL->row->bidang_usaha_id);
            	echo anchor($OMBU->get_link(), $OMBU->get_nama());
            	unset($OMBU);
            	?>
            </li>
            <li style="list-style:none;"><strong>Pendidikan dan Pengalaman</strong> : 
            	<?php
            	$OMKat = new OMaster_pendidikan($OL->row->pendidikan_id);
            	echo $OMKat->row->nama;
            	unset($OMKat);
            	?> - <?php echo $OL->row->tahun_pengalaman_kerja; ?> tahun
            </li>
            <li style="list-style:none;"><strong>Batas Waktu Melamar</strong> : 
            	<?php echo parse_date($OL->row->dt_expired,"d M Y") ?>
            </li>
		</ul>
	</div>
</div>