<?php
extract(get_object_vars($OK->row));
$OU = new OUser($user_id);
// jenis kelamin
$OTmp = new OMaster_jenis_kelamin($jenis_kelamin_id);
$jenis_kelamin = $OTmp->row->nama;
unset($OTmp);
// agama
$OMA = new OMaster_agama($agama_id);
$agama = $OMA->row->nama;
unset($OMA);
// keterampilan
$keterampilans = $OK->get_all_keterampilan();
$keterampilan = implode("<br />", $keterampilans);
// pendidikan
$pnddkn_row = $OK->get_last_pendidikan();
$OTmp_pnddkn = new OMaster_pendidikan($pnddkn_row->pendidikan_id);
$pendidikan = $OTmp_pnddkn->get_nama();
unset($OTmp_pnddkn);
// lokasi
$lokasi_row = $OK->get_current_lokasi();
$lokasi = $lokasi_row->nama;

$titleContentRightPanel = 'color: #f60; font-size: 18px; margin-bottom: 20px;"';
$column = 'list-style: none;';
$column_col = 'list-style: none !important; float: left;';
$pinfo_pic_s = 'margin-right: 10px;';
$pinfo_desc = ''; //list-style: none;
$pinfo_desc_pinfo_name = 'font-size: 24px;color: #f60;display: block;';
$pinfo_sk = 'color: #666;font-size: 18px;display: block;margin: 10px 0;';
$clr = 'clear:both;';
$no_border = 'border: 0;';
$no_border_imp = 'border: 0 !important;';
$tblpinfo_dt_td = 'border-bottom: 1px solid #ccc;color: #777;padding: 2px 0;';
$pinfo_sp = 'font-size: 14px;color: #666;';
?>
<ul style="<?php echo $column; ?>">
  <li style="<?php echo $column.$pinfo_pic_s; ?>"><img src="img/dummy8.jpg" width="50" height="50"> </li>
  <li style="<?php echo $column; ?>">
    <span style="<?php echo $pinfo_desc_pinfo_name; ?>"><?php echo $OK->get_nama(); ?></span>
    <?php echo $OU->row->email; ?>
  </li>
  <div style="<?php echo $clr; ?>"></div>
</ul>
<span style="<?php echo $pinfo_sk; ?>"><?php echo $keterampilan; ?></span>

<?php
// RESUME
$resume_r = $OK->get_resume();
?>
<h3 style="<?php echo $titleContentRightPanel; ?>">Resume</h3>
<p><strong><?php echo $resume_r->judul; ?></strong></p><br>
<?php echo $resume_r->isi; ?><br><br>

<h3 style="<?php echo $titleContentRightPanel; ?>">Curriculum Vitae</h3>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tblpinfo-dt">
  <tbody>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>" width="25%">Jenis Kelamin</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>" width="">: <?php echo $jenis_kelamin; ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>" width="25%">Nama Panggilan </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>" width=""> : <?php echo $nama_panggilan; ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Tempat &amp; Tanggal Lahir </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $tempat_lahir; ?>/<?=parse_date($tanggal_lahir,"d F Y","ID","long")?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Agama </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $agama; ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>" class="noborder">&nbsp;</td>
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>" class="noborder">&nbsp;</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Nomer KTP <br></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>"> : <?php echo $no_ktp; ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>" colspan="4" class="noborder">&nbsp;</td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td.$pinfo_sp; ?>"><strong class="pinfo-sp"> Alamat Asal </strong></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">&nbsp;</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td.$pinfo_sp; ?>"><strong class="pinfo-sp"> Alamat Sekarang </strong></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">&nbsp;</td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Jalan</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $asal_alamat; ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>"> Jalan </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $skrg_alamat; ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">RT/RW </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $asal_rt; ?>/<?php echo $asal_rw; ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">RT/RW </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo ($skrg_rt == "" ? "-" : $skrg_rt); ?>/<?php echo ($skrg_rw == "" ? "-" : $skrg_rw); ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Desa/Kelurahan </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $asal_kelurahan; ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Desa/Kelurahan </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo ($skrg_kelurahan  == "" ? "-" : $skrg_kelurahan); ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Kecamatan</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $asal_kecamatan; ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Kecamatan </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo ($skrg_kecamatan  == "" ? "-" : $skrg_kecamatan); ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Kabupaten</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>"> : <?php 
            if($asal_kabupaten_id != "")
            {
              $OMKab = new OMaster_lokasi_kabupaten($asal_kabupaten_id);
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Kabupaten </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>"> : <?php 
            if($skrg_kabupaten_id != "")
            {
              $OMKab = new OMaster_lokasi_kabupaten($skrg_kabupaten_id);
              echo $OMKab->row->nama;
              unset($OMKab);
            }else
            {
              echo "-";
            }
            ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Provinsi</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php 
            if($asal_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($asal_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Provinsi </td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php 
            if($skrg_propinsi_id != "")
            {
              $OMProp = new OMaster_lokasi_propinsi($skrg_propinsi_id);
              echo $OMProp->row->nama;
              unset($OMProp);
            }else
            {
              echo "-";
            }
            ?></td>
  </tr>
  <tr>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Telepon Rumah</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo $asal_telepon; ?></td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">Telepon Rumah</td>
    <td style="<?php echo $no_border.$tblpinfo_dt_td; ?>">: <?php echo ($skrg_telepon  == "" ? "-" : $skrg_telepon); ?></td>
  </tr>
  <tr class="noborder">
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>">Nomer HP</td>
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>">: <?php echo $asal_hp; ?> </td>
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>">Nomer HP </td>
    <td style="<?php echo $no_border_imp.$tblpinfo_dt_td; ?>">: <?php echo ($skrg_hp  == "" ? "-" : $skrg_hp); ?> </td>
  </tr>
</tbody></table>
<br>
<?php 
$grayBox = 'background: #e9e8e8;border: 1px solid #dedede;padding: 20px 10px;border-radius:4px;';
$gb_title = 'font-size: 18px;margin-bottom: 10px;display: block;';
$grayBox_table_td = 'border: 0;padding: 2px 0;';
$tbl_gbdt_thead_td = 'background: #7caad9;color: #fff;';
$tbl_gbdt_td = 'border-bottom: 1px solid #ccc;padding: 3px 0 !important;';
$tpl_arr = array(
                'O' => $OK,
                'grayBox' => $grayBox,
                'gb_title' => $gb_title,
                'grayBox_table_td' => $grayBox_table_td,
                'tbl_gbdt_thead_td' => $tbl_gbdt_thead_td,
                'tbl_gbdt_td' => $tbl_gbdt_td
                );
echo $this->load->view('email/kandidat/kesehatan', $tpl_arr, TRUE);
echo $this->load->view('email/kandidat/keluarga', $tpl_arr, TRUE);
echo $this->load->view('email/kandidat/pendidikan', $tpl_arr, TRUE);
echo $this->load->view('email/kandidat/pengalaman', $tpl_arr, TRUE);
echo $this->load->view('email/kandidat/keterampilan', $tpl_arr, TRUE);
echo $this->load->view('email/kandidat/bahasa', $tpl_arr, TRUE);
//echo $this->load->view('email/kandidat/kesehatan', $tpl_arr, TRUE);
?>
<?php /*
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Kesehatan</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td style="<?php echo $grayBox_table_td; ?>" width="25%">Tinggi/Berat Badan </td>
      <td style="<?php echo $grayBox_table_td; ?>" width="75%"> : 179 Cm</td>
    </tr>
    <tr>
      <td style="<?php echo $grayBox_table_td; ?>">Golongan Darah </td>
      <td style="<?php echo $grayBox_table_td; ?>">: O</td>
    </tr>
    <tr>
      <td style="<?php echo $grayBox_table_td; ?>">Kacamata </td>
      <td style="<?php echo $grayBox_table_td; ?>"> : Tidak</td>
    </tr>
    <tr>
      <td style="<?php echo $grayBox_table_td; ?>">Keluhan Kesehatan </td>
      <td style="<?php echo $grayBox_table_td; ?>"> : Tidak Ada</td>
    </tr>
  </tbody></table>
</div>
<br>
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Keluarga</span> Status Perkawinan  : Menikah<br>
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="7%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="23%">Anggota Keluarga</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="41%">Nama</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="20%">Pendidikan </td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="9%">Usia</td>
      </tr>
    </thead>
    <tbody><tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">1</td>
      <td style="<?php echo $tbl_gbdt_td; ?>"> Istri</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Della Sephani</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">D3</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">29</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Anak</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Budi Santoso</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">SD</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">12</td>
    </tr>
  </tbody></table>
</div>
<br>
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Pendidikan</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="6%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="24%" align="center">Tingkat Pendidikan</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="41%">Jurusan/ Program Studi</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="20%">Nama Sekolah/ Universitas</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="9%">Nilai Rata-Rata</td>
      </tr>
    </thead>
    <tbody><tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">1</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">SMU</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">IPA</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">AL-Azhar</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">8.2</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">D3</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Elektronika Komputer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">IPB</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">3.3</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">3</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">S1</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Ilmu Komputer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">IPB</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">5.0</td>
    </tr>
  </tbody></table>
</div>
<br>
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Pengalaman Kerja</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="15%" align="center">Waktu</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="26%">Perusahaan</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="28%">Bidang Usaha</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="19%">Jabatan</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="12%">Salary</td>
      </tr>
    </thead>
    <tbody><tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2006-2008</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">PT. Maju Selamanya</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Oil and Gas</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Programmer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="right">Rp. 300.000</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2008-2009</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">PT. Sejahtera Sentosa</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Manufaktur</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Designer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="right">Rp. 500.000</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2009-2013</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">PT. Makmur Seadanya</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Information Technology</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Programmer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="right">Rp. 1.000.000</td>
    </tr>
  </tbody></table>
</div>
<br>
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Keahlian Khusus</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="4%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="62%">Bidang Keahlian </td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="10%" align="center">Skala (5)</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="24%">Badan Sertifikasi</td>
      </tr>
    </thead>
    <tbody><tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">1</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">IT - Software / Network &gt; Programmer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">4</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Kreatif Agency</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">IT - Software / Network &gt; Programmer</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Kreatif Agency</td>
    </tr>
  </tbody></table>
</div>
<br>
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Penguasaan Bahasa</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="4%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="62%">Bahasa</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="10%" align="center">Skala (5)</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="24%">Badan Sertifikasi</td>
      </tr>
    </thead>
    <tbody><tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">1</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">English</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">5</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Kreatif Agency</td>
    </tr>
    <tr>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Mandarin</td>
      <td style="<?php echo $tbl_gbdt_td; ?>" align="center">2</td>
      <td style="<?php echo $tbl_gbdt_td; ?>">Kreatif Agency</td>
    </tr>
  </tbody></table>
</div>
*/ ?>