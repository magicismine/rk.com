<?php
$list = $O->get_pengalaman();
?><div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Pengalaman Kerja</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="15%" align="center">Waktu</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="26%">Perusahaan</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="28%">Bidang Usaha</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>"<?php /* ?> width="19%"<?php */ ?>>Jabatan</td>
        <?php /* ?><td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="12%">Salary</td><?php */ ?>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=1;
      foreach($list as $r):
        extract(get_object_vars($r));
        $OTmp = new OMaster_bidang_usaha($bidang_usaha_id);
        $bidang_usaha = $OTmp->get_nama();
        unset($OTmp);
      ?>
      <tr>
        <td style="<?php echo $tbl_gbdt_td; ?>" align="center"><?php echo get_only_date($tanggal_awal,"year"); ?>-<?php echo get_only_date($tanggal_akhir,"year") ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $perusahaan; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $bidang_usaha; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $jabatan; ?></td>
        <?php /* ?><td style="<?php echo $tbl_gbdt_td; ?>" align="right">Rp. 300.000</td><?php */ ?>
      </tr>
      <?php
        $i++;
      endforeach;
      ?>
    </tbody>
  </table>
</div>
<br>