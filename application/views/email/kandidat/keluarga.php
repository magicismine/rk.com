<?php
$list = $O->get_keluarga();
?><div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Keluarga</span> Status Perkawinan  : Menikah<br>
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="7%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="23%">Anggota Keluarga</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="9%">Gender</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="41%">Nama</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="20%">Pendidikan </td>
        <?php /* ?><td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="9%">Usia</td><?php */ ?>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=1;
      foreach($list as $r):
        extract(get_object_vars($r));
        $OTmp = new OMaster_shdk($shdk_id);
        $shdk = $OTmp->get_nama();
        unset($OTmp);
        $OTmp = new OMaster_jenis_kelamin($jenis_kelamin_id);
        $jenis_kelamin = substr($OTmp->get_nama(),0,1);
        unset($OTmp);
        $OTmp = new OMaster_pendidikan($pendidikan_id);
        $pendidikan = $OTmp->get_nama();
        unset($OTmp);
      ?>
      <tr>
        <td style="<?php echo $tbl_gbdt_td; ?>" align="center"><?php echo $i; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $shdk; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $jenis_kelamin; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $nama; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $pendidikan; ?></td>
        <?php /* ?><td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $umur; ?></td><?php */ ?>
      </tr>
      <?php
      $i++;
      endforeach;
      ?>
    </tbody>
  </table>
</div>
<br>