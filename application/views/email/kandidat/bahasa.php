<?php
$list = $O->get_bahasa();
?><div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Penguasaan Bahasa</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="4%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="62%">Bahasa</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="10%" align="center">Skala (5)</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="24%">Badan Sertifikasi</td>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=1;
      foreach($list as $r):
        extract(get_object_vars($r));
        $OTmp = new OMaster_bahasa($bahasa_id);
        $bahasa = $OTmp->get_nama();
        unset($OTmp);
      ?>
      <tr>
        <td style="<?php echo $tbl_gbdt_td; ?>" align="center"><?php echo $i; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $bahasa; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>" align="center"><?php echo $nilai_skala; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $sertifikasi; ?></td>
      </tr>
      <?php
        $i++;
      endforeach;
      ?>
    </tbody>
  </table>
</div>