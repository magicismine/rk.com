<?php
extract(get_object_vars($O->get_kesehatan()));
$OTmp = new OMaster_gol_darah($gol_darah_id);
$gol_darah = $OTmp->row->nama;
unset($OTmp);
?>
<div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Kesehatan</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tbody><tr>
      <td style="<?php echo $grayBox_table_td; ?>" width="25%">Tinggi/Berat Badan </td>
      <td style="<?php echo $grayBox_table_td; ?>" width="75%"> : <?php echo $tinggi; ?> cm/ <?php echo $berat; ?> kg</td>
    </tr>
    <tr>
      <td style="<?php echo $grayBox_table_td; ?>">Golongan Darah </td>
      <td style="<?php echo $grayBox_table_td; ?>">: <?php echo $gol_darah; ?></td>
    </tr>
    <tr>
      <td style="<?php echo $grayBox_table_td; ?>">Kacamata </td>
      <td style="<?php echo $grayBox_table_td; ?>"> : <?php echo (empty($kacamata_flag) ? "Tidak" : "Iya"); ?></td>
    </tr>
    <tr>
      <td style="<?php echo $grayBox_table_td; ?>">Keluhan Kesehatan </td>
      <td style="<?php echo $grayBox_table_td; ?>"> : <?php echo ($keluhan_kesehatan != "" ? $keluhan_kesehatan : "-"); ?></td>
    </tr>
  </tbody></table>
</div>
<br>