<?php
$list = $O->get_pendidikan();
?><div style="<?php echo $grayBox; ?>" class="grayBox"> <span <?php echo $gb_title; ?> class="gb-title">Pendidikan</span>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl-gbdt">
    <thead>
      <tr>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="6%" align="center">#</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="24%" align="center">Tingkat Pendidikan</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="41%">Jurusan/ Program Studi</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="20%">Nama Sekolah/ Universitas</td>
        <td style="<?php echo $tbl_gbdt_td.$tbl_gbdt_thead_td; ?>" width="9%">Nilai Rata-Rata</td>
      </tr>
    </thead>
    <tbody>
      <?php
      $i=1;
      foreach($list as $r):
        extract(get_object_vars($r));
        $OTmp = new OMaster_pendidikan($pendidikan_id);
        $pendidikan = $OTmp->get_nama();
        unset($OTmp);
      ?>
      <tr>
        <td style="<?php echo $tbl_gbdt_td; ?>" align="center"><?php echo $i; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>" align="center"><?php echo $pendidikan; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $jurusan; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $nama; ?></td>
        <td style="<?php echo $tbl_gbdt_td; ?>"><?php echo $nilai; ?></td>
      </tr>
      <?php
        $i++;
      endforeach;
      ?>
    </tbody>
  </table>
</div>
<br>