<div class="wrp-inner">
  <div class="doMenu"> <a href="<?php echo site_url('kandidat/resume')?>" class="m-resume">Buat Surat Lamaran</a> <a href="<?php echo site_url('perusahaan/lowongan'); ?>" class="m-jallert">Buat Lowongan Pekerjaan</a> 
  <ul id="reg-menu">
  <li>
    <a href="javascript:void(0);" class="m-reg">Registrasi</a>
      <ul>
        <li><a href="<?php echo site_url('account/register/kandidat'); ?>">Kandidat</a></li>
        <li><a href="<?php echo site_url('account/register/perusahaan'); ?>">Perusahaan</a></li>
      </ul>
  </li>
  </div>
  <?php
    echo $this->load->view('lowongan/tpl/search_form',$data);
  ?>
  <div id="BigContTab">
    <ul class="column BigContTabPanel">
      <li class="col bcpt bcptright" style="margin-right:1%"> <a href="<?php echo site_url('home/ajax/lowongan_pekerjaan');?>" data-target="#loper">LOWONGAN PEKERJAAN</a><span class="arrow"></span> <span class="bcpt_sp"></span></li>
      <li   class="col bcpt bcptleft"> <a href="<?php echo site_url('home/ajax/kandidat');?>" data-target="#kandidat">KANDIDAT</a><span class="arrow"></span> <span class="bcpt_sp"></span></li>
      <div class="clr"></div>
    </ul>
    <div id="BigContTabWrp">
      <div class="trapspacer"></div>
      <div id="bctp-loadarea">
        <div id="loper"> </div>
        <div id="kandidat"> </div>
      </div>
    </div>
  </div>
  <br>
  <?php  
  $OAds1 = new OAd(1);
  $photo = $OAds1->get_photo_original();
  if(($OAds1->row->photo != "") AND ($OAds1->row->active_flag == 1))
  {
    echo $this->load->view('ads/tpl_ad', array('O'=>$OAds1), TRUE);
  }
  else
  {
  ?>
    <img class="lazy" src ="<?php echo base_url("_assets/img/gray.gif"); ?>" data-original="<?php echo base_url()?>_assets/img/dummy6.jpg" alt="" title="" /> 
  <?php
  }
  unset($OAds1); 
  ?>
  <br>
  <br>
  <ul class="column">
    <li class="col" style="width:652px; margin-right:15px">
      <!-- Promo Kandidat -->
      <?php echo $this->load->view('home/promo_kandidat_box',NULL,TRUE); ?>
      <!-- Ads Middle Left -->
      <?php  
      $OAds2 = new OAd(2);
      $photo = $OAds2->get_photo_original();
      if(($OAds2->row->photo != "") AND ($OAds2->row->active_flag == 1))
      {
        echo $this->load->view('ads/tpl_ad', array('O'=>$OAds2), TRUE);
      }
      else
      {
      ?>
        <img class="lazy" src ="<?php echo base_url("_assets/img/gray.gif"); ?>" data-original="<?php echo base_url()?>_assets/img/dummy11.jpg" alt="" title="" width="652" height="93" /> 
      <?php
      }
      unset($OAds2); 
      ?>
      <br>
      <!-- Artikel -->
      <?php echo $this->load->view('home/artikel_list_box',NULL,TRUE); ?>
      <!-- Perusahaan -->
      <?php echo $this->load->view('home/perusahaan_list_box',NULL,TRUE); ?>
    </li>

    <li class="col" style="width:293px">
      <!-- Lowongan Baru -->
      <?php echo $this->load->view("home/lowongan_baru_box", NULL, TRUE); ?>
      <!-- Lowongan Populer -->
      <?php echo $this->load->view("home/lowongan_populer_box", NULL, TRUE); ?>
      <!-- Statistik -->
      <?php echo $this->load->view("tpl_statistik_box", NULL, TRUE); ?>
      
      <!-- Ads Right 1 -->
      <?php  
      $OAds3 = new OAd(3);
      $photo = $OAds3->get_photo_original();
      if(($OAds3->row->photo != "") AND ($OAds3->row->active_flag == 1))
      {
        echo $this->load->view('ads/tpl_ad', array('O'=>$OAds3), TRUE);
      }
      else
      {
      ?>
        <img class="lazy" src ="<?php echo base_url("_assets/img/gray.gif"); ?>" data-original="<?php echo base_url()?>_assets/img/dummy7.jpg" alt="" title="" width="292" height="93" /> 
      <?php
      }
      unset($OAds3); 
      ?>
      <!-- Ads Right 2 -->
      <?php  
      $OAds4 = new OAd(4);
      $photo = $OAds4->get_photo_original();
      if(($OAds4->row->photo != "") AND ($OAds4->row->active_flag == 1))
      {
        echo $this->load->view('ads/tpl_ad', array('O'=>$OAds4), TRUE);
      }
      else
      {
      ?>
        <img class="lazy" src ="<?php echo base_url("_assets/img/gray.gif"); ?>" data-original="<?php echo base_url()?>_assets/img/dummy7.jpg" alt="" title="" width="292" height="93" /> 
      <?php
      }
      unset($OAds4); 
      ?>
    </li>
    <div class="clr"></div>
  </ul>
  <br>
  <?php  
  $OAds5 = new OAd(5);
  $photo = $OAds5->get_photo_original();
  if(($OAds5->row->photo != "") AND ($OAds5->row->active_flag == 1))
  {
    echo $this->load->view('ads/tpl_ad', array('O'=>$OAds5), TRUE);
  }
  else
  {
  ?>
    <img class="lazy" src ="<?php echo base_url("_assets/img/gray.gif"); ?>" data-original="<?php echo base_url()?>_assets/img/dummy6.jpg" alt="" title="" /> 
  <?php
  }
  unset($OAds5); 
  ?>
  <br>
  <br>
  
  <!--wrapperinner--> 