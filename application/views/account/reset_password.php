<ul class="column account forgot">
  <li class="col" style="width:48%; margin-right:2%">
    <div class="wrp-hatOrange ">
      <div  class="hatOrange"> FORGOT PASSWORD<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
      <div class="ho-content grayLight">
        <div class="ho-spacer"></div>
        <?php echo print_status(); ?>
        <?
        if($invalid):
        ?>
        <h3>Session is expired!</h3>
        <p>Sorry, your session is expired.</p>
        <p>Please check your url address code again that has been sent to your email.</p>
        <p>If it still expired. Please contact the website administrator.</p>
        <p>Thank you.</p>
        <?
        else:
        ?>
        <h2>Reset your password?</h2>
        <div class="cform-wrap">
            <?=print_error($this->session->flashdata("warning"))?>
            <?=print_error(validation_errors())?>
            <form action="" method="post">
                <fieldset class="clearfix">
                    <p>Please enter your new password here.</p>
                    <div class="input-wrap">
                        <label for="password">Password*:</label><input type="password" name="password" id="password" class="text" title="Requirement: Min 8 Chars" autofocus required />
                    </div>
                    <div class="input-wrap">
                        <label for="passconf">Password Confirmation*:</label><input type="password" name="passconf" id="passconf" class="text" required />
                    </div>
                    <div class="input-wrap">
                        <label for="submit"></label>
                        <button type="submit" class="btn-grey"><span class="with-arr">Reset my password</span></button>
                    </div>
                </fieldset>
            </form>
        </div>
        <?
        endif;
        ?>
      </div>
    </div>
  </li>
  <div class="clr"></div>
</ul>

<style type="text/css" media="screen">
    .column.account{ padding-bottom: 10px;}
    .input-wrap{ display: inline-block;}
    .input-wrap label{ width: 150px;display: inline-block;}
    .input-wrap button{ background: #f60; color:white; border: none; padding: 5px 10px;  }    
</style>