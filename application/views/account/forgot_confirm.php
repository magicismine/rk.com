<div class="contain-main container single">
    <div class="post">
        <h2>Password Reset Instruction Has Been Sent!</h2>
        <p>We have just sent a password reset instruction to your email address "<?=$email?>".</p>
        <p>Please check your email in a few moment.</p>
        <p>If your inbox folder in your email does not received any mail from us, please check again your bulk or junk folder in your email account.</p>
    </div><!--.post-->
</div>