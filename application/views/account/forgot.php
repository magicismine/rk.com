<ul class="column account forgot">
  <li class="col" style="width:48%; margin-right:2%">
    <div class="wrp-hatOrange ">
      <div  class="hatOrange"> FORGOT PASSWORD<span class="ho-onleft"></span><span class="ho-onright"></span> </div>
      <div class="ho-content grayLight">
        <div class="ho-spacer"></div>
        <?php echo print_status(); ?>
        Masukkan email anda untuk meminta code dan mereset password anda.
        <br>
        <form action="" method="post">
            <fieldset class="clearfix">
                <p></p>
                <div class="input-wrap">
                    <label for="email">Email*:</label>
                    <input type="email" name="email" id="email" class="text" autofocus required  value="<?=set_value('email');?>"/>
                </div>
                <div class="input-wrap">
                    <label for=""></label>
                    <button type="submit" class="btn-grey"><span class="with-arr">Send me a resetting code</span></button>
                </div>
            </fieldset>
        </form>
      </div>
    </div>
  </li>
  <div class="clr"></div>
</ul>

<style type="text/css" media="screen">
    .column.account{ padding-bottom: 10px;}
    .input-wrap{ display: inline-block;}
    .input-wrap button{ background: #f60; color:white; border: none; padding: 5px 10px;  }    
</style>