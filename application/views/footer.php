</div>
  </div>
  <div class="clr"></div>
  <div id="footer" >
    <ul class="column footer-wrpcontent">
      <li class="col wrp-address">
        <h5 class="fcontact-title">KONTAK</h5>
        <br>
        <p>
          <!-- Jababeka II, Sektor Graha Asri <br>
          JL. Cisanggiri 2D No. 66 <br>
          Jatireja - Cikarang Timur, Bekasi 17530 <br>
          Telepon 021-29089606 Fax 021-29089606 <br>
          <br>
          Email :  recruitment@rumahkandidat.com  -->
          <?php
          $address = get_setting('contact_address');
          $phone = get_setting('contact_phone');
          $fax = get_setting('contact_fax');
          $email = get_setting('contact_email');
          echo nl2br($address)."<br />";
          //if(!empty($phone)) echo "Telepon ".$phone." ";
          //if(!empty($fax)) echo "Fax ".$fax." ";
          //if(!empty($email)) echo "<br /><br />Email : ".safe_mailto($email)." ";
          ?>
        </p>
      </li>

      <li class="col " style="width:220px">
        <label>TENTANG RUMAHKANDIDAT</label>
        <!-- <a href="#" title="">Tentang Rumah Kandidat</a> 
        <a href="#" title="">FAQ</a> 
        <a href="#" title="">Karir@rumahkandidat</a> 
        <a href="#" title="">Hubungi Kami</a> -->
        <?php
        $exc_list_page = OPage::get_list(0,0,"id ASC","nama LIKE '%hubungi%'");
        foreach($exc_list_page as $r)
        {
          $exclude_ids[] = $r->id;
        }
        $OP = new OPage;
        $list_page = OPage::get_list(0,0,"id ASC");
        foreach($list_page as $page)
        {
          if(in_array($page->id, $exclude_ids)) continue;
          $OP->setup($page);
          ?>
          <a href="<?php echo $OP->get_link(); ?>" title=""><?php echo $OP->row->nama; ?></a>
          <?php 
        } 
        unset($OP);
        ?>
        <?php echo anchor("contact", "Hubungi Kami", array()); ?>
      </li>
      <li class="col ">
        <label>KANDIDAT</label>
        <?php echo anchor("search/lowongan", "Temukan Pekerjaan", array()); ?>
        <?php echo anchor("kandidat/curriculum_vitae/edit", "Daftarkan CV", array()); ?>
        <?php echo anchor("kandidat/email_alert", "Job Alerts", array()); ?>
      </li>
      <li class="col ">
        <label>PERUSAHAAN</label>
        <?php echo anchor("perusahaan/lowongan/add", "Pasang Lowongan", array()); ?>
        <?php echo anchor("search/kandidat", "Cari Kandidat", array()); ?>
        <a href="<?php echo site_url('contact'); ?>" title="">Beriklan Bersama Kami</a>
        </li>
      <li class="col ">
        <label>PERFORMA</label>
        <a href="<?php echo site_url('performa'); ?>" title="">Info Kandidat </a></li>
      <div class="clr"></div>
      <!-- <div align="center">in {elapsed_time} seconds</div> -->
    </ul>
  </div>
</div>
<script src="<?php echo base_url("_assets/js/jquery-ui.min.js"); ?>"></script>
<script src="<?php echo base_url("_assets/js/jquery.lazyload.min.js"); ?>"></script>
<script src="<?php echo base_url("_assets/js/general.js?v=".date("ymdH")); ?>"></script>
<script src="<?php echo base_url("_assets/js/jquery.easytabs.min.js"); ?>"></script>
<script src="<?php echo base_url("_assets/js/moment.js"); ?>"></script>
<script src="<?php echo base_url("_assets/js/daterangepicker.min.js"); ?>"></script>
<script src="<?php echo base_url("_assets/js/demo.js"); ?>"></script>
<script src="<?php echo base_url("_assets/js/common.js"); ?>"></script>
<script src="<?php echo base_url("_assets/chosen/chosen.jquery.min.js"); ?>"></script>
<script src="<?php echo base_url("_assets/social/social.js"); ?>"></script>
<noscript>
<style>
body { overflow: hidden !important; }
</style>
<div id="noscript" style="position:fixed;top:0;left:0;bottom:0;right:0;z-index:10000;width:auto;height:auto;background-color:rgba(0,0,0,0.7);display:block;overflow:auto;overflow-y:scroll;">
	<div style="display:table;width:100%;height:100%;">
		<h2 style="margin:0;padding:0;font-size:2.2em;color:#fff;display:table-cell;vertical-align:middle;text-align:center;">
        <strong>Uppss... Javascript pada Browser ini sedang tidak aktif. Aktifkan untuk bisa mengakses.</strong>
        </h2>
    </div>
</div>
</noscript>
</body>
</html>