<?php
$ca = get_current_admin();
echo $this->load->view('lowongan/tpl/search_form',$data);
$list_lowongan = $O->get_lowongans(0, 0);
$total_lowongan = count($list_lowongan);
?>
<br>
<ul class="column pinfo">
  <li class="col pinfo-pic-s"><img src="<?php echo $O->get_photo('square') ?>" width="50" height="50"></li>
  <li class="col pinfo-desc"><span class="pinfo-name"><?php echo $O->row->nama; ?></span><?php echo $O->row->motto; ?></li>
  <div class="clr"></div>
</ul>
<br>
 
<div class="boxWhite" title="Deskripsi Perusahaan">
  <?php if($O->row->deskripsi != ""){echo nl2br($O->row->deskripsi);}else{ echo "Tidak ada deskripsi";} ?>
</div>
<br>
<div class="boxLightOrange">
  <table width="100%" border="0" cellspacing="0" cellpadding="0"   >
    <tr>
      <td><strong>Informasi Perusahaan</strong></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="25%">Alamat</td>
      <td> : <?php echo strip_tags($O->row->lokasi_alamat); ?> </td>
    </tr>
    <tr>
      <td width="17%">Bidang Usaha</td>
      <td>: <?php  
            $OMBU = new OMaster_bidang_usaha($O->row->bidang_usaha_id);
            echo $OMBU->row->nama;
            unset($OMBU);
            ?>
      </td>
    </tr>
    <tr>
      <td>Lokasi</td>
      <td>: <?php  
            $OMLK = new OMaster_lokasi_kabupaten($O->row->lokasi_kabupaten_id);
            echo $OMLK->row->nama;
            unset($OMLK);
            ?>
      </td>
    </tr>
    <tr>
      <td>Propinsi</td>
      <td> : 
            <?php  
            $OMLP = new OMaster_lokasi_propinsi($O->row->lokasi_kabupaten_id);
            echo $OMLP->row->nama;
            unset($OMLP);
            ?>
      </td>
    </tr>
    <tr>
      <td>Negara</td>
      <td>: 
          <?php  
          $OMLN = new OMaster_lokasi_negara($O->row->lokasi_negara_id);
          echo $OMLN->row->nama;
          unset($OMLN);
          ?>

      </td>
    </tr>
    <tr>
      <td>Jumlah Karyawan</td>
      <td> : <?php echo $O->row->jumlah_karyawan; ?></td>
    </tr>
    <tr>
      <td colspan="2" class="noborder">&nbsp;</td>
    </tr>
    <tr>
      <td width="17%"><strong  >Statistik</strong></td>
      <td width="83%">&nbsp;</td>
    </tr>
    <tr>
      <td>Daftar Sejak</td>
      <td>: <?php echo parse_date($O->row->dt_added,"d M Y"); ?></td>
    </tr>
    <tr>
      <td>Masa Berlaku Keanggotaan</td>
      <td>: <?php if($O->row->dt_expired != "" || $O->row->dt_expired != 0000-00-00){ echo parse_date($O->row->dt_expired,"d M Y" );}else{ echo "-";} ?></td>
    </tr>
    <tr>
      <td>Lowongan Tayang</td>
      <td>: <?php echo $tayang = count($O->get_total_lowongan_aktif());?></td>
    </tr>
    <tr class="noborder">
      <td>Total Lowongan</td>
      <td>: <?php echo $total_lowongan; ?></td>
    </tr>
  </table>
</div>
<br>

<div align="right"> Total <strong><?php echo $total_lowongan; ?></strong> Daftar Lowongan Yang Pernah Di buat.
  <?php /*  ?>, <a href="<?php echo site_url('perusahaan/lowongan') ?>">Lihat Semua</a><?php */ ?></div>
<br>

<div class="contens">
	<?php
	echo $this->load->view('perusahaan/tpl/list_item', array('list' => $list_lowongan), FALSE);
	?>
</div>