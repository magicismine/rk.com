<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list-table">
<thead>
	<tr>
		<td width="7%" align="center" valign="middle">Expired</td>
		<td width="59%" valign="middle">Deskripsi</td>
		<td width="16%" align="left" valign="middle">Bidang Usaha</td>
		<td width="18%" align="center" valign="middle">Lokasi</td>
	</tr>
</thead>
<tbody>
<?php 
if($list)
{
	$O = new OLowongan();
	foreach($list as $r):
		$O->setup($r);
		$OP = new OPerusahaan($O->row->perusahaan_id);
		$OMBU = new OMaster_bidang_usaha($O->row->bidang_usaha_id);
		$OMLK = new OMaster_lokasi_kabupaten($O->row->lokasi_kabupaten_id);
		$OMKat = new OMaster_pendidikan($O->row->pendidikan_id);
		?>
		<tr>
		<td align="right" valign="middle"><?php echo parse_date($O->row->dt_expired,"d M Y"); ?></td>
		<td valign="middle">
			<?php echo anchor($O->get_link(), $O->row->posisi) ?><br />
			<strong>Pendidikan dan Pengalaman : </strong><?php echo $O->row->posisi ?> - <?php echo $O->row->tahun_pengalaman_kerja; ?> tahun.
		</td>
		<td align="left" valign="middle">
		<?php
			echo $OMBU->row->nama;
		?>
		</td>
		<td align="center" valign="middle">
		<?php
			echo $OMLK->row->nama;
		?>
		</td>
		</tr>
		<?php
		unset($OP,$OMBU,$OMLK);
	endforeach;
	unset($O);
}
else
{
	echo "<tr><td colspan='4'>Tidak ada lowongan ditemukan.</td></tr>";
}
?>
</tbody>
</table>

<?php
if($pagination):
?>
<div id="paging_button" align="center">
<?php echo $pagination; ?>
</div>
<?php
endif;
?>