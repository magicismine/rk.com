<?php
$cu = get_logged_in_user();
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="description" content="<?php echo ($page_description != "" ? word_limiter(strip_tags($page_description),50): "lowongan kerja, pencari kerja, lowongan terbaru, lowongan pekerjaan, lowongan,pekerjaan,lowongan bumn,lowongan kerja bekasi"); ?>" />
<meta name="keywords" content="<?php echo ($page_keyword != "" ? strip_tags($page_keyword) : "lowongan kerja, pencari kerja, lowongan terbaru, lowongan pekerjaan, lowongan,pekerjaan,lowongan bumn,lowongan kerja bekasi"); ?>" />
<meta name="robot" content="index,follow" />
<meta name="copyright" content=" " />
<meta name="author" content=" " />
<meta name="revisit" content="2 days" />
<meta name="revisit-after" content="2 days" />
<meta name="classification" content=" " />
<meta name="document-classification" content="general" />
<meta name="rating" content="general" />
<meta name="googlebot" content="all,index,follow" />
<meta name="allow-search" content="yes" />
<meta name="audience" content="all" />
<meta name="distribution" content="Global" />
<meta name="DC.title" content=" " />
<meta name="geo.region" content=" " />
<meta name="geo.placename" content="Jakarta" />
<meta name="geo.position" content=" " />
<title><?php echo ($page_title != "" ? $page_title." - Rumah Kandidat" : "Rumah Kandidat – Situs Penyedia Lowongan dan Pencari Kerja"); ?></title>

<link rel="shortcut icon" href="<?php echo base_url("_assets/img/favicon.ico"); ?>" type="image/x-icon">
<link rel="icon" href="<?php echo base_url("_assets/img/favicon.ico"); ?>" type="image/x-icon">

<link href="<?php echo base_url("_assets/css/css.css?v=".date('ymdH')); ?>" rel="stylesheet">
<link href="<?php echo base_url("_assets/css/csshome.css?v=".date('ymdH')); ?>" rel="stylesheet">
<link href="<?php echo base_url("_assets/css/daterangepicker.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("_assets/css/smoothness/jquery-ui-1.8.9.custom.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("_assets/chosen/chosen.min.css"); ?>" rel="stylesheet">
<link href="<?php echo base_url("_assets/social/social.css"); ?>" rel="stylesheet">

<script>
var fpath = '<?php echo site_url(); ?>';
</script>
<script src="<?php echo base_url()?>_assets/js/jquery.js"></script>
<script src="<?php echo base_url()?>_assets/js/jquery-migrate-1.2.1.min.js"></script>

</head>

<body>
<div id="dialog"></div>
<div id="wrp-c">
  <div id="header">
    <div class="wrp-inner">
      <label class="logoCont"><a href="<?php echo site_url();?>"><img src="<?php echo site_url();?>_assets/img/logo.jpg" alt="Logo Rumah Kandidat" title="Rumah Kandidat" /></a></label>
      <?php if($cu){ 
        if($cu->role == "kandidat")
        {
          $O = new OKandidat($cu->id,"user_id");
          $nama = $O->get_nama();
          $role = "Kandidat";
        }else
        {
          $O = new OPerusahaan($cu->id,"user_id");
          $nama = $O->get_nama();
          $role = "Perusahaan";
        }
        
        ?>
      <div class="acc-panel"><a href="<?php echo site_url('account')?>">Selamat Datang, </a><?php echo $nama; ?> | <a href="<?php echo site_url("logout/user/{$cu->role}") ?>">Keluar</a>
      </div>
      <?php } 
      unset($O);
      ?>
    </div>
  </div>
  <div id="menutop"  >
    <div class=" wrp-inner">
      <ul class="column">
        <li class="col"><a href="<?php echo site_url()?>">HALAMAN UTAMA</a></li>
        <li class="col"><a href="<?php echo site_url('kandidat')?>">KANDIDAT</a></li>
        <li class="col"><a href="<?php echo site_url('perusahaan')?>">PERUSAHAAN</a></li>
        <?php //* ?>
        <li class="col"><a href="<?php echo site_url('performa')?>">PERFORMA</a></li>
        <?php //*/ ?>
        <div class="clr"></div>
      </ul>
    </div>
  </div>
  <div id="container">
    <div class="wrp-inner">