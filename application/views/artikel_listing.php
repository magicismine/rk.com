<div id="artikel">
	<h2>Artikel</h2>
	<div class="artikel_listing">
		<?php  
		foreach($list_artikel as $artikel):
			$O->setup($artikel);
		?>
		<div class="box-item">
			<a href="<?php echo $O->get_link(); ?>" class="list-image"><img src="<?php echo $O->get_photo('square'); ?>" width="93"></a>
			<h3><a href="<?php echo $O->get_link(); ?>"><?php echo $O->row->nama; ?></a></h3>
			<div class="description">
				<?php echo word_limiter(strip_tags($O->row->isi),20); ?>
			</div>
		</div>
		<?php 
		endforeach; 
		?>
	</div>
	<div id="paging_button">
		<?php echo $pagination; ?>
	</div>
</div>