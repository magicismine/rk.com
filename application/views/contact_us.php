<?php extract($_POST); ?>
<div id="artikel" class="artikel_detail">
	<h2>Hubungi Kami</h2>
	<div class="description">
		<div class="contact-left contact-column">
			<p style="margin-bottom: 10px;"><strong>Silahkan menghubungi kami dengan mengisi form kontak dibawah ini.</strong></p>
			<?php echo print_status(); ?>
			<?php echo print_error(validation_errors()); ?>
			<form action="" method="POST">
				<div class="input-wrap">
					<label for="name">Nama</label>
					<input type="text" name="name" value="<?php echo $name; ?>" class="name" required>
				</div>
				<div class="input-wrap">
					<label for="email">E-mail</label>
					<input type="email"  name="email" value="<?php echo $email; ?>" class="email" required>
				</div>
				<div class="input-wrap">
					<label for="subject">Subyek</label>
					<input type="text"  name="subject" value="<?php echo $subject; ?>" class="subject" required>
				</div>
				<div class="input-wrap">
					<label for="message" class="vtop">Pesan</label>
					<textarea class="message" name="message" required><?php echo $message; ?></textarea>
				</div>
				<div class="input-wrap">
					<label for="submit"></label>
					<input type="submit" value="Kirim Pesan">
				</div>
			</form>
		</div>
		<div class="contact-right contact-column">
			<p>
			<!-- Jababeka II, Sektor Graha Asri <br>
			JL. Cisanggiri 2D No. 66 <br>
			Jatireja - Cikarang Timur, Bekasi 17530 <br>
			Telepon 021-29089606 Fax 021-29089606 <br>
			<br>
			Email :  recruitment@rumahkandidat.com  -->
			<?php
			$sitename = get_setting('sitename');
			$address = get_setting('contact_address');
			$phone = get_setting('contact_phone');
			$fax = get_setting('contact_fax');
			$email = get_setting('contact_email');
			echo "<h3><strong>".$sitename."</strong></h3><br />";
			echo nl2br($address)."<br />";
			//if(!empty($phone)) echo "Telepon ".$phone." ";
			//if(!empty($fax)) echo "Fax ".$fax." ";
			//if(!empty($email)) echo "<br /><br />Email : ".safe_mailto($email)." ";
			?>

			</p>
			<!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->
		</div>
	</div>
</div>
<style type="text/css">
	.contact-column{ float:left;}
	.contact-left{ width: 450px; margin-right: 10px; padding-right: 10px;}
	/*.contact-right form{ padding: 10px; border: 1px solid lightGray; border-radius: 4px; }*/
	.contact-right{ /*width: 480px;*/ overflow: hidden; float: none; }
	
	.contact-column h3 { font-size: 16px; line-height: normal; }
	.contact-column .bottom-line { border-bottom: 1px solid; margin-bottom: 5px; }
	.contact-column .input-wrap{ margin-top: 5px; }
	.contact-column .input-wrap label{ display: inline-block; width: 60px; }
	.contact-column .vtop { vertical-align: top; margin-top: 5px; }
	
	.contact-column .input-wrap textarea,
	.contact-column .input-wrap input[type=email],
	.contact-column .input-wrap input[type=text]
	{
		border-radius: 4px; width: 79%; padding: 5px;
	}
	.contact-column .input-wrap input[type="submit"],
	.contact-column .input-wrap button
	{
		background: #f60; color: white; padding: 5px 15px; border: none; border-radius: 5px;
	}
</style>
