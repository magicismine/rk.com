<?php
$cu = get_logged_in_user();
?>
<div id="tab-container" class='tab-container'>
  <div class="titleTab">DAFTAR KANDIDAT</div>
  <div class='panel-container'>
    <ul class="column tabkandidatSearch">
      <li class="col tks-form" >
        <form action="<?php echo site_url('search/kandidat'); ?>">
          <ul class="column" style="position:relative">
            <li class="col">
              <label>Kategori</label>
              <?php echo OMaster_kategori::drop_down_select('kategori_id',$kategori_id,"","Pilih"); ?>
            </li>
            <li class="col">
              <label>Jabatan</label>
              <span id="subcategory_wrap">
                <?php echo OMaster_kategori::drop_down_select('subkategori_id',$subkategori_id,"","Pilih","parent_id<>0"); ?>
              </span>
            </li>
            <li class="col">
              <label>Umur</label>
              <span>
              <?php
              $umur_arr = array('18-20' => '18-20 tahun',
                                '20-27' => '20-27 tahun',
                                '>27' => '>27 tahun'
                                );
              //echo dropdown('umur',$umur_arr,$umur,'','Pilih');
              $arr = array(
                          "type"=>"number",
                          "maxlength"=>"2",
                          "size"=>"4",
                          "min"=>"18",
                          "max"=>"80",
                          "style"=>"padding:5px 10px;"
                          );
              $umur_mulai_arr = array_merge( $arr, array("name"=>"umur_mulai","value"=>$umur_mulai,"placeholder"=>"Mulai") );
              $umur_akhir_arr = array_merge( $arr, array("name"=>"umur_akhir","value"=>$umur_akhir,"placeholder"=>"Akhir") );
              echo form_input($umur_mulai_arr)."-";
              echo form_input($umur_akhir_arr);
              ?></span>
            </li>
            <?php /*  ?>
            <!--
            <li class="col">
              <label>Jabatan</label>
              <select name="">
                <option>Pilih</option>
              </select>
            </li>
             <li class="col" style="width:85px" >
              <label>Umur</label>
              <select name="" class="wd50">
                <option>Pilih</option>
              </select>
            </li> -->
            <?php */ ?>
            <li class="col">
              <label>Pendidikan</label>
              <?php echo OMaster_pendidikan::drop_down_select('pendidikan_id',$pendidikan_id,"","Pilih"); ?>
            </li>
            <li class="col" style="width:70px; ">
              <label>&nbsp;</label>
              <input type="submit" value="Search"  class="btn_Orange">
            </li>
            <?php /* ?>
            <li class="col" style="width:70px; ">
              <label>&nbsp;</label>
              <div style="margin-top: 12px;margin-left: 16px;">Atau</div>
            </li>
            <li class="col">
              <div class="right-panel" style="position:absolute;bottom: 10px; right:80px;">
                <a href="<?php echo site_url('search/kandidat'); ?>" class="view-all btn_Orange">Lihat Semua Kandidat</a>
              </div>
            </li>
            <?php */ ?>
            <div class="clr"></div>
          </ul>
        </form>
      </li>
      <li class="col tks-paging">
        <?php if($pagination){ ?>
        <div id="paging_button" class="custom" align="center">
          <?php echo $pagination; ?>
        </div>
        <?php } ?>
      </li>
      <div class="clr"></div>
    </ul>
    <br>
    <ul class="column tks-result">
      <?php  
      $OK = new OKandidat;
      foreach($list as $r)
      {
        $OK->setup($r);
        $keterampilans = $OK->get_all_keterampilan();
        $keterampilan_arr = NULL; $i = 0;
        foreach($keterampilans as $key => $val)
        {
          if($i == 3) break;
          $keterampilan_arr[$key] = $val;
          $i++;
        }
        $keterampilan = implode("<br />", $keterampilan_arr);
        $url = $OK->get_link();
        $photo = $OK->get_photo('avatar');
        $img = '<img class="lazy" src ="'.$photo.'" data-original="'.$photo.'" width="50" height="50" alt=""  title="">';
      ?>
      <li class="col">
        <label class="tks-pic"><?php echo anchor($url, $img); ?></label>
        <label class="tks-desc">
          <?php
          if($cu->role=="perusahaan"):
            echo anchor($url, '<strong>'.$OK->get_nama_lengkap().' ('.$OK->get_umur().')</strong><br />');
          endif;
          echo $keterampilan;
          ?>
          </a> </label>
      </li>
      <?php
      }
      unset($OK);
      ?>
      <div class="clr"></div>
    </ul>
  </div>
</div>