<script type='text/javascript'>
$('#tab-container').easytabs();
  </script>
<div id="tab-container" class='tab-container'>
  <ul class='etabs'>
    <li class='tab'><a href="#Kategori">KATEGORI</a></li>
    <li class='tab'><a href="#BidangUsaha">BIDANG USAHA</a></li>
    <li class='tab'><a href="#Lokasi">LOKASI</a></li>

  </ul>
    <div class="titleTab" style=" position:absolute;top:0">DAFTAR PEKERJAAN</div>

  <div class='panel-container'>
    <div id="Kategori">
      <ul class="bctw-list ">
        <?php  
        $list_kategori = OMaster_kategori::get_list(0,0,"nama ASC");
        foreach($list_kategori as $kategori):
        $OMK->setup($kategori);
        $filter_arr = array('kategori_id' => $OMK->id);
        $OLow1 = OLowongan::get_list_filter("",0,0,"lowongans.id DESC","",$filter_arr);
        $total_lowongan_kategori = get_db_total_rows();
		if($total_lowongan_kategori <= 0) continue;
        ?>
        <li ><?=anchor($OMK->get_link(),$OMK->get_nama()." (".$total_lowongan_kategori.")");?></li>
        <?php
        endforeach;
        unset($OMK);
        // OTHER
        $filter_arr = array('lowongans.kategori_id' => 0);
        $list = OLowongan::get_list_front(0,0,"lowongans.id DESC",$filter_arr);
        if($list)
        {
            ?>
            <li ><?=anchor('lowongan/listing/kategori/other', "Other (".get_db_total_rows().")");?></li>
            <?php
        }
        ?>
        <div class="clr"></div>
      </ul>
    </div>
    <div id="BidangUsaha">
      <ul class="bctw-list ">
        <?php  
        $list_bidang_usaha = OMaster_bidang_usaha::get_list(0,0,"nama ASC");
        foreach($list_bidang_usaha as $bidang_usaha):
        $OMBU->setup($bidang_usaha);
        $filter_arr = array('bidang_usaha_id' => $OMBU->id);
        $OLow2 = OLowongan::get_list_filter("",0,0,"lowongans.id DESC","",$filter_arr);
        $total_lowongan_bidang_usaha = get_db_total_rows();
		if($total_lowongan_bidang_usaha <= 0) continue;
        ?>
        <li ><?=anchor($OMBU->get_link(),$OMBU->get_nama()." (".$total_lowongan_bidang_usaha.")");?></li>
        <?php
        endforeach;
        unset($OMBU);
        // OTHER
        $filter_arr = array('lowongans.bidang_usaha_id' => 0);
        $list = OLowongan::get_list_front(0,0,"lowongans.id DESC",$filter_arr);
        if($list)
        {
            ?>
            <li ><?=anchor('lowongan/listing/bidang_usaha/other', "Other (".get_db_total_rows().")");?></li>
            <?php
        }
        ?>
        <div class="clr"></div>
      </ul>
    </div>
    <div id="Lokasi">
      <ul class="bctw-list ">
        <?php  
        $list_lokasi_kabupaten = OMaster_lokasi_kabupaten::get_list(0,0,"master_lokasi_propinsi_id ASC, id ASC");
        foreach($list_lokasi_kabupaten as $lokasi):
        $OMLKab->setup($lokasi);
        $filter_arr = array('lokasi_kabupaten_id' => $OMLKab->id);
        $OLow3 = OLowongan::get_list_filter("",0,0,"lowongans.id DESC","",$filter_arr);
        $total_lowongan_lokasi = get_db_total_rows();
		if($total_lowongan_lokasi <= 0) continue;
        ?>
        <li ><?=anchor($OMLKab->get_link(),$OMLKab->get_nama()." (".$total_lowongan_lokasi.")");?></li>
        <?php
        endforeach;
        unset($OMLKab);
        // OTHER
        $filter_arr = array('lowongans.lokasi_kabupaten_id' => 0);
        $list = OLowongan::get_list_front(0,0,"lowongans.id DESC",$filter_arr);
        if($list)
        {
            ?>
            <li ><?=anchor('lowongan/listing/lokasi/other', "Other (".get_db_total_rows().")");?></li>
            <?php
        }
        ?>
        <div class="clr"></div>
      </ul>
    </div>
  </div>
</div>