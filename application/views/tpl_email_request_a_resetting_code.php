Dear <?=$name?>,<br />
<br />
Please click <?=anchor("account/reset_password/$md5email/$md5password", "here")?> to reset your password.<br />
<br />
OR<br />
<br />
you can copy and paste this url address below to your browser address bar:<br />
<br />
<?=site_url("account/reset_password/$md5email/$md5password")?>
<br />
<br />
Thank you.<br />
<br />
<hr />
<small>Don't reply this email. This email is not monitored or you will not receive a response.</small><br />
<?=DOMAIN_NAME?><br />