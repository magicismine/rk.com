<h2>Users Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
        <input type="hidden" name="role" value="<?php echo $_GET["role"]; ?>" />
        <input type="hidden" name="perpage" value="<?php echo $perpage; ?>" />
    </form>
</div>
<div class="pull-left">
    <?php //echo anchor($this->curpage."/add?role=".$_GET['role'], "Create new",array("class" => "btn btn-success")); ?>
    <p>Silahkan menambah data melalui Front End.</p>
    <br>
    <div class='show-perpage' style="float:left;">
        Show per Page: 
        <form name="form-perpage" action="" method="get">
            <input type="hidden" name="role" value="<?php echo $_GET["role"]; ?>" />
            <?php echo dropdown('perpage',array('25' => 25,'50' => 50,'100' => 100),$perpage,"onchange='this.form.submit();'"); ?>
        </form>
    </div>  
</div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
if(!$list) echo "<p class='error'>The Users is empty.</p>";
else
{
?>
<p>Display <strong><?php echo count($list); ?></strong> of <strong><?php echo $total; ?></strong> items.</p>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
			<th>No</th>
            <th>Username</th>
            <th>Role</th>
            <th>Dt Added</th>
            <th>Status</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
        
    <?php 
    $i=1 + $uri;
    foreach($list as $row):
	
   	 	extract(get_object_vars($row));
		$O = new OUser();                        
		$O->setup($row);
        ?>                        
		<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
			<td><?=$i?></td>
            <td><?=$email?></td>
			<td><?=$role?></td>
			<td><?=$dt_added?></td>
            <td><?=(($status== "pasif" || $status == "") ?
            anchor($this->curpage."/set_status/aktif/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/disabled.gif")."' alt='NO' />",array('title' => 'Click to enable')) :
            anchor($this->curpage."/set_status/pasif/".$id."?".get_params($_GET), "<img src='".base_url("_assets/images/enabled.gif")."' alt='YES' />",array('title' => 'Click to disable')))?>
            </td>
			<td><?php
            $actions = NULL;
            $actions[] = anchor($this->curpage."/edit/".$id."?".get_params($_GET), "<img src='".base_url()."_assets/images/edit.gif' />");
            $actions[] = anchor($this->curpage."/delete/".$id."?".get_params($_GET), "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
            echo implode(" ", $actions);
            ?></td>
		</tr>

<?php 
    unset($O);
	$i++; 
    endforeach; 
?>
	</tbody>	
</table>

<?php echo $pagination; ?>
<?php
}
?>
