<?php
    if($row)
    {
        extract(get_object_vars($row));  
        $O = new OUser();
        $O->setup($row);
    }
    extract($_POST);
?>

<h2>User Form</h2><br/> 
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
if(sizeof($_GET) > 0)
{
    $roles = $_GET['role'];
}else
{
    $roles = $role;
}
?>

<?php //=form_open()?>
<form action="" method="post">
    <table class="tbl_form">
        <tr>
            <th>Username</th>
            <td>
                <input type="email" name="email" value="<?=$email?>" required autofocus /> 
                <br/><?=form_error('email')?>                
            </td>
        </tr>
        <tr>
            <th>Password</th>
            <td>
                <input type="text" name="password" value=""  />
                <br/><?=form_error('password')?>                
            </td>
        </tr>
        <tr>
            <th>Role</th>
            <td>
            	<?=form_dropdown("role",array("kandidat" => "Kandidat", "perusahaan" => "Perusahaan"),$roles)?>
                <br/><?=form_error('role')?>                
            </td>
        </tr>
                <tr>
            <td></td>
            <td>
                <button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/users/?role=".$roles)?>';"><span class="leftarrow icon"></span>Cancel</button>
            </td>
        </tr>    
    </table>    
<?=form_close()?>