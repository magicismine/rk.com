<h2>Settings Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<?php /*?><div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<?php */?>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
	if(!$list) echo "<p class='error'>The Settings is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
                    <th>ID</th>
                    <?php
                    /*$http_query = array();
                    $http_query["orderby"] = $_GET["orderby"];
                    $http_query["order"] = "ASC";		 
                    
                    if($_GET["orderby"] == "key")
                    {
						if($_GET["order"] == "ASC")
						{
							$http_query["order"] = "DESC";		 
						}
						else
						{
							$http_query["order"] = "ASC";		 
						}
                    }
                    $http_query["keyword"] = $_GET["keyword"];
                    $http_query["orderby"] = "key";
                    echo "<th>".anchor($this->curpage."?".http_build_query($http_query),"Key",array("style" => "color:#fff;"))."</th>";*/
                    ?>
                    <th>Key</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
		<?php 
            $i=1 + $uri;
			$O = new OSetting();                        
            foreach($list as $row):
            
                extract(get_object_vars($row));
                $O->setup($row);
                ?>
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$row->id?></td>
                    <td><?=$key?></td>
					<td><?=$name?></td>
					<td><?php echo ($type == "plain_text" ? nl2br($content) : "")?></td>
					<td><?php
						$actions = NULL;
						$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
						//$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
						echo implode(" ", $actions);
						?>
                    </td>
				</tr>
		
		        <?php 
				$i++; 
            endforeach; 
			unset($O);
        ?>
        	</tbody>	
        </table>
        
        <?php echo $pagination; ?>
<?php
  }
?>
