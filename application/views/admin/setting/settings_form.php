<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OSetting();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Setting Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Key</th>
            <td>
            	<input type="text" name="key" value="<?=$key?>" required autofocus <?php echo (isset($_GET['is_admin']) ? '' : 'disabled="disabled"')?> />
                <br/><?=form_error('key')?>                
            </td>
        </tr>
		<tr>
            <th>Name</th>
            <td>
            	<input type="text" name="name" value="<?=$name?>" required  /> 
                <br/><?=form_error('name')?>                
            </td>
        </tr>
		<tr>
            <th>Description</th>
            <td>
            	<textarea name="description" cols="80" rows="5" class="mceNoEditor" <?php echo (isset($_GET['is_admin']) ? '' : 'disabled="disabled"')?> ><?=$description?></textarea>
				<br/><?=form_error('description')?>                
            </td>
        </tr>
		<tr>
            <th>Content</th>
            <td>
            	<textarea name="content" cols="80" rows="10"<?php echo ($type == "plain_text" ? ' class="mceNoEditor"' : ''); ?>><?=$content?></textarea>
				<br/><?=form_error('content')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/settings")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>