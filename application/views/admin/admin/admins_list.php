
<h2>Admins Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
	if(!$list) echo "<p class='error'>The Admins is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>ID</th>
                    <th>Dt Added</th>					
                    <th>Username</th>
					<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OAdmin();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$row->id?></td>
                    <td><?=$dt_added?></td>
					<td><?=$username?></td>
					<td><?php 
					$actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
					if($username != "admin"){
					$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
					}
					echo implode(" ", $actions);?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?php echo $pagination; ?>
<?php
  }
?>
