<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow" />
    
	<title>Welcome to Administration Page</title>
    
    <link type="text/css" rel="stylesheet" href="<?=base_url("_assets/css/bootstrap.min.css")?>" />
    
    <link type="text/css" rel="stylesheet" href="<?=base_url("_assets/css/custom.css")?>" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    
    <script type="text/javascript" src="<?=base_url("_assets/js/jquery-ui.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("_assets/js/bootstrap.min.js")?>"></script>
    <script type="text/javascript" src="<?=base_url("_assets/js/common.js")?>"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>

</head>
<body id="loginpage">
	