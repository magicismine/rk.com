<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OPage();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Page Form</h2><br/>	
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Nama</th>
            <td>
            	<input type="text" name="nama" value="<?=$nama?>" required autofocus /> 
                <br/><?=form_error('nama')?>                
            </td>
        </tr>
		<tr>
            <th>Content</th>
            <td>
            	<textarea name="content" cols="80" rows="20"><?=$content?></textarea>
				<br/><?=form_error('content')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/pages")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>