<h2>Data Lamaran</h2><br />

<div class="pull-left">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?=$_GET["keyword"]?>" />
        <input type="hidden" name="page" value="<?=$_GET["page"]?>">
    </form>
</div>
<?php /* ?>
<div class="pull-left"><?=anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success"))?></div>
<?php */ ?>
<div class="clearfix"></div><br />

<?=print_status()?>

<?php
if(!$list) echo "<p class='error'>Data Kosong.</p>";
else
{
?>

<p>Display <strong><?php echo count($list); ?></strong> of <strong><?php echo $total; ?></strong> items.</p>

<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
			<th>No</th>
            <th colspan="2">Kandidat</th>
            <th>Lowongan (Perusahaan)</th>
            <th>Date</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>No</th>
            <th colspan="2">Kandidat</th>
            <th>Lowongan (Perusahaan)</th>
            <th>Date</th>
        </tr>
    </tfoot>
    <tbody>
        
    <?php 
        $i=1 + $uri;
        foreach($list as $row):
            //var_dump($row);
       	 	extract(get_object_vars($row));
            $OL = new OLowongan($lowongan_id);
            $OP = new OPerusahaan($OL->row->perusahaan_id);
            $OK = new OKandidat($kandidat_id);
            // keterampilan
            $keterampilans = $OK->get_all_keterampilan();
            //$keterampilan = implode("<br />", $keterampilans);
            $keterampilan = implode("<br />", array_unique($keterampilans));
            ?>                        
		<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
			<td><?=$i.".";?></td>
            <td width="60">
                <img src="<?php echo $OK->get_photo('square'); ?>" width="50" height="50" alt="" />
            </td>
            <td><a href="<?php echo $OK->get_link(); ?>"><strong><?php echo $OK->get_nama_lengkap(); ?> (<?php echo $OK->get_umur(); ?>)</strong></a><br />
                <?php
                  echo $keterampilan;
                ?>
            </td>
            <td><?=anchor($OL->get_link(), $OL->get_nama())?> (<?=anchor($OP->get_link(), $OP->get_nama())?>)</td>
            <td><?=parse_date_time($dt_added,"d F Y H:ia","ID","long")?></td>
            <?php /* ?>
			<td><?=trimmer($content,75);?></td>
			<td><?php $actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));echo implode(" ", $actions);?></td>
            <?php */ ?>
		</tr>
        <?php 
            unset($OL,$OK,$OP);
			$i++; 
        endforeach; 
        ?>
	</tbody>	
</table>

<?=$pagination?>
<?php
}
?>