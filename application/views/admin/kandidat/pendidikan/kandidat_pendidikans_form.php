<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OKandidat_pendidikan();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Kandidat Pendidikan Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Kandidat Id</th>
            <td>
            	<input type="text" name="kandidat_id" value="<?=$kandidat_id?>" required autofocus /> 
                <br/><?=form_error('kandidat_id')?>                
            </td>
        </tr>
		<tr>
            <th>Tahun Awal</th>
            <td>
            	<input type="text" name="tahun_awal" class="datepicker" value="<?=$tahun_awal?>" required  /> <br/><?=form_error('tahun_awal')?>                
            </td>
        </tr>
		<tr>
            <th>Tahun Akhir</th>
            <td>
            	<input type="text" name="tahun_akhir" class="datepicker" value="<?=$tahun_akhir?>" required  /> <br/><?=form_error('tahun_akhir')?>                
            </td>
        </tr>
		<tr>
            <th>Nama</th>
            <td>
            	<input type="text" name="nama" value="<?=$nama?>" required  /> 
                <br/><?=form_error('nama')?>                
            </td>
        </tr>
		<tr>
            <th>Jurusan</th>
            <td>
            	<input type="text" name="jurusan" value="<?=$jurusan?>" required  /> 
                <br/><?=form_error('jurusan')?>                
            </td>
        </tr>
		<tr>
            <th>Nilai</th>
            <td>
            	<input type="text" name="nilai" value="<?=$nilai?>" required  /> 
                <br/><?=form_error('nilai')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/kandidat_pendidikans")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>