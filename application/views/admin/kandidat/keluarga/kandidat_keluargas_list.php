
<h2>Kandidat Keluargas Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
	if(!$list) echo "<p class='error'>The Kandidat Keluargas is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>ID</th>
                    <th>Kandidat</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Shdk</th>
                    <th>Pendidikan</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OKandidat_keluarga();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$row->id?></td>
                    <td><?=$kandidat_id?></td>
					<td><?=$nama?></td>
					<td>	
    <?=($jenis_kelamin_id== "0" ? anchor($this->curpage."/set_jenis_kelamin_id/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") : anchor($this->curpage."/set_jenis_kelamin_id/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
</td>          
					<td>	
    <?=($shdk_id== "0" ? anchor($this->curpage."/set_shdk_id/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") : anchor($this->curpage."/set_shdk_id/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
</td>          
					<td>	
    <?=($pendidikan_id== "0" ? anchor($this->curpage."/set_pendidikan_id/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") : anchor($this->curpage."/set_pendidikan_id/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
</td>          
					<td><?php $actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));echo implode(" ", $actions);?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?php echo $pagination; ?>
<?php
  }
?>
