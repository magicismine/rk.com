<?php
if($row)
{
    extract(get_object_vars($row));  
    $O = new OKandidat_promo();
    $O->setup($row);
}
if(sizeof($_GET) > 0) extract($_GET);
if(sizeof($_POST) > 0) extract($_POST);
?>

<h2>Form Promo Kandidat</h2><br/>   
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?php //=form_open()?>
<form action="" method="post">
	<table class="tbl_form">
        <?php
        if(isset($_GET['kandidat_id']) && intval($_GET['kandidat_id']) > 0):
            $kandidat_readonly = TRUE;
        endif;
        ?>
        <tr>
            <th>Kandidat</th>
            <td>
                <?php echo OKandidat::drop_down_select("kandidat_id",$kandidat_id,'class="select-chosen"','',$kandidat_readonly); ?>
                <br/><?=form_error('kandidat_id')?>
            </td>
        </tr>
        <tr>
            <th>Perusahaan</th>
            <td>
            	<?php echo OPerusahaan::drop_down_select("perusahaan_id",$perusahaan_id,'class="select-chosen"'); ?>
                <br/><?=form_error('perusahaan_id')?>                
            </td>
        </tr>
		
		<tr>
            <th>Keterangan</th>
            <td>
            	<textarea name="note" cols="80" rows="20"><?=$note?></textarea>
				<br/><?=form_error('note')?>                
            </td>
        </tr>
        <tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url($this->curpage."?".get_params($_GET))?>';">
                    <span class="leftarrow icon"></span>Cancel
                </button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>
<script type="text/javascript">
    $(function(){
        $('.select-chosen').chosen();
    })
</script>