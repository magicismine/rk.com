<?php 
$params = get_params($_GET, array());
if($params) $get_params = "?".$params;
if(sizeof($_GET) > 0) extract($_GET);
if(sizeof($_POST) > 0) extract($_POST);
?><h2>Data Promo Kandidat</h2><br />
<?php /* ?>
<?php if ($_GET["keyword"] == ""): ?><?php endif; ?><div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?=$_GET["keyword"]?>" />
    </form>

</div>
<p>To sort items, simply drag and drop the rows.</p>
<?php */ ?>
<div class="pull-left"><?=anchor($this->curpage."/add?".get_params($_GET, array('page')), "Tambahkan Perusahaan",array("class" => "btn btn-success"))?></div>
<div class="clearfix"></div><br />

<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?php
if(!$list) echo "<p class='error'>Data Kosong.</p>";
else
{
?>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
			<th>No</th>
			<th>Kandidat</th>
			<th>Perusahaan</th>
			<th>Keterangan</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
        
    <?php 
	$O = new OKandidat_promo();                        
    $i=1 + $uri;
    foreach($list as $row):
	
   	 	extract(get_object_vars($row));
		$O->setup($row);
		?>                        
		<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
			<td><?=$i?></td>
			<td>
				<?php  
				$OTmp = new OKandidat($kandidat_id);
				echo $OTmp->get_nama_lengkap();
				unset($OTmp);
				?>
			</td>
            <td>
            	<?php  
				$OTmp = new OPerusahaan($perusahaan_id);
				echo $OTmp->get_nama();
				unset($OTmp);
				?>
            </td>
			<td><?=word_limiter(strip_tags($note),10)?></td>
			<td><?php 
			$actions = NULL;
			$actions[] = anchor($this->curpage."/edit/".$id.$get_params, "<img src='".base_url()."_assets/images/edit.gif' />");
			$actions[] = anchor($this->curpage."/delete/".$id.$get_params, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
			echo implode(" ", $actions);?></td>
		</tr>

		<?php 
		$i++; 
    endforeach; 
    unset($O);
	?>
	</tbody>	
</table>
<?php /*if ($_GET["keyword"] == ""): ?>
<script type="text/javascript">
	$(document).ready(function()
							   {
								   $('table.table tbody').sortable({
															  update: function(event,ui)
															  {
																  update_sorting();
															  }
															  });
							   });
	function update_sorting()
	{
		var url = '<?=site_url_url($this->curpage)?>/sorting';
		var total = $('table.table tbody tr').length;
		var orderlist = new Array(total);
		var count = 0;
		$('table.table tbody tr').each(function (elm)
														  {
															  orderlist[count] = $(this).attr('data_id');
															  count++;
														  });
		var updatelist = orderlist.join(",");
		$.ajax({
			   url: url,
			   data: { 'sorts' : updatelist },
			   type: 'POST'
			   });
	}
	</script>
<?php endif;*/ ?>
<?=$pagination?>
<?php
}
?>
