<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OKandidat_pengalaman();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Kandidat Pengalaman Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Kandidat Id</th>
            <td>
            	<input type="text" name="kandidat_id" value="<?=$kandidat_id?>" required autofocus /> 
                <br/><?=form_error('kandidat_id')?>                
            </td>
        </tr>
		<tr>
            <th>Tanggal Awal</th>
            <td>
            	<input type="text" name="tanggal_awal" class="datepicker" value="<?=$tanggal_awal?>" required  /> <br/><?=form_error('tanggal_awal')?>                
            </td>
        </tr>
		<tr>
            <th>Tanggal Akhir</th>
            <td>
            	<input type="text" name="tanggal_akhir" class="datepicker" value="<?=$tanggal_akhir?>" required  /> <br/><?=form_error('tanggal_akhir')?>                
            </td>
        </tr>
		<tr>
            <th>Perusahaan</th>
            <td>
            	<input type="text" name="perusahaan" value="<?=$perusahaan?>" required  /> 
                <br/><?=form_error('perusahaan')?>                
            </td>
        </tr>
		<tr>
            <th>Bidang Usaha Id</th>
            <td>
            	<input type="text" name="bidang_usaha_id" value="<?=$bidang_usaha_id?>" required  /> 
                <br/><?=form_error('bidang_usaha_id')?>                
            </td>
        </tr>
		<tr>
            <th>Jabatan</th>
            <td>
            	<input type="text" name="jabatan" value="<?=$jabatan?>" required  /> 
                <br/><?=form_error('jabatan')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/kandidat_pengalamans")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>