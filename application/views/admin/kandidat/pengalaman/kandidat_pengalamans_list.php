
<h2>Kandidat Pengalamans Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
	if(!$list) echo "<p class='error'>The Kandidat Pengalamans is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>ID</th>
                                            <th>Kandidat Id</th>
						                            <th>Tanggal Awal</th>
						                            <th>Tanggal Akhir</th>
						                            <th>Perusahaan</th>
						                            <th>Bidang Usaha Id</th>
						                            <th>Jabatan</th>
							<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OKandidat_pengalaman();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$row->id?></td>
                    <td><?=$kandidat_id?></td>
					<td><?=$tanggal_awal?></td>
					<td><?=$tanggal_akhir?></td>
					<td><?=$perusahaan?></td>
					<td><?=$bidang_usaha_id?></td>
					<td><?=$jabatan?></td>
					<td><?php $actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));echo implode(" ", $actions);?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?php echo $pagination; ?>
<?php
  }
?>
