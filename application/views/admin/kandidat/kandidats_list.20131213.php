<h2>Data Kandidat</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
	if(!$list) echo "<p class='error'>The Kandidats is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>ID</th>
                    <th>Nama Depan</th>
                    <th>Nama Akhir</th>
                    <th>Nama Panggilan</th>
                    <?php /*?><th>Agama</th>
                    <th>No Ktp</th>
                    <th>Jenis Kelamin</th>
                    <th>Tempat Lahir</th><?php */?>
                    <th>Tanggal Lahir</th>
                    <th>Foto</th>
                    <?php /*?><th>Asal Alamat</th>
                    <th>Asal Provinsi</th>
                    <th>Asal Kecamatan</th>
                    <th>Asal Kabupaten</th>
                    <th>Asal Kelurahan</th>
                    <th>Asal Telepon</th>
                    <th>Asal Rt</th>
                    <th>Asal Rw</th>
                    <th>Skrg Asal Flag</th>
                    <th>Skrg Provinsi</th>
                    <th>Skrg Kecamatan</th>
                    <th>Skrg Kabupaten</th>
                    <th>Skrg Kelurahan</th>
                    <th>Skrg Telepon</th>
                    <th>Skrg Rt</th>
                    <th>Skrg Rw</th>
                    <th>Status Kawin</th>
                    <th>Nama Pasangan</th>
                    <th>Newsletter Flag</th><?php */?>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OKandidat();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$row->id?></td>
                    <td><?=$nama_depan?></td>
					<td><?=$nama_akhir?></td>
					<td><?=$nama_panggilan?></td>
					<?php /*?><td><?=$agama_id?></td>
					<td><?=$no_ktp?></td>
					<td>
                    <?=($jenis_kelamin_id == "0" ?
					anchor($this->curpage."/set_jenis_kelamin_id/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") :
					anchor($this->curpage."/set_jenis_kelamin_id/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
                    </td>          
					<td><?=$tempat_lahir?></td><?php */?>
					<td><?=parse_date($tanggal_lahir,"d F Y","ID","long")?></td>
					<td><?=(empty($foto) ? "" : "YES")?></td>
					<?php /*?><td><?=$asal_alamat?></td>
					<td><?=$asal_propinsi_id?></td>
					<td><?=$asal_kecamatan_id?></td>
					<td><?=$asal_kabupaten_id?></td>
					<td><?=$asal_kelurahan_id?></td>
					<td><?=$asal_telepon?></td>
					<td><?=$asal_rt?></td>
					<td><?=$asal_rw?></td>
					<td>
                    <?=($skrg_asal_flag== "0" ?
					anchor($this->curpage."/set_skrg_asal_flag/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") :
					anchor($this->curpage."/set_skrg_asal_flag/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
                    </td>          
					<td><?=$skrg_propinsi_id?></td>
					<td><?=$skrg_kecamatan_id?></td>
					<td><?=$skrg_kabupaten_id?></td>
					<td><?=$skrg_kelurahan_id?></td>
					<td><?=$skrg_telepon?></td>
					<td><?=$skrg_rt?></td>
					<td><?=$skrg_rw?></td>
					<td>
                    <?=($status_kawin_id== "0" ?
					anchor($this->curpage."/set_status_kawin_id/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") :
					anchor($this->curpage."/set_status_kawin_id/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
                    </td>          
					<td><?=$nama_pasangan?></td>
					<td>
                    <?=($newsletter_flag== "0" ?
					anchor($this->curpage."/set_newsletter_flag/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") :
					anchor($this->curpage."/set_newsletter_flag/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
                    </td><?php */?>
					<td><?php
					$admin = str_replace(array("/kandidats"),"",$this->curpage);
					$actions = NULL;
					$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
					$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
					$action_options[] = anchor($admin."/kandidat_kesehatans/listing/".$id, "Kesehatan");
					$action_options[] = anchor($admin."/kandidat_keluargas/listing/".$id, "Keluarga");
					$action_options[] = anchor($admin."/kandidat_pendidikans/listing/".$id, "Pendidikan");
					$action_options[] = anchor($admin."/kandidat_pengalamans/listing/".$id, "Pengalaman Kerja");
					$action_options[] = anchor($admin."/kandidat_keterampilans/listing/".$id, "Ketrampilan");
					$action_options[] = anchor($admin."/kandidat_bahasas/listing/".$id, "Bahasa");
					$action_options[] = anchor($admin."/kandidat_dokumens/listing/".$id, "Dokumen");					
					echo implode(" ", $actions)."<br />";
					echo implode("<br />", $action_options);
					?></td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?php echo $pagination; ?>
<?php
  }
?>
