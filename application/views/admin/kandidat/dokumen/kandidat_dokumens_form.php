<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OKandidat_dokumen();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Kandidat Dokumen Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Kandidat Id</th>
            <td>
            	<input type="text" name="kandidat_id" value="<?=$kandidat_id?>" required autofocus /> 
                <br/><?=form_error('kandidat_id')?>                
            </td>
        </tr>
		<tr>
            <th>Dokumen</th>
            <td>
            	<input type="text" name="dokumen" value="<?=$dokumen?>" required  /> 
                <br/><?=form_error('dokumen')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/kandidat_dokumens")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>