<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OKandidat_resume();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Kandidat Resume Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Kandidat Id</th>
            <td>
            	<input type="text" name="kandidat_id" value="<?=$kandidat_id?>" required autofocus /> 
                <br/><?=form_error('kandidat_id')?>                
            </td>
        </tr>
		<tr>
            <th>Judul</th>
            <td>
            	<input type="text" name="judul" value="<?=$judul?>" required  /> 
                <br/><?=form_error('judul')?>                
            </td>
        </tr>
		<tr>
            <th>Isi</th>
            <td>
            	<textarea name="isi" cols="80" rows="20"><?=$isi?></textarea>
				<br/><?=form_error('isi')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/kandidat_resumes")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>