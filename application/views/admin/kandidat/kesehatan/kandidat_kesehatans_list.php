<h2>Data Kesehatan Kandidat</h2><br />
<?php
if(intval($kandidat_id) <= 0):
?>
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?=anchor($this->curpage."/add/".$kandidat_id, "Create new",array("class" => "btn btn-success"))?></div>
<?php
elseif(!$list):
?>
<div class="pull-left"><?=anchor($this->curpage."/add/".$kandidat_id, "Create new",array("class" => "btn btn-success"))?></div>
<?php
endif;
?>
<div class="clearfix"></div><br />

<?php
echo print_status();
?>

<?php
if(!$list) echo "<p class='error'>Data Kosong.</p>";
else
{
?>
<table class="table table-striped table-hover table-condensed">
    <thead>
        <tr>
			<?php /*?><th>ID</th><?php */?>
            <th>Kandidat</th>
            <th>Tinggi (cm)</th>
            <th>Berat (kg)</th>
            <th>Gol Darah</th>
            <th>Kacamata</th>
            <th>Keluhan Kesehatan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        
    <?php 
	$i=1 + $uri;
	$O = new OKandidat_kesehatan();                        
	foreach($list as $row):
	
		extract(get_object_vars($row));
		$O->setup($row);
		?>
		<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
			<?php /*?><td><?=$row->id?></td><?php */?>
            <td><?php
            $OTmp = new OKandidat($kandidat_id);
			echo $OTmp->row->nama_depan;
			unset($OTmp);
			?></td>
			<td><?=format_number($tinggi)?></td>
			<td><?=format_number($berat)?></td>
            <td><?php
            $OTmp = new OMaster_gol_darah($gol_darah_id);
			echo $OTmp->row->nama;
			unset($OTmp);
			?></td>
			<td>
            <?=($kacamata_flag== "0" ?
			anchor($this->curpage."/set_kacamata_flag/1/$id/$kandidat_id", "<img src='".base_url()."_assets/images/disabled.gif' />") :
			anchor($this->curpage."/set_kacamata_flag/0/$id/$kandidat_id", "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
            </td>          
			<td><?=$keluhan_kesehatan?></td>
			<td><?php
			$actions = NULL;
			$actions[] = anchor($this->curpage."/edit/$id/$kandidat_id", "<img src='".base_url()."_assets/images/edit.gif' />");
			$actions[] = anchor($this->curpage."/delete/$id/$kandidat_id", "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
			echo implode(" ", $actions);
			?></td>
		</tr>

		<?php 
		$i++; 
    endforeach; 
	unset($O);
	?>
	</tbody>	
</table>

<?php echo $pagination; ?>
<?php
}
?>
