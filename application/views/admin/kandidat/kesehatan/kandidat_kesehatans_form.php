<?php
if($row)
{
	extract(get_object_vars($row));	 
	$O = new OKandidat_kesehatan();
	$O->setup($row);
}
extract($_POST);
?>

<h2>Form Kesehatan Kandidat</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<form action="" method="post">
<table class="tbl_form">
    <tr>
        <th>Kandidat</th>
        <td>
            <?php
            echo OKandidat::drop_down_select("kandidat_id",$kandidat_id);
            ?>
        </td>
    </tr>
    <tr>
        <th>Tinggi</th>
        <td>
            <input type="text" name="tinggi" value="<?=$tinggi?>" class="fixed" size="6" maxlength="6" autofocus /> cm
        </td>
    </tr>
    <tr>
        <th>Berat</th>
        <td>
            <input type="text" name="berat" value="<?=$berat?>" class="fixed" size="6" maxlength="6" /> kg
        </td>
    </tr>
    <tr>
        <th>Golongan Darah</th>
        <td>
            <?php
            echo OMaster_gol_darah::drop_down_select("gol_darah_id",$gol_darah_id,"","-- Pilih --");
            ?>
        </td>
    </tr>
    <tr>
        <th>Menggunakan kacamata</th>
        <td>
            <?php
            echo form_checkbox("kacamata_flag",1,(empty($kacamata_flag) ? FALSE : TRUE),'class="fixed" id="kacamata_flag"');
            echo ' <label for="kacamata_flag">Ya</label>';
            ?>
        </td>
    </tr>
    <tr>
        <th>Keluhan Kesehatan</th>
        <td>
            <textarea name="keluhan_kesehatan" cols="80" rows="10" class="mceNoEditor"><?=$keluhan_kesehatan?></textarea>
        </td>
    </tr>
            <tr>
        <td></td>
        <td>
            <button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
            <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
            <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("{$this->curpage}/listing/$kandidat_id")?>';"><span class="leftarrow icon"></span>Cancel</button>
        </td>
    </tr>    
</table>
</form>