<?php
// required $list, $pagination, $curpage
if(!$list) echo "<p class='error'>No Data found.</p>";
else
{
?>
<p>Display <strong><?php echo count($list); ?></strong> of <strong><?php echo $total; ?></strong> items.</p>

<table class="table table-striped table-hover table-condensed">
    <thead>
      <tr>
				<th>No</th>
        <th colspan="2">Nama kandidat</th>
        <th>Pendidikan Terakhir</th>
        <th>T. Tinggal</th>
        <th>Added</th>
        <th>Action</th>
      </tr>
    </thead>
    <tfoot>
      <tr>
				<th>No</th>
        <th colspan="2">Nama kandidat</th>
        <th>Pendidikan Terakhir</th>
        <th>T. Tinggal</th>
        <th>Added</th>
        <th>Action</th>
      </tr>
    </tfoot>
    <tbody>
        
    <?php
    $i=1 + $uri;
    $OK = new OKandidat;
    foreach($list as $row):
    	extract(get_object_vars($row));
    	$OK->setup($row);
      // keterampilan
      $keterampilans = $OK->get_all_keterampilan();
      //$keterampilan = implode("<br />", $keterampilans);
     	$keterampilan = implode("<br />", array_unique($keterampilans));
      // pendidikan
      $pnddkn_row = $OK->get_last_pendidikan();
      $OTmp_pnddkn = new OMaster_pendidikan($pnddkn_row->pendidikan_id);
      $pendidikan = $OTmp_pnddkn->row->nama;
      unset($OTmp_pnddkn);
      // lokasi
      $lokasi_row = $OK->get_current_lokasi();
      $lokasi = $lokasi_row->nama;
			?>
		<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
			<td><?=$i?></td>
			<td width="60">
				<img src="<?php echo $OK->get_photo('square'); ?>" width="50" height="50" alt="" />
			</td>
			<td><a href="<?php echo $OK->get_link(); ?>"><strong><?php echo $OK->get_nama_lengkap(); ?> (<?php echo $OK->get_umur(); ?>)</strong></a><br />
        <?php
          echo $keterampilan;
        ?>
       </td>
			<td><?=$pendidikan?></td>
			<td><?=$lokasi?></td>
			<td><?=parse_date_time($dt_added,"d F Y H:ia","ID","long")?></td>
			<td><?php
				$admin = str_replace(array("/kandidats"),"",$curpage);
				$actions = $action_options = NULL;
				$actions[] = anchor($curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
				$actions[] = anchor($curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
				/*
				$action_options[] = anchor($admin."/kandidat_promos/listing?kandidat_id=".$id, "<img src='".base_url()."_assets/images/cms.gif' />", 'title="Promo ke Perusahaan"');
				$action_options[] = anchor($admin."/kandidat_kesehatans/listing/".$id, "Kesehatan");
				$action_options[] = anchor($admin."/kandidat_keluargas/listing/".$id, "Keluarga");
				$action_options[] = anchor($admin."/kandidat_pendidikans/listing/".$id, "Pendidikan");
				$action_options[] = anchor($admin."/kandidat_pengalamans/listing/".$id, "Pengalaman Kerja");
				$action_options[] = anchor($admin."/kandidat_keterampilans/listing/".$id, "Ketrampilan");
				$action_options[] = anchor($admin."/kandidat_bahasas/listing/".$id, "Bahasa");
				$action_options[] = anchor($admin."/kandidat_dokumens/listing/".$id, "Dokumen");
				//*/
				echo implode(" ", $actions)."<br />";
				echo implode("<br />", $action_options);
			?></td>
		</tr>

		<?php 
		$i++; 
	endforeach; 
	unset($O);
	?>
	</tbody>	
</table>

<?php echo $pagination; ?>
<?php
}
?>
