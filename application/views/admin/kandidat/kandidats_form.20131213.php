<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OKandidat();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Form Kandidat</h2><br/>	
<?php echo print_status(); ?>

<?=form_open()?>
	<table class="tbl_form">
        <tr>
            <th>User</th>
            <td>
                <?=OUser::drop_down_select('user_id',$user_id)?>
                <br/><?=form_error('user_id')?>                
            </td>
        </tr>
    	<tr>
            <th>Nama</th>
            <td>
            	<input type="text" name="nama_depan" value="<?=$nama_depan?>" placeholder="Nama Depan" class="fixed" required autofocus /> <input type="text" name="nama_akhir" value="<?=$nama_akhir?>" placeholder="Nama Akhir" class="fixed" /> 
                <br/><?=form_error('nama_depan')?><?=form_error('nama_akhir')?>                
            </td>
        </tr>
		<tr>
            <th>Nama Panggilan</th>
            <td>
            	<input type="text" name="nama_panggilan" value="<?=$nama_panggilan?>" class="fixed" /> 
                <br/><?=form_error('nama_panggilan')?>                
            </td>
        </tr>
		<tr>
            <th>Agama</th>
            <td>
            	<?=OMaster_agama::drop_down_select('agama_id',$agama_id)?>
                <br/><?=form_error('agama_id')?>                
            </td>
        </tr>
		<tr>
            <th>No Ktp</th>
            <td>
            	<input type="text" name="no_ktp" value="<?=$no_ktp?>" /> 
                <br/><?=form_error('no_ktp')?>                
            </td>
        </tr>
		<tr>
            <th>Tempat, Tanggal Lahir</th>
            <td>
            	<input type="text" name="tempat_lahir" value="<?=$tempat_lahir?>" class="fixed" />, <input type="text" name="tanggal_lahir" class="fixed datepicker_dob" value="<?=get_date_lang($tanggal_lahir,"ID")?>" />
                <br/><?=form_error('tempat_lahir')?><?=form_error('tanggal_lahir')?>
            </td>
        </tr>
		<tr>
        	<th>Asal</th>
            <td>
            <table>
                <tr>
                    <th>Alamat</th>
                    <td>
                        <textarea name="asal_alamat" cols="60" rows="2" class="mceNoEditor"><?=$asal_alamat?></textarea>
                        <br/><?=form_error('asal_alamat')?>                
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <label for="asal_rt">RT</label> &nbsp; <input type="text" name="asal_rt" id="asal_rt" value="<?=$asal_rt?>" class="fixed" maxlength="3" size="3" /> &nbsp; 
                        <label for="asal_rw">RW</label> &nbsp; <input type="text" name="asal_rw" id="asal_rw" value="<?=$asal_rw?>" class="fixed" maxlength="3" size="3" /> 
                        <br/><?=form_error('asal_rt')?><?=form_error('asal_rw')?>
                    </td>
                </tr>
                <tr>
                    <th>Propinsi</th>
                    <td>
                        <?=OMaster_lokasi_propinsi::drop_down_select('asal_propinsi_id',$asal_propinsi_id,'','-- Pilih --')?>
                        <br/><?=form_error('asal_propinsi_id')?>                
                    </td>
                </tr>
                <tr>
                    <th>Kabupaten</th>
                    <td>
                        <span id="asal_kabupaten_wrap" data-value="<?php echo $asal_kabupaten_id; ?>">Silahkan memilih Propinsi dahulu</span>
                        <?php //=OMaster_lokasi_kabupaten::drop_down_select('asal_kabupaten_id',$asal_kabupaten_id)?>
                        <br/><?=form_error('asal_kabupaten_id')?>                
                    </td>
                </tr>
                <tr>
                    <th>Kecamatan</th>
                    <td>
                        <?=form_input("asal_kecamatan",$asal_kecamatan)?>
                        <?php //=OMaster_lokasi_kecamatan::drop_down_select('asal_kecamatan_id',$asal_kecamatan_id)?>
                        <br/><?=form_error('asal_kecamatan')?>                
                    </td>
                </tr>
                <tr>
                    <th>Kelurahan</th>
                    <td>
                        <?=form_input("asal_kelurahan",$asal_kelurahan)?>
                        <?php //=OMaster_lokasi_kelurahan::drop_down_select('asal_kelurahan_id',$asal_kelurahan_id)?>
                        <br/><?=form_error('asal_kelurahan')?>                
                    </td>
                </tr>
                <tr>
                    <th>Telepon</th>
                    <td>
                        <input type="text" name="asal_telepon" value="<?=$asal_telepon?>" /> 
                        <br/><?=form_error('asal_telepon')?>                
                    </td>
                </tr>
            </table>
            </td>
        </tr>
        <tr>
            <th>Sekarang = Asal?</th>
            <td>
                <?=form_checkbox("skrg_asal_flag",1,(!empty($skrg_asal_flag) ? TRUE : FALSE),'class="fixed" id="skrg_asal_flag"')?> <label for="skrg_asal_flag">Iya sama dengan alamat asal.</label>
            </td>
        </tr>
        <tr>
        	<th>Sekarang</th>
            <td>
            <table id="skrg_table">
                <tr>
                    <th>Alamat</th>
                    <td>
                        <textarea name="skrg_alamat" cols="60" rows="2" class="mceNoEditor"><?=$skrg_alamat?></textarea>
                        <br/><?=form_error('skrg_alamat')?>                
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td>
                        <label for="skrg_rt">RT</label> &nbsp; <input type="text" name="skrg_rt" id="skrg_rt" value="<?=$skrg_rt?>" class="fixed" maxlength="3" size="3" /> &nbsp; 
                        <label for="skrg_rw">RW</label> &nbsp; <input type="text" name="skrg_rw" id="skrg_rw" value="<?=$skrg_rw?>" class="fixed" maxlength="3" size="3" /> 
                        <br/><?=form_error('skrg_rt')?><?=form_error('skrg_rw')?>
                    </td>
                </tr>
                <tr>
                    <th>Propinsi</th>
                    <td>
                        <?=OMaster_lokasi_propinsi::drop_down_select('skrg_propinsi_id',$skrg_propinsi_id,'','-- Pilih --')?>
                        <br/><?=form_error('skrg_propinsi_id')?>                
                    </td>
                </tr>
                <tr>
                    <th>Kabupaten</th>
                    <td>
                        <span id="skrg_kabupaten_wrap" data-value="<?php echo $skrg_kabupaten_id; ?>">Silahkan memilih Propinsi dahulu</span>
                        <? //=OMaster_lokasi_kabupaten::drop_down_select('skrg_kabupaten_id',$skrg_kabupaten_id)?>
                        <br/><?=form_error('skrg_kabupaten_id')?>                
                    </td>
                </tr>
                <tr>
                    <th>Kecamatan</th>
                    <td>
                        <?=form_input("skrg_kecamatan",$skrg_kecamatan)?>
                        <?php //=OMaster_lokasi_kecamatan::drop_down_select('skrg_kecamatan_id',$skrg_kecamatan_id)?>
                        <br/><?=form_error('skrg_kelurahan')?>                
                    </td>
                </tr>
                <tr>
                    <th>Kelurahan</th>
                    <td>
                        <?=form_input("skrg_kelurahan",$skrg_kelurahan)?>
                        <?php //=OMaster_lokasi_kelurahan::drop_down_select('skrg_kelurahan_id',$skrg_kelurahan_id)?>
                        <br/><?=form_error('skrg_kelurahan')?>                
                    </td>
                </tr>
                <tr>
                    <th>Telepon</th>
                    <td>
                        <input type="text" name="skrg_telepon" value="<?=$skrg_telepon?>" /> 
                        <br/><?=form_error('skrg_telepon')?>                
                    </td>
                </tr>
            </table>	
            </td>
        </tr>
		<tr>
            <th>Nama Pasangan</th>
            <td>
            	<input type="text" name="nama_pasangan" value="<?=$nama_pasangan?>" /> 
                <br/><?=form_error('nama_pasangan')?>                
            </td>
        </tr>
        <tr>
            <th>Foto</th>
            <td>
				<?php
                if($O->id != FALSE)
                {
                    echo "<img src='".$O->get_photo()."' />";
                }
                ?>
                <div style="display:block;" id="last-photo"></div>
                <div style="display:block;" id="preview-photo"></div>
                <div id="swfupload-control-photo" class="clear">
                    <p>Upload maximum <span id="total_photos">1</span> image file(jpg, png, gif) and having maximum size of 1 MB</p>
                    <input type="button" id="button-photo" />
                    <p id="queuestatus-photo" ></p>
                    <ol id="log-photo"></ol>
                    <span class="set"></span> 
                </div>
                <!-- END OF -->
                <br/><?=form_error('foto')?>                
            </td>
        </tr>
        <tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/kandidats")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>

<script>
$(function()
{
	get_kabupaten_ddl("asal_");
	get_kabupaten_ddl("skrg_");
	$('select#asal_propinsi_id').live('change', function(){
		get_kabupaten_ddl("asal_");
	});
	$('select#skrg_propinsi_id').live('change', function(){
		get_kabupaten_ddl("skrg_");
	});
	$('#skrg_asal_flag').live('click', function ()
	{
		var t = $(this);
		if(t.is(':checked') == true)
		{
			$('#skrg_table').css('opacity', 0.4);
			$('#skrg_table input, #skrg_table select, #skrg_table textarea').attr('disabled', 'disabled');
		}
		else
		{
			$('#skrg_table').css('opacity', 1);
			$('#skrg_table input, #skrg_table select, #skrg_table textarea').removeAttr('disabled');
		}
	});
});

function get_kabupaten_ddl(prefix)
{
	var t = $('#'+prefix+'kabupaten_wrap');
	var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': $('#'+prefix+'kabupaten_wrap').data('value'), 'name': prefix+'kabupaten_id', 'default': '-- Pilih --'}
	$.ajax({
		'type': 'POST',
		'async': false,
		url: '<?=site_url("ajax/get_kabupaten_ddl")?>',
		data: p,
		complete: function(xhr, status)
		{
			var ret = xhr.responseText;
			t.html(ret);
		}
	});
}
</script>