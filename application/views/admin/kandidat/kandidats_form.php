<?php
if($row)
{
	extract(get_object_vars($row));	 
	$O = new OKandidat();
	$O->setup($row);
	$data = array("row" => $row, "O" => $O);
}
extract($_POST);
$admin = $this->fpath."tpl/";
?>

<h2>Form Kandidat</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<form action="" method="post" enctype="multipart/form-data">
<table class="tbl_form">
    <tr>
        <th>User</th>
        <td>
            <?=OUser::drop_down_select('user_id',$user_id)?>
        </td>
    </tr>
</table>
<div id="myTabs">
    <ul class="nav nav-tabs" id="myTab">
        <li><a href="#identitas">Identitas</a></li>
        <li><a href="#kesehatan">Kesehatan</a></li>
        <li><a href="#keluarga">Keluarga</a></li>
        <li><a href="#pendidikan">Pendidikan</a></li>
        <li><a href="#pengalaman">Pengalaman Kerja</a></li>
        <li><a href="#keterampilan">Keterampilan</a></li>
        <li><a href="#bahasa">Bahasa</a></li>
        <li><a href="#dokumen">Dokumen</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="identitas"><?php echo $this->load->view($admin."/tpl_kandidat_identitas_form", $data, TRUE)?></div>
        <div class="tab-pane" id="kesehatan"><?php echo $this->load->view($admin."/tpl_kandidat_kesehatan_form", $data, TRUE)?></div>
        <div class="tab-pane" id="keluarga"><?php echo $this->load->view($admin."/tpl_kandidat_keluarga_form", $data, TRUE)?></div>
        <div class="tab-pane" id="pendidikan"><?php echo $this->load->view($admin."/tpl_kandidat_pendidikan_form", $data, TRUE)?></div>
        <div class="tab-pane" id="pengalaman"><?php echo $this->load->view($admin."/tpl_kandidat_pengalaman_form", $data, TRUE)?></div>
        <div class="tab-pane" id="keterampilan"><?php echo $this->load->view($admin."/tpl_kandidat_keterampilan_form", $data, TRUE)?></div>
        <div class="tab-pane" id="bahasa"><?php echo $this->load->view($admin."/tpl_kandidat_bahasa_form", $data, TRUE)?></div>
        <div class="tab-pane" id="dokumen"><?php echo $this->load->view($admin."/tpl_kandidat_dokumen_form", $data, TRUE)?></div>
    </div>
</div>
<div class="btn-block">
    <button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
    <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
    <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url($this->curpage)?>';"><span class="leftarrow icon"></span>Cancel</button>
</div>
</form>

<style>
#myTabs > ul { margin:20px 0; }
.tab-content { height:auto !important; }
</style>
<script type="text/javascript">
$('#myTabs').easytabs();
</script>
<script>
$(function()
{
	get_kabupaten_ddl("asal_");
	get_kabupaten_ddl("skrg_");
	$('select#asal_propinsi_id').live('change', function(){
		get_kabupaten_ddl("asal_");
	});
	$('select#skrg_propinsi_id').live('change', function(){
		get_kabupaten_ddl("skrg_");
	});

	get_kawasan_ddl("asal_");
    $('select#asal_kabupaten_id').live('change', function(){
        get_kawasan_ddl("asal_");
    });
    get_kawasan_ddl("skrg_");
    $('select#skrg_kabupaten_id').live('change', function(){
        get_kawasan_ddl("skrg_");
    });

	set_checkbox_flag('#skrg_asal_flag');
	$('#skrg_asal_flag').live('click', function ()
	{
		set_checkbox_flag( $(this) );
	});
		
	$('#keluarga-table .add_row').live('click', function(e)
	{
		TABLEROW.init('#keluarga-table');
		TABLEROW.add();
	});
	$('#keluarga-table .remove_row').live('click', function(e)
	{
		TABLEROW.init('#keluarga-table');
		TABLEROW.remove($(this));
	});
	
	$('#pendidikan-table .add_row').live('click', function(e)
	{
		TABLEROW.init('#pendidikan-table');
		TABLEROW.add();
	});
	$('#pendidikan-table .remove_row').live('click', function(e)
	{
		TABLEROW.init('#pendidikan-table');
		TABLEROW.remove($(this));
	});
	
	$('#pengalaman-table .add_row').live('click', function(e)
	{
		TABLEROW.init('#pengalaman-table');
		TABLEROW.add();
	});
	$('#pengalaman-table .remove_row').live('click', function(e)
	{
		TABLEROW.init('#pengalaman-table');
		TABLEROW.remove($(this));
	});

	$('#keterampilan-table .add_row').live('click', function(e)
	{
		TABLEROW.init('#keterampilan-table');
		TABLEROW.add();
	});
	$('#keterampilan-table .remove_row').live('click', function(e)
	{
		TABLEROW.init('#keterampilan-table');
		TABLEROW.remove($(this));
	});

	$('#bahasa-table .add_row').live('click', function(e)
	{
		TABLEROW.init('#bahasa-table');
		TABLEROW.add();
	});
	$('#bahasa-table .remove_row').live('click', function(e)
	{
		TABLEROW.init('#bahasa-table');
		TABLEROW.remove($(this));
	});

	$('#dokumen-table .add_row').live('click', function(e)
	{
		TABLEROW.init('#dokumen-table');
		TABLEROW.add();
	});
	$('#dokumen-table .remove_row').live('click', function(e)
	{
		TABLEROW.init('#dokumen-table');
		TABLEROW.remove($(this));
	});
	
});

function set_checkbox_flag(sel)
{
	var t = $(sel);
	var trgt = t.data('target');
	if(t.is(':checked') == true)
	{
		$(trgt).css('opacity', 0.4);
		$(trgt).find('input, select, textarea').attr('disabled', 'disabled');
	}
	else
	{
		$(trgt).css('opacity', 1);
		$(trgt).find('input, select, textarea').removeAttr('disabled');
	}
}
function get_kabupaten_ddl(prefix)
{
    var t = $('#'+prefix+'kabupaten_wrap');
    var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': t.data('value'), 'name': prefix+'kabupaten_id', 'no_default': 1}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
        }
    });
}
function get_kawasan_ddl(prefix)
{
  var t = $('#'+prefix+'kawasan_wrap');
  var p = {'kabupaten_id': $('#'+prefix+'kabupaten_id').val(), 'kawasan_id': t.data('value'), 'name': prefix+'kawasan_id', 'default': t.data('default')}
  $.ajax({
    'type': 'POST',
    'async': false,
    url: '<?php echo site_url("ajax/get_kawasan_ddl"); ?>',
    data: p,
    complete: function(xhr, status)
    {
      var ret = xhr.responseText;
      t.html(ret);
    }
  });
}
</script>