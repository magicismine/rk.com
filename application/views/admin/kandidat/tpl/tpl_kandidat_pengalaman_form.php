<?php /*?><table class="tbl_form">
<tr>
    <th>Waktu</th>
    <td>
    	<?php
        echo form_input("tanggal_awal",$tanggal_awal,'size="10" maxlength="10" class="fixed"');
		echo " - ";
		echo form_input("tanggal_akhir",$tanggal_akhir,'size="10" maxlength="10" class="fixed"');
		?>
    </td>
</tr>
<tr>
    <th>Perusahaan</th>
    <td>
    	<?php
        echo form_input("perusahaan",$perusahaan,'');
		?>
    </td>
</tr>
<tr>
    <th>Bidang Usaha</th>
    <td>
    	<?php
        echo OMaster_bidang_usaha::drop_down_select("bidang_usaha_id",$bidang_usaha_id);
		?>
    </td>
</tr>
<tr>
    <th>Jabatan</th>
    <td>
    	<?php
        echo form_input("jabatan",$jabatan,'');
		?>
    </td>
</tr>
</table><?php */?>

<table class="table table-condensed table-hover table-autoclone" id="pengalaman-table">
    <thead>
    <tr>
        <th>Waktu</th>
        <th>Perusahaan</th>
        <th>Bidang Usaha</th>
        <th>Jabatan</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
	<?php
	if($O->id)
	{
		$edits = $O->get_pengalaman();
		foreach($edits as $r):
			extract(get_object_vars($r));
		?>
		<tr>
			<td><?php
			echo form_input('edit_pengalaman_tanggal_awal['.$id.']',get_date_lang($tanggal_awal,"ID"),'size="10" maxlength="10" class="datepicker_dob"');
			echo " - ";
			echo form_input('edit_pengalaman_tanggal_akhir['.$id.']',get_date_lang($tanggal_akhir,"ID"),'size="10" maxlength="10" class="datepicker_dob"');
			?></td>
			<td><?php
			echo form_input('edit_pengalaman_perusahaan['.$id.']',$perusahaan,'size="50" class=""');
			?></td>
			<td><?php
			echo OMaster_bidang_usaha::drop_down_select('edit_pengalaman_bidang_usaha_id['.$id.']',$bidang_usaha_id);
			?></td>
			<td><?php
			echo form_input('edit_pengalaman_jabatan['.$id.']',$jabatan,'size="40" class="fixed"');
			?></td>
			<td>
			<?php
			echo form_checkbox('edit_pengalaman_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
			?>
			</td>
		</tr>
		<?php
		endforeach;
	}
    ?>
    <tr>
        <td><?php
		/*
		if(empty($dt)) $dt = date("Y-m-d");
		$ts = (is_string($dt) ? strtotime($dt) : $dt);
		$year = date("Y",$ts);
		$month = date("n",$ts);
		$day = date("d",$ts);
		if(empty($start_dt)) $start_dt = date("Y",strtotime("-80 year"));
		if(empty($end_dt)) $end_dt = date("Y");
		
        //echo "Mulai: ".date_ddl("pengalaman_tanggal_awal_date[]",$day).month_ddl("pengalaman_tanggal_awal_month[]",$month).year_ddl("pengalaman_tanggal_awal_year[]",($year-1),$start_dt,$end_dt);
		//echo "<br />";
		//echo "Akhir: ".date_ddl("pengalaman_tanggal_akhir_date[]",$day).month_ddl("pengalaman_tanggal_akhir_month[]",$month).year_ddl("pengalaman_tanggal_akhir_year[]",$year,$start_dt,$end_dt);
		*/
		echo form_input('pengalaman_tanggal_awal[]','','size="10" maxlength="10" class="datepicker_dob"');
		echo " - ";
		echo form_input('pengalaman_tanggal_akhir[]','','size="10" maxlength="10" class="datepicker_dob"');
        ?></td>
        <td><?php
        echo form_input('pengalaman_perusahaan[]','','size="50" class=""');
        ?></td>
        <td><?php
        echo OMaster_bidang_usaha::drop_down_select('pengalaman_bidang_usaha_id[]','','','-- Pilih --');
        ?></td>
        <td><?php
        echo form_input('pengalaman_jabatan[]','','size="40" class="fixed"');
        ?></td>
        <td><button type="button" class="close remove_row" aria-hidden="true" title="Hapus Baris">&times;</button></td>
    </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" class="text-right"><button type="button" class="btn btn-primary btn-sm add_row" title="Tambah Baris">+ Tambah</button></td>
        </tr>
    </tfoot>
</table>
