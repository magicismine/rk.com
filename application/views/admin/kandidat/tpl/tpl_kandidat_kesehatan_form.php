<?php
extract(get_object_vars($row));
if($O->id)
{
	extract(get_object_vars($O->get_kesehatan()));
}
?><table class="tbl_form">
<tr>
    <th>Tinggi</th>
    <td>
    	<?=form_input('kesehatan_tinggi',$tinggi,'class="fixed" size="6" maxlength="6" autofocus')?> cm
    </td>
</tr>
<tr>
    <th>Berat</th>
    <td>
    	<?=form_input('kesehatan_berat',$berat,'class="fixed" size="6" maxlength="6"')?> kg
    </td>
</tr>
<tr>
    <th>Golongan Darah</th>
    <td>
        <?php
        echo OMaster_gol_darah::drop_down_select("kesehatan_gol_darah_id",$gol_darah_id,"","-- Pilih --");
        ?>
    </td>
</tr>
<tr>
    <th>Menggunakan kacamata</th>
    <td>
        <?php
        echo form_checkbox("kesehatan_kacamata_flag",1,(empty($kacamata_flag) ? FALSE : TRUE),'class="fixed" id="kacamata_flag"');
        echo ' <label for="kacamata_flag">Ya</label>';
        ?>
    </td>
</tr>
<tr>
    <th>Keluhan Kesehatan</th>
    <td>
        <textarea name="kesehatan_keluhan_kesehatan" cols="80" rows="10" class="mceNoEditor"><?=$keluhan_kesehatan?></textarea>
    </td>
</tr>
</table>