<?php /*?>
<table class="tbl_form">
<tr>
    <th>Tingkat Pendidikan</th>
    <td>
    	<?php
        echo OMaster_pendidikan::drop_down_select('pendidikan_id',$pendidikan_id);
		?>
    </td>
</tr>
<tr>
    <th>Waktu</th>
    <td>
    	<?php
        echo form_input("tahun_awal",$tahun_awal,'size="10" maxlength="10" class="fixed"');
		echo " Sampai ";
		echo form_input("tahun_akhir",$tahun_akhir,'size="10" maxlength="10" class="fixed"');
		?>
    </td>
</tr>
<tr>
    <th>Nama Sekolah/ Perguruan Tinggi</th>
    <td>
    	<?php
        echo form_input("nama",$nama,'');
		?>
    </td>
</tr>
<tr>
    <th>Jurusan/ Program Studi</th>
    <td>
    	<?php
        echo form_input("jurusan",$jurusan,'');
		?>
    </td>
</tr>
<tr>
    <th>Nilai Rata-rata</th>
    <td>
    	<?php
        echo form_input("nilai",$nilai,'size="5" maxlength="5" class="fixed"');
		?>
    </td>
</tr>
</table>
<?php */?>
<table class="table table-condensed table-hover table-autoclone" id="pendidikan-table">
    <thead>
    <tr>
        <th>Tingkat Pendidikan</th>
        <th>Waktu</th>
        <th>Nama Sekolah/ Perguruan Tinggi</th>
        <th>Jurusan/ Program Studi</th>
        <th>Nilai Rata-rata</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
	<?php
	if($O->id)
	{
		$edits = $O->get_pendidikan();
		foreach($edits as $r):
			extract(get_object_vars($r));
		?>
		<tr>
			<td><?php
			echo OMaster_pendidikan::drop_down_select('edit_pendidikan_id['.$id.']',$pendidikan_id);
			?></td>
			<td><?php
			$start_year = date("Y",strtotime("-80 year"));
			$end_year = date("Y");
			echo year_ddl('edit_pendidikan_tahun_awal['.$id.']',$tahun_awal,$start_year,$end_year,"");
			echo " - ";
			echo year_ddl('edit_pendidikan_tahun_akhir['.$id.']',$tahun_akhir,$start_year,$end_year,"");
			?></td>
			<td><?php
			echo form_input('edit_pendidikan_nama['.$id.']',$nama,'size="60" class=""');
			?></td>
			<td><?php
			echo form_input('edit_pendidikan_jurusan['.$id.']',$jurusan,'size="40" class=""');
			?></td>
			<td><?php
			echo form_input('edit_pendidikan_nilai['.$id.']',$nilai,'size="5" maxlength="5" class="fixed"');
			?></td>
			<td>
			<?php
			echo form_checkbox('edit_pendidikan_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
			?>
			</td>
		</tr>
		<?php
		endforeach;
	}
    ?>
    <tr>
        <td><?php
        echo OMaster_pendidikan::drop_down_select('pendidikan_id[]','');
        ?></td>
        <td><?php
		$start_year = date("Y",strtotime("-80 year"));
		$end_year = date("Y");
        echo year_ddl('pendidikan_tahun_awal[]',(intval($end_year)-3),$start_year,$end_year,"");
		echo " - ";
		echo year_ddl('pendidikan_tahun_akhir[]',$end_year,$start_year,$end_year,"");
        ?></td>
        <td><?php
        echo form_input('pendidikan_nama[]','','size="60" class=""');
        ?></td>
        <td><?php
        echo form_input('pendidikan_jurusan[]','','size="40" class=""');
        ?></td>
        <td><?php
        echo form_input('pendidikan_nilai[]','','size="5" maxlength="5" class="fixed"');
        ?></td>
        <td><button type="button" class="close remove_row" aria-hidden="true" title="Hapus Baris">&times;</button></td>
    </tr>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6" class="text-right"><button type="button" class="btn btn-primary btn-sm add_row" title="Tambah Baris">+ Tambah</button></td>
        </tr>
    </tfoot>
</table>
