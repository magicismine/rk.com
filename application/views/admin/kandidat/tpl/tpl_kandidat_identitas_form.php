<?php
extract(get_object_vars($row));
?><table class="tbl_form">
    <tr>
        <th>Nama</th>
        <td>
            <input type="text" name="nama_depan" value="<?=$nama_depan?>" placeholder="Nama Depan" class="fixed" required autofocus /> <input type="text" name="nama_akhir" value="<?=$nama_akhir?>" placeholder="Nama Akhir" class="fixed" /> 
        </td>
    </tr>
    <tr>
        <th>Nama Panggilan</th>
        <td>
            <input type="text" name="nama_panggilan" value="<?=$nama_panggilan?>" class="fixed" /> 
        </td>
    </tr>
    <tr>
        <th>Agama</th>
        <td>
            <?=OMaster_agama::drop_down_select('agama_id',$agama_id)?>
        </td>
    </tr>
    <tr>
        <th>No Ktp</th>
        <td>
            <input type="text" name="no_ktp" value="<?=$no_ktp?>" /> 
        </td>
    </tr>
    <tr>
        <th>Tempat, Tanggal Lahir</th>
        <td>
            <input type="text" name="tempat_lahir" value="<?=$tempat_lahir?>" class="fixed" />, <input type="text" name="tanggal_lahir" class="fixed datepicker_dob" value="<?=get_date_lang($tanggal_lahir,"ID")?>" />
        </td>
    </tr>
    <tr>
        <th>Asal</th>
        <td>
        <table>
            <tr>
                <th>Alamat</th>
                <td>
                    <textarea name="asal_alamat" cols="60" rows="2" class="mceNoEditor"><?=$asal_alamat?></textarea>
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <label for="asal_rt">RT</label> &nbsp; <input type="text" name="asal_rt" id="asal_rt" value="<?=$asal_rt?>" class="fixed" maxlength="3" size="3" /> &nbsp; 
                    <label for="asal_rw">RW</label> &nbsp; <input type="text" name="asal_rw" id="asal_rw" value="<?=$asal_rw?>" class="fixed" maxlength="3" size="3" /> 
                </td>
            </tr>
            <tr>
                <th>Propinsi</th>
                <td>
                    <?=OMaster_lokasi_propinsi::drop_down_select('asal_propinsi_id',$asal_propinsi_id)?>
                </td>
            </tr>
            <tr>
                <th>Kabupaten</th>
                <td>
                    <span id="asal_kabupaten_wrap" data-value="<?php echo $asal_kabupaten_id; ?>">
                        Silahkan memilih Propinsi dahulu.
                    </span>
                    <?php //=OMaster_lokasi_kabupaten::drop_down_select('asal_kabupaten_id',$asal_kabupaten_id)?>
                </td>
            </tr>
            <tr>
                <th>Kawasan</th>
                <td>
                    <span id="asal_kawasan_wrap" data-value="<?php echo $asal_kawasan_id; ?>">
                        Silahkan memilih Kabupaten dahulu.
                    </span>
                </td>
            </tr>
            <tr>
                <th>Kecamatan</th>
                <td>
                    <?=form_input("asal_kecamatan",$asal_kecamatan)?>
                    <?php //=OMaster_lokasi_kecamatan::drop_down_select('asal_kecamatan_id',$asal_kecamatan_id)?>
                </td>
            </tr>
            <tr>
                <th>Kelurahan</th>
                <td>
                    <?=form_input("asal_kelurahan",$asal_kelurahan)?>
                    <?php //=OMaster_lokasi_kelurahan::drop_down_select('asal_kelurahan_id',$asal_kelurahan_id)?>
                </td>
            </tr>
            <tr>
                <th>Telepon</th>
                <td>
                    <input type="text" name="asal_telepon" value="<?=$asal_telepon?>" /> 
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <th>Alamat Sekarang = Asal?</th>
        <td>
            <label for="skrg_asal_flag"><?=form_checkbox("skrg_asal_flag",1,(!empty($skrg_asal_flag) ? TRUE : FALSE),'class="fixed" id="skrg_asal_flag" data-target="#skrg_table"')?>&nbsp;
            Iya sama dengan alamat asal.</label>
        </td>
    </tr>
    <tr>
        <th>Sekarang</th>
        <td>
        <table id="skrg_table">
            <tr>
                <th>Alamat</th>
                <td>
                    <textarea name="skrg_alamat" cols="60" rows="2" class="mceNoEditor"><?=$skrg_alamat?></textarea>
                    <br/><?=form_error('skrg_alamat')?>                
                </td>
            </tr>
            <tr>
                <th></th>
                <td>
                    <label for="skrg_rt">RT</label> &nbsp; <input type="text" name="skrg_rt" id="skrg_rt" value="<?=$skrg_rt?>" class="fixed" maxlength="3" size="3" /> &nbsp; 
                    <label for="skrg_rw">RW</label> &nbsp; <input type="text" name="skrg_rw" id="skrg_rw" value="<?=$skrg_rw?>" class="fixed" maxlength="3" size="3" /> 
                </td>
            </tr>
            <tr>
                <th>Propinsi</th>
                <td>
                    <?=OMaster_lokasi_propinsi::drop_down_select('skrg_propinsi_id',$skrg_propinsi_id)?>
                </td>
            </tr>
            <tr>
                <th>Kabupaten</th>
                <td>
                    <span id="skrg_kabupaten_wrap" data-value="<?php echo $skrg_kabupaten_id; ?>">
                        Silahkan memilih Propinsi dahulu
                    </span>
                    <? //=OMaster_lokasi_kabupaten::drop_down_select('skrg_kabupaten_id',$skrg_kabupaten_id)?>
                </td>
            </tr>
            <tr>
                <th>Kawasan</th>
                <td>
                    <span id="skrg_kawasan_wrap" data-value="<?php echo $skrg_kawasan_id; ?>">
                        Silahkan memilih Kabupaten dahulu.
                    </span>
                </td>
            </tr>
            <tr>
                <th>Kecamatan</th>
                <td>
                    <?=form_input("skrg_kecamatan",$skrg_kecamatan)?>
                    <?php //=OMaster_lokasi_kecamatan::drop_down_select('skrg_kecamatan_id',$skrg_kecamatan_id)?>
                </td>
            </tr>
            <tr>
                <th>Kelurahan</th>
                <td>
                    <?=form_input("skrg_kelurahan",$skrg_kelurahan)?>
                    <?php //=OMaster_lokasi_kelurahan::drop_down_select('skrg_kelurahan_id',$skrg_kelurahan_id)?>
                </td>
            </tr>
            <tr>
                <th>Telepon</th>
                <td>
                    <input type="text" name="skrg_telepon" value="<?=$skrg_telepon?>" /> 
                </td>
            </tr>
        </table>	
        </td>
    </tr>
    <tr>
        <th>Foto</th>
        <td>
            <?php
            if($O->id != FALSE)
            {
                echo "<img src='".$O->get_photo()."' />";
            }
            ?>
            <div style="display:block;" id="last-photo"></div>
            <div style="display:block;" id="preview-photo"></div>
            <div id="swfupload-control-photo" class="clear">
                <p>Upload maximum <span id="total_photos">1</span> image file(jpg, png, gif) and having maximum size of 1 MB</p>
                <input type="button" id="button-photo" />
                <p id="queuestatus-photo" ></p>
                <ol id="log-photo"></ol>
                <span class="set"></span> 
            </div>
            <!-- END OF -->
        </td>
    </tr>
</table>
