<?php /*?><table class="tbl_form">
<tr>
    <th>Upload Dokumen Pendukung</th>
    <td>
    	<?php
        echo form_upload('dokumen','','');
		?>
    </td>
</tr>
</table><?php */?>

<table class="table table-condensed table-hover table-autoclone" id="dokumen-table">
    <thead>
    <tr>
        <th>File</th>
        <th>Active?</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
	<?php
	if($O->id)
	{
		$edits = $O->get_dokumen();
		foreach($edits as $r):
			extract(get_object_vars($r));
		?>
		<tr>
			<td><?php
			echo form_upload('edit_dokumen['.$id.']','','');
			echo "<br />".$dokumen;
			?></td>
			<td><?php
			echo form_checkbox('edit_dokumen_aktif_flag['.$id.']',1,($aktif_flag == 1 ? TRUE : FALSE),'class="fixed"')." Ya";
			?></td>
			<td>
			<?php
			echo form_checkbox('edit_dokumen_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
			?>
			</td>
		</tr>
		<?php
		endforeach;
	}
    ?>
    <tr>
        <td><?php
        echo form_upload('dokumen[]','','');
        ?></td>
        <td><?php
        echo form_checkbox('dokumen_aktif_flag[]',1,TRUE,'class="fixed"')." Ya";
        ?></td>
        <td><button type="button" class="close remove_row" aria-hidden="true" title="Hapus Baris">&times;</button></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5" class="text-right"><button type="button" class="btn btn-primary btn-sm add_row" title="Tambah Baris">+ Tambah</button></td>
    </tr>
    </tfoot>
</table>
