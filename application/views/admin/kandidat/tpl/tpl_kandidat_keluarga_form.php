<?php
extract(get_object_vars($row));
?><table class="tbl_form">
<tr>
    <th>Status Nikah</th>
    <td>
    	<?php
        echo OMaster_status_kawin::drop_down_select("status_kawin_id", $status_kawin_id);
		?>
    </td>
</tr>
<tr>
    <th>Anggota Keluarga Serumah</th>
    <td>
    </td>
</tr>
<tr>
    <th>Nama Pasangan Suami/Istri</th>
    <td>
    	<?php
        echo form_input('nama_pasangan',$nama_pasangan,'');
		?>
    </td>
</tr>
<tr>
    <td colspan="2">
        <table class="table table-condensed table-hover table-autoclone" id="keluarga-table">
            <thead>
            <tr>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Hubungan</th>
                <th>Pendidikan</th>
                <th></th>
			</tr>
            </thead>
            <tbody>
            <?php
            if($O->id)
			{
				$edits = $O->get_keluarga();
				foreach($edits as $r):
					extract(get_object_vars($r));
				?>
				<tr>
					<td><?php
					echo form_input('edit_keluarga_nama['.$id.']',$nama,'class="fixed"');
					?></td>
					<td><?php
					echo OMaster_jenis_kelamin::drop_down_select('edit_keluarga_jenis_kelamin_id['.$id.']',$jenis_kelamin_id);
					?></td>
					<td><?php
					echo OMaster_shdk::drop_down_select('edit_keluarga_shdk_id['.$id.']',$shdk_id);
					?></td>
					<td><?php
					echo OMaster_pendidikan::drop_down_select('edit_keluarga_pendidikan_id['.$id.']',$pendidikan_id);
					?></td>
					<td>
					<?php
					echo form_checkbox('edit_keluarga_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
					?>
					</td>
				</tr>
				<?php
				endforeach;
			}
			?>
            <tr>
                <td><?php
                echo form_input('keluarga_nama[]',$keluarga_nama,'class="fixed"');
                ?></td>
                <td><?php
                echo OMaster_jenis_kelamin::drop_down_select('keluarga_jenis_kelamin_id[]',$keluarga_jenis_kelamin_id);
                ?></td>
                <td><?php
                echo OMaster_shdk::drop_down_select('keluarga_shdk_id[]',$keluarga_shdk_id);
                ?></td>
                <td><?php
                echo OMaster_pendidikan::drop_down_select('keluarga_pendidikan_id[]',$keluarga_pendidikan_id);
                ?></td>
                <td><button type="button" class="close remove_row" aria-hidden="true" title="Hapus Baris">&times;</button></td>
			</tr>
            </tbody>
            <tfoot>
            <tr>
            	<td colspan="5" class="text-right"><button type="button" class="btn btn-primary btn-sm add_row" title="Tambah Baris">+ Tambah</button></td>
            </tr>
            </tfoot>
        </table>
    </td>
</tr>
</table>