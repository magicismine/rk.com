<?php /*?><table class="tbl_form">
<tr>
    <th>Kategori Bidang Keahlian</th>
    <td>
    	<?php
        echo OMaster_kategori::drop_down_select("kategori_id",$kategori_id);
		?>
    </td>
</tr>
<tr>
    <th>Nilai Keahlian (skala 5)</th>
    <td>
    	<?php
		$arr = array("1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5);
        echo radios("nilai_skala",$arr,$nilai_skala,"class='fixed'"," &nbsp; ");
		?>
    </td>
</tr>
<tr>
    <th>Badan Sertifikasi</th>
    <td>
    	<?php
        echo form_input("sertifikasi",$sertifikasi,'');
		?>
    </td>
</tr>
</table><?php */?>
<?php
$arr = array("" => "-- Pilih --", "1" => 1, "2" => 2, "3" => 3, "4" => 4, "5" => 5);
?>
<table class="table table-condensed table-hover table-autoclone" id="keterampilan-table">
    <thead>
    <tr>
        <th>Kategori Bidang Keahlian</th>
        <th>Nilai Keahlian (skala 5)</th>
        <th>Badan Sertifikasi</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
	<?php
	if($O->id)
	{
		$edits = $O->get_keterampilan();
		foreach($edits as $r):
			extract(get_object_vars($r));
		?>
		<tr>
			<td><?php
			echo OMaster_kategori::drop_down_tree_select('edit_keterampilan_kategori_id['.$id.']',$kategori_id,'','-- Pilih --');
			?></td>
			<td><?php
			echo form_dropdown('edit_keterampilan_nilai_skala['.$id.']',$arr,$nilai_skala,"class='fixed'"," &nbsp; ");
			?></td>
			<td><?php
			echo form_input('edit_keterampilan_sertifikasi['.$id.']',$sertifikasi,'size="60"');
			?></td>
			<td>
			<?php
			echo form_checkbox('edit_keterampilan_delete['.$id.']',1,FALSE,'class="fixed"')." &nbsp; Delete";
			?>
			</td>
		</tr>
		<?php
		endforeach;
	}
    ?>
    <tr>
        <td><?php
        echo OMaster_kategori::drop_down_select('keterampilan_kategori_id[]','','','-- Pilih --');
        ?></td>
        <td><?php
        echo form_dropdown('keterampilan_nilai_skala[]',$arr,'',"class='fixed'"," &nbsp; ");
        ?></td>
        <td><?php
        echo form_input('keterampilan_sertifikasi[]','','size="60"');
        ?></td>
        <td><button type="button" class="close remove_row" aria-hidden="true" title="Hapus Baris">&times;</button></td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5" class="text-right"><button type="button" class="btn btn-primary btn-sm add_row" title="Tambah Baris">+ Tambah</button></td>
    </tr>
    </tfoot>
</table>
