<h2>Ads Data</h2><br />
<div class="clearfix"></div><br />
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?php
	if(!$list) echo "<p class='error'>The Ads is empty.</p>";
    else
    {
?>
		
        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>#</th>
                            <th>Nama</th>
                            <th>Url</th>
                            <th>Photo</th>
                            <th>Width</th>
                            <th>Height</th>
                            <th>Active ?</th>
							<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
						$O = new OAd();                        
						$O->setup($row); ?>                        
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$i;?></td>
                    <td><?=$nama?></td>
					<td><?=$url?></td>
					<td><img src='<?=$O->get_photo()?>' /></td><td><?=$width?></td>
					<td><?=$height?></td>
                    <td>
                        <?=(empty($active_flag) ?
                    anchor($this->curpage."/set_status/1/".$id, "<img src='".base_url("_assets/images/disabled.gif")."' alt='NO' />",array('title' => 'Click to Set Active')) :
                    anchor($this->curpage."/set_status/0/".$id, "<img src='".base_url("_assets/images/enabled.gif")."' alt='YES' />",array('title' => 'Click to Deactive')))?>

                    </td>
					<td>
                    <?php 
                    $actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");                
                    echo implode(" ", $actions);?>
                    </td>
				</tr>
		
        <?php 
            unset($O);
			$i++; 
            endforeach; 
        ?>
        	</tbody>	
        </table>
        
        <?=$pagination?>
<?php
  }
?>
