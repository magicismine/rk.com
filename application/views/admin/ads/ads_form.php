<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OAd();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Ad Form</h2><br/>	
<?=print_error($this->session->flashdata('warning'))?>
<?=print_success($this->session->flashdata('success'))?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Nama</th>
            <td>
            	<input type="text" name="nama" value="<?=$nama?>" required <?php if(($id == 1) OR ($id == 2) OR ($id == 3) OR ($id == 4) OR ($id == 5)){ echo "readonly";} ?> autofocus /> 
                <br/><?=form_error('nama')?>                
            </td>
        </tr>
		<tr>
            <th>Url</th>
            <td>
            	<input type="text" name="url" value="<?=$url?>" required  /> 
                <br/><?=form_error('url')?>                
            </td>
        </tr>
		<tr>
            <th>Photo</th>
            <td>
            	                    <?php
										 if($O->id != FALSE)
										 {
											 echo "<img src='".$O->get_photo()."' />";
										 }
										 ?>                    <div style="display:block;" id="last-photo"></div>
                    <div style="display:block;" id="preview-photo"></div>
                                                <div id="swfupload-control-photo" class="clear">
                        <p>Upload maximum <span id="total_photos">1</span> image files(jpg, png, gif), having maximum size of 1 MB</p>
                        <input type="button" id="button-photo" />
                        <p id="queuestatus-photo" ></p>
                        <ol id="log-photo"></ol>
                        <span class="set"></span> 
                    </div>
                    <!-- END OF -->
                    <br/><?=form_error('photo')?>                
            </td>
        </tr>
		<tr>
            <th>Width</th>
            <td>
            	<input type="text" name="width" value="<?=$width?>" <?php if(($id == 1) OR ($id == 2) OR ($id == 3) OR ($id == 4) OR ($id == 5)){ echo "readonly";} ?> /><span class="keterangan">*</span>
                <br/>
                <span class="keterangan">
                *If width value = 0, it means width from ORIGINAL IMAGE.<br>
                *If position = User's Left Sidebar, max value are 276 pixels.
                </span>
                <br>
                <?=form_error('width')?>                
            </td>
        </tr>
		<tr>
            <th>Height</th>
            <td>
            	<input type="text" name="height" value="<?=$height?>" <?php if(($id == 1) OR ($id == 2) OR ($id == 3) OR ($id == 4) OR ($id == 5)){ echo "readonly";} ?> /><span class="keterangan">*</span>
                <br/>
                <span class="keterangan">*If height value = 0, it means height from ORIGINAL IMAGE.</span>
                <br>
                <?=form_error('height')?>                
            </td>
        </tr>
        <tr>
            <th>Show On</th>
            <td>
            <?php 
            if(($id == 1) OR ($id == 2) OR ($id == 3) OR ($id == 4)  OR ($id == 5)) {?>
            <?php echo "Hanya Homepage"; ?>
                <input type="hidden" name="show_on" value="fixed">
            <?php }else{ ?>
                <select name="show_on">
                    <option <?php if((empty($show_on)) OR ($show_on == "fixed")) { echo "selected='selected'"; } ?> value="fixed">Tidak Ditampilkan </option>
                    <option <?php if($show_on == "both"){ echo "selected='selected'";} ?> value="both">Kandidat dan Perusahaan</option>
                    <option <?php if($show_on == "kandidat"){ echo "selected='selected'";} ?>  value="kandidat">Hanya Kandidat</option>
                    <option <?php if($show_on == "perusahaan"){ echo "selected='selected'";} ?>  value="perusahaan">Hanya Perusahaan</option>
                </select>
            <?php } ?>
            </td>
        </tr>
        <?php 
        if(($id == 1) OR ($id == 2) OR ($id == 3) OR ($id == 4)  OR ($id == 5)) {} else{
        ?>
        <tr>
            <th>Position</th>
            <td>
                <select name="position">
                    <option <?php if(empty($position) OR ($position == "user_sidebar_left")) { echo "selected = 'selected'"; } ?> value="user_sidebar_left">User's Left Sidebar</option>
                    <option <?php if($position == "user_bottom") { echo "selected = 'selected'"; } ?> value="user_bottom">User's bottom</option>
                </select>
            </td>
        </tr>
        <?php } ?>
        <tr>
            <th>Active ?</th>
            <td>
                <select name="active_flag">
                    <option <?php if(empty($active_flag)) { echo "selected = 'selected'"; } ?> value="0">No</option>
                    <option <?php if($active_flag == 1) { echo "selected = 'selected'"; } ?> value="1">Yes</option>
                </select>
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/ads")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>
<style>
    .keterangan{ font-size: 10px; color: red; padding: 5px 10px 5px 0px;}

</style>