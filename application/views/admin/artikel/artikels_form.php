<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OArtikel();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Artikel Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Nama</th>
            <td>
            	<input type="text" name="nama" value="<?=$nama?>" required autofocus /> 
                <br/><?=form_error('nama')?>                
            </td>
        </tr>
		<tr>
            <th>Isi</th>
            <td>
            	<textarea name="isi" cols="80" rows="20"><?=$isi?></textarea>
				<br/><?=form_error('isi')?>                
            </td>
        </tr>
		<tr>
            <th>Image</th>
            <td>
                	<?php
					 if($O->id != FALSE)
					 {
						 echo "<img src='".$O->get_photo()."' />";
					 }
					 ?>                    
					<div style="display:block;" id="last-photo"></div>
                    <div style="display:block;" id="preview-photo"></div>
                                                <div id="swfupload-control-photo" class="clear">
                        <p>Upload maximum <span id="total_photos">1</span> image files(jpg, png, gif), having maximum size of 1 MB</p>
                        <input type="button" id="button-photo" />
                        <p id="queuestatus-photo" ></p>
                        <ol id="log-photo"></ol>
                        <span class="set"></span> 
                    </div>
                    <!-- END OF -->
                    <br/><?=form_error('photo')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/artikels")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>