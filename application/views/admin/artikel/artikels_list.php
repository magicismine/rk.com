<h2>Artikels Data</h2><br />
<?php if ($_GET["keyword"] == ""): ?><p>To sort items, simply drag and drop the rows.</p><?php endif; ?><div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
if(!$list) echo "<p class='error'>The Artikels is empty.</p>";
else
{
?>


<table class="table table-striped table-hover table-condensed">
  <thead>
  	<tr>
			<th>ID</th>
			<th>Nama</th>
			<th>Isi</th>
			<th>Photo</th>
			<th>Action</th>
    </tr>
  </thead>
  <tbody>
    
	<?php 
	$i=1 + $uri;
	foreach($list as $row):

		extract(get_object_vars($row));
		$O = new OArtikel();                        
		$O->setup($row);
		?>
	<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
		<td><?=$row->id?></td>
		<td><?=$nama?></td>
		<td><?=word_limiter(strip_tags($isi), 50)?></td>
		<td><img src='<?=$O->get_photo()?>' /></td><td><?php $actions = NULL;$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));echo implode(" ", $actions);?></td>
	</tr>

	<?php 
		unset($O);
		$i++; 
	endforeach; 
	?>
	</tbody>	
</table>

<?php if ($_GET["keyword"] == ""): ?>
<script type="text/javascript">
$(document).ready(function() {
	$('table.table tbody').sortable({
	update: function(event,ui)
	{
	  update_sorting();
	}
	});
});
function update_sorting()
{
	var url = '<?=site_url("admin/artikels/sorting")?>';
	var total = $('table.table tbody tr').length;
	var orderlist = new Array(total);
	var count = 0;
	$('table.table tbody tr').each(function (elm) {
		orderlist[count] = $(this).attr('data_id');
		count++;
	});
	var updatelist = orderlist.join(",");
	$.ajax({
		url: url,
		data: { 'sorts' : updatelist },
		type: 'POST'
	});
}
</script>
<?php endif; ?>

<?php echo $pagination; ?>

<?php
}
?>
