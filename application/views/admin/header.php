<?php
	session_start();
	$_SESSION['KCFINDER']=array();
	$_SESSION['KCFINDER']['disabled'] = false;
	$_SESSION['KCFINDER']['uploadURL'] = "../tinymcpuk/uploads";
	$_SESSION['KCFINDER']['uploadDir'] = "";
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo get_setting('sitename'); ?></title>
    
    <link rel="stylesheet" href="<?=base_url("_assets/css/admin.css")?>" />
    <?php /*?><link rel="stylesheet" href="<?=base_url("_assets/css/css3buttons.css")?>" /><?php */?>
    <link rel="stylesheet" href="<?=base_url("_assets/css/smoothness/jquery-ui-1.8.9.custom.css")?>" />
    <link rel="stylesheet" href="<?=base_url("_assets/css/bootstrap.min.css")?>" />
    <link rel="stylesheet" href="<?=base_url("_assets/chosen/chosen.min.css")?>" />
    <link rel="stylesheet" href="<?=base_url("_assets/css/custom.css")?>" />
    
    <?php /*?><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script><?php */?>
    <script src="<?=base_url("_assets/js/jquery-1.7.1.min.js")?>"></script>
    <script src="<?=base_url("_assets/js/jquery-ui.min.js")?>"></script>
    <script src="<?=base_url("_assets/js/bootstrap.min.js")?>"></script>
    <script src="<?=base_url("_assets/js/jquery.easytabs.min.js")?>"></script>
    <script src="<?=base_url("_assets/chosen/chosen.jquery.min.js")?>"></script>
    <script src="<?=base_url("_assets/chosen/chosen.init.js")?>"></script>
    <script src="<?=base_url("_assets/js/common.js")?>"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/humane-js/3.0.6/humane.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>

</head>
<body>
<div id="dialog"></div>
	<div class="navbar navbar-default navbar-static-top">
    	<div class="container">
            <div class="navbar-header"><strong><a href="<?php echo site_url("admin"); ?>" class="navbar-brand"><?php echo get_setting('sitename'); ?></a></strong></div>
        	<div class="navbar-collapse collapse">
              <?php
			  $cu = get_current_admin();
			  if($cu) $this->load->view('admin/tpl_nav_admin');
			  ?>
              <ul class="nav navbar-nav navbar-right">
              	<li class="dropdown">
                	
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Account <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><?=anchor("admin/home/changepassword", "Change Password", $changepassword_nav)?></li>
                        <li><?=anchor("admin/home/logout", "Logout", $logout_nav)?></li>      
                    </ul>
                </li>
                <li><?php /*?><a target="_blank" href="<?php echo site_url(); ?>">View Site &gt;</a><?php */?><button type="button" class="btn btn-success navbar-btn" onclick="location.href='<?php echo base_url(); ?>'">View Site</button></li>
              </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <div class="container">
    	
    </div>
    
	<div id="container" class="container">
	
	
    
    <div id="body">