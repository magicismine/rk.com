<?php  
extract($_GET);
?>
<h2>Data Lowongan</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left">
	<?php //echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?>
	Silahkan menambah data melalui Front End.
</div>
<div class="clearfix"></div><br />
<form method="get">
	<strong>Filter By</strong><br />
	Perusahaan: <?php echo OPerusahaan::drop_down_select('filter_perusahaan', $filter_perusahaan, 'class="chosen-select"', '-- SELECT --'); ?>
	<br />
	<br />
	<strong>Sort By</strong>: 
	<select name="orderby" class="chosen-select">
		<option <?php echo ($orderby == "newest" ? "selected" : ""); ?> value="newest">Newest</option>
		<option <?php echo ($orderby == "perusahaan" ? "selected" : ""); ?> value="perusahaan">Perusahaan</option>
		<option <?php echo ($orderby == "kode" ? "selected" : ""); ?> value="kode">Kode</option>
		<?php /* ?>
		<option <?php echo ($orderby == "level_karir" ? "selected" : ""); ?> value="level_karir">Level Karir</option>
		<?php */ ?>
		<option <?php echo ($orderby == "kategori" ? "selected" : ""); ?> value="kategori">Kategori</option>
		<option <?php echo ($orderby == "posisi" ? "selected" : ""); ?> value="posisi">Posisi</option>
		<option <?php echo ($orderby == "waktu_kerja" ? "selected" : ""); ?> value="waktu_kerja">Waktu Kerja</option>
		<option <?php echo ($orderby == "pendidikan" ? "selected" : ""); ?> value="pendidikan">Pendidikan</option>
		<option <?php echo ($orderby == "umur" ? "selected" : ""); ?> value="umur">Umur</option>
		<option <?php echo ($orderby == "pengalaman_kerja" ? "selected" : ""); ?> value="pengalaman_kerja">Pengalaman Kerja</option>
		<option <?php echo ($orderby == "gaji" ? "selected" : ""); ?> value="gaji">Gaji</option>
	</select>
	&nbsp; 
	<select name="order" class="chosen-select">
		<option  <?php echo ($order == "DESC" ? "selected" : ""); ?> value="DESC">Z - A</option>
		<option  <?php echo ($order == "ASC" ? "selected" : ""); ?> value="ASC">A - Z</option>
	</select>
	&nbsp; 
	<input type="submit" value="Submit" class="btn btn-success" style="padding:0px 12px;">
</form><br />

<?php echo print_status(); ?>

<?php
if(!$list) echo "<p class='error'>Data Kosong.</p>";
else
{
?>
<?php //* ?>
<p>Display <strong><?php echo count($list); ?></strong> of <strong><?php echo $total; ?></strong> items.</p>
<?php //*/ ?>
<?php /* ?>
<!-- pager --> 
<div class="pager"> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/first.png" class="first"/> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/prev.png" class="prev"/> 
  <span class="pagedisplay"></span> <!-- this can be any element, including an input --> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/next.png" class="next"/> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/last.png" class="last"/> 
  <select class="pagesize" title="Select page size"> 
    <option selected="selected" value="10">10</option> 
    <option value="20">20</option> 
    <option value="30">30</option> 
    <option value="40">40</option> 
  </select>
  <select class="gotoPage" title="Select page number"></select>
</div>
<table class="tablesorter" data-sortlist="[[3,1]]" style="font-size:0.8em;">
<?php */ ?>
<table class="table table-condensed table-striped table-hover" style="font-size:0.8em;">
  <thead>
    <tr>
			<th data-sorter="false">No</th>
      <?php /*<th>Tgl Dibuat</th>*/ ?>
      <th data-sorter="false">Tgl Berlaku</th>
      <th>Perusahaan/ Kontak</th>
      <th>Kode</th>
      <th>Level Karir</th>
      <th>Kategori</th>
      <th>Posisi</th>
      <th>Naker</th>
      <th>Waktu Kerja</th>
      <th>Pendidikan</th>
      <th>Umur</th>
      <th>Peng. Kerja</th>
      <th>JK</th>
      <th>Gaji</th>
      <th data-sorter="false">Lokasi</th>
      <th>Hits</th>
      <th>Pelamar</th>
      <th>Rekomendasi</th>
      <th>List Pelamar</th>
      <th data-sorter="false">Action</th>
    </tr>
  </thead>
  <tfoot>
    <tr>
			<th>No</th>
      <?php /*<th>Tgl Dibuat</th>*/ ?>
      <th>Tgl Berlaku</th>
      <th>Perusahaan/ Kontak</th>
      <th>Kode</th>
      <th>Level Karir</th>
      <th>Kategori</th>
      <th>Posisi</th>
      <th>Naker</th>
      <th>Waktu Kerja</th>
      <th>Pendidikan</th>
      <th>Umur</th>
      <th>Peng. Kerja</th>
      <th>JK</th>
      <th>Gaji</th>
      <th>Lokasi</th>
      <th>Hits</th>
      <th>Pelamar</th>
      <th>Rekomendasi</th>
      <th>List Pelamar</th>
      <th>Action</th>
    </tr>
  </tfoot>
  <tbody>
        
    <?php 
$i=1 + $uri;
$O = new OLowongan();                        
foreach($list as $row):

	extract(get_object_vars($row));
	$O->setup($row);

	$lamarans = $O->get_lamarans(0,0);
	$total_lamaran = get_db_total_rows();
	$rekomendasi = $O->get_all_rekomendasi(0,0);
	$total_rekomendasi = get_db_total_rows();
?>
<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
	<td><?=$i?></td>
	<?php /*<td><?=parse_date($dt_added,"d M Y","ID","short")?></td>*/ ?>
	<td><?=parse_date($dt_expired,"d M Y","ID","short")?></td>
	<td><?php
		$OTmp = new OPerusahaan($perusahaan_id);
		echo $OTmp->row->nama;
		unset($OTmp);
	?>
	<hr />
	<?php
		$arr = array();
		if(!empty($kontak_nama)) $arr[] = $kontak_nama;
		if(!empty($kontak_email)) $arr[] = $kontak_email;
		if(!empty($kontak_telp)) $arr[] = $kontak_telp;
		echo implode("<br />",$arr);
	?></td>
	<td><?=$kode?></td>
	<td><?php
		$level_karirs = $O->get_level_karir_arr();
		echo implode(", ", array_values($level_karirs));
		/*$OTmp = new OMaster_level_karir($level_karir_id);
		echo $OTmp->row->nama;
		unset($OTmp);*/
	?></td>
	<td><?php
		$OTmp = new OMaster_kategori($kategori_id);
		echo $OTmp->row->nama;
		unset($OTmp);
	?></td>
	<td><?=$posisi?></td>
	<td><?=$jumlah_tenaga_kerja?></td>
	<td><?php
		$OTmp = new OMaster_waktu_kerja($waktu_kerja_id);
		echo $OTmp->row->nama;
		unset($OTmp);
	?></td>
	<td><?php
		$OTmp = new OMaster_pendidikan($pendidikan_id);
		echo $OTmp->row->nama;
		unset($OTmp);
	?></td>
	<td><?=$umur_mulai?> - <?=$umur_sampai?></td>
	<td><?=$tahun_pengalaman_kerja?></td>
	<td><?php
		$OTmp = new OMaster_jenis_kelamin($jenis_kelamin_id);
		echo substr($OTmp->row->nama,0,1);
		unset($OTmp);
	?></td>
	<td><?=format_number($gaji)?></td>
            <td><?php
		$arr = array();
		if(!empty($lokasi_negara_id))
		{
			$OTmp = new OMaster_lokasi_negara($lokasi_negara_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_propinsi_id))
		{
			$OTmp = new OMaster_lokasi_propinsi($lokasi_propinsi_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_kabupaten_id))
		{
			$OTmp = new OMaster_lokasi_kabupaten($lokasi_kabupaten_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_kecamatan)) $arr[] = $lokasi_kecamatan;
		if(!empty($lokasi_kelurahan)) $arr[] = $lokasi_kelurahan;
		if(!empty($lokasi_kawasan)) $arr[] = $lokasi_kawasan;
		if(!empty($lokasi_alamat)) $arr[] = $lokasi_alamat;
		echo implode("<br />",array_reverse($arr));
	?></td>
	<td><?=$hits?></td>
	<td>(<?=$total_lamaran?>) pelamar</td>
	<td>(<?=$total_rekomendasi?>) rekomendasi</td>
	<td><?php echo anchor($this->curpage."/manage_lamarans/".$id."/?".$get_param, "List Pelamar"); ?></td>
	<td><?php
		$actions = NULL;
		$actions[] = anchor($this->curpage."/edit/".$id."/?".$get_param, "<img src='".base_url()."_assets/images/edit.gif' />");
		$actions[] = anchor($this->curpage."/delete/".$id."/?".$get_param, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
		//$actions[] = anchor($this->curpage."/manage_lamarans/".$id."/?".$get_param, "Pelamar", 'title="Manage Lamaran"');
		echo implode(" ", $actions);
	?></td>
</tr>

<?php 
$i++; 
    endforeach; 
unset($O);
?>
	</tbody>	
</table>
<?php /* ?>
<!-- pager --> 
<div class="pager"> 
	<img src="http://mottie.github.com/tablesorter/addons/pager/icons/first.png" class="first"/> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/prev.png" class="prev"/> 
  <span class="pagedisplay"></span> <!-- this can be any element, including an input --> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/next.png" class="next"/> 
  <img src="http://mottie.github.com/tablesorter/addons/pager/icons/last.png" class="last"/> 
  <select class="pagesize" title="Select page size"> 
    <option selected="selected" value="10">10</option> 
    <option value="20">20</option> 
    <option value="30">30</option> 
    <option value="40">40</option> 
  </select>  
  <select class="gotoPage" title="Select page number"></select>
</div>
<link rel="stylesheet" href="<?php echo base_url('_assets/sorter') ?>/theme.default.css">
<script src="<?php echo base_url('_assets/sorter') ?>/jquery.tablesorter.min.js"></script>
<script src="<?php echo base_url('_assets/sorter') ?>/jquery.tablesorter.pager.min.js"></script>
<script src="<?php echo base_url('_assets/sorter') ?>/jquery.tablesorter.widgets.min.js"></script>
<script src="<?php echo base_url('_assets/sorter') ?>/default.sorter.init.js"></script>
<?php */ ?>
<?php echo $pagination; ?>
<?php
}
?>