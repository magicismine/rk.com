<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OLowongan();
		$O->setup($row);
        $level_karirs = $O->get_level_karir_arr();
        $level_karir_id = array_keys($level_karirs);
	}
	extract($_POST);
?>

<h2>Lowongan Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<form action="" method="post">
<table class="tbl_form">
    <?php /*?><tr>
        <th>Judul</th>
        <td>
            <input type="text" name="judul" value="<?=$judul?>" autofocus required /> 
        </td>
    </tr><?php */?>
    <tr>
        <th>Tanggal Masa Berlaku</th>
        <td>
            <input type="text" name="dt_expired" class="fixed datepicker" size="10" maxlength="10" value="<?=get_date_lang($dt_expired,"ID")?>" required />
        </td>
    </tr>
    <tr>
        <th>Perusahaan</th>
        <td>
            <?php
            echo OPerusahaan::drop_down_select("perusahaan_id",$perusahaan_id,'class="chosen-select"');
            ?>
        </td>
    </tr>
    <tr>
        <th>Kontak</th>
        <td>
            <table class="tbl_form" width="75%">
            <tr>
                <th width="30%">Nama</th>
                <td>
                    <input type="text" name="kontak_nama" value="<?=$kontak_nama?>" required /> 
                </td>
            </tr>
            <tr>
                <th>Email</th>
                <td>
                    <input type="email" class="text" name="kontak_email" value="<?=$kontak_email?>" required /> 
                </td>
            </tr>
            <tr>
                <th>Telp</th>
                <td>
                    <input type="text" name="kontak_telp" value="<?=$kontak_telp?>" required /> 
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th>Kode Referensi</th>
        <td>
            <input type="text" name="kode" value="<?=$kode?>" /> 
        </td>
    </tr>
    <tr>
        <th>Level Pekerjaan</th>
        <td>
            <?php
            echo OMaster_level_karir::drop_down_select("level_karir_id[]", $level_karir_id, 'class="chosen-select-multiple" multiple');
            ?>
        </td>
    </tr>
    <tr>
        <th>Kategori Pekerjaan</th>
        <td>
            <?php
            echo OMaster_kategori::drop_down_select("kategori_id", $kategori_id, 'class="chosen-select"');
            ?>
        </td>
    </tr>
    <tr>
        <th>Posisi</th>
        <td>
            <input type="text" name="posisi" value="<?=$posisi?>" required /> 
        </td>
    </tr>
    <tr>
        <th>Jumlah Tenaga Kerja</th>
        <td>
            <input type="number" min="1" size="3" name="jumlah_tenaga_kerja" value="<?=$jumlah_tenaga_kerja?>" class="fixed text" required /> 
        </td>
      </tr>
    <tr>
        <th>Deskripsi</th>
        <td>
            <textarea name="deskripsi" class=""  required cols="130" rows="25"><?=$deskripsi?></textarea>
            <?php /*?><fieldset style="border: 1px solid black; padding: 10px; font-size: 10px;margin-top: 10px;">
                <strong>Masukkan list kualifikasi dengan cara perbaris:</strong><br>
                Contohnya seperti dibawah ini:
                <br>
                Apple <br>
                Orange
            </fieldset><?php */?>
      </td>
    </tr>
    <tr>
        <th>Waktu Kerja</th>
        <td>
            <?php
            echo OMaster_waktu_kerja::drop_down_select("waktu_kerja_id", $waktu_kerja_id);
            ?>
        </td>
    </tr>
    <tr>
        <th>Jenjang Pendidikan</th>
        <td>
            <?php
            echo OMaster_pendidikan::drop_down_select("pendidikan_id", $pendidikan_id);
            ?>
        </td>
    </tr>
    <tr>
        <th>Umur</th>
        <td>
            <input type="text" name="umur_mulai" value="<?=$umur_mulai?>" size="2" maxlength="2" class="fixed" /> Sampai
            <input type="text" name="umur_sampai" value="<?=$umur_sampai?>" size="2" maxlength="2" class="fixed" />
        </td>
    </tr>
    <tr>
        <th>Pengalaman Kerja</th>
        <td>
            <input type="text" name="tahun_pengalaman_kerja" class="fixed" size="2" maxlength="2" value="<?=$tahun_pengalaman_kerja?>" /> Tahun
        </td>
    </tr>
    <tr>
        <th>Gender</th>
        <td>
            <?php
            echo OMaster_jenis_kelamin::radio_select("jenis_kelamin_id", (empty($jenis_kelamin_id) ? 1 : intval($jenis_kelamin_id)), 'class="fixed"',' &nbsp; ');
            ?>
        </td>
    </tr>
    <tr>
        <th>Gaji yang ditawarkan</th>
        <td>
        	<label for="gaji_nego_flag">
			<?php
            echo form_checkbox("gaji_nego_flag",1,(empty($gaji_nego_flag) ? FALSE : TRUE),'id="gaji_nego_flag" class="fixed" data-target="#gaji_wrap"');
            ?>&nbsp; Negosiasi</label><br />
            <div id="gaji_wrap">
            Rp. <input type="text" name="gaji" id="gaji" class="currency fixed" value="<?=(empty($gaji) ? 5000000 : $gaji)?>" />
            </div>
        </td>
    </tr>
    <tr>
        <th>Lokasi Kerja = Lokasi Kantor?</th>
        <td><label for="lokasi_kerja_kantor_flag">
		<?php
        echo form_checkbox("lokasi_kerja_kantor_flag",1,(empty($lokasi_kerja_kantor_flag) ? FALSE : TRUE),'id="lokasi_kerja_kantor_flag" class="fixed" data-target="#lokasi-table"');
		?>&nbsp; Iya sama dengan lokasi kantor.</label>
        </td>
	</tr>
    <tr>
        <th>Lokasi Kerja</th>
        <td>
        <table width="75%" id="lokasi-table">
            <tr>
                <th width="30%">Alamat</th>
                <td>
                    <textarea name="lokasi_alamat" cols="40" rows="3" class="mceNoEditor"><?=$lokasi_alamat?></textarea>
                </td>
            </tr>
            <tr>
                <th>Negara</th>
                <td>
                    <?php echo OMaster_lokasi_negara::drop_down_select("lokasi_negara_id",$lokasi_negara_id); ?>
                </td>
            </tr>
            <tr>
                <th>Propinsi</th>
                <td>
                    <span id="lokasi_propinsi_wrap" data-value="<?php echo $lokasi_propinsi_id; ?>" data-option="" data-default="Pilih Propinsi">
                        Silahkan memilih Negara.
                        <?php //echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id",$lokasi_propinsi_id); ?>
                    </span>
                </td>
            </tr>
            <tr>
                <th>Kabupaten</th>
                <td>
                    <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>" data-option="" data-default="Pilih Kabupaten">
                        Silahkan memilih Propinsi.
                    </span>
                    <?php //echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id); ?>
                </td>
            </tr>
            <tr>
                <th>Kawasan</th>
                <td>
                    <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>" data-option="" data-default="Pilih Kawasan">
                        Silahkan memilih Kabupaten.
                    </span>
                    <?php //echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id); ?>
                </td>
            </tr>
            <tr>
                <th>Kecamatan</th>
                <td>
                    <input type="text" name="lokasi_kecamatan" value="<?=$lokasi_kecamatan?>" /> 
                </td>
            </tr>
            <tr>
                <th>Kelurahan</th>
                <td>
                    <input type="text" name="lokasi_kelurahan" value="<?=$lokasi_kelurahan?>" /> 
                </td>
            </tr>
            <tr>
                <th>Kawasan</th>
                <td>
                    <input type="text" name="lokasi_kawasan" value="<?=$lokasi_kawasan?>" /> 
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <th>Hits</th>
        <td>
            <input type="text" name="hits" value="<?=$hits?>" size="10" class="fixed" /> 
        </td>
    </tr>
    <tr>
        <td></td>
        <td>
            <button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
            <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
            <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url($curpage)?>';"><span class="leftarrow icon"></span>Cancel</button>
        </td>
    </tr>    
</table>
</form>

<script src="<?=base_url("_assets/js/jquery.number.js")?>"></script>
<script>
$(function()
{
    get_propinsi_ddl("lokasi_");
    $('select#lokasi_negara_id').live('change', function(){
        get_propinsi_ddl("lokasi_");
    });

	get_kabupaten_ddl("lokasi_");
	$('select#lokasi_propinsi_id').live('change', function(){
		get_kabupaten_ddl("lokasi_");
	});

    get_kawasan_ddl("lokasi_");
    $('select#lokasi_kabupaten_id').live('change', function(){
        get_kawasan_ddl("lokasi_");
    });
	
	set_checkbox_flag('#lokasi_kerja_kantor_flag');
	$('#lokasi_kerja_kantor_flag').live('click', function ()
	{
		set_checkbox_flag( $(this) );
	});
	
	set_checkbox_flag('#gaji_nego_flag');
	$('#gaji_nego_flag').live('click', function ()
	{
		set_checkbox_flag( $(this) );
	});

	// Set up the number formatting.
	$('.currency').number( true, 0 );
	// Get the value of the number for the demo.
	set_currency('.currency');
	$('.currency').on('keyup',function()
	{
		var t = $(this);
		set_currency(t);
	});
    //chosen
    $('.chosen-select').chosen();
    $('.chosen-select-multiple').chosen({ width: '95%'});
});

function set_currency(sel)
{
	var t = $(sel);
	t.next().remove();
	t.parent().append('<input type="hidden" name="'+t.attr('name')+'" value="'+t.val()+'" />');
}
function set_checkbox_flag(sel)
{
	var t = $(sel);
	var trgt = t.data('target');
	if(t.is(':checked') == true)
	{
		$(trgt).css('opacity', 0.4);
		$(trgt).find('input, select, textarea').attr('disabled', 'disabled');
	}
	else
	{
		$(trgt).css('opacity', 1);
		$(trgt).find('input, select, textarea').removeAttr('disabled');
	}
}
function get_propinsi_ddl(prefix)
{
    var t = $('#'+prefix+'propinsi_wrap');
    var p = {'negara_id': $('#'+prefix+'negara_id').val(), 'propinsi_id': t.data('value'), 'name': prefix+'propinsi_id', 'no_default': 1}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: '<?php echo site_url("ajax/get_propinsi_ddl"); ?>',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
        }
    });
}
function get_kabupaten_ddl(prefix)
{
    var t = $('#'+prefix+'kabupaten_wrap');
    var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': t.data('value'), 'name': prefix+'kabupaten_id', 'no_default': 1}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
        }
    });
}
function get_kawasan_ddl(prefix)
{
  var t = $('#'+prefix+'kawasan_wrap');
  var p = {'kabupaten_id': $('#'+prefix+'kabupaten_id').val(), 'kawasan_id': t.data('value'), 'name': prefix+'kawasan_id', 'default': t.data('default')}
  $.ajax({
    'type': 'POST',
    'async': false,
    url: '<?php echo site_url("ajax/get_kawasan_ddl"); ?>',
    data: p,
    complete: function(xhr, status)
    {
      var ret = xhr.responseText;
      t.html(ret);
    }
  });
}
</script>