<h2>Master Lokasi Kawasans Data</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left"><?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?></div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
	if(!$list) echo "<p class='error'>The Master Lokasi Kawasans is empty.</p>";
    else
    {
?>        
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr>
					<th>ID</th>
                    <th>Negara</th>
                    <th>Propinsi</th>
                    <th>Kabupaten</th>
					<th>Kawasan</th>
					<th>Action</th>
                </tr>
            </thead>
            <tbody>
                
            <?php 
                $i=1 + $uri;
                foreach($list as $row):
				
               	 	extract(get_object_vars($row));
					$O = new OMaster_lokasi_kawasan();                        
					$O->setup($row);
			?>
				<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
    				<td><?=$i?></td>
                    <td><?php
                    $OTmp = new OMaster_lokasi_negara($master_lokasi_negara_id);
					echo $OTmp->row->nama;
					unset($OTmp);
					?></td>
                    <td><?php
                    $OTmp = new OMaster_lokasi_propinsi($master_lokasi_propinsi_id);
					echo $OTmp->row->nama;
					unset($OTmp);
					?></td>
                    <td><?php
                    $OTmp = new OMaster_lokasi_kabupaten($master_lokasi_kabupaten_id);
					echo $OTmp->row->nama;
					unset($OTmp);
					?></td>
                    <td><?=$nama?></td>
					<td>
						<?php
						$actions = NULL;
						$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />", array("title" => "Edit"));
						$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("title" => "Delete", "onclick" => "return confirm('Are you sure?');"));
						$actions[] = anchor($this->curpage."/add/".$id, "<img src='".base_url()."_assets/images/cms.gif' />", array("title" => "Duplicate"));
						echo implode(" ", $actions);
						?>
					</td>
				</tr>
	        <?php 
	            unset($O);
				$i++; 
	            endforeach; 
	        ?>
        	</tbody>	
        </table>
        
        <?php echo $pagination; ?>
<?php
	}
?>
