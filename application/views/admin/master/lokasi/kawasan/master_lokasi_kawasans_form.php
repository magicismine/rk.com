<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OMaster_lokasi_kawasan();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Master Lokasi Kawasan Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
        <tr>
            <th>Negara</th>
            <td>
            	<?=OMaster_lokasi_negara::drop_down_select("master_lokasi_negara_id", $master_lokasi_negara_id)?>
            </td>
        </tr>
    	<tr>
            <th>Propinsi</th>
            <td>
            	<?=OMaster_lokasi_propinsi::drop_down_select("master_lokasi_propinsi_id", $master_lokasi_propinsi_id)?>
            </td>
        </tr>
        <tr>
            <th>Kabupaten</th>
            <td>
            	<span id="master_lokasi_kabupaten_wrap" data-value="<?=$master_lokasi_kabupaten_id?>" data-default="Pilih" data-option="">
            		Pilih Kabupaten terlebih dahulu.
            	</span>
            	<?php //echo OMaster_lokasi_kabupaten::drop_down_select("master_lokasi_kabupaten_id", $master_lokasi_kabupaten_id); ?>
            </td>
        </tr>
    	<tr>
            <th>Kawasan</th>
            <td>
            	<input type="text" name="nama" value="<?=$nama?>" required autofocus /> 
                <br/><?=form_error('nama')?>                
            </td>
        </tr>
        <tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/master_lokasi_kawasans")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>
<script>
$(function()
{

	get_kabupaten_ddl("master_lokasi_");
	$(document).on('change', 'select#master_lokasi_propinsi_id', function(e){
	get_kabupaten_ddl("master_lokasi_");
	});

	get_kawasan_ddl("master_lokasi_");
	$(document).on('change', 'select#master_lokasi_kabupaten_id', function(e){
	get_kawasan_ddl("master_lokasi_");
	});

});

function get_kabupaten_ddl(prefix)
{
	var t = $('#'+prefix+'kabupaten_wrap');
	var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': t.data('value'), 'name': prefix+'kabupaten_id', 'no_default': 1, 'option': t.data('option')}
	$.ajax({
	    'type': 'POST',
	    'async': false,
	    url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
	    data: p,
	    complete: function(xhr, status)
	    {
	      var ret = xhr.responseText;
	      t.html(ret);
	    }
  	});
}

function get_kawasan_ddl(prefix)
{
	var t = $('#'+prefix+'kawasan_wrap');
	var p = {'kabupaten_id': $('#'+prefix+'kabupaten_id').val(), 'kawasan_id': t.data('value'), 'name': prefix+'kawasan_id', 'no_default': 1, 'option': t.data('option')}
	$.ajax({
		'type': 'POST',
		'async': false,
		url: '<?php echo site_url("ajax/get_kawasan_ddl"); ?>',
		data: p,
		complete: function(xhr, status)
		{
		  var ret = xhr.responseText;
		  t.html(ret);
		}
	});
}
</script>