<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OMaster_lokasi_negara();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Master Lokasi Negara Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Nama</th>
            <td>
            	<textarea name="nama[]" autofocus cols="60" rows="10" class="mceNoEditor"><?php echo $nama; ?></textarea><br />
            	<small>1 Negara/baris</small>
            	<?php /* ?>
            	<input type="text" name="nama" value="<?=$nama?>" required autofocus />
            	<?php */ ?>
            </td>
        </tr>
        <tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url($this->curpage)?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>