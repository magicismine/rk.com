<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OMaster_shdk();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Master Shdk Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<?=form_open()?>
	<table class="tbl_form">
    	<tr>
            <th>Nama</th>
            <td>
            	<input type="text" name="nama" value="<?=$nama?>" required autofocus /> 
                <br/><?=form_error('nama')?>                
            </td>
        </tr>
				<tr>
			<td></td>
			<td>
            	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
                <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
                <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url("admin/master_shdks")?>';"><span class="leftarrow icon"></span>Cancel</button>
			</td>
		</tr>    
	</table>    
<?=form_close()?>