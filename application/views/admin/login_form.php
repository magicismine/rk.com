<?php
if(sizeof($_POST) > 0) extract($_POST);
?>
<div class="container">
	<div class="col-md-offset-4 col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading"><h4 class="text-center">Web Administration</h4></div>
            <div class="panel-body">
                <?php echo print_error(validation_errors()); ?>
                <?php echo print_error($this->session->flashdata('warning')); ?>
                <form action="" role="form" method="post">
                	<div class="form-group">
                        <input type="text" name="username" id="username" class="form-control" value="<?=$username?>" placeholder="Username" autofocus />
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control" value="<?=$password?>" placeholder="Password" />
                    </div>
                    <p align="center"><button class="btn btn-success">Login</button></p>
                </form>
            </div>
            <div class="panel-footer text-center"></div>
        </div>
    </div>
</div>
    