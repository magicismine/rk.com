	</div>
    <div class="clear"></div>
    <div id="footer">
		<p>Page rendered in <strong>{elapsed_time}</strong> seconds</p>
    </div>
</div>
<script type="text/javascript" src="<?=base_url("_assets/tinymcpuk/jscripts/tiny_mce/tiny_mce.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/tinymcpuk/jscripts/tiny_mce/my_tiny_mce.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/swfupload/swfupload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/jquery.swfupload.js")?>"></script>
<script type="text/javascript" src="<?=base_url("_assets/js/jquery.swfupload.init.js")?>"></script>
<script type="text/javascript">
$(function(){
	var total_photos = $('#total_photos').text();
	jquery_upload_image("<?=base_url()?>","-photo",".set","image[]","#preview-photo","1 MB",total_photos);
	
	var total_photos_over = $('#total_photos_over').text();
	jquery_upload_image("<?=base_url()?>","-photo-over",".set_over","image_over[]","#preview-photo-over","1 MB",total_photos_over);
});	
</script>
<style type="text/css" >
	#SWFUpload_Console { display:none; position:absolute; z-index:1000; bottom:0; right:10px; }
	#swfupload-control-photo p
	{ margin:10px 5px; font-size:0.9em; }
	#log-photo
	{ margin:0; padding:0; width:500px;}
	#log-photo li
	{ list-style-position:inside; margin:2px; border:1px solid #ccc; padding:10px; font-size:12px; font-family:Arial, Helvetica, sans-serif; color:#333; background:#fff; position:relative;}
	#log-photo li .progressbar
	{ border:1px solid #333; height:5px; background:#fff; }
	#log-photo li .progress
	{ background:#999; width:0%; height:5px; }
	#log-photo li p
	{ margin:0; line-height:18px; }
	#log-photo li.success
	{ border:1px solid #339933; background:#ccf9b9; }
	#log-photo li span.cancel
	{ position:absolute; top:5px; right:5px; width:20px; height:20px; background:url('<?=base_url("_assets/js/swfupload/cancel.png")?>') no-repeat; cursor:pointer; }
	
	.form_style .full { width:98%; }
	.error{color:#FF0000}
	
	.tbl_separate{}
	.tbl_separate tr{ vertical-align:top; }
	.tbl_separate tr td{ padding:5px; vertical-align:top; }
	
	.image_list{ list-style:none; float:left; }
	.image_list li{ float:left; margin: 0 10px 10px 0; }
	.form_right label input { vertical-align:top; }
	.form_right label img { vertical-align:top; }
</style>
</body>
</html>