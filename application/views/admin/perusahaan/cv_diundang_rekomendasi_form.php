<?php
if($row)
{
    extract(get_object_vars($row));	 
    $O = new OKandidat;
    $O->setup($row);
}
extract($_POST);
?>

<h2>Set Rekomendasi Form</h2><br/>	
<?php
echo parse_breadcrumb($breadcrumb_arr);
echo print_error(validation_errors());
echo print_status();
?>

<?php //=form_open()?>
<form action="" method="post">
<table class="tbl_form">
	<tr>
        <th>Lowongan</th>
        <td>
        	<?php echo $lowongan_ddl; ?>
        </td>
    </tr>
    <tr>
		<td></td>
		<td>
        	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
            <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
            <button class="btn btn-danger" type="button" onclick="window.location.href='<?=site_url($curpage)?>';"><span class="leftarrow icon"></span>Cancel</button>
		</td>
	</tr>    
</table>    
<?=form_close()?>