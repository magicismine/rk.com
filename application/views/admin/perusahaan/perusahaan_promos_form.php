<?php
if($row)
{
    extract(get_object_vars($row));	 
    $O = new OPerusahaan_promo;
    $O->setup($row);
}
extract($_POST);
?>

<h2>Promo Kandidat Form</h2><br/>	
<?php
echo parse_breadcrumb($breadcrumb_arr);
echo print_error(validation_errors());
echo print_status();
?>

<?php //=form_open()?>
<form action="" method="post">
<table class="tbl_form">
	<tr>
        <th>Judul</th>
        <td>
        	<input type="text" name="name" value="<?=$name?>" required autofocus /> 
            <br/><?=form_error('name')?>                
        </td>
    </tr>
	<tr>
        <th>Isi</th>
        <td>
        	<textarea name="description" cols="80" rows="20"><?=$description?></textarea>
			<br/><?=form_error('description')?>                
        </td>
    </tr>
    <tr>
		<td></td>
		<td>
        	<button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
            <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
            <button class="btn btn-danger" type="button" onclick="window.location.href='<?=site_url($curpage)?>';"><span class="leftarrow icon"></span>Cancel</button>
		</td>
	</tr>    
</table>    
<?=form_close()?>