<?php
if(sizeof($_GET))
{
	$get_params = "?".get_params($_GET);
}
?><h2>Data Perusahaan</h2><br />
<div class="pull-right">
	<form action="" method="get">
        Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
    </form>
</div>
<div class="pull-left">
	<?php //echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?>
	Silahkan menambah data melalui Front End.
</div>
<div class="clearfix"></div><br />

<?php echo print_status(); ?>

<?php
if(!$list) echo "<p class='error'>Data Kosong.</p>";
else
{
?>
<p>Display <strong><?php echo count($list); ?></strong> of <strong><?php echo $total; ?></strong> items.</p>
<table class="table table-striped table-hover table-condensed" style="font-size:0.8em;">
  <thead>
		<tr>
			<th>No</th>
			<th>Tgl Daftar</th>
			<th>Tgl Berlaku</th>
			<th>Nama/ Email</th>
			<th>Logo</th>
			<th>Jumlah Karyawan</th>
			<th>Bidang Usaha</th>
			<th>Kontak</th>
			<th>Alamat</th>
			<th>Newsletter</th>
			<th>CV Disimpan</th>
			<th>CV Diundang</th>
			<th>Lowongan</th>
			<th>Promo</th>
			<th>Action</th>
		</tr>
  </thead>
  <tfoot>
		<tr>
			<th>No</th>
			<th>Tgl Daftar</th>
			<th>Tgl Berlaku</th>
			<th>Nama/ Email</th>
			<th>Logo</th>
			<th>Jumlah Karyawan</th>
			<th>Bidang Usaha</th>
			<th>Kontak</th>
			<th>Alamat</th>
			<th>Newsletter</th>
			<th>CV Disimpan</th>
			<th>CV Diundang</th>
			<th>Lowongan</th>
			<th>Promo</th>
			<th>Action</th>
		</tr>
  </tfoot>
  <tbody>
    
<?php 
$i=1 + $uri;
$O = new OPerusahaan;
foreach($list as $row):

	extract(get_object_vars($row));
	$O->setup($row);
	$cv_disimpan = $O->search_cvs("", 0, 0, "", "tandai=1");
	$total_cv_disimpan = get_db_total_rows();
	$cv_diundang = $O->search_cvs("", 0, 0, "", "undang=1");
	$total_cv_diundang = get_db_total_rows();
	$lowongan_res = $O->get_lowongans(0,0);
	$total_lowongan = get_db_total_rows();
	$promo_res = $O->get_promos(0,0);
	$total_promo = get_db_total_rows();
?>
<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
	<td><?=$i?></td>
	<td><?=parse_date($dt_added,"d M Y","ID","short")?></td>
	<td><?=parse_date($dt_expired,"d M Y","ID","short")?></td>
	<td><?=$nama?><br />
		<?=$email?></td>
	<td><img src="<?=$logo?>" /></td>
	<td><?=$jumlah_karyawan?></td>
	<td><?php
	$OTmp = new OMaster_bidang_usaha($bidang_usaha_id);
	echo $OTmp->row->nama;
	unset($OTmp);
	?></td>
	<td><?php
		$arr = array();
		if(!empty($kontak_nama)) $arr[] = $kontak_nama;
		if(!empty($kontak_email)) $arr[] = $kontak_email;
		if(!empty($kontak_telp)) $arr[] = $kontak_telp;
		echo implode("<br />",$arr);
	?></td>
	<td><?php
		$arr = array();
		if(!empty($lokasi_negara_id))
		{
			$OTmp = new OMaster_lokasi_negara($lokasi_negara_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_propinsi_id))
		{
			$OTmp = new OMaster_lokasi_propinsi($lokasi_propinsi_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_kabupaten_id))
		{
			$OTmp = new OMaster_lokasi_kabupaten($lokasi_kabupaten_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_kawasan_id))
		{
			$OTmp = new OMaster_lokasi_kawasan($lokasi_kawasan_id);
			$arr[] = $OTmp->row->nama;
			unset($OTmp);
		}
		if(!empty($lokasi_kecamatan)) $arr[] = $lokasi_kecamatan;
		if(!empty($lokasi_kelurahan)) $arr[] = $lokasi_kelurahan;
		//if(!empty($lokasi_kawasan)) $arr[] = $lokasi_kawasan;
		if(!empty($lokasi_alamat)) $arr[] = $lokasi_alamat;
		echo implode("<br />",array_reverse($arr));
	?></td>
	<td>
	<?=($newsletter_flag== "0" ?
		anchor($this->curpage."/set_newsletter_flag/1/".$id.$get_params, "<img src='".base_url()."_assets/images/disabled.gif' />") :
		anchor($this->curpage."/set_newsletter_flag/0/".$id.$get_params, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
	</td>
	<td><?php echo empty($total_cv_disimpan) ? "(0) CV disimpan" : anchor($this->curpage."/cv_disimpan/".$id.$get_params, "(".$total_cv_disimpan.") CV disimpan", 'title="Lihat" class="btn btn-default btn-xs"'); ?></td>
	<td><?php echo empty($total_cv_diundang) ? "(0) CV diundang" : anchor($this->curpage."/cv_diundang/".$id.$get_params, "(".$total_cv_diundang.") CV diundang", 'title="Lihat" class="btn btn-default btn-xs"'); ?></td>
	<td><?php echo empty($total_lowongan) ? "(0) lowongan" : anchor(str_replace("perusahaan", "lowongan", $this->curpage)."?filter_perusahaan=".$id, "(".intval($total_lowongan).") lowongan", 'class="btn btn-default btn-xs"'); ?></td>
	<td><?php echo empty($total_promo) ? "(0) promo kandidat" : anchor($this->curpage."/manage_promo_kandidat/".$id.$get_params, "(".intval($total_promo).") promo kandidat", 'class="btn btn-default btn-xs"'); ?></td>
	<td><?php
		$actions = NULL;
		$actions[] = anchor($this->curpage."/edit/".$id.$get_params, "<img src='".base_url()."_assets/images/edit.gif' />");
		$actions[] = anchor($this->curpage."/delete/".$id.$get_params, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
		//$actions[] = anchor($this->curpage."/manage_promo_kandidat/".$id, "<img src='".base_url()."_assets/images/cms.gif' />", 'title="Manage Promo"');
		//$actions[] = anchor(str_replace("perusahaan", "lowongan", $this->curpage)."?filter_perusahaan=".$id, "<img src='".base_url()."_assets/images/cms.gif' />", 'title="Manage Lowongan"');
		echo implode(" ", $actions);
	?></td>
</tr>

	<?php 
	$i++; 
endforeach; 
unset($O);
?>
	</tbody>	
</table>

<?php echo $pagination; ?>
<?php
}
?>
