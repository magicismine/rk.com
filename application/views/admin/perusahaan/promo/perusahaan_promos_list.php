<h2>Data Promo Kandidat</h2><br />
<div class="pull-right">
	<form action="" method="get">
	  Search: <input type="text" name="keyword" value="<?php echo $_GET["keyword"]; ?>" />
	</form>
</div>

<div class="pull-left">
	<?php echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?>
</div>
<div class="clearfix"></div><br />

<?php
//echo parse_breadcrumb($breadcrumb_arr);
echo print_status();
?>

<?php
if(!$list) echo "<p class='error'>Promo Kandidat is empty.</p>";
else
{
?>
<table class="table table-striped table-hover table-condensed">
  <thead>
    <tr>
			<th>No</th>
			<th>Tanggal</th>
			<th>Perusahaan</th>
			<th>Judul</th>
			<th>Active</th>
			<th>Action</th>
    </tr>
  </thead>
  <tbody>
      
<?php 
$i=1 + $uri;
$O = new OPerusahaan_promo;
foreach($list as $row):

	extract(get_object_vars($row));
	$O->setup($row);
	$OP = new OPerusahaan($perusahaan_id);
	?>
	<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
		<td><?=$i?></td>
		<td><?=parse_date($dt_added,"d M Y","ID","short")?></td>
		<td><?php echo $OP->get_nama(); ?></td>
		<td><?php echo $name; ?></td>
		<td><?=(empty($active) ?
			anchor($this->curpage."/set_active/1/".$id, "<img src='".base_url()."_assets/images/disabled.gif' />") :
			anchor($this->curpage."/set_active/0/".$id, "<img src='".base_url()."_assets/images/enabled.gif' />"))?>
		</td> 
		<td><?php
			$actions = NULL;
			$actions[] = anchor($this->curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
			$actions[] = anchor($this->curpage."/delete/".$id, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
			echo implode(" ", $actions);
		?></td>
	</tr>

  <?php 
    unset($OP);
		$i++; 
  endforeach; 
  unset($O);
  ?>
	</tbody>	
</table>

<?php echo $pagination; ?>
<?php
}
?>
