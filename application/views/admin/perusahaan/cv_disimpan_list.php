<?php 
if(sizeof($_GET) > 0) $get_params = "?".get_params($_GET);
?><h2>Data CV Kandidat yang di simpan</h2><br />
<div class="pull-right">
	<form action="" method="get">
    <?php
    foreach ($_GET as $key => $value) {
      if($key != "keyword2") echo form_hidden($key,$value);
    }
    ?>
    Search: <input type="text" name="keyword2" value="<?php echo $_GET["keyword2"]; ?>" />
  </form>
</div>
<?php /* ?>
<div class="pull-left">
	<?php //echo anchor($this->curpage."/add", "Create new",array("class" => "btn btn-success")); ?>
	Silahkan menambah data melalui Front End.
</div>
<?php */ ?>
<div class="clearfix"></div><br />

<?php
echo parse_breadcrumb($breadcrumb_arr);
echo print_status();
?>

<?php
if(!$list) echo "<p class='error'>The Data is empty.</p>";
else
{
?>

<table class="table table-striped table-hover table-condensed">
    <thead>
      <tr>
				<th>No</th>
        <th colspan="2">Nama kandidat</th>
        <th>Pendidikan Terakhir</th>
        <th>T. Tinggal</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        
    <?php
		$i=1 + $uri;
		$OK = new OKandidat;
    foreach($list as $row):
    	extract(get_object_vars($row));
    	$OK->setup($row);
      // keterampilan
      $keterampilans = $OK->get_all_keterampilan();
      //$keterampilan = implode("<br />", $keterampilans);
      $keterampilan = implode("<br />", array_unique($keterampilans));
      // pendidikan
      $pnddkn_row = $OK->get_last_pendidikan();
      $OTmp_pnddkn = new OMaster_pendidikan($pnddkn_row->pendidikan_id);
      $pendidikan = $OTmp_pnddkn->row->nama;
      unset($OTmp_pnddkn);
      // lokasi
      $lokasi_row = $OK->get_current_lokasi();
      $lokasi = $lokasi_row->nama;
			?>
		<tr class="<?=alternator("odd", "even")?>" data_id="<?=$row->id?>">
			<td><?=$i?></td>
			<td width="60">
				<img src="<?php echo $OK->get_photo('square'); ?>" width="50" height="50" alt="" />
			</td>
			<td><a href="<?php echo $OK->get_link(); ?>"><strong><?php echo $OK->get_nama_lengkap(); ?> (<?php echo $OK->get_umur(); ?>)</strong></a><br />
        <?php
          echo $keterampilan;
        ?>
       </td>
			<td><?=$pendidikan?></td>
			<td><?=$lokasi?></td>
			<td><?php
			$actions = NULL;
			//$actions[] = anchor($curpage."/edit/".$id, "<img src='".base_url()."_assets/images/edit.gif' />");
			$actions[] = anchor($curpage."/delete/".$id.$get_params, "<img src='".base_url()."_assets/images/delete.gif' />", array("onclick" => "return confirm('Are you sure?');"));
			echo implode(" ", $actions)."<br />";
			?></td>
		</tr>

		<?php
    $i++; 
  endforeach; 
	unset($OK);
	unset($O);
	?>
	</tbody>	
</table>

<?php echo $pagination; ?>
<?php
}
?>
