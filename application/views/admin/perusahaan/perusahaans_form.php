<?php
	if($row)
	{
		extract(get_object_vars($row));	 
		$O = new OPerusahaan();
		$O->setup($row);
	}
	extract($_POST);
?>

<h2>Perusahaan Form</h2><br/>	
<?php
echo parse_breadcrumb();
echo print_error(validation_errors());
echo print_status();
?>

<form action="" method="post">
<table class="tbl_form">
    <tr>
        <th>User</th>
        <td>
            <?php
            echo OUser::drop_down_select('user_id',$user_id);
            ?>
        </td>
    </tr>
    <tr>
        <th>Nama</th>
        <td>
            <input type="text" name="nama" value="<?=$nama?>" required autofocus />
        </td>
    </tr>
    <tr>
        <th>Email</th>
        <td>
            <input type="email" class="text" name="email" value="<?=$email?>" />
        </td>
    </tr>
    <tr>
        <th>Jumlah Karyawan</th>
        <td>
            <input type="text" name="jumlah_karyawan" value="<?=$jumlah_karyawan?>" class="fixed" size="5" maxlength="5" /> Orang
        </td>
    </tr>
    <tr>
        <th>Bidang Usaha</th>
        <td>
            <?php
            echo OMaster_bidang_usaha::drop_down_select('bidang_usaha_id',$bidang_usaha_id);
            ?>
        </td>
    </tr>
    <tr>
        <th>Motto</th>
        <td>
            <input type="text" name="motto" value="<?=$motto?>" />
        </td>
    </tr>
    <tr>
        <th>Deskripsi</th>
        <td>
            <textarea name="deskripsi" cols="60" rows="20"><?=$deskripsi?></textarea>
        </td>
    </tr>
    <tr>
        <th>Kontak</th>
        <td>
            <table class="tbl_form" width="80%">
            <tr>
                <th width="30%">Nama</th>
                <td>
                    <input type="text" name="kontak_nama" value="<?=$kontak_nama?>" /> 
                </td>
            </tr>
            <tr>
                <th>Email</th>
                <td>
                    <input type="email" class="text" name="kontak_email" value="<?=$kontak_email?>" /> 
                </td>
            </tr>
            <tr>
                <th>Telp</th>
                <td>
                    <input type="text" name="kontak_telp" value="<?=$kontak_telp?>" /> 
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th>Lokasi</th>
        <td>
        <table class="table-form" width="80%">
            <tr>
                <th width="30%">Alamat</th>
                <td>
                    <textarea name="lokasi_alamat" cols="40" rows="3" class="mceNoEditor"><?=$lokasi_alamat?></textarea>
                </td>
            </tr>
            <tr>
                <th>Negara</th>
                <td>
                    <?php echo OMaster_lokasi_negara::drop_down_select("lokasi_negara_id",$lokasi_negara_id); ?>
                </td>
            </tr>
            <tr>
                <th>Propinsi</th>
                <td>
                    <?php echo OMaster_lokasi_propinsi::drop_down_select("lokasi_propinsi_id",$lokasi_propinsi_id); ?>
                </td>
            </tr>
            <tr>
                <th>Kabupaten</th>
                <td>
                    <span id="lokasi_kabupaten_wrap" data-value="<?php echo $lokasi_kabupaten_id; ?>" data-option="" data-default="">
                        Silahkan memilih Propinsi.
                    </span>
                    <?php //echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id); ?>
                </td>
            </tr>
            <tr>
                <th>Kawasan</th>
                <td>
                    <span id="lokasi_kawasan_wrap" data-value="<?php echo $lokasi_kawasan_id; ?>" data-option="" data-default="Pilih Kawasan">
                        Silahkan memilih Kabupaten.
                    </span>
                    <?php //echo OMaster_lokasi_kabupaten::drop_down_select("lokasi_kabupaten_id",$lokasi_kabupaten_id); ?>
                </td>
            </tr>
            <tr>
                <th>Kecamatan</th>
                <td>
                    <input type="text" name="lokasi_kecamatan" value="<?=$lokasi_kecamatan?>" /> 
                </td>
            </tr>
            <tr>
                <th>Kelurahan</th>
                <td>
                    <input type="text" name="lokasi_kelurahan" value="<?=$lokasi_kelurahan?>" /> 
                </td>
            </tr>
            <?php /* ?>
            <tr>
                <th>Kawasan</th>
                <td>
                    <input type="text" name="lokasi_kawasan" value="<?=$lokasi_kawasan?>" /> 
                </td>
            </tr>
            <?php */ ?>
        </table>
        </td>
    </tr>
    <tr>
        <th>Tanggal Berlaku</th>
        <td>
            <input type="text" name="dt_expired" class="fixed datepicker" value="<?=get_date_lang($dt_expired,"ID")?>" size="10" maxlength="10" />
        </td>
    </tr>
    <tr>
        <th>Logo</th>
        <td>
            <?php
            if($O->id != FALSE)
            {
                echo "<img src='".$O->get_photo()."' />";
            }
            ?>
            <div style="display:block;" id="last-photo"></div>
            <div style="display:block;" id="preview-photo"></div>
            <div id="swfupload-control-photo" class="clear">
                <p>Upload maximum <span id="total_photos">1</span> image file(jpg, png, gif) and having maximum size of 1 MB</p>
                <input type="button" id="button-photo" />
                <p id="queuestatus-photo" ></p>
                <ol id="log-photo"></ol>
                <span class="set"></span>
            </div>
            <!-- END OF -->
        </td>
    </tr>
    <?php /*?><tr>
        <th>Logo</th>
        <td>
            <input type="file" name="logo" value="<?=$logo?>" />
        </td>
    </tr><?php */?>
    <tr>
        <td></td>
        <td>
            <button class="btn btn-success" type="submit"><span class="check icon"></span>Save</button>
            <button class="btn" type="reset"><span class="reload icon"></span>Reset</button>
            <button class="btn btn-danger" type="button" onclick="location.href='<?=site_url($this->curpage)?>';"><span class="leftarrow icon"></span>Cancel</button>
        </td>
    </tr>    
</table>
</form>

<script>
$(function()
{
	get_kabupaten_ddl("lokasi_");
	$('select#lokasi_propinsi_id').live('change', function(){
		get_kabupaten_ddl("lokasi_");
	});

    get_kawasan_ddl("lokasi_");
    $('select#lokasi_kabupaten_id').live('change', function(){
        get_kawasan_ddl("lokasi_");
    });
});

function get_kabupaten_ddl(prefix)
{
    var t = $('#'+prefix+'kabupaten_wrap');
    var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': t.data('value'), 'name': prefix+'kabupaten_id', 'no_default': 1}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: '<?php echo site_url("ajax/get_kabupaten_ddl"); ?>',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
        }
    });
}
function get_kawasan_ddl(prefix)
{
  var t = $('#'+prefix+'kawasan_wrap');
  var p = {'kabupaten_id': $('#'+prefix+'kabupaten_id').val(), 'kawasan_id': t.data('value'), 'name': prefix+'kawasan_id', 'default': t.data('default')}
  $.ajax({
    'type': 'POST',
    'async': false,
    url: '<?php echo site_url("ajax/get_kawasan_ddl"); ?>',
    data: p,
    complete: function(xhr, status)
    {
      var ret = xhr.responseText;
      t.html(ret);
    }
  });
}
</script>
