<?php
$uri = $this->uri->segment(2);
$active_b = "active";
if($uri == "home") $home_nav = $active_b;
if(stristr($uri,"changepassword")) $changepassword_nav = $active_b; 
$uri_name = $uri."_nav";
$$uri_name = $active_b;
?>
<ul class="navbar-nav nav">
<li class="<?=$users_nav?>">
    <?=anchor("javascript:void(0)","Users <span class='caret'></span>", array('class' => 'dropdown-toggle','data-toggle' => 'dropdown'))?>
    <ul class="dropdown-menu">
        <li><?=anchor("admin/users?role=kandidat","Kandidat", $users_nav)?><li>
        <li><?=anchor("admin/users?role=perusahaan","Perusahaan", $users_nav)?><li>
        <li class="<?=$admins_nav?>"><?=anchor("admin/admins","Admins", $admins_nav)?></li>
    </ul>
</li>
<li class="<?=$kandidats_nav?> <?=$lamarans_nav?>"><?=anchor("javascript:void(0)","Kandidats <span class='caret'></span>", array('class' => 'dropdown-toggle','data-toggle' => 'dropdown'))?>
    <ul class="dropdown-menu">
        <li class="<?=$kandidats_nav?>"><?=anchor("admin/kandidats","Kandidat", $kandidats_nav)?></li>
        <li class="<?=$lamarans_nav?>"><?=anchor("admin/lamarans","Lamaran", $lamarans_nav)?></li>
    </ul>
</li>
<?php /*?><li class="<?=$kandidats_nav?> <?=$kandidat_bahasas_nav?> <?=$kandidat_dokumens_nav?> <?=$kandidat_keluargas_nav?> <?=$kandidat_kesehatans_nav?> <?=$kandidat_keterampilans_nav?> <?=$kandidat_pendidikans_nav?> <?=$kandidat_pengalamans_nav?> <?=$kandidat_resumes_nav?>">
    <?=anchor("javascript:void(0)","Kandidat <span class='caret'></span>", array('class' => 'dropdown-toggle','data-toggle' => 'dropdown'))?>
    <ul class="dropdown-menu">
        <li class="<?=$kandidats_nav?>"><?=anchor("admin/kandidats","Manage Kandidat", $kandidats_nav)?></li>
        <li class="<?=$kandidat_resumes_nav?>"><?=anchor("admin/kandidat_resumes","Kandidat Resumes", $kandidat_resumes_nav)?></li>
        <li class="<?=$kandidat_bahasas_nav?>"><?=anchor("admin/kandidat_bahasas","Kandidat Bahasa", $kandidat_bahasas_nav)?></li>
        <li class="<?=$kandidat_dokumens_nav?>"><?=anchor("admin/kandidat_dokumens","Kandidat Dokumen", $kandidat_dokumens_nav)?></li>
        <li class="<?=$kandidat_keluargas_nav?>"><?=anchor("admin/kandidat_keluargas","Kandidat Keluarga", $kandidat_keluargas_nav)?></li>
        <li class="<?=$kandidat_kesehatans_nav?>"><?=anchor("admin/kandidat_kesehatans","Kandidat Kesehatan", $kandidat_kesehatans_nav)?></li>
        <li class="<?=$kandidat_keterampilans_nav?>"><?=anchor("admin/kandidat_keterampilans","Kandidat Keterampilan", $kandidat_keterampilans_nav)?></li>
        <li class="<?=$kandidat_pendidikans_nav?>"><?=anchor("admin/kandidat_pendidikans","Kandidat Pendidikan", $kandidat_pendidikans_nav)?></li>
        <li class="<?=$kandidat_pengalamans_nav?>"><?=anchor("admin/kandidat_pengalamans","Kandidat Pengalaman", $kandidat_pengalamans_nav)?></li>
    </ul>
</li><?php */?>
<li class="<?=$master_agamas_nav?> <?=$master_bahasas_nav?> <?=$master_bidang_usahas_nav?> <?=$master_gol_darahs_nav?> <?=$master_jenis_kelamins_nav?> <?=$master_kategoris_nav?> <?=$master_level_karirs_nav?> <?=$master_lokasi_negaras_nav?> <?=$master_lokasi_propinsis_nav?> <?=$master_lokasi_kabupatens_nav?> <?=$master_lokasi_kawasans_nav?> <?=$master_lokasi_kecamatans_nav?> <?=$master_lokasi_kelurahans_nav?> <?=$master_pendidikans_nav?> <?=$master_shdks_nav?> <?=$master_statuses_kawins_nav?> <?=$master_waktu_kerjas_nav?>">
    <?=anchor("javascript:void(0)","Master <span class='caret'></span>", array('class' => 'dropdown-toggle','data-toggle' => 'dropdown'))?>
    <ul class="dropdown-menu">
        <li class="<?=$master_agamas_nav?>"><?=anchor("admin/master_agamas","Agama", $master_agamas_nav)?></li>
        <li class="<?=$master_bahasas_nav?>"><?=anchor("admin/master_bahasas","Bahasa", $master_bahasas_nav)?></li>
        <li class="<?=$master_bidang_usahas_nav?>"><?=anchor("admin/master_bidang_usahas","Bidang Usaha", $master_bidang_usahas_nav)?></li>
        <li class="<?=$master_gol_darahs_nav?>"><?=anchor("admin/master_gol_darahs","Gol Darah", $master_gol_darahs_nav)?></li>
        <li class="<?=$master_jenis_kelamins_nav?>"><?=anchor("admin/master_jenis_kelamins","Jenis Kelamin", $master_jenis_kelamins_nav)?></li>
        <li class="<?=$master_kategoris_nav?>"><?=anchor("admin/master_kategoris","Kategori", $master_kategoris_nav)?></li>
        <li class="<?=$master_level_karirs_nav?>"><?=anchor("admin/master_level_karirs","Level Karir", $master_level_karirs_nav)?></li>
        <?php /* ?>
        <li class="<?=$master_lokasi_negaras_nav?>"><?=anchor("admin/master_lokasi_negaras","Lokasi Negara", $master_lokasi_negaras_nav)?></li>
        <?php */ ?>
        <li class="<?=$master_lokasi_propinsis_nav?>"><?=anchor("admin/master_lokasi_propinsis","Lokasi Propinsi", $master_lokasi_propinsis_nav)?></li>
        <li class="<?=$master_lokasi_kabupatens_nav?>"><?=anchor("admin/master_lokasi_kabupatens","Lokasi Kota / Kabupaten", $master_lokasi_kabupatens_nav)?></li>
        <li class="<?=$master_lokasi_kawasans_nav?>"><?=anchor("admin/master_lokasi_kawasans","Lokasi Kawasan", $master_lokasi_kawasans_nav)?></li>
        <li class="<?=$master_pendidikans_nav?>"><?=anchor("admin/master_pendidikans","Pendidikan", $master_pendidikans_nav)?></li>
        <li class="<?=$master_shdks_nav?>"><?=anchor("admin/master_shdks","Hubungan Keluarga", $master_shdks_nav)?></li>
        <li class="<?=$master_statuses_kawins_nav?>"><?=anchor("admin/master_statuses_kawins","Status Kawin", $master_statuses_kawins_nav)?></li>
        <li class="<?=$master_status_kerjas_nav?>"><?=anchor("admin/master_status_kerjas","Status Kerja", $master_status_kerjas_nav)?></li>
        <li class="<?=$master_waktu_kerjas_nav?>"><?=anchor("admin/master_waktu_kerjas","Waktu Kerja", $master_waktu_kerjas_nav)?></li>
        <?php /*?>
        <li class="<?=$master_lokasi_kecamatans_nav?>"><?=anchor("admin/master_lokasi_kecamatans","Master Lokasi Kecamatan", $master_lokasi_kecamatans_nav)?></li>
        <li class="<?=$master_lokasi_kelurahans_nav?>"><?=anchor("admin/master_lokasi_kelurahans","Master Lokasi Kelurahan", $master_lokasi_kelurahans_nav)?></li><?php */?>
    </ul>
</li>
<li class="<?=$perusahaans_nav?> <?=$lowongans_nav?> <?=$perusahaan_promos_nav?>">
    <?=anchor("javascript:void(0)","Perusahaan <span class='caret'></span>", array('class' => 'dropdown-toggle','data-toggle' => 'dropdown'))?>
    <ul class="dropdown-menu">
        <li class="<?=$perusahaans_nav?>"><?=anchor("admin/perusahaans","Perusahaan", $perusahaans_nav)?><li>
        <li class="<?=$lowongans_nav?>"><?=anchor("admin/lowongans","Lowongan", $lowongans_nav)?></li>
        <li class="<?=$perusahaan_promos_nav?>"><?=anchor("admin/perusahaan_promos","Promo Kandidat", $perusahaan_promos_nav)?></li>
    </ul>
    

</li>
<li class="<?=$pages_nav?>"><?=anchor("admin/pages","Halaman", $pages_nav)?></li>
<li class="<?=$artikels_nav?>"><?=anchor("admin/artikels","Artikel", $artikels_nav)?></li>
<li class="<?=$settings_nav?>">
    <?=anchor("javascript:void(0)","Setting <span class='caret'></span>", array('class' => 'dropdown-toggle','data-toggle' => 'dropdown'))?>
    <ul class="dropdown-menu">
        <li><?=anchor("admin/settings","Web Settings", $settings_nav)?></li>
        <li><?=anchor("admin/ads","Ads", $ads_nav)?></li>
    </ul>
    
</li>
</ul>
