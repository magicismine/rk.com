<div id="artikel">
	<h2>Promo Kandidat</h2>
	<div class="artikel_listing">
		<?php  
		foreach($list as $r):
			$O->setup($r);
			$OPTmp = new OPerusahaan($r->perusahaan_id);
		?>
		<div class="box-item">
			<a href="<?php echo site_url($O->get_link()); ?>" class="list-image"><img src="<?php echo $OPTmp->get_photo('square'); ?>" width="93"></a>
			<h3><a href="<?php echo site_url($O->get_link()); ?>"><?php echo $O->row->name; ?></a></h3>
			<div class="description">
				<?php echo word_limiter(strip_tags($O->row->description),20); ?>
			</div>
		</div>
		<?php
			unset($OPTmp);
		endforeach; 
		?>
	</div>
	<div id="paging_button">
		<?php echo $pagination; ?>
	</div>
</div>