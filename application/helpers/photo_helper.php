<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* PHOTO HELPER */

function auto_delete_photo($filename)
{
	// delete orig file
	$orig_file = FCPATH."/_assets/images/temp/".$filename;
	if(is_file($orig_file)) unlink($orig_file);
	// delete every type of sizes
	$CI =& get_instance();
	$CI->config->load('image');
	$imagesizes = $CI->config->item('image_sizes');
	foreach($imagesizes as $folder => $imagesize)
	{
		$dst_dir = FCPATH."/_assets/images/uploads/{$folder}/";
		$file = $dst_dir.$filename;
		if(is_file($file)) unlink($file);
	}
	
}

function auto_resize_photo_moo($filename)
{
	// requires
	$CI =& get_instance();
	// Load image config
	$CI->config->load('image');
	$imagesizes = $CI->config->item('image_sizes');
	// Load ImageMoo Lib
	$CI->load->library("image_moo");
	
	foreach($imagesizes as $folder => $imagesize)
	{
		$arr = explode("|",$imagesize);
		$src_dir = FCPATH."/_assets/images/temp/";
		$dst_dir = FCPATH."/_assets/images/uploads/{$folder}/";
		if(!is_dir($dst_dir))
		{
			mkdir($dst_dir,0777,TRUE);
			if(!is_writable($dst_dir))
			{
				// Gets file permissions
				$fileperms = substr(sprintf('%o', fileperms($dst_dir)), -4);
				// Change permissions
				if(function_exists('chmod'))
				{
					if(!chmod($dst_dir,0777))
					{
						return "Unable to chmod {$dst_dir}";
					}
				}
			}
		}
		
		$tmp_file = $src_dir.$filename;
		$dest_file = $dst_dir.$filename;
		list($w, $h, $type, $attr) = getimagesize($tmp_file);
		$dim_x = intval($arr[0]);
		$dim_y = intval($arr[1]);
		if($arr[2] == "true")
		{
			$CI->image_moo
				->load($tmp_file)
				//->resize_crop($dim_x,$dim_y)
				->set_background_colour("#FFF")
				->resize($dim_x,$dim_y,TRUE)
				->set_jpeg_quality(90)
				->save($dest_file,TRUE);
		}
		else{
			$CI->image_moo
				->load($tmp_file)
				->resize($dim_x,$dim_y)
				->set_jpeg_quality(90)
				->save($dest_file,TRUE);
		}
		$files[] = base_url(str_replace(FCPATH, "", $dest_file));
	}
	return $files;
}

function auto_resize_photo($filename)
{
	$CI =& get_instance();
	$CI->config->load('image');
	$imagesizes = $CI->config->item('image_sizes');
	foreach($imagesizes as $folder => $imagesize)
	{
		$arr = explode("|",$imagesize);
		$src_dir = FCPATH."/_assets/images/temp/";
		$dst_dir = FCPATH."/_assets/images/uploads/{$folder}/";
		if(!is_dir($dst_dir)) mkdir($dst_dir,0777,TRUE);
		
		$tmp_file = $src_dir.$filename;
		$dest_file = $dst_dir.$filename;
		list($w, $h, $type, $attr) = getimagesize($tmp_file);
		$dim_x = intval($arr[0]);
		$dim_y = intval($arr[1]);
		if(intval($arr[0]) > 0 && intval($arr[1]) > 0)
		{
			
			if(trim($arr[2]) == "true") 
			{
				$wx = doubleval($w/$dim_x);
				$hx = doubleval($h/$dim_y);
				if($h >= $w)
				{
					if(doubleval($h/$wx) >= $dim_y)
					{
						$length_type = "width";
						resize_photo($tmp_file, $dest_file, $dim_x, $length_type);
					}
					else {
						$length_type = "height";
						resize_photo($tmp_file, $dest_file, $dim_y, $length_type);
					}
				}
				else {
					if(doubleval($w/$hx) >= $dim_x)
					{
						$length_type = "height";
						resize_photo($tmp_file, $dest_file, $dim_y, $length_type);
					}
					else {
						$length_type = "width";
						resize_photo($tmp_file, $dest_file, $dim_x, $length_type);
					}
				}
				// crop top
				$y_top = false;
				if(trim($arr[3]) == "true")
				{
					$y_top = true;
				}
				crop_photo($dest_file,$dest_file,$dim_x,$dim_y,$y_top);
			}
			else {
				resize_photo($src_dir.$filename,$dst_dir.$filename,$arr[0],"width");
			}
		}
		$files[] = base_url("_assets/images/uploads/{$folder}/{$filename}");
	}
	return $files;
}

function auto_resize_photo_ori($filename)
{
	$CI =& get_instance();
	$CI->config->load('image');
	$imagesizes = $CI->config->item('image_sizes');
	foreach($imagesizes as $folder => $imagesize)
	{
		$arr = explode("|",$imagesize);
		$src_dir = FCPATH."/_assets/images/temp/";
		if(!is_dir($src_dir)) mkdir($src_dir,0777,TRUE);
		$dst_dir = FCPATH."/_assets/images/uploads/{$folder}/";
		if(!is_dir($dst_dir)) mkdir($dst_dir,0777,TRUE);
		resize_photo($src_dir.$filename,$dst_dir.$filename,$arr[0],"width");
		if($arr[2] == "true")
		{
			crop_photo($dst_dir.$filename,$dst_dir.$filename,$arr[0],$arr[1]);
		}
		$files[] = base_url("_assets/images/uploads/{$folder}/{$filename}");
	}
	return $files;
}

function save_photo($photo_name,$dir="",$sizes=array("600", "400", "200", "100", "200xcrops"),$fixed_crop_sizes=NULL)
{
	$CI =& get_instance();
	
	if(trim($photo_name) == "" || trim($dir) == "") return FALSE;
	$photo_root_path = FCPATH."/_assets/images/";
	$tmp_photo_root_path = $photo_root_path."temp/";
	$copy_photo_root_path = $photo_root_path.$dir."/";
	
	$tmp_file = $tmp_photo_root_path.$photo_name;
	$tmp_resize_file = $tmp_photo_root_path."resize_".$photo_name;
	if(!is_file($tmp_file)) return FALSE;
	else
	{
		// var_dump("FOUND");
		$CI->load->helper("image");
		$fpath = $copy_photo_root_path;
		list($w, $h, $type, $attr) = getimagesize($tmp_file);
			
		/*
		$file_arr = explode(".",$photo_name,2);
		$ext = $file_arr[count($file_arr)-1];
		$random = random_string('alnum', 16);
		$newfile = time()."_".$random.".".$ext;
		//*/
		$newfile = $photo_name;
		
		foreach($sizes as $size)
		{
			if($size == "200xcrops")
			{
				if($w > $h) $length_type = "height";
				if($h > $w) $length_type = "width";
				if($w >= 200) resize_photo($tmp_file, $fpath.$size."/".$newfile, 200, $length_type);
				else copy($tmp_file, $fpath.$size."/".$newfile);
				
				crop_photo($fpath.$size."/".$newfile, $fpath.$size."/".$newfile,200,200);
			}
			else
			{
				if($w >= intval($size)) resize_photo($tmp_file, $fpath.$size."/".$newfile, $size, "auto");
				else copy($tmp_file, $fpath.$size."/".$newfile);
			}
		}
		
		//$size == "cropped_80x65"
		if($fixed_crop_sizes != NULL)
		{
			foreach($fixed_crop_sizes as $size)
			{
				$size_arr = explode("_",$size);
				$size_dim_arr = explode("x",$size_arr[1]);
				$dim_x = intval($size_dim_arr[0]);
				$dim_y = intval($size_dim_arr[1]);
				if(intval($size_dim_arr[0]) > 0 && intval($size_dim_arr[1]) > 0)
				{
					if($w >= $dim_x && $h >= $dim_y)
					{
						$wx = doubleval($w/$dim_x);
						$hx = doubleval($h/$dim_y);
						if($h >= $w)
						{
							if(doubleval($h/$wx) >= $dim_y)
							{
								$length_type = "width";
								resize_photo($tmp_file, $fpath.$size."/".$newfile, $dim_x, $length_type);
							}
							else {
								$length_type = "height";
								resize_photo($tmp_file, $fpath.$size."/".$newfile, $dim_y, $length_type);
							}
						}
						else {
							if(doubleval($w/$hx) >= $dim_x)
							{
								$length_type = "height";
								resize_photo($tmp_file, $fpath.$size."/".$newfile, $dim_y, $length_type);
							}
							else {
								$length_type = "width";
								resize_photo($tmp_file, $fpath.$size."/".$newfile, $dim_x, $length_type);
							}
						}
					} 
					else copy($tmp_file, $fpath.$size."/".$newfile);
					
					crop_photo($fpath.$size."/".$newfile, $fpath.$size."/".$newfile,$dim_x,$dim_y);
				}
			}
		}
		
		copy($tmp_file, $fpath."originals/".$newfile);
		// if(is_file($tmp_file)) unlink($tmp_file);
		if(is_file($tmp_resize_file))
		{
			//copy($tmp_resize_file, $copy_photo_root_path."resize_".$photo_name);
			// unlink($tmp_resize_file);
		}
		return $newfile;
	}
}

function get_photo_url($photo_name,$dir="",$dim="200xcrops",$no_default_img=FALSE)
{
	$targetPath = FCPATH."/_assets/images/{$dir}/";
	$targetFile =  str_replace('//','/',$targetPath) . $dim . '/' . $photo_name;
	if(!is_file($targetFile) || trim($photo_name) == "" || trim($dir) == "")
	{
		if($no_default_img) return;
		else {
			 $targetFile = $targetPath."none.png";
		}
	}
	
	$newTargetFile = str_replace(FCPATH,'',$targetFile);
	return $newTargetFile;
}

function delete_photo($photo_name,$dir="",$sizes = array("originals", "600", "400", "200", "100", "200xcrops"),$fixed_crop_sizes=NULL)
{
	if(trim($photo_name) == "" || trim($dir) == "") return FALSE;
	$photo_root_path = FCPATH."/_assets/images/";
	$file_photo_root_path = $photo_root_path.$dir."/";
	
	$file_path = $file_photo_root_path.$photo_name;
	$file_resize_path = $file_photo_root_path."resize_".$photo_name;
	
	//$sizes = array("600", "400", "200", "crops");
	//$sizes = array("originals", "600", "400", "200", "100", "200xcrops"/*, "cropped_80x65"*/);
	foreach($sizes as $size)
	{
		$file_res_path = $file_photo_root_path.$size."/".$photo_name;
		if(is_file($file_res_path)) unlink($file_res_path);
	}
	if(!is_null($fixed_crop_sizes))
	{
		foreach($fixed_crop_sizes as $size)
		{
			$file_res_path = $file_photo_root_path.$size."/".$photo_name;
			if(is_file($file_res_path)) unlink($file_res_path);
		}
	}
	return TRUE;
}
