<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* DATE HELPER */

function dt_ddl($prefix, $dt="", $start_dt="", $end_dt="") // $date_value in YYYY-MM-DD
{
	if(empty($dt)) $dt = date("Y-m-d");
	$ts = (is_string($dt) ? strtotime($dt) : $dt);
	$year = date("Y",$ts);
	$month = date("n",$ts);
	$day = date("d",$ts);
	if(empty($start_dt)) $start_dt = date("Y",strtotime("-80 year"));
	if(empty($end_dt)) $end_dt = date("Y");
	
	return month_ddl("{$prefix}_month",$month).date_ddl("{$prefix}_date",$day).year_ddl("{$prefix}_year",$year,$start_dt,$end_dt);
}

function dob_ddl($prefix, $dt="", $start_dt="", $end_dt="") // $date_value in YYYY-MM-DD
{
	if(empty($dt)) $dt = date("Y-m-d");
	$ts = (is_string($dt) ? strtotime($dt) : $dt);
	$year = date("Y",$ts);
	$month = date("n",$ts);
	$day = date("d",$ts);
	if(empty($start_dt)) $start_dt = date("Y",strtotime("-80 year"));
	if(empty($end_dt)) $end_dt = date("Y",strtotime("-15 year"));
	
	return month_ddl("{$prefix}_month",$month).date_ddl("{$prefix}_date",$day).year_ddl("{$prefix}_year",$year,$start_dt,$end_dt);
}

function year_ddl($name,$val = "", $start = "", $end = "", $option = "")
{
	if($start == "") $start = date("Y");
	if($end == "") $end = date("Y",strtotime("+5 year"));
	$ret = "<select name='$name' id='$name' $option>";
	for($i = $start; $i <= $end; $i++)
	{
		if($i == $val) $selected = 'selected="selected"'; else $selected = '';
		$ret .= "<option value='$i' $selected>$i</option>";
	}
	$ret .= "</select>";
	return $ret;
}

function month_ddl($name,$val = "", $option = "")
{ 
	$start = 1;
	$end = 12;
	$ret = "<select name='$name' id='$name' $option>";
	for($i = $start; $i <= $end; $i++)
	{
		if($i < 10) $month = "0$i"; else $month = $i;
		if($month == $val) $selected = 'selected="selected"'; else $selected = '';
		
		$disp = date("M",strtotime("2008-$month-01"));
		$ret .= "<option value='$month' $selected>$disp</option>";
	}
	$ret .= "</select>";
	return $ret;
}

function date_ddl($name,$val = "", $option = "")
{
	$start = 1;
	$end = 31;
	$ret = "<select name='$name' id='$name' $option>";
	for($i = $start; $i <= $end; $i++)
	{
		if($i == $val) $selected = 'selected="selected"'; else $selected = '';
		$ret .= "<option value='$i' $selected>$i</option>";
	}
	$ret .= "</select>";
	return $ret;
}


function get_date_from_ddl($prefix, $array_type = "POST")
{
	if($array_type == "POST") $array = $_POST;
	else $array = $_GET;
	$month = $array["{$prefix}_month"];
	$date = $array["{$prefix}_date"];
	$year = $array["{$prefix}_year"];
	return "{$year}-{$month}-{$date}";
}

function year_custom_ddl($name,$val = "", $start = "", $end = "", $option = "", $default = "")
{
	if($start == "") $start = date("Y");
	if($end == "") $end = date("Y",strtotime("+5 year"));
	$ret = "<select name='$name' id='$name' $option>";
	if(!empty($default)) $ret .= "<option value=''>{$default}</option>";
	for($i = $start; $i <= $end; $i++)
	{
		if($i == $val) $selected = 'selected="selected"'; else $selected = '';
		$ret .= "<option value='$i' $selected>$i</option>";
	}
	$ret .= "</select>";
	return $ret;
}
