<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* FORM HELPER SECTION */

function radios($name,$arr=array(),$selected_value="",$extraparam="",$separator="<br />")
{
	foreach($arr as $val => $display)
	{
		if($val == $selected_value) $selected = 'checked="checked"'; else $selected = "";
		$ret .= "<label for='{$name}_{$val}'><input type='radio' id='{$name}_{$val}' name='$name' value='$val' $selected $extraparam /> ".$display."</label>".$separator;
	}
	return $ret;
}

function checkboxes($name,$arr,$selected_values = array(), $optional = "", $separator = "<br />")
{
	$id = str_replace("[]","",$name);
	foreach($arr as $val => $display)
	{
		if(in_array($val,$selected_values)) { $checked = 'checked="checked"'; } else $checked = '';
		$ret .= "<label for='{$id}_".url_title($val)."'><input type='checkbox' value='{$val}' name='{$name}' id='{$id}_".url_title($val)."' $checked> $display</label>".$separator;
	}
	return $ret;
}

function dropdown($name,$arr,$selected_value = "", $optional = "", $default_value="",$readonly = FALSE)
{
	$ret = "<select ".($readonly ? "" : "name='{$name}'")." id='{$name}' $optional ".($readonly ? "disabled='disabled'" : "").">";
	if(trim($default_value) != "") $ret .= "<option value=''>$default_value</option>";
	foreach($arr as $val => $display)
	{
		if(is_array($selected_value)) { if(@in_array($val,$selected_value)) $selected = 'selected="selected"'; else $selected = ""; }
		else { if($val == $selected_value) $selected = 'selected="selected"'; else $selected = ""; }
		//if($val == $selected_value) { $selected = 'selected="selected"'; } else $selected = '';
		$ret .= "<option value='{$val}' $selected>$display</option>";
	}
	$ret .= "</select>";
	if($readonly) $ret .= "<input type='hidden' name='{$name}' value='{$selected_value}' />";
	return $ret;
}

function gen_ddl_set($name, $arr = array(), $selected_val = "", $extra = "", $multi = false)
{
	if($multi) $multiple = "multiple";
	$ret .= "<select name='$name' id='$name' $multiple $extra>";
	if($selected_val == "") $selected = 'selected="selected"'; else $selected = '';
	/*$ret .= "<option value='' $selected>--- select one ---</option>";*/
	foreach($arr as $key => $val)
	{
		if($multi) { if(@in_array($val,$selected_val)) $selected = 'selected="selected"'; else $selected = ""; }
		else { if($key == $selected_val) $selected = 'selected="selected"'; else $selected = ""; }
		$ret .= "<option value='$key' $selected>$val</option>";
	}
	$ret .= "</select>";
	return $ret;
}
