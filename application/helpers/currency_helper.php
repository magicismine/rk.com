<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function format_number($num, $dec=0, $prefix="", $suffix="")
{
	return $prefix.number_format($num, $dec, ',', '.').$suffix;
}

function dollar_format($num)
{
	return format_number($num, 2, "USD ", $suffix="");
}

function rupiah_format($total)
{
	return format_number($num, 0, "Rp. ", "");
}

function currency_format($total,$currency = "USD")
{
	if($currency == "USD") return dollar_format($total);
	else return rupiah_format($total).",-";
}

function get_tax_percentage()
{
	return 0.1;
}

function get_tax($total)
{
	return $total*get_tax_percentage();
}

function get_shipping_fee()
{
	return 10000;
}
