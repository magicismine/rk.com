<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function admin_home() { redirect("admin/home"); }
function admin_login() { redirect("admin/home/login"); }
function admin_logout() { redirect("admin/home/logout"); }
function user_login($role) 
{ 
	if($role == ""){ redirect("");}
	if($role == "kandidat")
	{
		redirect("account/login/kandidat"); 
	}else
	{
		redirect("account/login/perusahaan"); 
	}
}
function user_home($role) 
{
	//if($role == ""){ redirect("");}
	if($role == "kandidat")
	{
		redirect("kandidat/home");
	}else
	{
		redirect("perusahaan/home");
	}
}
function user_logout($role) 
{
	redirect("logout/user/$role");
}
function user_inactive() 
{ 
	redirect('logout/inactive');
}

function emptyres($res)
{
	if(!is_object($res) || $res->num_rows() == 0) return true;
	else return false;
}
function get_db_total_rows($db = false)
{
	if($db == false)
	{
		$CI =& get_instance();
		$res = $CI->db->query("SELECT FOUND_ROWS() AS total;"); 
	}
	else
	{
		$res = $db->query("SELECT FOUND_ROWS() AS total;");
	}
	if(emptyres($res)) return 0;
	$tmp = $res->row();
	return $tmp->total;
	
}
function get_umur_min()
{
	return 18;
}
function get_umur_max()
{
	return 50;
}
function get_pengalaman_min()
{
	return 0;
}
function get_pengalaman_max()
{
	return 20;
}
?>