<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function array_merge_special($arr1,$arr2)
{
	if(empty($arr1)) return $arr2;
	if(empty($arr2)) return $arr1;
	$arr = array();
	foreach($arr1 as $key => $value)
	{
		$arr[$key] = $value;
	}
	foreach($arr2 as $key => $value)
	{
		$arr[$key] = $value;
	}
	return $arr;
	
}

function csv_from_array($array, $delim = ",", $newline = "\n", $enclosure = '"')
{
	if ( ! is_array($array) && count($array) < 1) return FALSE;

	$out = '';

	foreach ($array as $row)
	{
		foreach ($row as $item)
		{
			$out .= $enclosure.str_replace($enclosure, $enclosure.$enclosure, $item).$enclosure.$delim;
		}
		$out = rtrim($out);
		$out .= $newline;
	}

	return $out;
}
