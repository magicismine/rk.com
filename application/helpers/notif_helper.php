<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* NOTIFICATION HELPER SECTION */

function print_status()
{
	$CI =& get_instance();
	if($CI->session->flashdata('success') != "") echo print_success($CI->session->flashdata('success'));
	if($CI->session->flashdata('error') != "") echo print_error($CI->session->flashdata('error'));
	if($CI->session->flashdata('warning') != "") echo print_warning($CI->session->flashdata('warning'));
	if($CI->session->flashdata('info') != "") echo print_info($CI->session->flashdata('info'));
}

function warning($a, $b="success")
{
	$CI =& get_instance();
	switch ($a)
	{
		case "add":
			$str = "added";
			break;
		case "edit":
			$str = "updated";
			break;
		case "delete":
			$str = "deleted";
			break;
		case "cancel":
			$str = "canceled";
			break;
		case "active":
			$str = "activated";
			break;
		case "inactive":
			$str = "inactivated";
			break;
		default:
			$str = "";
			break;
	}
	if($str != "")
	{
		if($b == "success") $warning_string = "The data has been $str";
		else if($b == "fail") $warning_string = "The data can not be $str";
		else $warning_string = "";
	}
	if($warning_string == "") return "";
	else {
		if($b == "fail") return $CI->session->set_flashdata('warning', $warning_string);
		if($b == "success") return $CI->session->set_flashdata('success', $warning_string);
	}
}

function print_success($str)
{
	return ($str != "" ? "<div class=\"success\">".$str."</div>" : "");
}

function print_error($str)
{
	return ($str != "" ? "<div class=\"error\">".$str."</div>" : "");
}

function print_warning($str)
{
	return ($str != "" ? "<div class=\"notice\">".$str."</div>" : "");
}

function print_info($str)
{
	return ($str != "" ? "<div class=\"info\">".$str."</div>" : "");
}