<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function password_strength_check($pwd)
{
	$error = NULL;
	if( strlen($pwd) < 8 ) {
		$error[] = "Password too short!";
	}
	
	if( strlen($pwd) > 20 ) {
		$error[] = "Password too long!";
	}
	
	if( !preg_match("#[0-9]+#", $pwd) ) {
		$error[] = "Password must include at least one number!";
	}
	
	
	if( !preg_match("#[a-z]+#", $pwd) ) {
		$error[] = "Password must include at least one letter!";
	}
	
	
	if( !preg_match("#[A-Z]+#", $pwd) ) {
		$error[] = "Password must include at least one CAPS!";
	}
	
	
	
	if( !preg_match("#\W+#", $pwd) ) {
		$error[] = "Password must include at least one symbol!";
	}
	
	
	if(count($error) > 0){
		return $error;
	} else {
		return TRUE;
	}	
}

function password_check($pwd)
{
	$error = NULL;

	if ($pwd){
		return TRUE;
	} else {
		return FALSE;
	}

}

function validate_email($email)
{
	list($prefix, $domain) = split("@",$email);

	if(function_exists("getmxrr") && getmxrr($domain, $mxhosts))
	{
		return true;
	}
	else if(function_exists("checkdnsrr") && checkdnsrr($domain, "MX"))
	{
		return true;
	}
	else if(@fsockopen($domain, 25, $errno, $errstr, 5))
	{
		return true;
	}
	else
	{
		return false;
	}

}

function validate_text_contain_email($text)
{
    if (preg_match('/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/si', $text))
	{
		return true;
	}
	else return false;
}