<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* PAGINATION HELPER */

function getPagination($total, $perpage, $url, $num_links = 5, $template=FALSE, $set_template)
{
	$CI =& get_instance();
	$CI->load->library('pagination');
	
	$config['base_url'] 	= site_url($url);
	$config['total_rows'] 	= $total;
	$config['per_page'] 	= $perpage; 
	//$config['uri_segment'] 	= $uri_segment;
	$config['num_links'] 	= $num_links;
	$config['page_query_string'] = TRUE;
	$config['query_string_segment'] = 'page';
	if($template):
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = '&laquo; First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['last_link'] = 'Last &raquo;';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['next_link'] = '&raquo;';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo;';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a href="#">';
	$config['cur_tag_close'] = '</a></li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	endif;
	if(is_array($set_template))
	{
		foreach($set_template as $key => $val)
		{
			$config[$key] = $val;
		}
	}
		
	$CI->pagination->initialize($config);
	return $CI->pagination->create_links();
}

function genPagination($total, $perpage, $url, $uri_segment, $num_links = 5, $template=FALSE, $set_template)
{
	$CI =& get_instance();
	$CI->load->library('pagination');
	
	$config['base_url'] 	= site_url($url);
	$config['total_rows'] 	= $total;
	$config['per_page'] 	= $perpage; 
	$config['uri_segment'] 	= $uri_segment;
	$config['num_links'] 	= $num_links;
	if($template):
	$config['full_tag_open'] = '<ul class="pagination">';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = '&laquo; First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['last_link'] = 'Last &raquo;';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['next_link'] = '&raquo;';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo;';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active"><a>';
	$config['cur_tag_close'] = '</a></li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	endif;
	if(is_array($set_template))
	{
		foreach($set_template as $key => $val)
		{
			$config[$key] = $val;
		}
	}

	$CI->pagination->initialize($config);
	/*
	<ul class="pagination">
	<li class="disabled"><a href="#">&laquo;</a></li>
	<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
	...
	</ul>
	*/
	return $CI->pagination->create_links();
}

function getPaginationLowongan($total, $perpage, $url, $num_links = 5, $template=FALSE, $set_template)
{
	$CI =& get_instance();
	$CI->load->library('pagination');
	
	$config['base_url'] 	= site_url($url);
	$config['total_rows'] 	= $total;
	$config['per_page'] 	= $perpage; 
	//$config['uri_segment'] 	= $uri_segment;
	$config['num_links'] 	= $num_links;
	$config['page_query_string'] = TRUE;
	$config['query_string_segment'] = 'page';
	if(is_array($set_template))
	{
		foreach($set_template as $key => $val)
		{
			$config[$key] = $val;
		}
	}
	if($template):
	$config['full_tag_open'] = '<ul>';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = '&laquo; First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['last_link'] = 'Last &raquo;';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['next_link'] = '&raquo;';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo;';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active">';
	$config['cur_tag_close'] = '</li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	endif;
		
	$CI->pagination->initialize($config);
	return $CI->pagination->create_links();
}

function genPaginationLowongan($total, $perpage, $url, $uri_segment, $num_links = 5, $template=FALSE, $set_template)
{
	$CI =& get_instance();
	$CI->load->library('pagination');
	
	$config['base_url'] 	= site_url($url);
	$config['total_rows'] 	= $total;
	$config['per_page'] 	= $perpage; 
	$config['uri_segment'] 	= $uri_segment;
	$config['num_links'] 	= $num_links;
	if(is_array($set_template))
	{
		foreach($set_template as $key => $val)
		{
			$config[$key] = $val;
		}
	}
	if($template):
	$config['full_tag_open'] = '<ul>';
	$config['full_tag_close'] = '</ul>';
	$config['first_link'] = '&laquo; First';
	$config['first_tag_open'] = '<li>';
	$config['first_tag_close'] = '</li>';
	$config['last_link'] = 'Last &raquo;';
	$config['last_tag_open'] = '<li>';
	$config['last_tag_close'] = '</li>';
	$config['next_link'] = '&raquo;';
	$config['next_tag_open'] = '<li>';
	$config['next_tag_close'] = '</li>';
	$config['prev_link'] = '&laquo;';
	$config['prev_tag_open'] = '<li>';
	$config['prev_tag_close'] = '</li>';
	$config['cur_tag_open'] = '<li class="active">';
	$config['cur_tag_close'] = '</li>';
	$config['num_tag_open'] = '<li>';
	$config['num_tag_close'] = '</li>';
	endif;

	$CI->pagination->initialize($config);
	/*
	<ul class="pagination">
	<li class="disabled"><a href="#">&laquo;</a></li>
	<li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
	...
	</ul>
	*/
	return $CI->pagination->create_links();
}