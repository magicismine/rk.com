<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* LOGIN SECTION */

function get_current_admin()
{ 
	$CI =& get_instance();
	if($CI->session->userdata('aname') == false || $CI->session->userdata('apass') == false) return false;
	return get_admin($CI->session->userdata('aname'),$CI->session->userdata('apass')); 
}
function get_admin($aname,$apass)
{
	$CI =& get_instance();
	$q = "SELECT * FROM admins WHERE username = ? AND password = ?";
	$res = $CI->db->query($q,array($aname,$apass)); 
	if(emptyres($res)) return false;
	else return $res->row();
}
function get_logged_in_user()
{ 
	$CI =& get_instance();
	if($CI->session->userdata('uname') == false || $CI->session->userdata('upass') == false) return false;
	return get_user($CI->session->userdata('uname'),$CI->session->userdata('upass')); 
}
function get_user($uname,$upass,$role ="",$status)
{
	$CI =& get_instance();
	$q = "SELECT * FROM users WHERE email = ? AND password = ?";
	$res = $CI->db->query($q,array($uname,$upass)); 
	if(emptyres($res)) return false;
	else return $res->row();
}
function set_login_session($uname,$upass,$type="admin",$status)
{
	$CI =& get_instance();
	if($type=="admin") $CI->session->set_userdata(array("aname"=>$uname, "apass"=>$upass));
	else $CI->session->set_userdata(array("uname"=>$uname, "upass"=>$upass, "role"=>$type,"status" => $status));
}
function unset_login_session($type="admin")
{
	$CI =& get_instance();
	if($type=="admin") $CI->session->unset_userdata(array("aname"=>"", "apass"=>""));
	else $CI->session->unset_userdata(array("uname"=>"", "upass"=>"", "role"=>"","status" => ""));
}
