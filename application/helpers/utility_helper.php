<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function parse_breadcrumb($arr=NULL)
{
	$CI =& get_instance();
	if(count($arr) <= 0)
	{
		$arr = array(
					$CI->uri->slash_segment(1).$CI->uri->segment(2)."?".get_params($CI->input->get()) => humanize($CI->uri->segment(2)),
					"" => humanize($CI->uri->segment(3))
					);
	}
	$tmp_arr = NULL;
	foreach($arr as $k => $v)
	{
		if(empty($k)) $tmp_arr[] = $v;
		else $tmp_arr[] = anchor($k, $v);
	}
	$ret = '<ol class="breadcrumb"><li>'.implode("</li><li>", $tmp_arr).'</li></ol>';
	return $ret;
}

function get_params($params=NULL, $exclude_arr=NULL)
{
	$get_arr = $get_array_arr = NULL;
	if(sizeof($params))
	{
		foreach($params as $key => $val):
			if(!in_array($key, $exclude_arr) && $val != "" && !is_array($val)) $get_arr[$key] = $val;
			else 
			{
				//$key = htmlspecialchars($key, ENT_QUOTES);
				foreach($val as $k => $v)
				{
					$get_array_arr[] = $key."[]=".$v;
				}
			}
		endforeach;
		//var_dump($get_array_arr);
		if(count($get_arr) > 0) $ret = http_build_query($get_arr,'','&');
		if(count($get_array_arr) > 0)
		{
			$ret = $ret."&".implode("&", $get_array_arr);
		}
		return $ret;
	}
	return false;
}

function get_in_params($params=NULL, $include_arr=NULL)
{
	$get_arr = $get_array_arr = NULL;
	if(sizeof($params))
	{
		foreach($params as $key => $val):
			if(in_array($key, $include_arr) && $val != "" && !is_array($val)) $get_arr[$key] = $val;
			else 
			{
				//$key = htmlspecialchars($key, ENT_QUOTES);
				foreach($val as $k => $v)
				{
					$get_array_arr[] = $key."[]=".$v;
				}
			}
		endforeach;
		//var_dump($get_array_arr);
		if(count($get_arr) > 0) $ret = http_build_query($get_arr);
		if(count($get_array_arr) > 0)
		{
			$ret = $ret."&amp;".implode("&amp;", $get_array_arr);
		}
		return $ret;
	}
	return false;
}

function get_setting($key="")
{
	$CI =& get_instance();
	$q = "SELECT * FROM settings WHERE `key` = ?";
	$res= $CI->db->query($q,array($key));
	if(emptyres($res)) return FALSE;
	else
	{
		$r = $res->row();
		return $r->content;
	}
}

function format_size($size) {
	$sizes = array(" Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB");
	if ($size == 0) { return('n/a'); } else {
	return (round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) . $sizes[$i]); }
}

function parse_youtube_url($url,$return='embed',$width='',$height='',$rel=0){
	$urls = parse_url($url);

	//url is http://youtu.be/xxxx
	if($urls['host'] == 'youtu.be'){ 
		$id = ltrim($urls['path'],'/');
	}
	//url is http://www.youtube.com/embed/xxxx
	else if(strpos($urls['path'],'embed') == 1){ 
		$id = end(explode('/',$urls['path']));
	}
	 //url is xxxx only
	else if(strpos($url,'/')===false){
		$id = $url;
	}
	//http://www.youtube.com/watch?feature=player_embedded&v=m-t4pcO99gI
	//url is http://www.youtube.com/watch?v=xxxx
	else{
		parse_str($urls['query']);
		$id = $v;
		if(!empty($feature)){
			$id = end(explode('v=',$urls['query']));
		}
	}
	//return embed iframe
	if($return == 'embed'){
		return '<iframe src="http://www.youtube.com/embed/'.$id.'?rel='.$rel.'" frameborder="0" width="'.($width?$width:637).'" height="'.($height?$height:349).'"></iframe>';
	}
	//return normal thumb
	else if($return == 'thumb'){
		return 'http://i1.ytimg.com/vi/'.$id.'/default.jpg';
	}
	//return hqthumb
	else if($return == 'hqthumb'){
		return 'http://i1.ytimg.com/vi/'.$id.'/hqdefault.jpg';
	}
	// else return id
	else{
		return $id;
	}
	// echo parse_youtube_url('http://youtu.be/zc0s358b3Ys','hqthumb'); //return http://i1.ytimg.com/vi/zc0s358b3Ys/hqdefault.jpg 
	// echo parse_youtube_url('http://www.youtube.com/watch?v=zc0s358b3Ys','embed'); //return embed code (iframe) 
}

function filter_tag_style($html)
{
	//http://stackoverflow.com/questions/3026096/remove-all-attributes-from-an-html-tag
	/*
/              # Start Pattern
 <             # Match '<' at beginning of tags
 (             # Start Capture Group $1 - Tag Name
  [a-z]         # Match 'a' through 'z'
  [a-z0-9]*     # Match 'a' through 'z' or '0' through '9' zero or more times
 )             # End Capture Group
 [^>]*?        # Match anything other than '>', Zero or More times, not-greedy (wont eat the /)
 (\/?)         # Capture Group $2 - '/' if it is there
 >             # Match '>'
/i            # End Pattern - Case Insensitive
	*/
	return preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $html);
}
function get_default_html_tags()
{
	$html_tag_default = 'html, body, div, span, applet, object, iframe,
	h1, h2, h3, h4, h5, h6, p, blockquote, pre,
	a, abbr, acronym, address, big, cite, code,
	del, dfn, em, img, ins, kbd, q, s, samp,
	small, strike, strong, sub, sup, tt, var,
	b, u, i, center,
	dl, dt, dd, ol, ul, li,
	fieldset, form, label, legend,
	table, caption, tbody, tfoot, thead, tr, th, td,
	article, aside, canvas, details, embed, 
	figure, figcaption, footer, header, hgroup, 
	menu, nav, output, ruby, section, summary,
	time, mark, audio, video';
	
	$html_tag_default_arr = explode(",",$html_tag_default);
	$html_tag_exc_arr = array('html', 'body', 'applet', 'object', 'iframe',
								'acronym', 'code',
								'tt', 'var',
								'canvas', 'embed',
								'output', 'ruby',
								'audio', 'video');
	$arr = NULL;
	foreach($html_tag_default_arr as $tag)
	{
		$tag = trim($tag);
		if(!in_array($tag,$html_tag_exc_arr))
		{
			$arr[] = $tag;
		}
	}
	return $arr;
}
function strip_only($str, $tags)
{
    if(!is_array($tags)) { $tags = explode(',',$tags); }
    $tagsPattern = implode('|', $tags);
    return preg_replace("#</?({$tagsPattern})[^>]*>#is", '', $str);
	/*
	echo strip_only($str, 'span,a');
	// OR
	echo strip_only($str, array('span', 'a'));
	*/
}

/** * close all open xhtml tags at the end of the string
 * * @param string $html
 * @return string
 * @author Milian <mail@mili.de>
 */
function closetags($html){
	#put all opened tags into an array
	preg_match_all('#<([a-z]+)(?: .*)?(?<![/|/ ])>#iU', $html, $result);
	$openedtags = $result[1];   #put all closed tags into an array
	preg_match_all('#</([a-z]+)>#iU', $html, $result);
	$closedtags = $result[1];
	$len_opened = count($openedtags);
	# all tags are closed
	if (count($closedtags) == $len_opened) {
		return $html;
	}
	$openedtags = array_reverse($openedtags);
	# close tags
	for ($i=0; $i < $len_opened; $i++) {
		if (!in_array($openedtags[$i], $closedtags)){
			$html .= '</'.$openedtags[$i].'>';
		}
		else {
			unset($closedtags[array_search($openedtags[$i], $closedtags)]);
		}
	}
	return $html;
}
function filter_html_tags($html)
{
	$default_tags = get_default_html_tags();
    foreach ($default_tags as $tag)
    {
    	$allowable_tags .= "<".$tag.">";
    }
    // strip un allowed tags
    $html = strip_tags($html, $allowable_tags);
    // complete tags open
    $html = closetags($html);
    // strip style
    $html = filter_tag_style($html);
    // strip blank space and decode special chars
    $html = htmlspecialchars_decode(str_replace(array("<p>&nbsp;</p>"), array(""), $html));
    return $html;
}
function trimmer($str,$maxchar = 15)
{
	$str = strip_tags($str);
	if(strlen($str) <= $maxchar) return $str;
	else return substr($str,0,$maxchar-3)." ..";
}

function get_lang()
{
	$CI =& get_instance();
	$lang = $CI->lang->lang();
	$CI->session->set_userdata('lang', $lang);
	return $lang;
}

function get_tokenizer($file)
{
$content = file_get_contents($file);
$tokens = token_get_all($content);
$output = '';

foreach($tokens as $token) {
 if(is_array($token)) {
  list($index, $code, $line) = $token;
  switch($index) {
   case T_OPEN_TAG_WITH_ECHO:
    $output .= '<?php echo ';
    break;
   case T_OPEN_TAG:
    $output .= '<?php ';
    break;
   default:
    $output .= $code;
    break;
  }

 }
 else {
  $output .= $token;
 }
}
return $output;
}

function get_total_perusahaan()
{
	$CI =& get_instance();
	$q = "SELECT COUNT(*) as total FROM perusahaans";
	$res = $CI->db->query($q); 
	if(emptyres($res)) return false;
	else return $res->row();
}

function get_total_kandidat()
{
	$CI =& get_instance();
	$q = "SELECT COUNT(*) as total FROM kandidats";
	$res = $CI->db->query($q); 
	if(emptyres($res)) return false;
	else return $res->row();
}

function get_total_lowongan()
{
	$CI =& get_instance();
	$q = "SELECT COUNT(*) as total FROM lowongans";
	$res = $CI->db->query($q); 
	if(emptyres($res)) return false;
	else return $res->row();
}