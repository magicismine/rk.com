<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* TIME HELPER */

function datediff($d1, $d2, $format)
{
  $formatDefinitions = array(
    'years' 	=> 31536000,
    'months' 	=> 2592000,
    'weeks' 	=> 604800,
    'days' 		=> 86400,
    'hours' 	=> 3600,
    'minutes' 	=> 60,
    'seconds' 	=> 1
  );
  $time = time();
	if(empty($d1)) $d1 = $time;
	if(empty($d2)) $d2 = $time;
	$d1 = (is_string($d1) ? strtotime($d1) : $d1);
	$d2 = (is_string($d2) ? strtotime($d2) : $d2);

  $delta = abs($d1 - $d2);

  $seconds = array();
  foreach ($formatDefinitions as $definition => $divider)
  {
      /*
      if (strpos($format, $definition) !== FALSE)
      {
      */
          $seconds[$definition] = floor($delta / $divider);
          //$delta = $delta % $divider;
      //}
  }
  //return strtr($format, $seconds);
  return $seconds; 
}

function datediff_old($d1, $d2)
{
	$time = time();
	if(empty($d1)) $d1 = $time;
	if(empty($d2)) $d2 = $time;
	$d1 = (is_string($d1) ? strtotime($d1) : $d1);
	$d2 = (is_string($d2) ? strtotime($d2) : $d2);
	$diff_secs = abs($d1 - $d2);  
	$base_year = min(date("Y", $d1), date("Y", $d2));  
	$diff = mktime(0, 0, $diff_secs, 1, 1, $base_year);  
	return array( 
				"years" => date("Y", $diff) - $base_year, 
				"months_total" => (date("Y", $diff) - $base_year) * 12 + date("n", $diff) - 1,  
				"months" => date("n", $diff) - 1,  
				"days_total" => floor($diff_secs / (3600 * 24)),  
				"days" => date("j", $diff) - 1,  
				"hours_total" => floor($diff_secs / 3600),  
				"hours" => date("G", $diff),  
				"minutes_total" => floor($diff_secs / 60),  
				"minutes" => (int) date("i", $diff),  
				"seconds_total" => $diff_secs,  
				"seconds" => (int) date("s", $diff)  
				);  
} 

function parse_month($m,$lang="ID",$type="long")
{
	$month_arr = $month_lang_arr = array();
	if($type == "short")
	{
		$month_lang_arr = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		if($lang == "ID") 
		$month_lang_arr = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agst","Sept","Okt","Nop","Des");
	} else {
		$month_lang_arr = array("January","February","March","April","May","June","July","August","September","October","November","December");
		if($lang == "ID") 
		$month_lang_arr = array("Januari","Pebruari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
	}
	$m = intval($m)-1;
	return $month_lang_arr[$m];
}
function parse_date($dt, $format="M d, Y", $lang="ID", $month_type="short")
{
	if($dt == "0000-00-00" || intval($dt) == 0) return "";
	if(empty($format)) $format = "M d, Y";
	$month_arr = $month_lang_arr = array();
	if($month_type == "short")
	{
		$month_arr = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		if($lang == "ID") 
		$month_lang_arr = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agst","Sept","Okt","Nop","Des");
	} else {
		$month_arr = array("January","February","March","April","May","June","July","August","September","October","November","December");
		if($lang == "ID") 
		$month_lang_arr = array("Januari","Pebruari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
	}
	$time = (is_string($dt) ? strtotime($dt) : $dt);  
	//if($lang == "ID") $format = "j M Y";
	$ret = date($format,$time);
	if($lang == "ID") $ret = str_replace($month_arr, $month_lang_arr, $ret);
	return $ret;
}

function parse_date_time($dt, $format="M d, Y g:ia", $lang="ID", $month_type="short")
{
	if($dt == "0000-00-00 00:00:00" || intval($dt) == 0) return "";
	if(empty($format)) $format = "M d, Y g:ia";
	$month_arr = $month_lang_arr = array();
	if($month_type == "short")
	{
		$month_arr = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		if($lang == "ID")
		{
			$month_lang_arr = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agst","Sept","Okt","Nop","Des");
		}
	} else {
		$month_arr = array("January","February","March","April","May","June","July","August","September","October","November","December");
		if($lang == "ID")
		{
			$month_lang_arr = array("Januari","Pebruari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");
		}
	}
	$time = (is_string($dt) ? strtotime($dt) : $dt);  
	//if($lang == "ID") $format = "j M Y G:i";
	$ret = date($format,$time);
	if($lang == "ID") $ret = str_replace($month_arr, $month_lang_arr, $ret);
	return $ret;
}
function getrelativetime($ts)
{
	$curyear = date("Y");
	$today = date("Y-m-d");
	$yesterday = date("Y-m-d",strtotime("-1 day"));
	$lastweek = date("Y-m-d",strtotime("-1 week"));
	$todayts = date("Y-m-d",strtotime($ts));
	$yearts = date("Y",strtotime($ts));
	$CI =& get_instance();
	$CI->load->helper('date');
	if($today == $todayts)
	{
		return str_replace(array(" hours"," hour"," minutes"," minute",","	),array("h","h","m","m",""),strtolower(timespan(strtotime($ts),time())." ago"));
	}
	if($todayts == $yesterday)
	{
		return "Yesterday at ".date("g:ia",strtotime($ts));
	}
	if($todayts > $lastweek)
	{
		return date("D \a\\t g:ia",strtotime($ts));
	}
	if($yearts == $curyear)
	{
		return date("M d \a\\t g:ia",strtotime($ts));
	}
	return date("M d, Y \a\\t g:ia",strtotime($ts));
	   
}
function format2date($d1,$d2)
{
	$d1 = (is_string($d1) ? strtotime($d1) : $d1);  
	$d2 = (is_string($d2) ? strtotime($d2) : $d2);  
	// if they are the same date
	if(date("mdY",$d1) == date("mdY",$d2))
	{
		$datedisp = date("F, d Y", $d1);
	}
	// if same month and same year
	else if(date("m Y",$d1) == date("m Y",$d2))
	{
		if(intval(date("j",$d2))-intval(date("j",$d1)) <= 1)
		{
			$datedisp = date('j',$d1).' & '.date('j',$d2).' '.date('F',$d1).' '.date('Y',$d1);
		}
		else
		// if the days are more than 1
		{
			$datedisp = date('j',$d1).' &ndash; '.date('j',$d2).' '.date('F',$d1).' '.date('Y',$d1);
		}
	} 
	// if they are not of the same month and year
	else
	{
		if(date("Y",$d1) == date("Y",$d2))
		{
			$datedisp = date('j', $d1)." ".date('F', $d1)." &ndash; ".date('j', $d2)." ".date('F', $d2)." ".date('Y',$d1);
		}
		else
		{
			$datedisp = date("F, d Y", $d1)." &ndash; ".date("F, d Y", $d2);
		}
	}
	$tmp = $datedisp;
	
	$time_d1 = date("H:i:s",$d1);
	$time_d2 = date("H:i:s",$d2);
	//var_dump($time_d1, $time_d2);
	if(intval($time_d1) > 0 || intval($time_d2) > 0)
	{
		if(intval($time_d1) > 0 && intval($time_d2) > 0) $tmp .= " @ ".date("g:ia", $d1)." &ndash; ".date("g:ia", $d2);
		if(intval($time_d1) > 0 && intval($time_d2) == 0) $tmp .= " @ ".date("g:ia", $d1)." &ndash; end";
	}
	return $tmp;
}

function get_date_lang($dt,$lang="EN")
{
	$dt_arr = explode("-",$dt);
	$dt_arr_reverse = array_reverse($dt_arr);
	if((substr($dt,4,1) == "-") && (substr($dt,4,1) == "-")) //yyyy-mm-dd
	{
		if($lang == "ID") $ret = implode("-",$dt_arr_reverse);
		if($lang == "EN") $ret = implode("-",$dt_arr);
	}
	if((substr($dt,2,1) == "-") && (substr($dt,5,1) == "-")) //dd-mm-yyyy
	{
		if($lang == "ID") $ret = implode("-",$dt_arr);
		if($lang == "EN") $ret = implode("-",$dt_arr_reverse);
	}
	return $ret;
}

function get_age($dt_born)
{
	/*
	$input = $dt_born;
	$dt_inputan = explode("-",$input);
	$past = "$dt_inputan[2]-$dt_inputan[1]-$dt_inputan[0]";

	$now = date("j-n-Y");
	$beda = abs(strtotime($now) - strtotime($past));
	$years = floor($beda / (365*60*60*24));
	*/
	$diff = datediff($dt_born);
	$years = $diff['years'];

	return $years;
}

function get_only_date($date,$show = "full")
{
	$input = $date;
	$dt_inputan = explode("-",$input);
	$date_divide = "$dt_inputan[2]-$dt_inputan[1]-$dt_inputan[0]";

	if($show == "year")
	{
		$res = $dt_inputan[0];
	}elseif($show == "month")
	{
		$res = $dt_inputan[1];
	}elseif($show == "day")
	{
		$res = $dt_inputan[2];
	}else
	{
		$res = parse_date($date,"Y-M-d","ID","long");
	}
	return $res;
}