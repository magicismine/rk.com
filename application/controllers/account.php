<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->curpage 	= "account";
		//error_reporting(E_ALL);
		$cu = get_logged_in_user();
		$role = $cu->role;
		if($cu) user_home($role);
	}
	
	public function index()
	{
		$this->login($role);
	}

	public function inactive()
	{
		die('Your account is inactive');
	}
	
	public function login($role)
	{


		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');

		if ($this->form_validation->run() == TRUE):
		
			extract($_POST);
			$check = get_user($email,md5(md5($password)),$role);
			$status = $check->status;
			$roles = $check->role;
			//var_dump($check);die();			
			if($check):
				//var_dump($_POST, $email, md5(md5($password))); die();
				set_login_session($email,md5(md5($password)),$roles,$status);
				//var_dump($roles);die();
				if($status == "aktif")
				{
					if($roles != "")
					{
						user_home($roles);
					}
					else
					{
						user_logout($roles);	
					}
				}else
				{
					user_inactive();
				}
			else:
				$this->session->set_flashdata("error", "Invalid Email and Password Combination.");
				redirect("account/login/");
			endif;
			
		endif;		
		
		$data['accountNav'] = "active";
		$data['role'] = $role;
		$this->load->view($this->fhead, $data);
		$this->load->view('users/login', $data);
		$this->load->view($this->ffoot, $data);
	}

	public function register($role,$a = "", $par1)
	{
		require_once(FCPATH.'/recaptchalib.php');

		// Get a key from https://www.google.com/recaptcha/admin/create
		$publickey = RECAPTCHA_PUBLIC;
		$privatekey = RECAPTCHA_PRIVATE;

		# the response from reCAPTCHA
		$resp = null;
		# the error code from reCAPTCHA, if any
		$error = null;

		
			if($a == ""):
				
				if($role == "kandidat"){
					$this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required|min_length[2]|xss_clean');
				}
				if($role == "perusahaan"){
					$this->form_validation->set_rules('nama', 'Nama Perusahaan', 'trim|required|min_length[2]|xss_clean');
					$this->form_validation->set_rules('kontak_nama', 'Nama Kontak', 'trim|required');
					$this->form_validation->set_rules('kontak_email', 'Email Kontak', 'trim|required');
					$this->form_validation->set_rules('lokasi_alamat', 'Alamat', 'trim|required');
				}
				$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[conf_password]|min_length[8]|md5');
				$this->form_validation->set_rules('conf_password', 'Password Confirmation', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
				$this->form_validation->set_rules('term', 'Term', 'trim|required');
				$this->form_validation->set_rules('recaptcha_response_field', 'Kode Gambar', 'trim|required');
				$this->form_validation->set_message('is_unique',"This email has been used. Please use another email.");
				$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
				
				if ($this->form_validation->run() == TRUE):
				
					extract($_POST);
					//var_dump($_POST, $password); die();

					# was there a reCAPTCHA response?
					if ($_POST["recaptcha_response_field"])
					{
	        			$resp = recaptcha_check_answer ($privatekey,
	                                        $_SERVER["REMOTE_ADDR"],
	                                        $_POST["recaptcha_challenge_field"],
	                                        $_POST["recaptcha_response_field"]);
	        			//var_dump($resp->is_valid);die();
						if (!$resp->is_valid)
						{
							// What happens when the CAPTCHA was entered incorrectly
							//die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." . "(reCAPTCHA said: " . $resp->error . ")");
							$data['error_code'] = "Invalid Kode.";
						}
						else
						{
							// Your code here to handle a successful verification
							$code = random_string('alnum', 16);
							$arr = NULL;
							$arr = array("password" => md5($password), "email" => $email,"role" => $role,"status" => "aktif");
							$new_id = OUser::add($arr);
							$O = new OUser($new_id);
							
							if($role == "kandidat")
							{
								
								$arr = array(
											"nama_depan" => $nama_depan,
											"nama_akhir" => $nama_akhir,
											"pengalaman_id" => $pengalaman_id
											);
								if($newsletter_flag != "")
								{
									$arr['newsletter_flag'] = $newsletter_flag;
								}
								$res = $O->update_info($arr);
								
								if(!empty($new_id) && $res):
									//email template
									/*$OET = new OEmail_template('register_success',"template_key");
									if(empty($OET->row->description) && empty($OET->row->subject)){}
									else
									{
										$to = $email;
										$subject = $OET->row->subject;
										$body = $this->load->view("tpl_email_template", array("OET" => $OET), TRUE);
										noreply_mail($to,$subject,$body);
									}*/
									$OKandidat = new OKandidat($res2);
									$OKandidat->update_url_title();
									unset($OKandidat);

									set_login_session($email,md5($password),$role,"aktif");
									if($_GET['rd'] != "") redirect($_GET['rd']);
									else redirect("kandidat");
									
								endif;
							}
							if($role == "perusahaan")
							{
								$arr = array(
										"nama" => $nama,
										"bidang_usaha_id" => $bidang_usaha_id,
										"kontak_nama" => $kontak_nama,
										"kontak_email" => $kontak_email,
										"lokasi_alamat" => $lokasi_alamat,
										"lokasi_negara_id" => $lokasi_negara_id,
										"lokasi_propinsi_id" => $lokasi_propinsi_id,
										"lokasi_kabupaten_id" => $lokasi_kabupaten_id,
										"lokasi_kecamatan" => $lokasi_kecamatan,
										"lokasi_kelurahan" => $lokasi_kelurahan,
										"lokasi_kawasan" => $lokasi_kawasan
										);
								if($newsletter_flag != "")
								{
									$arr['newsletter_flag'] = $newsletter_flag;
								}
								$res = $O->update_info($arr, "perusahaan");

								if(!empty($new_id) && $res):

									// SENT USER EMAIL
									/*$OET = new OEmail_template('register_success',"template_key");
									if(empty($OET->row->description) && empty($OET->row->subject)){}
									else
									{
										$to = $email;
										$subject = $OET->row->subject;
										$body = $this->load->view("tpl_email_template", array("OET" => $OET), TRUE);
										noreply_mail($to,$subject,$body);
									}*/

									set_login_session($email,md5($password),$role,"aktif");
									if($_GET['rd'] != "") redirect($_GET['rd']);
									else redirect("perusahaan");
									
								endif;
							}
						}
					}

				endif;

				$data['captcha'] = recaptcha_get_html($publickey);
		
				//$data['initjs'] = array("account");
				$data['accountNav'] = "active";
				$this->load->view($this->fhead, $data);
				if($role == "perusahaan")
				{
					$this->load->view('users/perusahaan/registrasi_perusahaan', $data);
				}else
				{
					$this->load->view('users/kandidat/registrasi_kandidat', $data);
				}
					

				$this->load->view($this->ffoot, $data);
			
			elseif($a == "confirm"):
			
				//$data['initjs'] = array("account");
				$data['email'] = $this->session->flashdata("register_email");
				$data['accountNav'] = "active";
				$this->load->view($this->fhead, $data);
				$this->load->view('account_register_confirm', $data);
				$this->load->view($this->ffoot, $data);
			
			elseif($a == "activate"):
			
				$code = $par1;
				$check = $this->db->query("SELECT * FROM users WHERE activation_code = ?", array($code));
				if(emptyres($check)) $data['invalid'] = TRUE;
				$data['accountNav'] = "active";
				$this->load->view($this->fhead, $data);
				$this->load->view('account_register_activate', $data);
				$this->load->view($this->ffoot, $data);
			
			endif;
		/*else:
			if($a == ""):
			
				$this->form_validation->set_rules('nama', 'Nama Perusahaan', 'trim|required|min_length[2]|xss_clean');
				$this->form_validation->set_rules('kontak_nama', 'Nama Kontak', 'trim|required');
				$this->form_validation->set_rules('kontak_email', 'Email Kontak', 'trim|required');
				$this->form_validation->set_rules('lokasi_alamat', 'Alamat', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[conf_password]|min_length[8]|callback_password_check|md5');
				$this->form_validation->set_rules('conf_password', 'Password Confirmation', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
				$this->form_validation->set_rules('term', 'Term', 'trim|required');
				$this->form_validation->set_rules('recaptcha_response_field', 'Kode Gambar', 'trim|required');
				$this->form_validation->set_message('is_unique',"This email has been used. Please use another email.");
				$this->form_validation->set_error_delimiters('<p class="text-red">', '</p>');
				
				if ($this->form_validation->run() == TRUE):
				
					extract($_POST);
					//var_dump($_POST, $password); die();
					$code = random_string('alnum', 16);
					$arr = NULL;
					$arr = array("password" => md5($password), "email" => $email,"role" => $role,"status" => "aktif");
					$new_id = OUser::add($arr);

					$O = new OUser($new_id);
					$arr = array(
							"nama" => $nama,
							"bidang_usaha_id" => $bidang_usaha_id,
							"kontak_nama" => $kontak_nama,
							"kontak_email" => $kontak_email,
							"lokasi_alamat" => $lokasi_alamat,
							"lokasi_negara_id" => $lokasi_negara_id,
							"lokasi_propinsi_id" => $lokasi_propinsi_id,
							"lokasi_kabupaten_id" => $lokasi_kabupaten_id,
							"lokasi_kecamatan" => $lokasi_kecamatan,
							"lokasi_kelurahan" => $lokasi_kelurahan,
							"lokasi_kawasan" => $lokasi_kawasan
							);
					if($newsletter_flag != "")
					{
						$arr['newsletter_flag'] = $newsletter_flag;
					}
					$res = $O->update_info($arr, "perusahaan");
					if(!empty($new_id) && $res):
					
						
						
						// SENT USER EMAIL
						/*$OET = new OEmail_template('register_success',"template_key");
						if(empty($OET->row->description) && empty($OET->row->subject)){}
						else
						{
							$to = $email;
							$subject = $OET->row->subject;
							$body = $this->load->view("tpl_email_template", array("OET" => $OET), TRUE);
							noreply_mail($to,$subject,$body);
						}/

						set_login_session($email,md5($password),$role,"aktif");
						if($_GET['rd'] != "") redirect($_GET['rd']);
						else redirect("perusahaan");
						
					endif;
					
				endif;
		
				//$data['initjs'] = array("account");
				$data['accountNav'] = "active";
				$this->load->view($this->fhead, $data);
				if($role == "perusahaan")
				{
					$this->load->view('users/perusahaan/registrasi_perusahaan', $data);
				}else
				{
					$this->load->view('users/kandidat/registrasi_kandidat', $data);
				}
					

				$this->load->view($this->ffoot, $data);
			
			elseif($a == "confirm"):
			
				//$data['initjs'] = array("account");
				$data['email'] = $this->session->flashdata("register_email");
				$data['accountNav'] = "active";
				$this->load->view($this->fhead, $data);
				$this->load->view('account_register_confirm', $data);
				$this->load->view($this->ffoot, $data);
			
			elseif($a == "activate"):
			
				$code = $par1;
				$check = $this->db->query("SELECT * FROM users WHERE activation_code = ?", array($code));
				if(emptyres($check)) $data['invalid'] = TRUE;
				$data['accountNav'] = "active";
				$this->load->view($this->fhead, $data);
				$this->load->view('account_register_activate', $data);
				$this->load->view($this->ffoot, $data);
			
			endif;

		endif;
		*/
	}
	
	public function password_check($pwd)
	{
		if (!password_check($pwd))
		{
			$this->form_validation->set_message('password_check', 'The %s field must contain at least one number, one letter and having between 8-20 characters in length!');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function forgot($a="")
	{
		if($a == "")
		{
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
			$this->form_validation->set_error_delimiters('', '<br />');
			
			if ($this->form_validation->run() == TRUE):
			
				extract($_POST);
				
				$check = $this->db->query("SELECT * FROM users WHERE email = ?", array($email));
				if(emptyres($check)) $data['invalid'] = TRUE;
				else
				{
					extract(get_object_vars($check->row()));
					//email the user
					$to = $email;
					//$OET = new OEmail_template('forgot_password',"template_key");

					/*if(empty($OET->row->description) && empty($OET->row->subject))
					{
						$subject = "An resetting code received | ".DOMAIN_NAME;
						$body = $this->load->view("tpl_email_request_a_resetting_code", array("name" => $name, "md5email" => md5(md5($email)), "md5password" => $password), TRUE);
					}else{
						$subject = $OET->row->subject;
						$body = $this->load->view("tpl_email_template", array("name" => $name, "md5email" => md5(md5($email)), "md5password" => $password,"OET" => $OET), TRUE);
					}*/
					$subject = "An resetting code received | ".DOMAIN_NAME;
						$body = $this->load->view("tpl_email_request_a_resetting_code", array("name" => $name, "md5email" => md5(md5($email)), "md5password" => $password), TRUE);
					noreply_mail($to,$subject,$body);

					$this->session->set_flashdata("forgot_email", $email);
					redirect($this->curpage."/forgot/confirm");
				}
				
			endif;
			
			//$data['initjs'] = array("account");
			$data['accountNav'] = "active";
			$this->load->view($this->fhead, $data);
			$this->load->view('account/forgot', $data);
			$this->load->view($this->ffoot, $data);
		}
		
		if($a == "confirm")
		{
			//$data['initjs'] = array("account");
			$data['email'] = $this->session->flashdata("forgot_email");
			$data['accountNav'] = "active";
			$this->load->view($this->fhead, $data);
			$this->load->view('account/forgot_confirm', $data);
			$this->load->view($this->ffoot, $data);
		}
	}
	
	public function email_check($email)
	{
		$check = $this->db->query("SELECT * FROM users WHERE email = ?", array($email));
		if (emptyres($check))
		{
			$this->form_validation->set_message('email_check', "This email ({$email}) is not registered.");
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function reset_password($md5email, $md5password)
	{
		$check = $this->db->query("SELECT * FROM users WHERE md5(md5(email)) = ? AND password = ?", array($md5email, $md5password));
		if(emptyres($check)) $data['invalid'] = TRUE;
		
		$this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|min_length[8]|callback_password_check|md5');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required');
		$this->form_validation->set_error_delimiters('', '<br />');
		
		if ($this->form_validation->run() == TRUE):
			
			$U = new OUser();
			$U->setup($check->row());
			extract($_POST);
			$arr = NULL;
			$arr = array("password" => md5($password));
			$res = $U->edit($arr);
			if($res)
			{
				$email = $U->row->email;
				set_login_session($email,md5($password),"user");
				//$this->session->set_flashdata("success", "Your password has been resetted. Please login to access your account");
				user_home();
			}
			else {
				$this->session->set_flashdata("error", "Sorry your password can not be resetted. Please contact the administrator!");
				redirect(current_url());
			}
		endif;
		
		$data['accountNav'] = "active";
		$this->load->view($this->fhead, $data);
		$this->load->view('account/reset_password', $data);
		$this->load->view($this->ffoot, $data);
	}
	
	public function facebook_connect()
	{
		session_start();
		if($_SESSION['facebook_login_status'] != "OK")
		{
			session_destroy();
			user_logout();
			exit();
		}
		
		require './facebook/src/facebook.php';
		
		// Create our Application instance (replace this with your appId and secret).
		$config = array();
		$config['appId'] = FB_APP_ID;
		$config['secret'] = FB_APP_SECRET;
		$config['fileUpload'] = false; // optional
		$facebook = new Facebook($config);
		
		// Get User ID
		$user = $facebook->getUser();
		if ($user) {
			try {
				// Proceed knowing you have a logged in user who's authenticated.
				$user_profile = $facebook->api('/me');
			} catch (FacebookApiException $e) {
				error_log($e);
				$user = null;
			}
		}
		if(!$user_profile)
		{
			session_destroy();
			user_logout();
			exit();
		}
		else {
			extract($user_profile);
			$res = $this->db->query("SELECT * FROM users WHERE fb_id = ? OR email = ?", array($user,$email));
			if(emptyres($res))
			{
				$this->load->helper(array("string"));
				
				$password = random_string("alnum", 20);
				$password = md5(md5($password));
				if(empty($name)) $name = $first_name." ".$last_name;
				
				$arr["fb_id"] = $user;
				$arr["email"] = $email;
				$arr["password"] = $password;
				$arr["name"] = $name;
				if(!empty($gender)) $arr["gender"] = $gender;
				$new = OUser::add($arr);
				
				if($new)
				{
					$O = new OUser($new);
					
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy("http://graph.facebook.com/".$user."/picture?type=large", $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
					
					set_login_session($email,$password,"user");
				}
			}
			else {
				$row = $res->row();
				$fb_login_count = $row->fb_login_count + 1;
				$arr["fb_login_count"] = $fb_login_count;
				$arr["fb_id"] = $user;
				//$arr["email"] = $email;
				$O = new OUser();
				$O->setup($row);
				$res = $O->edit($arr);
				if($res && empty($row->photo))
				{
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy("http://graph.facebook.com/".$user."/picture?type=large", $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
				}
				extract(get_object_vars($O->row));
				set_login_session($email,$password,"user");
			}
			if($_GET['rd'] != "") redirect($_GET['rd']);
			else user_home();
			exit();
		}
	}
	
	public function twitter_connect()
	{
		session_start();
		if($_SESSION['twitter_login_status'] != "OK")
		{
			session_destroy();
			user_logout();
			exit();
		}
		
		require_once('./twitter/twitteroauth/twitteroauth.php');
		
		/* If access tokens are not available redirect to connect page. */
		if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
			header('Location: /twitter.php?logout');
		}
		/* Get user access tokens out of the session. */
		$access_token = $_SESSION['access_token'];
		
		/* Create a TwitterOauth object with consumer/user tokens. */
		$connection = new TwitterOAuth(TW_CONSUMER_KEY, TW_CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
		
		/* If method is set change API call made. Test is called by default. */
		$user_profile = $connection->get('account/verify_credentials');

		if(!$user_profile)
		{
			session_destroy();
			user_logout();
			exit();
		}
		else {
			extract(get_object_vars($user_profile));
			$email = $screen_name;
			$res = $this->db->query("SELECT * FROM users WHERE twitter_id = ? OR email = ?", array($id,$email));
			if(emptyres($res))
			{
				$this->load->helper(array("string"));
				
				$password = random_string("alnum", 20);
				$password = md5(md5($password));
				
				$arr["twitter_id"] = $id;
				$arr["email"] = $email;
				$arr["password"] = $password;
				$arr["name"] = $name;
				if(!empty($gender)) $arr["gender"] = $gender;
				$new = OUser::add($arr);
				
				if($new)
				{
					$O = new OUser($new);
					
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy(str_replace("_normal", "", $profile_image_url), $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
					set_login_session($email,$password,"user");
				}
			}
			else {
				$row = $res->row();
				$twitter_login_count = $row->twitter_login_count + 1;
				$arr["twitter_login_count"] = $twitter_login_count;
				$arr["twitter_id"] = $id;
				//$arr["email"] = $email;
				$O = new OUser();
				$O->setup($row);
				$res = $O->edit($arr);
				if($res && empty($row->photo))
				{
					$this->load->helper(array("image","string"));
					$this->config->load('image');
					$imagesizes = $this->config->item('image_sizes');
					
					$uploaddir = FCPATH.'/_assets/images/temp/';
					$random = time()."_".random_string("alnum", 20);
					$newfile =  $random. ".jpg";
					$file = $uploaddir . $newfile;
					if (copy(str_replace("_normal", "", $profile_image_url), $file))
					{
						// resize the image to lower size
						$resize_newfile = "resize_".$newfile;
						resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
						$O->update_photo($newfile);
					}
				}
				extract(get_object_vars($O->row));
				set_login_session($email,$password,"user");
			}
			if($_GET['rd'] != "") redirect($_GET['rd']);
			else user_home();
			exit();
		}
	}
}

/* End of file account.php */
/* Location: ./application/controllers/account.php */