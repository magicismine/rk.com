<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lowongan extends CI_Controller {

	var $cu;
	public function __construct()
	{
		parent::__construct();	
		//error_reporting(-1);
		$this->vpath = "lowongan";
		$this->header = "header";
		$this->footer = "footer";
		$this->curpage = "lowongan";
		$this->cu = $cu = get_logged_in_user();
		$this->ca = $ca = get_current_admin();
		// PASTIKAN BAHWA LOWONGAN DI TAMPILKAN JIKA PERUSAHAAN DALAM KONDISI AKTIF
	}

	public function index()
	{
		$perpage = 0;
		$OMKat = $data['OMKat'] = new OMaster_kategori();
		$OLow = $data['OLow'] = new OLowongan();
		//$data['list'] = OLowongan::get_list($page,$perpage,"posisi ASC");
		$data['list'] = OLowongan::get_list_filter("",$page,$perpage,"lowongans.posisi ASC", "lowongans.active=1");

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/listing',$data);
		$this->load->view($this->footer,$data);
	}

	public function listing($param_1,$param_2,$param_3)
	{
		$perpage = 0;
		$type = $param_1;
		$url_title = $param_2;
		if($url_title == "other")
		{
			$data['table_title'] = humanize($type)." Others";

			/*
			$where_arr = array('users.status' => 'aktif');
			if($type == "kategori") $where_arr = array_merge($where_arr, array('lowongans.kategori_id' => 0));
			if($type == "bidang_usaha") $where_arr = array_merge($where_arr, array('lowongans.bidang_usaha_id' => 0));
			if($type == "lokasi") $where_arr = array_merge($where_arr, array('lowongans.lokasi_kabupaten_id' => 0));
			$this->db->select('SQL_CALC_FOUND_ROWS lowongans.*', FALSE)
					->from('lowongans')
					->join('perusahaans', 'perusahaans.id = lowongans.perusahaan_id', 'inner')
					->join('users', 'users.id = perusahaans.user_id', 'inner')
					->where($where_arr)
					->group_by('lowongans.id');
			$res = $this->db->get();
			$list = $res->result();
			*/
			if($type == "kategori") $filter_arr = array('lowongans.kategori_id' => 0);
			if($type == "bidang_usaha") $filter_arr = array('lowongans.bidang_usaha_id' => 0);
			if($type == "lokasi_propinsi") $filter_arr = array('lowongans.lokasi_propinsi_id' => 0);
			if($type == "lokasi_kabupaten") $filter_arr = array('lowongans.lokasi_kabupaten_id' => 0);
			if($type == "lokasi_kawasan") $filter_arr = array('lowongans.lokasi_kawasan_id' => 0);
			$list = OLowongan::get_list_front(0,0,"lowongans.id DESC",$filter_arr);
			//var_dump($this->db->last_query());
		}
		else
		{
			if($type == "kategori")
			{
				$OTmp= new OMaster_kategori($url_title,"url_title");
				$data['table_title'] = "Kategori ".$OTmp->get_nama();
				$filter_arr = array('kategori_id' => $OTmp->id);
				$data['page_title'] = "{$OTmp->row->nama}";
			}
			if($type == "bidang_usaha")
			{
				$OTmp= new OMaster_bidang_usaha($url_title,"url_title");
				$data['table_title'] = "Bidang Usaha ".$OTmp->get_nama();
				$filter_arr = array('bidang_usaha_id' => $OTmp->id);
				$data['page_title'] = "{$OTmp->row->nama}";
			}
			if($type == "lokasi_propinsi")
			{
				//$OTmp= new OMaster_lokasi_propinsi($url_title,"url_title");
				$OTmp= new OMaster_lokasi_propinsi($url_title);
				$data['table_title'] = "Lokasi Propinsi ".$OTmp->get_nama();
				$filter_arr = array('lokasi_propinsi_id' => $OTmp->id);
			}
			if($type == "lokasi_kabupaten")
			{
				$OTmp= new OMaster_lokasi_kabupaten($url_title,"url_title");
				$data['table_title'] = "Lokasi Kabupaten ".$OTmp->get_nama();
				$filter_arr = array('lokasi_kabupaten_id' => $OTmp->id);
				$data['page_title'] = "{$OTmp->row->nama}";
			}
			if($type == "lokasi_kawasan")
			{
				$OTmp= new OMaster_lokasi_kawasan($url_title,"url_title");
				$data['table_title'] = "Lokasi Kawasan ".$OTmp->get_nama();
				$filter_arr = array('lokasi_kawasan_id' => $OTmp->id);
			}

			$list = OLowongan::get_list_filter("",$page,$perpage,"lowongans.id DESC","lowongans.active=1",$filter_arr);
		}
		$data['list'] = $list;
		$data['total_list'] = get_db_total_rows();

		
		$data['page_description'] = "";
		$data['page_keyword'] = "";

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/listing',$data);
		$this->load->view($this->footer,$data);

		unset($OMKat,$OLow);
	}
	/*
	public function listing_kategori($url_title,$page = 0)
	{
		$perpage = 0;
		$OMKat = $data['OMKat'] = new OMaster_kategori($url_title,"url_title");
		$OLow = $data['OLow'] = new OLowongan();
		$data['list'] = OLowongan::get_list($page,$perpage,"id DESC","kategori_id = {$OMKat->id}");
		$data['total_list'] = get_db_total_rows();
		$data['table_title'] = "Kategori ".$OMKat->row->nama;

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/listing',$data);
		$this->load->view($this->footer,$data);

		unset($OMKat,$OLow);
	}

	public function listing_bidang_usaha($url_title,$page = 0)
	{
		$perpage = 0;
		$OMBU = $data['OMBU'] = new OMaster_bidang_usaha($url_title,"url_title");
		$OLow = $data['OLow'] = new OLowongan();
		$data['list'] = OLowongan::get_list($page,$perpage,"id DESC","bidang_usaha_id = {$OMBU->id}");
		$data['total_list'] = get_db_total_rows();
		$data['table_title'] = "Bidang Usaha ".$OMBU->row->nama;

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/listing',$data);
		$this->load->view($this->footer,$data);

		unset($OMKat,$OLow);
	}

	public function listing_lokasi($url_title,$page = 0)
	{
		$perpage = 0;
		$OMLKab = $data['OMLKab'] = new OMaster_lokasi_kabupaten($url_title,"url_title");
		$OLow = $data['OLow'] = new OLowongan();
		$data['list'] = OLowongan::get_list($page,$perpage,"id DESC","lokasi_kabupaten_id = {$OMLKab->id}");
		$data['total_list'] = get_db_total_rows();
		$data['table_title'] = "Lokasi ".$OMLKab->row->nama;

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/listing',$data);
		$this->load->view($this->footer,$data);

		unset($OMKat,$OLow);
	}
	*/

	public function detail($url_title_perusahaan,$url_title_lowongan)
	{	
		// check valid params
		if(empty($url_title_perusahaan) || empty($url_title_lowongan)){
			redirect("");
			exit;
		}
		$OP = $data['OP'] = new OPerusahaan($url_title_perusahaan,"url_title");
		$O = $data['O'] = new OLowongan($url_title_lowongan,"url_title");
		if(empty($O->id) || empty($OP->id)){
			redirect("");
			exit;
		}
		// check user permissions
		if(empty($this->ca->id)) :

			if(empty($this->cu->id)) {
				if(intval($O->row->active) <= 0) {
					redirect("");
					exit;
				}
			}
			else {

				if(intval($O->row->active) <= 0) :

					if($this->cu->role == "kandidat") {
						$OU = new OUser;
						$OU->setup($this->cu);
						$kandidat_row = $OU->get_info();
						$OK = new OKandidat;
						$OK->setup($kandidat_row);
						// check if lowongan is not active but has been applied is still see the lowongan
						if(!$OK->is_lowongan_lamar($O->id))
						{
							redirect("");
							exit;
						}
					}
					else {
						redirect("");
						exit;
					}

				endif;

			}

		endif;

		if($this->session->userdata("show_lowongan_".$O->id) == "")
		{
			$O->update_hits();
			$this->session->set_userdata("show_lowongan_".$O->id, $O->row->url_title);
		}

		$data['page_title'] = "{$O->row->posisi}";
		$data['page_description'] = "{$O->row->deskripsi}";
		$data['page_keyword'] = "";

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/detail',$data);
		$this->load->view($this->footer,$data);

		unset($OP,$O);
	}

}

/* End of file lowongan.php */
/* Location: ./application/controllers/lowongan.php */