<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perusahaan_promos extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //error_reporting(-1);
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/perusahaan/promo/";
        $this->curpage 	= "admin/perusahaan_promos";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        extract($_GET);
        $start 				= intval($_GET['page']);
        $perpage 			= 0;
        $data['orderby'] = $orderby;
        $data['order'] = $order;
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
        $where = "";
        $perusahaan_id = intval($perusahaan_id);
        $O = new OPerusahaan_promo;
        if($perusahaan_id > 0)
        {
            $where = 'perusahaan_id='.$perusahaan_id;
        }
        if($_GET['keyword'] != "")
        {
        	$data['list'] 	= $O->search($_GET['keyword'],$ordering,$where);
        }
        else
        {	
        	$data['list'] 	= $O->get_list($start, $perpage,$ordering,$where);
        }
        $data['uri'] 		= intval($start);
        $total 				= get_db_total_rows();
        $url 				= $this->curpage."/listing?".get_params($_GET,array('page'));
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'perusahaan_promos_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add()
    {
        extract($_GET);

        // setup validations
        if(sizeof($_POST) > 0)
        {
            extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            
            $perusahaan_id = intval($perusahaan_id);
            if($perusahaan_id > 0)
            {
                $arr['perusahaan_id'] = $perusahaan_id;
            }
            $O = new OPerusahaan_promo;
            $new = $O->add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
            redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'perusahaan_promos_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        extract($_GET);
        $curpage = $this->curpage."?".get_params($_GET);

        $O = new OPerusahaan_promo($id);
        
        if(!empty($id))
        {           
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage);
                exit();
            }
        }
        
        if(sizeof($_POST) > 0)
        {
            extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            
            $perusahaan_id = intval($perusahaan_id);
            if($perusahaan_id > 0)
            {
                $arr['perusahaan_id'] = $perusahaan_id;
            }
            $res = $O->edit($arr);
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'perusahaan_promos_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        extract($_GET);
        $curpage = $this->curpage."?".get_params($_GET);

        $O = new OPerusahaan_promo($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($curpage);
        exit();
    }

    public function set_active($active=0, $id)
    {   
        $data['curpage'] = $curpage = $this->curpage."?".get_params($_GET);

        $O = new OPerusahaan_promo($id);
        
        $arr['active'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($curpage);
        exit();
    }

    public function regenerate_urls()
    {
        $O = new OPerusahaan_promo;
        $list = $O->get_list();
        foreach ($list as $r)
        {
            $O->setup($r);
            $O->update_url_title();
            echo 'UPDATED!<br />';
        }
        unset($O);
        echo 'DONE';
    }
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
        $sorts = explode(",",$this->input->post('sorts'));
        $this->db->update('perusahaan_promo',array('ordering' => 0));
        $count = 1;
        foreach($sorts as $id)
        {
                $this->db->update('perusahaan_promo',array('ordering' => $count),array('id' => intval($id)));
                $count++;
        }
        die("DONE");
    }

    
        
        
}

/* End of file perusahaan_promos.php */
/* Location: ./application/controllers/admin/perusahaan_promos.php */