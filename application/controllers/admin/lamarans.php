<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lamarans extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/lamaran/";
        $this->curpage 	= "admin/lamarans";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
        $keyword = $_GET['keyword'];
        $get_params = get_params($_GET);

        if($keyword != "")
        {
        	$where = "nama LIKE '%{$keyword}%' OR posisi LIKE '%{$keyword}%' OR nama_depan LIKE '%{$keyword}%' OR nama_akhir LIKE '%{$keyword}%'";
        	$data['keyword']= $keyword;
        	$data['page']  	= intval($start);
        	$data['list'] 	= OLowongan::get_all_pelamar(0,0,$ordering,$where);
        	//var_dump($hehe); die();
        }
        else
        {
        	$data['list'] 	= OLowongan::get_all_pelamar($start, $perpage,$ordering);
        }
        //var_dump($this->db->last_query());
        $data['uri'] 		= intval($start);
        $data['total']      = $total = get_db_total_rows();
        $url 				= $this->curpage."/listing?".$get_params;
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'lamaran_list', $data);
        $this->load->view($this->ffoot, $data);
    }

}

/* End of file lamarans.php */
/* Location: ./application/controllers/admin/lamarans.php */