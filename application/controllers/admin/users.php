<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/user/";
        $this->curpage 	= "admin/users";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start              = intval($_GET['page']);
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];
        extract($_GET);
        $getparams = get_params($_GET);
        if($perpage != "")
        {
            $perpage = $data['perpage']   = $perpage;    
        }else
        {
            $perpage = $data['perpage']   = 25;    
        }

        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
            $ordering = "{$data['orderby']} {$data['order']}";
        }
        if($_GET['role'] == "kandidat" || $_GET['role'] == "perusahaan")
        {
            if($role != "")
            {
                $arrs[] = "role = '$role'";    
            }
            if($keyword != "")
            {
                $arrs[] = "email LIKE '%{$keyword}%'";
            }

            if(sizeof($_GET) > 0 )
            {
                $add_arrs = implode(' AND ',$arrs);    
            }
            
            
            //$where = "role = '{$role}'";
            $data['list']   = OUser::get_list($start, $perpage,$ordering,$add_arrs);
            
        }else
        {
            if($_GET['keyword'] != "")
            {
                $data['list']   = OUser::search($_GET['keyword'],$ordering,$role);
            }
            else
            {
                
                $data['list']   = OUser::get_list($start, $perpage,$ordering);
            }
        }
        //var_dump($this->db->last_query());

        $data['uri']        = intval($start);
        $data['total']      = $total = get_db_total_rows();
        $url                = $this->curpage."/listing?{$getparams}";
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add()
    {
        show_404();
        exit;
    	// setup validations
        $this->form_validation->set_rules('email', 'Username', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_message('is_unique',"This email has been used. Please use another email.");
        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run() == TRUE):

			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title","password");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            
			$arr['password'] = md5(md5($password));
            $new = OUser::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
			redirect($this->curpage."/listing?role=".$role);
            exit();

        endif;

        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        $O = new OUser($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($this->curpage."?".get_params($_GET));
                exit();
            }
        }
        
        $this->form_validation->set_rules('email', 'Username', 'trim|required|valid_email|callback_email_check['.$O->id.']');
        //$this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters('', '<br />');
        

        if ($this->form_validation->run() == TRUE):

			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title","password");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            if(!empty($password))
			{
				$arr['password'] = md5(md5($password));
			}
            $res = $O->edit($arr);
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage."?".get_params($_GET));
            exit();
        
        endif;

        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        $O = new OUser($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage."?".get_params($_GET));
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
		$sorts = explode(",",$this->input->post('sorts'));
		$this->db->update('users',array('ordering' => 0));
		$count = 1;
		foreach($sorts as $id)
		{
				$this->db->update('users',array('ordering' => $count),array('id' => intval($id)));
				$count++;
		}
		die("DONE");
    }    

    public function set_status($new_status="pasif",$id){
        $page = $_GET['page'];
        $O = new OUser($id);
        //$O->edit(array('status' => "pasif"));

        $res = $O->edit(array('status' => $new_status));
        if($res) warning("edit", "success");
        redirect($this->curpage."?".urldecode(get_params($_GET)));
        exit();
    }

    public function email_check($str,$id)
    {
        $check = $this->db->get_where('users', array('email' => $str, 'id !=' => $id) );
        if (!emptyres($check))
        {
            $this->form_validation->set_message('email_check', 'This email has been used. Please use another email.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }
        
/*
public function manage_carts($id,$a = "list",$target_id = "")
{
	$data['curid'] = $id;
	$data['target_id'] = $target_id;
    if($a == "list")
    {
        $q = "SELECT * FROM carts WHERE user_id = ? ORDER BY id ASC";
        $res = $this->db->query($q,array($id));
        $data['list'] = $res->result();
        $data['curpage'] = $this->curpage."/manage_carts/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_cart_list', $data);
        $this->load->view($this->ffoot, $data);
        
    }
    if($a == "add")
    {
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
            $arr['user_id'] = $id;
            $new = OCart::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
            redirect($this->curpage."/manage_carts/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_cart_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "edit")
    {
        $data['O'] = $O = new OCart($target_id);
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
           
            $res = $O->edit($arr);
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage."/manage_carts/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $data['row'] = $O->row;
        $data['curpage'] = $this->curpage."/manage_carts/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_cart_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "delete")
    {
        $O = new OCart($id);
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage."/manage_carts/".$id);
        exit();
    }
}

public function manage_orders($id,$a = "list",$target_id = "")
{
	$data['curid'] = $id;
	$data['target_id'] = $target_id;
    if($a == "list")
    {
        $q = "SELECT * FROM orders WHERE user_id = ? ORDER BY id ASC";
        $res = $this->db->query($q,array($id));
        $data['list'] = $res->result();
        $data['curpage'] = $this->curpage."/manage_orders/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_order_list', $data);
        $this->load->view($this->ffoot, $data);
        
    }
    if($a == "add")
    {
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
            $arr['user_id'] = $id;
            $new = OOrder::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
            redirect($this->curpage."/manage_orders/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_order_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "edit")
    {
        $data['O'] = $O = new OOrder($target_id);
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
           
            $res = $O->edit($arr);
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage."/manage_orders/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $data['row'] = $O->row;
        $data['curpage'] = $this->curpage."/manage_orders/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_order_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "delete")
    {
        $O = new OOrder($id);
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage."/manage_orders/".$id);
        exit();
    }
}

public function manage_challenge_comments($id,$a = "list",$target_id = "")
{
	$data['curid'] = $id;
	$data['target_id'] = $target_id;
    if($a == "list")
    {
        $q = "SELECT * FROM challenge_comments WHERE user_id = ? ORDER BY id ASC";
        $res = $this->db->query($q,array($id));
        $data['list'] = $res->result();
        $data['curpage'] = $this->curpage."/manage_challenge_comments/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_challenge_comment_list', $data);
        $this->load->view($this->ffoot, $data);
        
    }
    if($a == "add")
    {
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
            $arr['user_id'] = $id;
            $new = OChallenge_comment::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
            redirect($this->curpage."/manage_challenge_comments/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_challenge_comment_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "edit")
    {
        $data['O'] = $O = new OChallenge_comment($target_id);
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
           
            $res = $O->edit($arr);
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage."/manage_challenge_comments/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $data['row'] = $O->row;
        $data['curpage'] = $this->curpage."/manage_challenge_comments/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_challenge_comment_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "delete")
    {
        $O = new OChallenge_comment($id);
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage."/manage_challenge_comments/".$id);
        exit();
    }
}

public function manage_challenges($id,$a = "list",$target_id = "")
{
	$data['curid'] = $id;
	$data['target_id'] = $target_id;
    if($a == "list")
    {
        $q = "SELECT * FROM challenges WHERE user_id = ? ORDER BY id ASC";
        $res = $this->db->query($q,array($id));
        $data['list'] = $res->result();
        $data['curpage'] = $this->curpage."/manage_challenges/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_challenge_list', $data);
        $this->load->view($this->ffoot, $data);
        
    }
    if($a == "add")
    {
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
            $arr['user_id'] = $id;
            $new = OChallenge::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
            redirect($this->curpage."/manage_challenges/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_challenge_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "edit")
    {
        $data['O'] = $O = new OChallenge($target_id);
        if(sizeof($_POST) > 0)
        {
            $arr = $_POST;
            unset($arr['submit']);   
           
            $res = $O->edit($arr);
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage."/manage_challenges/".$id);
            exit();
            
        }
        $data['user_id'] = $id;
        $data['row'] = $O->row;
        $data['curpage'] = $this->curpage."/manage_challenges/";
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'users_manage_challenge_form', $data);
        $this->load->view($this->ffoot, $data);
    }
    if($a == "delete")
    {
        $O = new OChallenge($id);
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage."/manage_challenges/".$id);
        exit();
    }
}
*/
}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */