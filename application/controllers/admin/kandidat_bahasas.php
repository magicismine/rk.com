<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kandidat_bahasas extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/kandidat/bahasa/";
        $this->curpage 	= "admin/kandidat_bahasas";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
        if($_GET['keyword'] != "")
        {
        	$data['list'] 	= OKandidat_bahasa::search($_GET['keyword'],$ordering);
        }
        else
        {
        	
        	$data['list'] 	= OKandidat_bahasa::get_list($start, $perpage,$ordering);
        }
        $data['uri'] 		= intval($start);
        $total 				= get_db_total_rows();
        $url 				= $this->curpage."/listing?";
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidat_bahasas_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add()
    {
    	// setup validations
        if(sizeof($_POST) > 0)
        {
		extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            
            $new = OKandidat_bahasa::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
                        redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidat_bahasas_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        $O = new OKandidat_bahasa($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($this->curpage);
                exit();
            }
        }
        
        if(sizeof($_POST) > 0)
        {
		extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            
            $res = $O->edit($arr);
                        if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidat_bahasas_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        $O = new OKandidat_bahasa($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage);
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
            $sorts = explode(",",$this->input->post('sorts'));
            $this->db->update('kandidat_bahasas',array('ordering' => 0));
            $count = 1;
            foreach($sorts as $id)
            {
                    $this->db->update('kandidat_bahasas',array('ordering' => $count),array('id' => intval($id)));
                    $count++;
            }
            die("DONE");
    }

    
        
        
}

/* End of file kandidat_bahasas.php */
/* Location: ./application/controllers/admin/kandidat_bahasas.php */