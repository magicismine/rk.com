<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller 
{
	var $head = "admin/header";
	var $foot = "admin/footer";
	var $curpage = "admin/home";
	var $admin;
	
	public function __construct()
	{
		parent::__construct();
		//error_reporting(-1);
		$this->admin = get_current_admin();
	}
	
	public function index()
	{
		if(!$this->admin) $this->login();
		else $this->main();
	}
	
	public function main()
	{
		if(!$this->admin) admin_login();
		$data['home_nav'] = "active";
		
		$this->load->view($this->head, $data);
		$this->load->view('admin/home');
		$this->load->view($this->foot);
	}
	
	public function login()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
		$this->form_validation->set_error_delimiters('', '<br />');
		
		if ($this->form_validation->run() == TRUE)
		{
			extract($_POST);
			$admin_res = get_admin($username, md5($password));
			if($admin_res == FALSE) $this->session->set_flashdata("warning", "Invalid account.");
			else set_login_session($username, md5($password), "admin");
			redirect($this->curpage); 
			exit();
		}
		$this->load->view('admin/header_login', $data);
		$this->load->view('admin/login_form');
		$this->load->view('admin/footer_login');
	}
	
	public function changepassword()
	{
		if(!$this->admin) $this->login();
		$this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
		$this->form_validation->set_error_delimiters('', '<br />');
		
		if ($this->form_validation->run() == TRUE)
		{
			extract($_POST);
			$this->db->update('admins',array('password' => md5($password)),array('id' => $this->admin->id));
			$admin_res = get_admin($username, md5($password));
			$this->session->set_flashdata("warning", "Password Updated.");
			set_login_session($this->admin->username, md5($password));
			redirect("admin/home/changepassword"); 
			exit();
		}
		$this->load->view($this->head, $data);
		$this->load->view('admin/change_password');
		$this->load->view($this->foot);
	}
	
	public function logout()
	{
		unset_login_session();
		redirect($this->curpage); 
		exit();
	}

	public function test($a = "image_upload")
	{
		if(!$this->admin) $this->login();
		if(sizeof($_POST) > 0)
		{
			foreach($_POST['image'] as $filename)
			{
				
				$files = auto_resize_photo($filename);
				echo "<p>Image <strong>{$filename}</strong> successfully uploaded and created:<br />";
				echo @implode("<br />",$files)."</p>";
			}
			die("UPLOAD PHOTOS DONE!!");
		}
		$this->load->view($this->head, $data);
		$this->load->view('admin/test_image_upload');
		$this->load->view($this->foot);
	}
	
	public function delete_upload_dir($dir="")
	{
		if (! is_dir($dir))
		{
        return FALSE;
    }
    $phpver = phpversion();
    if(version_compare($phpver, '5.2') <= 0)
    {
			if (substr($dir, strlen($dir) - 1, 1) != '/') {
	        $dir .= '/';
	    }
	    $files = glob($dir . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_dir($file)) {
	            $this->delete_upload_dir($file);
	        } else {
	            unlink($file);
	        }
	    }
	    rmdir($dir);

    } else {

			$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
			$files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
			foreach($files as $file)
			{
			    if ($file->getFilename() === '.' || $file->getFilename() === '..'){
			        continue;
			    }
			    if ($file->isDir()) {
			        rmdir($file->getRealPath());
			    } else {
			        unlink($file->getRealPath());
			    }
			}
			rmdir($dir);
		}
	}

	public function regenerate_thumbnails()
	{
		if(!$this->admin) admin_login();
		$this->delete_upload_dir(FCPATH."/_assets/images/uploads");
		if ($handle = opendir(FCPATH."/_assets/images/temp/")) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != ".." && !stristr($entry,"resize_")) {
					echo "Resizing $entry ... ";
					auto_resize_photo($entry);
					echo "DONE<br />";
				}
			}
			closedir($handle);
		}
	}
	
	public function cleanup_images()
	{ 
		if(!$this->admin) admin_login();
		$q = "SHOW TABLES;";
		$res = $this->db->query($q);
		$values = array_values($res->result_array());
		$images = array();
		foreach($values as $row)
		{
			$tmp = array_values($row);
			$table = $tmp[0];
			$fields = $this->db->list_fields($table);
			if( in_array("photo", $fields) || in_array("foto", $fields) || in_array("logo", $fields))
			{
				$q = "SELECT * FROM {$table}";
				$res = $this->db->query($q);
				if(!emptyres($res))
				{
					foreach($res->result() as $row)
					{
						if(!empty($row->photo)) $images[] = $row->photo;
						if(!empty($row->foto)) $images[] = $row->foto;
						if(!empty($row->logo)) $images[] = $row->logo;
					}
				}
			}
		}
		//var_dump($in_arr,$images); die();
		$this->load->helper('directory');
		$map = directory_map(FCPATH."_assets/images/temp/");
		foreach($map as $file)
		{
			if(empty($file)) continue;
			if(!in_array($file,$images)) 
			{
				
				echo "DELETE FILE: ".$file."<br />";
				auto_delete_photo($file);
			}
			else
			{
				echo "NO DELETE FILE: ".$file."<br />";
			}
		}
		echo "DONE";
		/*
		foreach($images as $image)
		{
			auto_delete_photo($image);
		}
		*/
		
	}

	public function regenerate_lowongan_kodes()
	{
		$list = OLowongan::get_list(0,0,"id ASC","");
		$O = new OLowongan;
		foreach ($list as $r)
		{
			$O->setup($r);
			$kode = str_pad($r->id, 6, "0", STR_PAD_LEFT);
			$O->edit(array('kode' => $kode));
		}
		unset($O);
		echo "DONE";
	}

	public function regenerate_lowongans()
	{
		$list = OLowongan::get_list(0,0,"id ASC","");
		$O = new OLowongan;
		$i=1;
		foreach ($list as $r)
		{
			$O->setup($r);
			$dt_added = date("Y-m-d", strtotime($r->dt_added));

			$array = array('id !=' => $r->id, 'judul' => $r->judul, 'deskripsi ' => $r->deskripsi);
			$check = $this->db->like('dt_added', $dt_added, 'after')->where($array)->get("lowongans");
			if(!emptyres($check))
			{
				echo "{$i}. Delete Lowongan<br />";
				$O->delete();
			}
			$i++;
		}
		unset($O);
		echo "DONE";
	}

	public function set_auto_lamars()
	{
		$q = "SELECT * FROM kandidat_notif_queues WHERE sent_flag = 0 LIMIT 0,500";
		$res = $this->db->query($q);
		if(!emptyres($res))
		{
			$result = $res->result();

			foreach ($result as $r)
			{
				$kandidat_obj = json_decode($r->kandidat_data);
				$lowongan_obj = json_decode($r->lowongan_data);
				//var_dump($lowongan_obj, $lowongan_obj->id);
				//die();
				$OK = new OKandidat($r->kandidat_id);
				$OU = new OUser($kandidat_obj->user_id);
				$OL = new OLowongan($r->lowongan_id);
				$OP = new OPerusahaan($lowongan_obj->perusahaan_id);
				
				// set lamar
				$OK->set_lowongan_lamar($r->lowongan_id);
				
				// send an email notif to kandidat
				$to = $OU->row->email;
				$subject = "AUTO LAMAR NOTIF: Anda telah melamar otomatis lowongan ".$OL->get_nama()." on ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
				$content = $this->load->view('email/auto_lamar/kandidat', array('OL' => $OL, 'OP' => $OP), TRUE);
				if(noreply_mail($to,$subject,$content))
				{
					// update kandidat_notif_queues: set sent_flag and set sent_dt
					echo "<p>SENT to KANDIDAT: ".$OK->get_nama()." (".$OU->row->email.")</p>";
				}
				else echo "<p><strong>FAILED</strong> SENT to KANDIDAT: ".$OK->get_nama()." (".$OU->row->email.")</p>";

				// send an email notif to perusahaan
				$to = $OP->row->email;
				$subject = "KANDIDAT LAMAR NOTIF: ".$OK->get_nama()." telah melamar otomatis lowongan ".$OL->get_nama()." ".parse_date(date("Y-m-d"))." | ".DOMAIN_NAME;
				$content = $this->load->view('email/auto_lamar/perusahaan', array('OK' => $OK), TRUE);
				if(noreply_mail($to,$subject,$content))
				{
					// update kandidat_notif_queues: set sent_flag and set sent_dt
					echo "<p>SENT to PERUSAHAAN: ".$OP->get_nama()." (".$OP->row->email.")</p>";
				}
				else echo "<p><strong>FAILED</strong> SENT to PERUSAHAAN: ".$OP->get_nama()." (".$OP->row->email.")</p>";
				unset($OK,$OU,$OL,$OP);
				
				// set sent_flag and sent_date
				$this->db->update('kandidat_notif_queues',
							array('sent_flag'=>1,
									'sent_date'=>date('Y-m-d H:i:s')
									),
							array('id' => $r->id)
							);
				die('DONE');
			}
		}
		else die('No Auto Lamar Queue Match!!');
	}

	public function generate_auto_lamars()
	{
		// check the match of today
		$where_arr = array();
		$q = "SELECT kandidat_auto_lamars.* FROM kandidat_auto_lamars
					INNER JOIN kandidats
						ON kandidats.id = kandidat_auto_lamars.kandidat_id
					WHERE (kandidat_auto_lamars.dt_added BETWEEN DATE_SUB(CURDATE(), INTERVAL 3 DAY) AND NOW())
						AND kandidat_auto_lamars.email_alert_active_flag = 1
					";
		$res = $this->db->query($q,$where_arr);
		if(!emptyres($res))
		{
			$res = $res->result();
			foreach($res as $r)
			{
				$add_sql = "";
				$add_sql_arr = $where_arr = NULL;
				// fitler from kandidat_auto_lamars
				/*
				waktu_kerja
				status_kerja
				kategori
				sub_kategori/jabatan
				lokasi
				*/
				if(!empty($r->waktu_kerja_ids))
				{
					$add_sql_arr[] = "lowongans.waktu_kerja_id IN(".$r->waktu_kerja_ids.")";
				}
				/*if(!empty($r->status_kerja_ids))
				{
					$add_sql_arr[] = "lowongans.status_kerja_id IN(".$r->status_kerja_ids.")";
				}*/
				if(!empty($r->kategori_ids))
				{
					$add_sql_arr[] = "lowongans.kategori_id IN(".$r->kategori_ids.")";
				}
				if(!empty($r->lokasi_ids))
				{
					$add_sql_arr[] = "lowongans.lokasi_propinsi_id IN(".$r->lokasi_ids.")";
				}
				
				// filter from kandidats
				$OK = new OKandidat($r->kandidat_id);
				/*
				level_karir
				umur
				gender
				pendidikan
				*/
				if(!empty($OK->row->level_karir_id))
				{
					$add_sql_arr[] = "lowongans.level_karir_id = ?";
					$where_arr[] = intval($OK->row->level_karir_id);
				}
				$kandidat_umur = $OK->get_umur();
				if(intval($kandidat_umur) > 0)
				{
					$add_sql_arr[] = "(? BETWEEN lowongans.umur_mulai AND lowongans.umur_sampai)";
					$where_arr[] = intval($kandidat_umur);
				}
				if(!empty($OK->row->jenis_kelamin_id))
				{
					$add_sql_arr[] = "lowongans.jenis_kelamin_id = ?";
					$where_arr[] = intval($OK->row->jenis_kelamin_id);
				}
				$pendidikan = $OK->get_pendidikan();
				if($pendidikan)
				{
					$pendidikan_arr = NULL;
					foreach($pendidikan as $r)
					{
						$pendidikan_arr[] = $r->pendidikan_id;
					}
					$add_sql_arr[] = "lowongans.pendidikan_id IN(".implode(",",$pendidikan_arr).")";
				}

				// generate add sql
				if(count($add_sql_arr) > 0)
				{
					$add_sql = " AND ".implode(" AND ", $add_sql_arr);
				}
				// check if the lowongans is not expired and match with the filters
				$q = "SELECT SQL_CALC_FOUND_ROWS lowongans.* FROM lowongans
						WHERE lowongans.dt_expired >= NOW()
						{$add_sql}
						";
				$low_res = $this->db->query($q,$where_arr);
				//var_dump($this->db->last_query(), get_db_total_rows());
				if(!emptyres($low_res))
				{
					$low_res = $low_res->result();
					foreach($low_res as $low_r)
					{
						// check if the kandidat has been apply lowongan and skip it
						if($OK->check_lowongan_lamar($low_r->id))
						{
							echo " SKIP ";
							continue;
						}

						//die('Hi');
						$kandidat_data = (array) $OK->row;
						$lowongan_data = (array) $low_r;
						$arr = array(
									'kandidat_id'=> intval($r->kandidat_id),
									'lowongan_id'=> intval($low_r->id),
									'kandidat_data'=> json_encode($kandidat_data),
									'lowongan_data'=> json_encode($lowongan_data)
									);
						// check kandidat_notif_queues exist of today
						$q = "SELECT * FROM kandidat_notif_queues
								WHERE sent_flag = 0
								AND (dt_added BETWEEN DATE_SUB(CURDATE(), INTERVAL 3 DAY) AND NOW())
								AND kandidat_id = ?
								AND lowongan_id = ?
								";
						$where_arr = array(intval($r->kandidat_id), intval($low_r->id));
						$check = $this->db->query($q, $where_arr);
						if(emptyres($check))
						{
							// add new data
							$this->db->insert('kandidat_notif_queues',$arr);
							echo "New Inserted!";
						}
						else 
						{
							// edit data
							$this->db->update('kandidat_notif_queues',$arr,array('id' => $check->row()->id));
							echo "Updated!";
						}
					}
				}
				else die('No Lowongan Match!');
				unset($OK);
			}
			//endforeach
		}
		else
		{
			die('No Auto Lamar Match!');
		}
	}

}

/* End of file home.php */
/* Location: ./application/controllers/admin/home.php */