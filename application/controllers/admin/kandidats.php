<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kandidats extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/kandidat/";
        $this->curpage 	= "admin/kandidats";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "id DESC";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
        if($_GET['keyword'] != "")
        {
        	$data['list'] 	= OKandidat::search($_GET['keyword'],$ordering);
        }
        else
        {
        	
        	$data['list'] 	= OKandidat::get_list($start, $perpage,$ordering);
        }
        //var_dump($this->db->last_query());

        $data['uri'] 		= intval($start);
        $data['total']      = $total = get_db_total_rows();
        $url 				= $this->curpage."/listing?";
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidats_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add()
    {
    	redirect($this->curpage); exit;
    	// setup validations
      if(sizeof($_POST) > 0)
      {
				extract($_POST);
            
        $arr = NULL;
        $exclude_arr = array("submit","url_title");
				$kesehatan_arr['kacamata_flag'] = 0;
        
        foreach($_POST as $key => $val)
        {
          if(!in_array($key, $exclude_arr) && !is_array($val))
          {	
            if(trim($val) != "")
						{
							if(substr($key,0,10) == "kesehatan_")
							{
								$key = str_replace("kesehatan_","",$key);
								$kesehatan_arr[$key] = $val;							
							}
							else
							{
								$arr[$key] = trim($val);
							}
						}
          }
        }
            
				$arr['tanggal_lahir'] = get_date_lang($tanggal_lahir,"EN");
				$new = OKandidat::add($arr);
				if($new)
				{
					$O = new OKandidat($new);
					// update lokasi skrg
					if($this->input->post('skrg_asal_flag') == 1)
					{
						$arr = NULL;
						$arr['skrg_alamat'] = $O->row->asal_alamat;
						$arr['skrg_rt'] = $O->row->asal_rt;
						$arr['skrg_rw'] = $O->row->asal_rw;
						//$arr['skrg_negara_id'] = $O->row->asal_negara_id;
						$arr['skrg_propinsi_id'] = $O->row->asal_propinsi_id;
						$arr['skrg_kabupaten_id'] = $O->row->asal_kabupaten_id;
						$arr['skrg_kecamatan'] = $O->row->asal_kecamatan;
						$arr['skrg_kelurahan'] = $O->row->asal_kelurahan;
						$arr['skrg_kawasan_id'] = $O->row->asal_kawasan_id;
						$O->edit($arr);
					}
					// update photo
					foreach($_POST['image'] as $image)
					{
						$O->update_photo($image);
					}
					// update url title
					$O->update_url_title();
					// update kesehatan
					$O->set_kesehatan($kesehatan_arr);
					// update keluarga
					foreach($keluarga_nama as $key => $val)
					{
						if(empty($val)) continue;
						$keluarga_arr = array();
						$keluarga_arr['nama'] = $keluarga_nama[$key];
						$keluarga_arr['jenis_kelamin_id'] = $keluarga_jenis_kelamin_id[$key];
						$keluarga_arr['shdk_id'] = $keluarga_shdk_id[$key];
						$keluarga_arr['pendidikan_id'] = $keluarga_pendidikan_id[$key];
						$O->set_keluarga($keluarga_arr);
					}
					// update pendidikan
					foreach($pendidikan_nama as $key => $val)
					{
						if(empty($val)) continue;
						$pendidikan_arr = array();
						$pendidikan_arr['pendidikan_id'] = $pendidikan_id[$key];
						$pendidikan_arr['tahun_awal'] = $pendidikan_tahun_awal[$key];
						$pendidikan_arr['tahun_akhir'] = $pendidikan_tahun_akhir[$key];
						$pendidikan_arr['nama'] = $pendidikan_nama[$key];
						$pendidikan_arr['jurusan'] = $pendidikan_jurusan[$key];
						$pendidikan_arr['nilai'] = $pendidikan_nilai[$key];
						$O->set_pendidikan($pendidikan_arr);
					}
					// update pengalaman kerja
					foreach($pengalaman_perusahaan as $key => $val)
					{
						if(empty($val)) continue;
						$pengalaman_arr = array();
						if(strtotime($pengalaman_tanggal_awal[$key]) > strtotime($pengalaman_tanggal_akhir[$key]))
						{
							$tmp_awal = $pengalaman_tanggal_awal[$key];
							$tmp_akhir = $pengalaman_tanggal_akhir[$key];
							$pengalaman_tanggal_awal[$key] = $tmp_akhir;
							$pengalaman_tanggal_akhir[$key] = $tmp_awal;
						}
						$pengalaman_arr['tanggal_awal'] = get_date_lang($pengalaman_tanggal_awal[$key],"EN");
						$pengalaman_arr['tanggal_akhir'] = get_date_lang($pengalaman_tanggal_akhir[$key],"EN");
						$pengalaman_arr['perusahaan'] = $pengalaman_perusahaan[$key];
						$pengalaman_arr['bidang_usaha_id'] = $pengalaman_bidang_usaha_id[$key];
						$pengalaman_arr['jabatan'] = $pengalaman_jabatan[$key];
						$O->set_pengalaman($pengalaman_arr);
					}
					// update keterampilan
					foreach($keterampilan_kategori_id as $key => $val)
					{
						if(empty($val)) continue;
						$keterampilan_arr = array();
						$keterampilan_arr['kategori_id'] = $keterampilan_kategori_id[$key];
						$keterampilan_arr['nilai_skala'] = $keterampilan_nilai_skala[$key];
						$keterampilan_arr['sertifikasi'] = $keterampilan_sertifikasi[$key];
						$O->set_keterampilan($keterampilan_arr);
					}
					// update bahasa
					foreach($bahasa_id as $key => $val)
					{
						if(empty($val)) continue;
						$bahasa_arr = array();
						$bahasa_arr['bahasa_id'] = $bahasa_id[$key];
						$bahasa_arr['nilai_skala'] = $bahasa_nilai_skala[$key];
						$bahasa_arr['sertifikasi'] = $bahasa_sertifikasi[$key];
						$O->set_bahasa($bahasa_arr);
					}
					// update dokumens
					$O->set_dokumens($_FILES['dokumen'],$dokumen_aktif_flag);
					/*
					$allow_exts = array("docx","doc","pdf","rtf");
					foreach($_FILES['dokumen']['error'] as $key => $val)
					{
						if($_FILES["dokumen"]["name"][$key] == "" ||
							$value != UPLOAD_ERR_OK ||
							$_FILES["dokumen"]["size"][$key] > 1048576
						) continue;
						
						$filename = $_FILES["dokumen"]["name"][$key];
						$ext = end(explode(".",$filename));
						if(!in_array($ext,$allow_exts)) continue;
						
						$filename_arr = explode(".",$filename,2);
						$random = url_title($filename_arr[0], '-', TRUE)."_".time()."_".random_string("alnum", 6);
						$file = $random.".".$ext;
						$upload_dir = FCPATH.'/_assets/files/';
						if(!is_dir($upload_dir)) mkdir($upload_dir, 0777, true);
						
						if (move_uploaded_file($_FILES['dokumen']['tmp_name'][$key], $upload_dir.$file))
						{
							$dokumen_arr = array();
							$dokumen_arr['dokumen'] = $file;
							$O->set_dokumen($dokumen_arr);
						}
					}
					*/
					
					warning("add", "success");
				}
				else warning("add", "fail");
            	
				redirect($this->curpage);
				exit();
      }
      $data['cu'] = $this->cu;
      $this->load->view($this->fhead, $data);
      $this->load->view($this->fpath.'kandidats_form', $data);
      $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        $O = new OKandidat($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($this->curpage);
                exit();
            }
        }
        
        if(sizeof($_POST) > 0)
        {
			/*echo "<pre>";
			print_r($_POST);
			echo "</pre>";*/
			//die();
			
			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title","");
			$kesehatan_arr['kacamata_flag'] = 0;
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(substr($key,0,10) == "kesehatan_")
					{
						$key = str_replace("kesehatan_","",$key);
						$kesehatan_arr[$key] = $val;
					}
					else
					{
						$arr[$key] = trim($val);
					}
                }
            }
            $arr['tanggal_lahir'] = get_date_lang($tanggal_lahir,"EN");
            // update lokasi skrg
			if($this->input->post('skrg_asal_flag') == 1)
            {
                $arr['skrg_alamat'] = $arr['asal_alamat'];
                $arr['skrg_rt'] = $arr['asal_rt'];
                $arr['skrg_rw'] = $arr['asal_rw'];
                //$arr['skrg_negara_id'] = $arr['asal_negara_id'];
                $arr['skrg_propinsi_id'] = $arr['asal_propinsi_id'];
                $arr['skrg_kabupaten_id'] = $arr['asal_kabupaten_id'];
                $arr['skrg_kecamatan'] = $arr['asal_kecamatan'];
                $arr['skrg_kelurahan'] = $arr['asal_kelurahan'];
                $arr['skrg_kawasan_id'] = $arr['asal_kawasan_id'];
            }
            $res = $O->edit($arr);
			// update photo
			foreach($_POST['image'] as $image)
			{
				$O->update_photo($image);
			}
			// update url title
            $O->refresh();
            $O->update_url_title();
			
			/*** ADD PROGRESS ***/
			// update kesehatan
			$O->edit_kesehatan($kesehatan_arr);
			// update keluarga
			foreach($keluarga_nama as $key => $val)
			{
				if(empty($val)) continue;
				$keluarga_arr = array();
				$keluarga_arr['nama'] = $keluarga_nama[$key];
				$keluarga_arr['jenis_kelamin_id'] = $keluarga_jenis_kelamin_id[$key];
				$keluarga_arr['shdk_id'] = $keluarga_shdk_id[$key];
				$keluarga_arr['pendidikan_id'] = $keluarga_pendidikan_id[$key];
				$O->set_keluarga($keluarga_arr);
			}
			// update pendidikan
			foreach($pendidikan_nama as $key => $val)
			{
				if(empty($val)) continue;
				$pendidikan_arr = array();
				$pendidikan_arr['pendidikan_id'] = $pendidikan_id[$key];
				$pendidikan_arr['tahun_awal'] = $pendidikan_tahun_awal[$key];
				$pendidikan_arr['tahun_akhir'] = $pendidikan_tahun_akhir[$key];
				$pendidikan_arr['nama'] = $pendidikan_nama[$key];
				$pendidikan_arr['jurusan'] = $pendidikan_jurusan[$key];
				$pendidikan_arr['nilai'] = $pendidikan_nilai[$key];
				$O->set_pendidikan($pendidikan_arr);
			}
			// update pengalaman kerja
			foreach($pengalaman_perusahaan as $key => $val)
			{
				if(empty($val)) continue;
				$pengalaman_arr = array();
				if(strtotime($pengalaman_tanggal_awal[$key]) > strtotime($pengalaman_tanggal_akhir[$key]))
				{
					$tmp_awal = $pengalaman_tanggal_awal[$key];
					$tmp_akhir = $pengalaman_tanggal_akhir[$key];
					$pengalaman_tanggal_awal[$key] = $tmp_akhir;
					$pengalaman_tanggal_akhir[$key] = $tmp_awal;
				}
				$pengalaman_arr['tanggal_awal'] = get_date_lang($pengalaman_tanggal_awal[$key],"EN");
				$pengalaman_arr['tanggal_akhir'] = get_date_lang($pengalaman_tanggal_akhir[$key],"EN");
				$pengalaman_arr['perusahaan'] = $pengalaman_perusahaan[$key];
				$pengalaman_arr['bidang_usaha_id'] = $pengalaman_bidang_usaha_id[$key];
				$pengalaman_arr['jabatan'] = $pengalaman_jabatan[$key];
				$O->set_pengalaman($pengalaman_arr);
			}
			// update keterampilan
			foreach($keterampilan_kategori_id as $key => $val)
			{
				if(empty($val)) continue;
				$keterampilan_arr = array();
				$keterampilan_arr['kategori_id'] = $keterampilan_kategori_id[$key];
				$keterampilan_arr['nilai_skala'] = $keterampilan_nilai_skala[$key];
				$keterampilan_arr['sertifikasi'] = $keterampilan_sertifikasi[$key];
				$O->set_keterampilan($keterampilan_arr);
			}
			// update bahasa
			foreach($bahasa_id as $key => $val)
			{
				if(empty($val)) continue;
				$bahasa_arr = array();
				$bahasa_arr['bahasa_id'] = $bahasa_id[$key];
				$bahasa_arr['nilai_skala'] = $bahasa_nilai_skala[$key];
				$bahasa_arr['sertifikasi'] = $bahasa_sertifikasi[$key];
				$O->set_bahasa($bahasa_arr);
			}
			// update dokumens
			$O->set_dokumens($_FILES['dokumen'],$dokumen_aktif_flag);

			/*** EDIT PROGRESS ***/
			// edit keluarga
			foreach($edit_keluarga_nama as $key => $val)
			{
				if($edit_keluarga_delete[$key] == 1)
				{
					$O->delete_keluarga($key);
					continue;
				}
				$keluarga_arr = array();
				$keluarga_arr['nama'] = $edit_keluarga_nama[$key];
				$keluarga_arr['jenis_kelamin_id'] = $edit_keluarga_jenis_kelamin_id[$key];
				$keluarga_arr['shdk_id'] = $edit_keluarga_shdk_id[$key];
				$keluarga_arr['pendidikan_id'] = $edit_keluarga_pendidikan_id[$key];
				$O->edit_keluarga($key, $keluarga_arr);
			}
			// edit pendidikan
			foreach($edit_pendidikan_nama as $key => $val)
			{
				if($edit_pendidikan_delete[$key] == 1)
				{
					$O->delete_pendidikan($key);
					continue;
				}
				$pendidikan_arr = array();
				$pendidikan_arr['pendidikan_id'] = $edit_pendidikan_id[$key];
				$pendidikan_arr['tahun_awal'] = $edit_pendidikan_tahun_awal[$key];
				$pendidikan_arr['tahun_akhir'] = $edit_pendidikan_tahun_akhir[$key];
				$pendidikan_arr['nama'] = $edit_pendidikan_nama[$key];
				$pendidikan_arr['jurusan'] = $edit_pendidikan_jurusan[$key];
				$pendidikan_arr['nilai'] = $edit_pendidikan_nilai[$key];
				$O->edit_pendidikan($key, $pendidikan_arr);
			}
			// edit pengalaman kerja
			foreach($edit_pengalaman_perusahaan as $key => $val)
			{
				if($edit_pengalaman_delete[$key] == 1)
				{
					$O->delete_pengalaman($key);
					continue;
				}
				$pengalaman_arr = array();
				if(strtotime($edit_pengalaman_tanggal_awal[$key]) > strtotime($edit_pengalaman_tanggal_akhir[$key]))
				{
					$tmp_awal = $edit_pengalaman_tanggal_awal[$key];
					$tmp_akhir = $edit_pengalaman_tanggal_akhir[$key];
					$edit_pengalaman_tanggal_awal[$key] = $tmp_akhir;
					$edit_pengalaman_tanggal_akhir[$key] = $tmp_awal;
				}
				$pengalaman_arr['tanggal_awal'] = get_date_lang($edit_pengalaman_tanggal_awal[$key],"EN");
				$pengalaman_arr['tanggal_akhir'] = get_date_lang($edit_pengalaman_tanggal_akhir[$key],"EN");
				$pengalaman_arr['perusahaan'] = $edit_pengalaman_perusahaan[$key];
				$pengalaman_arr['bidang_usaha_id'] = $edit_pengalaman_bidang_usaha_id[$key];
				$pengalaman_arr['jabatan'] = $edit_pengalaman_jabatan[$key];
				$O->edit_pengalaman($key, $pengalaman_arr);
			}
			// edit keterampilan
			foreach($edit_keterampilan_kategori_id as $key => $val)
			{
				if($edit_keterampilan_delete[$key] == 1)
				{
					$O->delete_keterampilan($key);
					continue;
				}
				$keterampilan_arr = array();
				$keterampilan_arr['kategori_id'] = $edit_keterampilan_kategori_id[$key];
				$keterampilan_arr['nilai_skala'] = $edit_keterampilan_nilai_skala[$key];
				$keterampilan_arr['sertifikasi'] = $edit_keterampilan_sertifikasi[$key];
				$O->edit_keterampilan($key, $keterampilan_arr);
			}
			// edit bahasa
			foreach($edit_bahasa_id as $key => $val)
			{
				if($edit_bahasa_delete[$key] == 1)
				{
					$O->delete_bahasa($key);
					continue;
				}
				$bahasa_arr = array();
				$bahasa_arr['bahasa_id'] = $edit_bahasa_id[$key];
				$bahasa_arr['nilai_skala'] = $edit_bahasa_nilai_skala[$key];
				$bahasa_arr['sertifikasi'] = $edit_bahasa_sertifikasi[$key];
				$O->edit_bahasa($key, $bahasa_arr);
			}
			// edit dokumens
			$O->edit_dokumens($_FILES['edit_dokumen'],$edit_dokumen_aktif_flag);
			foreach($edit_dokumen_aktif_flag as $key => $val)
			{
				if($edit_dokumen_delete[$key] == 1)
				{
					$O->delete_dokumen($key);
					continue;
				}
				$dokumen_arr = array();
				$dokumen_arr['aktif_flag'] = $edit_dokumen_aktif_flag[$key];
				$O->edit_dokumen($key, $dokumen_arr);
			}
			//handle delete if checked
			foreach($edit_dokumen_delete as $key => $val)
			{
				if($edit_dokumen_delete[$key] == 1)
				{
					$O->delete_dokumen($key);
					continue;
				}
			}
			
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidats_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        $O = new OKandidat($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage);
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
		$sorts = explode(",",$this->input->post('sorts'));
		$this->db->update('kandidats',array('ordering' => 0));
		$count = 1;
		foreach($sorts as $id)
		{
				$this->db->update('kandidats',array('ordering' => $count),array('id' => intval($id)));
				$count++;
		}
		die("DONE");
    }

    
    public function set_jenis_kelamin_id($active=0, $id)
    {	
        $O = new OKandidat($id);
        
        $arr['jenis_kelamin_id'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($this->curpage);
        exit();
    }


public function set_skrg_asal_flag($active=0, $id)
    {	
        $O = new OKandidat($id);
        
        $arr['skrg_asal_flag'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($this->curpage);
        exit();
    }


public function set_status_kawin_id($active=0, $id)
    {	
        $O = new OKandidat($id);
        
        $arr['status_kawin_id'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($this->curpage);
        exit();
    }


public function set_newsletter_flag($active=0, $id)
    {	
        $O = new OKandidat($id);
        
        $arr['newsletter_flag'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($this->curpage);
        exit();
    }
    
        
}

/* End of file kandidats.php */
/* Location: ./application/controllers/admin/kandidats.php */