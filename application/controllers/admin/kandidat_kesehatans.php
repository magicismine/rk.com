<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kandidat_kesehatans extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/kandidat/kesehatan/";
        $this->curpage 	= "admin/kandidat_kesehatans";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($kandidat_id=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
		$where = "";
		if(!empty($kandidat_id))
		{
			$data['kandidat_id'] = $kandidat_id = intval($kandidat_id);
			$where = 'kandidat_id='.$kandidat_id;
			$perpage = 1;
		}
        if($_GET['keyword'] != "")
        {
        	$data['list'] 	= OKandidat_kesehatan::search($_GET['keyword'],0,0,$ordering,$where);
        }
        else
        {
        	$data['list'] 	= OKandidat_kesehatan::get_list($start,$perpage,$ordering,$where);
        }
        $data['uri'] 		= intval($start);
        $total 				= get_db_total_rows();
        $url 				= $this->curpage."/listing?";
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidat_kesehatans_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add($kandidat_id=0)
    {
		if(!empty($kandidat_id))
		{
			$data['kandidat_id'] = $kandidat_id = intval($kandidat_id);
		}
    	// setup validations
        if(sizeof($_POST) > 0)
        {
			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
			$arr['kacamata_flag'] = 0;
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            
            $new = OKandidat_kesehatan::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
			redirect($this->curpage."/listing/".$kandidat_id);
            exit();
        }
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidat_kesehatans_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id,$kandidat_id)
    {
		if(!empty($kandidat_id))
		{
			$data['kandidat_id'] = $kandidat_id = intval($kandidat_id);
		}
        $O = new OKandidat_kesehatan($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($this->curpage."/listing/".$kandidat_id);
                exit();
            }
        }
        
        if(sizeof($_POST) > 0)
        {
			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
			$arr['kacamata_flag'] = 0;
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            
            $res = $O->edit($arr);
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage."/listing/".$kandidat_id);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'kandidat_kesehatans_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id,$kandidat_id)
    {
		if(!empty($kandidat_id))
		{
			$data['kandidat_id'] = $kandidat_id = intval($kandidat_id);
		}
        $O = new OKandidat_kesehatan($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($this->curpage."/listing/".$kandidat_id);
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
            $sorts = explode(",",$this->input->post('sorts'));
            $this->db->update('kandidat_kesehatans',array('ordering' => 0));
            $count = 1;
            foreach($sorts as $id)
            {
                    $this->db->update('kandidat_kesehatans',array('ordering' => $count),array('id' => intval($id)));
                    $count++;
            }
            die("DONE");
    }

    
    public function set_gol_darah_id($active=0, $id, $kandidat_id)
    {	
		if(!empty($kandidat_id))
		{
			$data['kandidat_id'] = $kandidat_id = intval($kandidat_id);
		}
        $O = new OKandidat_kesehatan($id);
        
        $arr['gol_darah_id'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($this->curpage."/listing/".$kandidat_id);
        exit();
    }


	public function set_kacamata_flag($active=0, $id, $kandidat_id)
    {	
		if(!empty($kandidat_id))
		{
			$data['kandidat_id'] = $kandidat_id = intval($kandidat_id);
		}
        $O = new OKandidat_kesehatan($id);
        
        $arr['kacamata_flag'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($this->curpage."/listing/".$kandidat_id);
        exit();
    }
    
        
}

/* End of file kandidat_kesehatans.php */
/* Location: ./application/controllers/admin/kandidat_kesehatans.php */