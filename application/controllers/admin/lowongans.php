<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lowongans extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		//error_reporting(-1);
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/lowongan/";
        $this->curpage 	= "admin/lowongans";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $orderby = $_GET['orderby'];
        $data['order'] = $order =  $_GET['order'];
        $get_param = $data['get_param'] = get_params($_GET,"");
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "lowongans.id DESC";
        else
        {
        	//$ordering = "{$data['orderby']} {$data['order']}";
            switch ($orderby) {
                case 'perusahaan':
                    $ordering = "perusahaans.nama {$order}";
                    break;
                case 'newest':
                    $ordering = "lowongans.id {$order}";
                    break;
                case 'kode':
                    $ordering = "lowongans.kode {$order}";
                    break;
                /*
                case 'level_karir':
                    $ordering = "lowongans.level_karir_id {$order}";
                    break;
                */
                case 'kategori':
                    $ordering = "master_kategoris.nama {$order}";
                    break;
                case 'posisi':
                    $ordering = "lowongans.posisi {$order}";
                    break;
                case 'waktu_kerja':
                    $ordering = "master_waktu_kerjas.nama {$order}";
                    break;
                case 'pendidikan':
                    $ordering = "master_pendidikans.nama {$order}";
                    break;
                case 'umur':
                    $ordering = "lowongans.umur_sampai {$order}";
                    break;
                case 'pengalaman_kerja':
                    $ordering = "lowongans.tahun_pengalaman_kerja {$order}";
                    break;
                case 'gaji':
                    $ordering = "lowongans.gaji {$order}";
                    break;
                default:
                    $ordering = "lowongans.id DESC";
                    break;
            }
        }
        $keywords = $_GET['keyword'];
        if(!empty($keywords)) $ordering = "";
        $data['list']   = OLowongan::get_list_filter($keywords,$start,$perpage,$ordering,"",$_GET);
        $data['uri'] 		= intval($start);
        $data['total']      = $total = get_db_total_rows();
        $url 				= $this->curpage."/listing?".get_params($_GET, array('page'));

        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'lowongans_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function deskripsi_check($text)
    {
        if (validate_text_contain_email($text))
        {
            $this->form_validation->set_message('deskripsi_check', 'The Deskripsi field can not contain an email.');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function add()
    {
        redirect($this->curpage); exit;
    	// setup validations
        $this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_deskripsi_check');

        $this->form_validation->set_rules('kontak_nama', 'Kontak Nama', 'trim|required');
        $this->form_validation->set_rules('kontak_email', 'Kontak Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('kontak_telp', 'Kontak Telp', 'trim|required');

        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run() != FALSE)
        {
			extract($_POST);
            
            $arr = NULL;
			$arr['gaji_nego_flag'] = 0;
			$arr['lokasi_kerja_kantor_flag'] = 0;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            $arr['dt_expired'] = get_date_lang($dt_expired,"EN");

            $new = OLowongan::add($arr);
            if($new)
            {
                $O = new OLowongan($new);
                $OP = new OPerusahaan($O->row->perusahaan_id);
                $arr = NULL;
                $arr['bidang_usaha_id'] = $OP->row->bidang_usaha_id;

                if($this->input->post('lokasi_kerja_kantor_flag') == 1)
                {
                    $arr['lokasi_alamat'] = $OP->row->lokasi_alamat;
                    $arr['lokasi_negara_id'] = $OP->row->lokasi_negara_id;
                    $arr['lokasi_propinsi_id'] = $OP->row->lokasi_propinsi_id;
                    $arr['lokasi_kabupaten_id'] = $OP->row->lokasi_kabupaten_id;
                    $arr['lokasi_kecamatan'] = $OP->row->lokasi_kecamatan;
                    $arr['lokasi_kelurahan'] = $OP->row->lokasi_kelurahan;
                    $arr['lokasi_kawasan_id'] = $OP->row->lokasi_kawasan_id;                    
                }
                unset($OP);
                $O->edit($arr);
                unset($O);
                warning("add", "success");
            }
            else warning("add", "fail");
            	
			// update url title
			$O = new OLowongan($new);
			$O->update_url_title();
            $O->update_level_karirs($_POST['level_karir_id']);
			redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'lowongans_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        $data['curpage'] = $curpage = $this->curpage."?".get_params($_GET);
        $O = new OLowongan($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage);
                exit();
            }
        }
        
        $this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|min_length[2]');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_deskripsi_check');

        $this->form_validation->set_rules('kontak_nama', 'Kontak Nama', 'trim|required');
        $this->form_validation->set_rules('kontak_email', 'Kontak Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('kontak_telp', 'Kontak Telp', 'trim|required');

        $this->form_validation->set_error_delimiters('', '<br />');

        if ($this->form_validation->run() != FALSE)
        {
			extract($_POST);
            
            $arr = NULL;
			$arr['gaji_nego_flag'] = 0;
			$arr['lokasi_kerja_kantor_flag'] = 0;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            $arr['dt_expired'] = get_date_lang($dt_expired,"EN");

            $res = $O->edit($arr);
			// update url title
            $O->refresh();
            $O->update_url_title();
            $O->update_level_karirs($_POST['level_karir_id']);

            if($this->input->post('lokasi_kerja_kantor_flag') == 1)
            {
                $OP = new OPerusahaan($O->row->perusahaan_id);
                $arr = NULL;
                $arr['bidang_usaha_id'] = $OP->row->bidang_usaha_id;
                $arr['lokasi_alamat'] = $OP->row->lokasi_alamat;
                $arr['lokasi_negara_id'] = $OP->row->lokasi_negara_id;
                $arr['lokasi_propinsi_id'] = $OP->row->lokasi_propinsi_id;
                $arr['lokasi_kabupaten_id'] = $OP->row->lokasi_kabupaten_id;
                $arr['lokasi_kecamatan'] = $OP->row->lokasi_kecamatan;
                $arr['lokasi_kelurahan'] = $OP->row->lokasi_kelurahan;
                $arr['lokasi_kawasan_id'] = $OP->row->lokasi_kawasan_id;
                unset($OP);
                $O->edit($arr);
            }
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'lowongans_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        $curpage = $this->curpage."?".get_params($_GET);
        $O = new OLowongan($id);
        
        // DONT DELETE JUST SET ACTIVE TO 0
        $arr['status'] = 0;
        $res = $O->edit($arr);

        //$res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($curpage);
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
            $sorts = explode(",",$this->input->post('sorts'));
            $this->db->update('lowongans',array('ordering' => 0));
            $count = 1;
            foreach($sorts as $id)
            {
                    $this->db->update('lowongans',array('ordering' => $count),array('id' => intval($id)));
                    $count++;
            }
            die("DONE");
    }

    
    public function set_jenis_kelamin_id($active=0, $id)
    {	
        $curpage = $this->curpage."?".get_params($_GET);
        $O = new OLowongan($id);
        
        $arr['jenis_kelamin_id'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($curpage);
        exit();
    }
    
    public function manage_lamarans($id,$a="list",$par1,$par2)
    {
        $data['curpage'] = $curpage = $this->curpage."/manage_lamarans/{$id}";
        $curpage_params = $curpage."?".get_params($_GET);
        if(empty($id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['O'] = $O = new OLowongan($id);
        $data['OP'] = $OP = new OPerusahaan($O->row->perusahaan_id);
        if(empty($O->id))
        {
            redirect($this->curpage);
            exit;
        }
        $breadcrumb_arr = array($this->curpage."?".get_params($_GET,array("keyword2")) => 'Lowongans',
                                '' => $O->get_nama()." (".$OP->get_nama().")"
                                );
        $data['breadcrumb_arr'] = $breadcrumb_arr;
        //list
        if($a == "" || $a == "list")
        {
            $data['cu'] = $this->cu;
            if(empty($_GET['keyword2']))
            {
                $list = $O->get_lamarans(0,0);
            }
            else
            {
                extract($_GET);
                $keyword2 = $this->db->escape_str($keyword2);
                $list = $O->get_lamarans(0,0,'id DESC',"(
                                                        kandidats.nama_depan LIKE '%{$keyword2}%'
                                                        OR kandidats.nama_akhir LIKE '%{$keyword2}%'
                                                        OR kandidats.nama_panggilan LIKE '%{$keyword2}%'
                                                        )");
            }
            $data['list'] = $list;
            //var_dump($this->db->last_query());
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'lowongan_lamarans_list', $data);
            $this->load->view($this->ffoot, $data);
        }
        //delete
        /*elseif($a == "delete")
        {
            $kandidat_id = intval($par1);
        
            if(empty($kandidat_id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage_params);
                exit();
            }
        
            $res = $O->delete_rekomendasi($kandidat_id);
            if($res) warning("delete", "success");
            else warning("delete", "fail");
            redirect($curpage_params);
            exit();
        }*/
        //recommendation
        elseif($a == "set_recommendation")
        {
            $kandidat_id = intval($par2);
        
            if(empty($kandidat_id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage_params);
                exit();
            }
        
            if(intval($par1) == 1) $res = $O->set_rekomendasi($kandidat_id);
            else $res = $O->delete_rekomendasi($kandidat_id);
            
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($curpage_params);
            exit();
        }
        //set terima recommendation
        elseif($a == "set_terima_recommendation")
        {
            $kandidat_id = intval($par2);
        
            if(empty($kandidat_id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage_params);
                exit();
            }
        
            if(intval($par1) == 1) $res = $O->set_terima_rekomendasi($kandidat_id);
            else $res = $O->delete_terima_rekomendasi($kandidat_id);
            
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($curpage_params);
            exit();
        }
        unset($O,$OP,$data);
    }

}

/* End of file lowongans.php */
/* Location: ./application/controllers/admin/lowongans.php */