<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perusahaans extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->fhead 	= "admin/header";
        $this->ffoot 	= "admin/footer";
        $this->fpath 	= "admin/perusahaan/";
        $this->curpage 	= "admin/perusahaans";
        
        $this->cu = $cu = get_current_admin();
        if(!$cu) admin_logout();
    }
    
    public function index()
    {
        $this->listing(0);
    }
    
    public function listing($start=0)
    {
        $start 				= intval($_GET['page']);
        $perpage 			= 10;
        $data['orderby'] = $_GET['orderby'];
        $data['order'] = $_GET['order'];
        if($data['orderby'] == "" && $data['order'] == "") $ordering = "";
        else
        {
        	$ordering = "{$data['orderby']} {$data['order']}";
        }
        if($_GET['keyword'] != "")
        {
            $perpage = 0;
        	$data['list'] 	= OPerusahaan::search($_GET['keyword'],$ordering);
        }
        else
        {
        	
        	$data['list'] 	= OPerusahaan::get_list($start, $perpage,$ordering);
        }
        $data['uri'] 		= intval($start);
        $data['total']      = $total = get_db_total_rows();
        $url 				= $this->curpage."/listing?";
        
        $data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
        
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'perusahaans_list', $data);
        $this->load->view($this->ffoot, $data);
    }
    
    public function add()
    {
        redirect($this->curpage); exit;
    	// setup validations
        // ========= KURANG FORM VALIDASI
        if(sizeof($_POST) > 0)
        {
			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    if(trim($val) != "") $arr[$key] = trim($val);	
                }
            }
            
			$arr['dt_expired'] = get_date_lang($dt_expired,"EN");
            $new = OPerusahaan::add($arr);
            if($new) warning("add", "success");
            else warning("add", "fail");
            	
			// update url title
			$O = new OPerusahaan($new);
			foreach($_POST['image'] as $image)
			{
				$O->update_photo($image);
			}
			$O->update_url_title();
			redirect($this->curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'perusahaans_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function edit($id)
    {
        $data['curpage'] = $curpage = $this->curpage."?".get_params($_GET);
        $O = new OPerusahaan($id);
        
        if(!empty($id))
        {			
            if($O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage);
                exit();
            }
        }
        
        // ========= KURANG FORM VALIDASI
        if(sizeof($_POST) > 0)
        {
			extract($_POST);
            
            $arr = NULL;
            $exclude_arr = array("submit","url_title");
            
            foreach($_POST as $key => $val)
            {
                if(!in_array($key, $exclude_arr) && !is_array($val))
                {	
                    $arr[$key] = trim($val);	
                }
            }
            
            $arr['dt_expired'] = get_date_lang($dt_expired,"EN");
			$res = $O->edit($arr);
            // update url title
            foreach($_POST['image'] as $image)
            {
                $O->update_photo($image);
            }
            $O->refresh();
            $O->update_url_title();
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($curpage);
            exit();
        }
        $data['cu'] = $this->cu;
        $data['row'] = $O->row;
        $this->load->view($this->fhead, $data);
        $this->load->view($this->fpath.'perusahaans_form', $data);
        $this->load->view($this->ffoot, $data);	
    }
    
    public function delete($id)
    {
        $curpage = $this->curpage."?".get_params($_GET);
        $O = new OPerusahaan($id);
        
        $res = $O->delete();
        if($res) warning("delete", "success");
        else warning("delete", "fail");
        redirect($curpage);
        exit();
    }	
    
    // this is an ajax call from the view list for sorting purposes.
    public function sorting()
    {
        $sorts = explode(",",$this->input->post('sorts'));
        $this->db->update('perusahaans',array('ordering' => 0));
        $count = 1;
        foreach($sorts as $id)
        {
            $this->db->update('perusahaans',array('ordering' => $count),array('id' => intval($id)));
            $count++;
        }
        die("DONE");
    }

    
    public function set_newsletter_flag($active=0, $id)
    {	
        $data['curpage'] = $curpage = $this->curpage."?".get_params($_GET);

        $O = new OPerusahaan($id);
        
        $arr['newsletter_flag'] = $active;
        $res = $O->edit($arr);
        
        if($res) warning("edit", "success");
        else warning("edit", "fail");
        redirect($curpage);
        exit();
    }
    
    //PROMO KANDIDAT
    public function manage_promo_kandidat($id,$a="list",$par1,$par2)
    {
        $data['curpage'] = $curpage = $this->curpage."/manage_promo_kandidat/{$id}";
        if(empty($id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['OP'] = $OP = new OPerusahaan($id);
        if(empty($OP->id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['O'] = $O = new OPerusahaan_promo;
        $breadcrumb_arr = array($this->curpage => 'Perusahaans',
                                "" => $OP->get_nama()
                                );
        $data['breadcrumb_arr'] = $breadcrumb_arr;
        //list
        if($a == "" || $a == "list")
        {
            $data['cu'] = $this->cu;
            if(empty($_GET['keyword2']))
            {
                $list = $O->get_list(0,0,'id DESC',"perusahaan_id=".intval($OP->id));
            }
            else
            {
                extract($_GET);
                $list = $O->search($keyword2,0,0,'id DESC',"perusahaan_id=".intval($OP->id));
            }
            $data['list'] = $list;
            //var_dump($this->db->last_query());
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'perusahaan_promos_list', $data);
            $this->load->view($this->ffoot, $data);
        }
        //add
        elseif($a == "add")
        {
            // validation
            $this->form_validation->set_rules('name', 'Judul', 'trim|required');
            $this->form_validation->set_error_delimiters('', '<br />');

            if($this->form_validation->run())
            {
                extract($_POST);
                
                $arr = array('perusahaan_id' => $OP->id);
                $exclude_arr = array("submit","url_title");
                
                foreach($_POST as $key => $val)
                {
                    if(!in_array($key, $exclude_arr) && !is_array($val))
                    {   
                        if(trim($val) != "") $arr[$key] = trim($val);   
                    }
                }
                
                $new = $O->add($arr);
                if($new) warning("add", "success");
                else warning("add", "fail");
                    
                // update url title
                $O = new OPerusahaan_promo($new);
                /*foreach($_POST['image'] as $image)
                {
                    $O->update_photo($image);
                }*/
                $O->update_url_title();
                redirect($curpage);
                exit();
            }
            $data['cu'] = $this->cu;
            $data['breadcrumb_arr'] = $breadcrumb_arr + array('' => "Add");
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'perusahaan_promos_form', $data);
            $this->load->view($this->ffoot, $data); 
        }
        //edit
        elseif($a == "edit")
        {
            $id = intval($par1);
            $O = new OPerusahaan_promo($id);
        
            if(empty($id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage);
                exit();
            }
            // validation
            $this->form_validation->set_rules('name', 'Judul', 'trim|required');
            $this->form_validation->set_error_delimiters('', '<br />');

            if($this->form_validation->run())
            {
                extract($_POST);
                
                $arr = NULL;
                $exclude_arr = array("submit","url_title");
                
                foreach($_POST as $key => $val)
                {
                    if(!in_array($key, $exclude_arr) && !is_array($val))
                    {   
                        $arr[$key] = trim($val);    
                    }
                }
                
                $res = $O->edit($arr);
                // update url title
                /*foreach($_POST['image'] as $image)
                {
                    $O->update_photo($image);
                }*/
                $O->refresh();
                $O->update_url_title();
                if($res) warning("edit", "success");
                else warning("edit", "fail");
                redirect($curpage);
                exit();
            }
            $data['cu'] = $this->cu;
            $data['row'] = $O->row;
            $data['breadcrumb_arr'] = $breadcrumb_arr + array('' => "Edit");
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'perusahaan_promos_form', $data);
            $this->load->view($this->ffoot, $data); 
        }
        //delete
        elseif($a == "delete")
        {
            $id = intval($par1);
            $O = new OPerusahaan_promo($id);
        
            if(empty($id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage);
                exit();
            }
        
            $res = $O->delete();
            if($res) warning("delete", "success");
            else warning("delete", "fail");
            redirect($curpage);
            exit();
        }
        //activate
        elseif($a == "activate")
        {
            $id = intval($par2);
            $O = new OPerusahaan_promo($id);
        
            if(empty($id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage);
                exit();
            }
        
            $arr['active'] = intval($par1);
            $res = $O->edit($arr);
            
            if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($curpage);
            exit();
        }
        unset($O,$OP,$data);
    }

    //CV
    public function cv_disimpan($id,$a="list",$par1,$par2)
    {
        $data['curpage'] = $curpage = $this->curpage."/cv_disimpan/{$id}";
        $curpage_params = $curpage."?".get_params($_GET);
        if(empty($id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['OP'] = $OP = new OPerusahaan($id);
        if(empty($OP->id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['O'] = $O = new OKandidat;
        $breadcrumb_arr = array($this->curpage."?".get_params($_GET,array("keyword2")) => 'Perusahaans',
                                "" => $OP->row->nama
                                );
        $data['breadcrumb_arr'] = $breadcrumb_arr;
        //list
        if($a == "" || $a == "list")
        {
            $data['cu'] = $this->cu;
            extract($_GET);
            $list = $OP->search_cvs($keyword2,0,0,'',"tandai=1");
            $data['list'] = $list;
            //var_dump($this->db->last_query());
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'cv_disimpan_list', $data);
            $this->load->view($this->ffoot, $data);
        }
        //delete
        elseif($a == "delete")
        {
            $id = intval($par1);
            $O = new OKandidat($id);
        
            if(empty($id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage_params);
                exit();
            }
        
            $res = $OP->delete_cv($id);
            if($res) warning("delete", "success");
            else warning("delete", "fail");
            redirect($curpage_params);
            exit();
        }
        unset($O,$OP,$data);
    }

    //CV
    public function cv_diundang($id,$a="list",$par1,$par2)
    {
        $data['curpage'] = $curpage = $this->curpage."/cv_diundang/{$id}";
        $curpage_params = $curpage."?".get_params($_GET);
        if(empty($id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['OP'] = $OP = new OPerusahaan($id);
        if(empty($OP->id))
        {
            redirect($this->curpage);
            exit;
        }
        $data['O'] = $O = new OKandidat;
        $breadcrumb_arr = array($this->curpage."?".get_params($_GET,array("keyword2")) => 'Perusahaans',
                                "" => $OP->row->nama
                                );
        $data['breadcrumb_arr'] = $breadcrumb_arr;
        //list
        if($a == "" || $a == "list")
        {
            $data['cu'] = $this->cu;
            extract($_GET);
            $list = $OP->search_cvs($keyword2,0,0,'',"undang=1");
            $data['list'] = $list;
            //var_dump($this->db->last_query());
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'cv_diundang_list', $data);
            $this->load->view($this->ffoot, $data);
        }
        //set_rekomendasi
        elseif($a == "set_rekomendasi")
        {
            $id = intval($par1);
            $O = new OKandidat($id);
        
            if(empty($id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage_params);
                exit();
            }
            
            extract($_POST);

            $list = $OP->get_lowongans(0,0,'dt_added DESC','dt_expired > NOW()');
            $lowongan_arr = NULL;
            if(!$list)
            {
                $this->session->set_flashdata('error', 'No Active Lowongan Found.');
                redirect($curpage_params);
                exit;
            }
            $OTmp = new OLowongan;
            foreach ($list as $r)
            {
                $OTmp->setup($r);
                $lowongan_arr[$r->id] = $OTmp->get_nama();
            }
            unset($OTmp);
            $data['lowongan_ddl'] = dropdown('lowongan_id', $lowongan_arr, $lowongan_id);

            // validation
            $this->form_validation->set_rules('lowongan_id', 'Lowongan', 'trim|required');
            $this->form_validation->set_error_delimiters('', '<br />');

            if($this->form_validation->run())
            {                
                $arr = NULL;
                $exclude_arr = array("submit","url_title");
                
                $res = $O->set_lowongan_lamar($lowongan_id);

                if($res)
                {
                    $OL = new OLowongan($lowongan_id);
                    $OL->set_rekomendasi($O->id);
                    warning("edit", "success");
                }
                else warning("edit", "fail");
                redirect($curpage_params);
                exit();
            }
            $data['cu'] = $this->cu;
            $data['row'] = $O->row;
            $data['breadcrumb_arr'] = $breadcrumb_arr + array('' => "Set Rekomendasi ".$O->get_nama_lengkap());
            $this->load->view($this->fhead, $data);
            $this->load->view($this->fpath.'cv_diundang_rekomendasi_form', $data);
            $this->load->view($this->ffoot, $data); 
            unset($O);
        }
        //delete
        elseif($a == "delete")
        {
            $id = intval($par1);
            $O = new OKandidat($id);
        
            if(empty($id) || $O->row == FALSE)
            {
                $this->session->set_flashdata('warning', 'ID does not exist.');
                redirect($curpage_params);
                exit();
            }
        
            $res = $OP->delete_cv($id,'undang');
            if($res) warning("delete", "success");
            else warning("delete", "fail");
            redirect($curpage_params);
            exit();
        }
        unset($O,$OP,$data);
    }

}

/* End of file perusahaans.php */
/* Location: ./application/controllers/admin/perusahaans.php */