<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{
		$this->user($role);
	}

	public function user($role)
	{
		$this->session->sess_destroy();
		if($role == "kandidat")
		{
			unset_login_session("kandidat");
			redirect("kandidat");
		}
		elseif($role == "perusahaan")
		{
			unset_login_session("perusahaan");
			redirect("perusahaan");
		}
		else{
			redirect("");
		}
	}

	public function inactive()
	{
		//$this->cu = $cu = get_logged_in_user();
		session_start();
		session_destroy();
		unset_login_session("perusahaan");
		unset_login_session("kandidat");
		$this->load->view('header', $data);
		$this->load->view('inactive', $data);
		$this->load->view('footer', $data);
	}
	
}

/* End of file logout.php */
/* Location: ./application/controllers/logout.php */