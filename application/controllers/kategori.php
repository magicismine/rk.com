<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function index()
	{
		redirect("");
	}

	public function listing($url_title,$page = 0)
	{
		$perpage = 0;
		$OMKat = $data['OMKat'] = new OMaster_kategori($url_title,"url_title");
		$OLow = $data['OLow'] = new OLowongan();
		$data['list'] = OLowongan::get_list($page,$perpage,"id DESC","kategori_id = {$OMKat->id}");

		$this->load->view('header',$data);
		$this->load->view('kategori_lowongan',$data);
		$this->load->view('footer',$data);

		unset($OMKat,$OLow);
	}

}

/* End of file kategori.php */
/* Location: ./application/controllers/kategori.php */