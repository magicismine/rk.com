<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->parent_path 	= "users/perusahaan";
		$this->vpath 	= $this->parent_path."/promo";
		$this->curpage 	= "perusahaan/promo";
		//error_reporting(-1);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "perusahaan" )
			{	
				user_logout('perusahaan');
			}
			else
			{
				if($this->cu->status != "aktif")
				{
					user_inactive();
					exit;
				}
				// setup to the Object
				$this->OU = $OU = new OUser();
				$OU->setup($cu);
				
				$row = $OU->get_info("perusahaan");
				if(!$row)
				{
					$OU->update_info(array('email' => $OU->row->email), "perusahaan");
					$row = $OU->get_info("perusahaan");
				}
				$this->OP = $OP = new OPerusahaan();
				$OP->setup($row);
			}	
		}
		else
		{
			redirect('account/login/perusahaan');
		}

	}
	
	public function index()
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$perpage = 5;
		
		$where = "";
		if(trim($keyword) != "")
		{
			$perpage = 0;
			$where = '';
		}
		$data['list'] = $list = $O->get_promos($start,$perpage,"id DESC",$where);
		$data['perpage'] = $perpage;
		$data['total'] = $total = get_db_total_rows();
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/index',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$O);
	}

	public function add()
	{
		$data['current'] = "promo";
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		$data['row'] = $OP->row;

		// ========= FORM VALIDASI
		$this->form_validation->set_rules('name', 'Judul', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('description', 'Isi', 'trim|required|min_length[100]');

		$this->form_validation->set_error_delimiters('', '<br />');

		if ($this->form_validation->run() != FALSE)
		{
			$include_keys = array(
								"name", "description"
								);
			foreach($_POST as $key => $val)
			{
				if(in_array($key, $include_keys) && !is_array($val))
				{
					if(!empty($val))
					{
						$arr[$key] = $this->input->post($key);
					}
				}
			}
			
			$new_id = $OP->add_promo($arr);
			//$O = new OLowongan($new_id);
			//$O->update_url_title();
			
			$this->session->set_flashdata("success", "Promo  berhasil ditambahkan.");
			unset($OU,$OP,$O);
			redirect($this->curpage);
			exit;
		}
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/form',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$OP,$O);
	}
	
	public function edit($id)
	{
		$data['current'] = "lowongan";
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		
		$data['OPp'] = $OPp = new OPerusahaan_promo($id, 'url');
		$data['row'] = $OPp->row;
		
		// check user permission
		if(empty($OPp->id) || $OPp->row->perusahaan_id != $OP->id)
		{
			unset($OU,$OPp,$OP);
			redirect($this->curpage);
			exit;
		}
		
		// ========= KURANG FORM VALIDASI
		$this->form_validation->set_rules('name', 'Judul', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('description', 'Isi', 'trim|required|min_length[100]');

		$this->form_validation->set_error_delimiters('', '<br />');

		if ($this->form_validation->run() != FALSE)
		{
			$include_keys = array(
								"name", "description"
								);
			foreach($_POST as $key => $val)
			{
				if(in_array($key, $include_keys) && !is_array($val))
				{
					$arr[$key] = $this->input->post($key);
				}
			}
			
			$res = $OPp->edit($arr);
			$OPp->refresh();
			$OPp->update_url_title();

			$this->session->set_flashdata("success", "Promo tersebut berhasil diupdate.");
			unset($OU,$OP,$OPp);
			redirect($this->curpage);
			exit;
		}
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/form',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$OP,$OPp);
	}
	
	public function delete($id)
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		
		$OK = new OKandidat($id, "url_title");
		
		$res = $O->delete_perusahaan_promo($OK->id);
		
		if($res) $this->session->set_flashdata("success", "Promo tersebut berhasil dihapus.");
		else $this->session->set_flashdata("error", $OK->get_nama()." gagal dihapus.");
		unset($O,$OK);

		redirect($this->curpage);
		exit;
	}
	
}

/* End of file cv.php */
/* Location: ./application/controllers/perusahaan/cv.php */