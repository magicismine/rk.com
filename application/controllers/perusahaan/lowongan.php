<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lowongan extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->parent_path 	= "users/perusahaan";
		$this->vpath 	= $this->parent_path."/lowongan";
		$this->curpage 	= "perusahaan/lowongan";
		//error_reporting(-1);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "perusahaan" )
			{	
				user_logout('perusahaan');
			}
			else
			{
				if($this->cu->status != "aktif")
				{
					user_inactive();
					exit;
				}
				// setup to the Object
				$this->OU = $OU = new OUser();
				$OU->setup($cu);
				
				$row = $OU->get_info("perusahaan");
				if(!$row)
				{
					$OU->update_info(array('email' => $OU->row->email), "perusahaan");
					$row = $OU->get_info("perusahaan");
				}
				$this->OP = $OP = new OPerusahaan();
				$OP->setup($row);
			}	
		}else
		{
			redirect('account/login/perusahaan');
		}

	}
	
	public function index()
	{
		$data['current'] = "lowongan";
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		$data['row'] = $OP->row;
		
		extract($_GET);
		$page = intval($page);
		$perpage = 5;
		
		if(trim($keyword) != "")
		{
			$perpage = 0;
			$list = $OP->search_lowongans($keyword,$start,$perpage,"id DESC","active=1",$kategori_id,$lokasi_propinsi_id,$lokasi_kabupaten_id);
		}
		else
		{
			$list = $OP->get_lowongans($start,$perpage,"id DESC","active=1");
		}
		$data['list'] = $list;
		$data['perpage'] = $perpage;
		
		$data['total'] = $total = get_db_total_rows();
		$data['total_lowongan_aktif'] = $OP->get_total_lowongan_aktif();
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/index',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$OP);
	}
	
	public function semua()
	{
		$data['current'] = "lowongan";
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		
		extract($_GET);
		$page = intval($page);
		$perpage = 15;
		
		$data['list'] = $OP->get_lowongans($start,$perpage,"id DESC");
		$data['total_lowongan'] = $total = get_db_total_rows();
		$data['total_lowongan_aktif'] = $OP->get_total_lowongan_aktif();
		
		$url = $this->curpage."/semua?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/list',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$OP);
	}
	

	public function deskripsi_check($text)
	{
		if (validate_text_contain_email($text))
		{
			$this->form_validation->set_message('deskripsi_check', 'The Deskripsi field can not contain an email.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function add()
	{
		$data['current'] = "lowongan";
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		$data['row'] = $OP->row;

		// ========= FORM VALIDASI
		$this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('jumlah_tenaga_kerja', 'Jumlah Tenaga Kerja', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_deskripsi_check');

		$this->form_validation->set_rules('kontak_nama', 'Kontak Nama', 'trim|required');
		$this->form_validation->set_rules('kontak_email', 'Kontak Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('kontak_telp', 'Kontak Telp', 'trim|required');

		$this->form_validation->set_error_delimiters('', '<br />');

		if ($this->form_validation->run() != FALSE)
		{
			$include_keys = array(
								"judul", "deskripsi", "dt_expired",
								"kontak_nama", "kontak_email", "kontak_telp",
								"kualifikasi", "kode", "level_karir_id", "kategori_id", "posisi", "jumlah_tenaga_kerja", "waktu_kerja_id", "pendidikan_id", "bidang_usaha_id", "umur_mulai", "umur_sampai", "tahun_pengalaman_kerja", "jenis_kelamin_id",
								"gaji_nego_flag", "gaji",
								"lokasi_kerja_kantor_flag", "lokasi_alamat", "lokasi_negara_id", "lokasi_propinsi_id", "lokasi_kabupaten_id", "lokasi_kecamatan", "lokasi_kelurahan", "lokasi_kawasan_id",
								"hits"
								);
			$arr = array("lokasi_kerja_kantor_flag" => 0, "perusahaan_id" => $OP->id);
			$arr['gaji_nego_flag'] = 0;
			foreach($_POST as $key => $val)
			{
				if(in_array($key, $include_keys) && !is_array($val))
				{
					if(!empty($val))
					{
						$arr[$key] = $this->input->post($key);
					}
				}
				$$key = $this->input->post($key);
			}
			// parse INDO date
			$arr['dt_expired'] = get_date_lang($this->input->post('dt_expired'),"EN");
			// bidang usaha will added from the perusahaan profile.
			$arr['bidang_usaha_id'] = $OP->row->bidang_usaha_id;
			// replace the kode to regenerate the lowongans added together
			$arr['kode'] = OLowongan::generate_auto_number();
			// if lokasi kerja == lokasi kantor
			if($this->input->post('lokasi_kerja_kantor_flag') == 1)
			{
				$arr['lokasi_alamat'] = $OP->row->lokasi_alamat;
				$arr['lokasi_negara_id'] = $OP->row->lokasi_negara_id;
				$arr['lokasi_propinsi_id'] = $OP->row->lokasi_propinsi_id;
				$arr['lokasi_kabupaten_id'] = $OP->row->lokasi_kabupaten_id;
				$arr['lokasi_kecamatan'] = $OP->row->lokasi_kecamatan;
				$arr['lokasi_kelurahan'] = $OP->row->lokasi_kelurahan;
				$arr['lokasi_kawasan_id'] = $OP->row->lokasi_kawasan_id;
			}

			// check duplicate in 1 day
			$check = $OP->get_lowongans(0,1,"","judul = '{$judul}' AND deskripsi = '{$deskripsi}' AND dt_added LIKE '".date('Y-m-d')."%'");
			if(!$check)
			{
				$this->session->set_flashdata("error", "Maaf, Anda tidak bisa menambahkan lowongan yang identik.");
			}
			else
			{
				$new_id = $OP->add_lowongan($arr);
				$O = new OLowongan($new_id);
				$O->update_url_title();
				$O->update_level_karirs($_POST['level_karir_id']);
				
				$this->session->set_flashdata("success", "Lowongan tersebut berhasil ditambahkan.");
			}
			unset($OU,$OP,$O);
			redirect($this->curpage);
			exit;
		}
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/form',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$OP,$O);
	}
	
	public function edit($id)
	{
		$data['current'] = "lowongan";
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		
		$data['OL'] = $OL = new OLowongan($id);
		$data['row'] = $OL->row;
		
		if(empty($OL->id) || intval($OL->row->active) <= 0 || $OL->row->perusahaan_id != $OP->id)
		{
			unset($OU,$OL,$OP);
			redirect($this->curpage);
			exit;
		}
		
		// ========= KURANG FORM VALIDASI
		$this->form_validation->set_rules('posisi', 'Posisi', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('jumlah_tenaga_kerja', 'Jumlah Tenaga Kerja', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_deskripsi_check');

		$this->form_validation->set_rules('kontak_nama', 'Kontak Nama', 'trim|required');
		$this->form_validation->set_rules('kontak_email', 'Kontak Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('kontak_telp', 'Kontak Telp', 'trim|required');

		$this->form_validation->set_error_delimiters('', '<br />');

		if ($this->form_validation->run() != FALSE)
		{
			$include_keys = array(
								"judul", "deskripsi", "dt_expired",
								"kontak_nama", "kontak_email", "kontak_telp",
								"kualifikasi", "kode", "level_karir_id", "kategori_id", "posisi", "jumlah_tenaga_kerja", "waktu_kerja_id", "pendidikan_id", "bidang_usaha_id", "umur_mulai", "umur_sampai", "tahun_pengalaman_kerja", "jenis_kelamin_id",
								"gaji_nego_flag", "gaji",
								"lokasi_kerja_kantor_flag", "lokasi_alamat", "lokasi_negara_id", "lokasi_propinsi_id", "lokasi_kabupaten_id", "lokasi_kecamatan", "lokasi_kelurahan", "lokasi_kawasan_id",
								"hits"
								);
			$arr = array("lokasi_kerja_kantor_flag" => 0);
			$arr['gaji_nego_flag'] = 0;
			foreach($_POST as $key => $val)
			{
				if(in_array($key, $include_keys) && !is_array($val))
				{
					$arr[$key] = $this->input->post($key);
				}
			}
			// parse INDO date
			$arr['dt_expired'] = get_date_lang($this->input->post('dt_expired'),"EN");
			// bidang usaha will added from the perusahaan profile.
			$arr['bidang_usaha_id'] = $OP->row->bidang_usaha_id;
			
			if($this->input->post('lokasi_kerja_kantor_flag') == 1)
			{
				$arr['lokasi_alamat'] = $OP->row->lokasi_alamat;
				$arr['lokasi_negara_id'] = $OP->row->lokasi_negara_id;
				$arr['lokasi_propinsi_id'] = $OP->row->lokasi_propinsi_id;
				$arr['lokasi_kabupaten_id'] = $OP->row->lokasi_kabupaten_id;
				$arr['lokasi_kecamatan'] = $OP->row->lokasi_kecamatan;
				$arr['lokasi_kelurahan'] = $OP->row->lokasi_kelurahan;
				$arr['lokasi_kawasan_id'] = $OP->row->lokasi_kawasan_id;
			}
			//print_r($arr); die();
			$res = $OL->edit($arr);
			$OL->refresh();
			$OL->update_url_title();
			$OL->update_level_karirs($_POST['level_karir_id']);

			$this->session->set_flashdata("success", "Lowongan tersebut berhasil diupdate.");
			unset($OU,$OP,$OL);
			redirect($this->curpage);
			exit;
		}
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/form',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$OP,$OL);
	}
	
	public function delete($id)
	{
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		
		$OL = new OLowongan($id);
		
		if(empty($OL->id) || intval($OL->row->active) <= 0 || $OL->row->perusahaan_id != $OP->id)
		{
			unset($OU,$OP);
			redirect($this->curpage);
			exit;
		}
		
		$OL->edit(array('active' => 0));
		//$OL->delete();
		
		$this->session->set_flashdata("success", "Lowongan tersebut berhasil dihapus.");
		unset($OU,$OP,$OL);
		redirect($this->curpage);
		exit;
	}

	public function lihat_rekomendasi($url_lowongan)
	{
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		$error_string = "";

		$data['OL'] = $OL = new OLowongan($url_lowongan,"url_title");
		if($OL->id == ""){ $error_string = 'Lowongan Error'; }
		// check permission
		if($OP->id != $OL->row->perusahaan_id)
		{
			$error_string = 'Anda tidak diperbolehkan melihat kandidat selain perusahaan anda sendiri!';
		}
		if(!empty($error_string))
		{
			$this->session->set_flashdata("error", $error_string);
			redirect($mainpage);
			exit;
		}
		// setup Kandidat Object
		$data['OK'] = $OK = new OKandidat;
		// get list
		$data['list'] = $OL->get_all_rekomendasi(0,0,"id ASC","lowongans.perusahaan_id = {$OP->id}");
		$data['total'] = get_db_total_rows();

		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/list_rekomendasi',$data);	
		$this->load->view($this->ffoot,$data);

		unset($OK,$OP,$OL,$data);
	}

	public function delete_rekomendasi($url_lowongan,$url_kandidat)
	{
		$mainpage = $this->curpage."/lihat_rekomendasi/".$url_lowongan;
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		$error_string = "";

		$OL = new OLowongan($url_lowongan,"url_title");
		if($OL->id == ""){ $error_string = 'Lowongan Kosong'; }

		$OK = new OKandidat($url_kandidat,"url_title");
		if($OK->id == ""){ $error_string = 'Kandidat Kosong'; }
		// check permission
		if($OP->id != $OL->row->perusahaan_id)
		{
			$error_string = 'Anda tidak diperbolehkan melihat kandidat selain perusahaan anda sendiri!';
		}

		if(!empty($error_string))
		{
			$this->session->set_flashdata("error", $error_string);
			redirect($mainpage);
			exit;
		}

		// execute
		$res = $OL->delete_rekomendasi($OK->id);
		
		unset($OP,$OK,$OL,$data);
		if($res) $this->session->set_flashdata("sucess", "Berhasil dihapus.");
		else $this->session->set_flashdata("error", "Gagal dihapus. Internal Error!");
		redirect($mainpage);
		exit;
	}

	public function set_terima_rekomendasi($url_lowongan,$url_kandidat)
	{
		$mainpage = $this->curpage."/lihat_rekomendasi/".$url_lowongan;
		$data['cu'] = $cu = $this->cu;
		$data['OP'] = $OP = $this->OP;
		$error_string = "";

		$OL = new OLowongan($url_lowongan,"url_title");
		if($OL->id == ""){ $error_string = 'Lowongan Kosong'; }

		$OK = new OKandidat($url_kandidat,"url_title");
		if($OK->id == ""){ $error_string = 'Kandidat Kosong'; }
		// check permission
		if($OP->id != $OL->row->perusahaan_id)
		{
			$error_string = 'Anda tidak diperbolehkan melihat kandidat selain perusahaan anda sendiri!';
		}

		if(!empty($error_string))
		{
			$this->session->set_flashdata("error", $error_string);
			redirect($mainpage);
			exit;
		}
		// execute
		$res = $OL->set_terima_rekomendasi($OK->id);
		
		unset($OP,$OK,$OL,$data);
		if($res) $this->session->set_flashdata("success", "Kandidat tersebut berhasil diterima.");
		else $this->session->set_flashdata("error", "Kandidat tersebut gagal diterima. Internal Error!");
		redirect($mainpage);
		exit;
	}

}

/* End of file lowongan.php */
/* Location: ./application/controllers/perusahaan/lowongan.php */