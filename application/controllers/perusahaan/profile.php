<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/perusahaan";
		$this->curpage 	= "perusahaan/profile";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "perusahaan" )
			{	
				user_logout('perusahaan');
			}
			else
			{
				if($this->cu->status != "aktif")
				{	
					user_inactive();
					exit;
				}
				// setup to the Object
				$this->OU = $OU = new OUser();
				$OU->setup($cu);
				
				$row = $OU->get_info("perusahaan");
				if(!$row)
				{
					$OU->update_info(array('email' => $OU->row->email), "perusahaan");
					$row = $OU->get_info("perusahaan");
				}
				$this->OP = $OP = new OPerusahaan();
				$OP->setup($row);
			}	
		}
		else
		{
			redirect('account/login/perusahaan');
			exit;
		}

	}
	
	public function index()
	{
		$data['current'] = "profile";
		$data['cu'] = $cu = $this->cu;
		$data['O'] = $O = $this->OP;
		$data['row'] = $O->row;
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/profile/index',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$O);
	}
	
	public function edit()
	{
		$data['current'] = "profile";
		$data['cu'] = $cu = $this->cu;
		$data['O'] = $O = $this->OP;
		$data['row'] = $O->row;

		// ========= KURANG FORM VALIDASI
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|min_length[2]');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required|callback_deskripsi_check');

		$this->form_validation->set_rules('kontak_nama', 'Kontak Nama', 'trim|required');
		$this->form_validation->set_rules('kontak_email', 'Kontak Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('kontak_telp', 'Kontak Telp', 'trim|required');

		$this->form_validation->set_error_delimiters('', '<br />');

		if ($this->form_validation->run() != FALSE)
		{
			$include_keys = array("nama", "email", "website", "logo", "jumlah_karyawan", "bidang_usaha_id", "motto", "deskripsi", "kontak_nama", "kontak_email", "kontak_telp", "lokasi_alamat", "lokasi_negara_id", "lokasi_propinsi_id", "lokasi_kabupaten_id", "lokasi_kecamatan", "lokasi_kelurahan", "lokasi_kawasan", "newsletter_flag");
			foreach($_POST as $key => $val)
			{
				if(in_array($key, $include_keys))
				{
					$arr[$key] = $this->input->post($key);
				}
			}
			foreach($_POST['image'] as $image)
			{
				$O->update_photo($image);
			}
			$res = $O->edit($arr);
			$O->refresh();
			$O->update_url_title();
			$this->session->set_flashdata("success", "Profil anda berhasil diupdate.");
			redirect($this->curpage);
			exit;
		}
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/profile/form',$data);	
		$this->load->view($this->ffoot,$data);
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/perusahaan/profile.php */