<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->vpath 	= "users/perusahaan";
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->curpage 	= "perusahaan/home";
		$this->mainpage 	= "perusahaan";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "perusahaan" )
			{	
				user_logout('perusahaan');
			}
			else
			{
				if($this->cu->status != "aktif")
				{
					user_inactive();
					exit;
				}
				// setup to the Object
				$this->OU = $OU = new OUser();
				$OU->setup($cu);
				
				$row = $OU->get_info("perusahaan");
				if(!$row)
				{
					$OU->update_info(array('email' => $OU->row->email), "perusahaan");
					$row = $OU->get_info("perusahaan");
				}
				$this->OP = $OP = new OPerusahaan();
				$OP->setup($row);
			}	
		}else
		{
			redirect('account/login/perusahaan');
		}

	}
	
	public function index()
	{
		/*echo "Login as ".$this->session->userdata('role')."<br>";
		echo "home perusahaan";
		echo "<br><a href='".site_url('logout/user/perusahaan')."'>Logout</a>";*/
		//var_dump($this->cu->status);
		
		$data['current'] = "home";
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$O = new OPerusahaan();
		$data['O'] = $O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$perpage = 5;
		
		$data['list'] = $O->get_lowongans($start,$perpage,"id DESC");
		$data['total_lowongan'] = $total = get_db_total_rows();
		$data['total_lowongan_aktif'] = $O->get_total_lowongan_aktif();
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/index',$data);	
		$this->load->view($this->ffoot,$data);
		unset($O,$OU);
	}
	
	public function logout()
	{
		unset_login_session("perusahaan");
		redirect("");
	}

}

/* End of file home.php */
/* Location: ./application/controllers/perusahaan/home.php */