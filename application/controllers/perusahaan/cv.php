<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cv extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->parent_path 	= "users/perusahaan";
		$this->vpath 	= $this->parent_path."/cv";
		$this->curpage 	= "perusahaan/cv";
		//error_reporting(-1);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "perusahaan" )
			{	
				user_logout('perusahaan');
			}
			else
			{
				if($this->cu->status != "aktif")
				{
					user_inactive();
					exit;
				}
				// setup to the Object
				$this->OU = $OU = new OUser();
				$OU->setup($cu);
				
				$row = $OU->get_info("perusahaan");
				if(!$row)
				{
					$OU->update_info(array('email' => $OU->row->email), "perusahaan");
					$row = $OU->get_info("perusahaan");
				}
				$this->OP = $OP = new OPerusahaan();
				$OP->setup($row);
			}	
		}else
		{
			redirect('account/login/perusahaan');
		}

	}
	
	public function index()
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$perpage = 5;
		if(isset($_GET['viewall'])) $perpage = 0;
		
		/*if(trim($keyword) != "")
		{
			$perpage = 0;*/
			$data['list'] = $list = $O->search_cvs($keyword,$start,$perpage,"","tandai=1",$_GET);
		/*}
		else
		{
			$data['list'] = $list = $O->get_cvs($start,$perpage,"id DESC");
		}*/
		//var_dump($this->db->last_query());
		$data['perpage'] = $perpage;
		$data['total'] = $total = get_db_total_rows();
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/index',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$O);
	}

	/*public function semua()
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$data['perpage'] = $perpage = 15;
		
		$data['list'] = $list = $O->get_cvs($start,$perpage,"id DESC");
		$data['total'] = $total = get_db_total_rows();
		
		$url = $this->curpage."/semua?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
		
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/list',$data);	
		$this->load->view($this->ffoot,$data);
		unset($OU,$O);
	}*/
	
	public function add($id)
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		$OK = new OKandidat($id, "url_title");
		
		$res = $O->tandai_cv($OK->id);
		
		if($res) $this->session->set_flashdata("success", $OK->get_nama()." berhasil ditandai.");
		else $this->session->set_flashdata("error", $OK->get_nama()." gagal ditandai.");
		unset($O,$OK);
		if(!empty($_SERVER['HTTP_REFERER'])) redirect($_SERVER['HTTP_REFERER']);
		else redirect($this->curpage);
		exit;
	}
	
	public function undang($id)
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		$OK = new OKandidat($id, "url_title");
		
		$res = $O->undang_cv($OK->id);
		
		if($res) $this->session->set_flashdata("success", $OK->get_nama()." berhasil diundang.");
		else $this->session->set_flashdata("error", $OK->get_nama()." gagal diundang.");
		unset($O,$OK);
		if(!empty($_SERVER['HTTP_REFERER'])) redirect($_SERVER['HTTP_REFERER']);
		else redirect($this->curpage);
		exit;
	}
	
	public function delete($id)
	{
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("perusahaan");
		$data['O'] = $O = new OPerusahaan();
		$O->setup($row);
		
		$OK = new OKandidat($id, "url_title");
		
		$res = $O->delete_cv($OK->id);
		
		if($res) $this->session->set_flashdata("success", "Data berhasil dihapus.");
		else $this->session->set_flashdata("error", $OK->get_nama()." gagal dihapus.");
		unset($O,$OK);

		redirect($this->curpage);
		exit;
	}
	
}

/* End of file cv.php */
/* Location: ./application/controllers/perusahaan/cv.php */