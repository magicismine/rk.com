<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_kandidat extends CI_Controller {

	var $cu;
	public function __construct()
	{
		parent::__construct();
		//error_reporting(-1);
		$this->vpath = "kandidat";
		$this->header = "header";
		$this->footer = "footer";
		$this->curpage = "user_kandidat";
		$this->cu = $cu = get_logged_in_user();
	}


	public function index()
	{
		redirect("");
	}

	public function details($id)
	{
		$cu = $this->cu;
		$data['O'] = $O = new OKandidat($id);
		if($O->id == ""){ redirect(""); exit; }
		if($cu->role == "perusahaan")
		{
			$OU = new OUser();
			$OU->setup($cu);
			$row = $OU->get_info("perusahaan");
			$data['OP'] = $OP = new OPerusahaan();
			$OP->setup($row);
		}
		
		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/detail',$data);
		$this->load->view($this->footer,$data);
		unset($O);
	}

}

/* End of file user_kandidat.php */
/* Location: ./application/controllers/user_kandidat.php */