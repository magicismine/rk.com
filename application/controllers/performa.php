<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Performa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(-1);
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->curpage 	= "performa";
	}
	
	public function index()
	{
		$data['performaNav'] = "active";
		
		/*
		1. Angker PEMULA = pendidikan terakhir (tahun lulus sekarang) + tidak ada pengalaman
		2. Angker SETELAH PHK = ada pengalaman
		3. Jumlah PERUSAHAAN = lowongans group by perusahaan
		4. Jumlah POSISI = lowongans group by posisi
		5. KEBUTUHAN Naker = jumlah tenaga yang di butuhkan per lowongan?
		6. PEMENUHAN Naker = jumlah kandidat yang di terima?
		*/
		/* SET DEFAULT VALUES */
		if(sizeof($_GET) <= 0)
		{
			$data['_GET'] = $_GET = array('lokasi_negara_id' => 1, 'lokasi_propinsi_id' => 12, 'lokasi_kabupaten_id' => 34, 'tahun' => date('Y'));
		}
		/* FILTER LOKASI */
		if(sizeof($_GET) > 0)
		{
			extract($_GET);
			$kandidat_tmp = $lowongan_tmp = NULL;
			$kandidat_where = $lowongan_where = "";
			foreach ($_GET as $key => $val)
			{
				if(empty($val)) continue;
				if(stristr($key, "negara"))
				{
					$OTmp = new OMaster_lokasi_negara($val, "id");
					if($OTmp->id):
					//$kandidat_tmp[] = "asal_propinsi_id = ".intval($OTmp->id);
					$lowongan_tmp[] = "lokasi_negara_id = ".intval($OTmp->id);
					endif;
				}
				if(stristr($key, "propinsi"))
				{
					$OTmp = new OMaster_lokasi_propinsi($val, "id");
					if($OTmp->id):
					$kandidat_tmp[] = "asal_propinsi_id = ".intval($OTmp->id);
					$lowongan_tmp[] = "lokasi_propinsi_id = ".intval($OTmp->id);
					endif;
				}
				if(stristr($key, "kabupaten"))
				{
					$OTmp = new OMaster_lokasi_kabupaten($val, "id");
					if($OTmp->id):
					$kandidat_tmp[] = "asal_kabupaten_id = ".intval($OTmp->id);
					$lowongan_tmp[] = "lokasi_kabupaten_id = ".intval($OTmp->id);
					endif;
				}
				if(stristr($key, "kawasan"))
				{
					$OTmp = new OMaster_lokasi_kabupaten($val, "id");
					if($OTmp->id):
					$kandidat_tmp[] = "asal_kawasan_id = '".$val."'";
					$lowongan_tmp[] = "lokasi_kawasan_id = '".$val."'";
					endif;
				}
				unset($OTmp);
			}
			if(count($kandidat_tmp) > 0)
			{
				$kandidat_where = " AND ".implode(" AND ", $kandidat_tmp);
			}
			if(count($lowongan_tmp) > 0)
			{
				$lowongan_where = " AND ".implode(" AND ", $lowongan_tmp);
			}
		}
		$performas = NULL;
		if(empty($tahun)) $tahun = date("Y");
		$list = OKandidat::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$kandidat_where}");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OK = new OKandidat;
			foreach ($list as $r)
			{
				$OK->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				// #1
				$last_graduate = $OK->get_pendidikan(0,1,'tahun_akhir DESC',"tahun_akhir = '".$tahun."'");
				if($last_graduate && !$pengalamans)
				{
					$performas[1][$month]++;
				}
				// #2
				$pengalamans = $OK->get_pengalaman(0,1,'tanggal_akhir ASC');
				if($pengalamans)
				{
					$performas[2][$month]++;
				}
			}
			unset($OK);
		}
		// #3
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where} GROUP BY perusahaan_id");
		//var_dump($this->db->last_query(), get_db_total_rows());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[3][$month]++;
			}
			unset($OL);
		}
		// #4
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where} GROUP BY posisi");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[4][$month]++;
			}
			unset($OL);
		}
		// #5
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where}");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[5][$month] = $performas[5][$month] + $r->jumlah_tenaga_kerja;
			}
			unset($OL);
		}
		// #6
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where}");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$n_diterima = 0;
				$q = "SELECT count(*) as total
							FROM kandidat_lowongan_lamars
							WHERE lowongan_id = ".intval($OL->id)."
								AND diterima_flag = 1
							";
				$diterima_res = $this->db->query($q);
				if(!emptyres($diterima_res))
				{
					$n_diterima = intval($diterima_res->row()->total);
				}
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[6][$month] = $performas[6][$month] + $n_diterima;
			}
			//var_dump($performas[6]);
			unset($OL);
		}


		$data['performas'] = $performas;
		$list = OKandidat::get_list(0,1,'dt_added ASC');
		if($list):
			$data['tahun_awal'] = date("Y", strtotime($list[0]->dt_added));
		endif;
		//var_dump($performas);

		$this->load->view($this->fhead, $data);
		$this->load->view('performa', $data);
		$this->load->view($this->ffoot, $data);
	}


	public function to_pdf()
	{
		
		if(sizeof($_POST) <= 0)
		{
			$data['_GET'] = $_GET = array('lokasi_negara_id' => 1, 'lokasi_propinsi_id' => 12, 'lokasi_kabupaten_id' => 34, 'tahun' => date('Y'));
		}
		// FILTER LOKASI 
		if(sizeof($_POST) > 0)
		{
			extract($_POST);
			$kandidat_tmp = $lowongan_tmp = NULL;
			$kandidat_where = $lowongan_where = "";
			foreach ($_POST as $key => $val)
			{
				if(empty($val)) continue;
				if(stristr($key, "negara"))
				{
					$OTmp = new OMaster_lokasi_negara($val, "id");
					if($OTmp->id):
					//$kandidat_tmp[] = "asal_propinsi_id = ".intval($OTmp->id);
					$lowongan_tmp[] = "lokasi_negara_id = ".intval($OTmp->id);
					endif;
				}
				if(stristr($key, "propinsi"))
				{
					$OTmp = new OMaster_lokasi_propinsi($val, "id");
					if($OTmp->id):
					$kandidat_tmp[] = "asal_propinsi_id = ".intval($OTmp->id);
					$lowongan_tmp[] = "lokasi_propinsi_id = ".intval($OTmp->id);
					endif;
				}
				if(stristr($key, "kabupaten"))
				{
					$OTmp = new OMaster_lokasi_kabupaten($val, "id");
					if($OTmp->id):
					$kandidat_tmp[] = "asal_kabupaten_id = ".intval($OTmp->id);
					$lowongan_tmp[] = "lokasi_kabupaten_id = ".intval($OTmp->id);
					endif;
				}
				if(stristr($key, "kawasan"))
				{
					$OTmp = new OMaster_lokasi_kabupaten($val, "id");
					if($OTmp->id):
					$kandidat_tmp[] = "asal_kawasan_id = '".$val."'";
					$lowongan_tmp[] = "lokasi_kawasan_id = '".$val."'";
					endif;
				}
				unset($OTmp);
			}
			if(count($kandidat_tmp) > 0)
			{
				$kandidat_where = " AND ".implode(" AND ", $kandidat_tmp);
			}
			if(count($lowongan_tmp) > 0)
			{
				$lowongan_where = " AND ".implode(" AND ", $lowongan_tmp);
			}
		}
		$performas = NULL;
		if(empty($tahun)) $tahun = date("Y");
		$list = OKandidat::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$kandidat_where}");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OK = new OKandidat;
			foreach ($list as $r)
			{
				$OK->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				// #1
				$last_graduate = $OK->get_pendidikan(0,1,'tahun_akhir DESC',"tahun_akhir = '".$tahun."'");
				if($last_graduate && !$pengalamans)
				{
					$performas[1][$month]++;
				}
				// #2
				$pengalamans = $OK->get_pengalaman(0,1,'tanggal_akhir ASC');
				if($pengalamans)
				{
					$performas[2][$month]++;
				}
			}
			unset($OK);
		}
		// #3
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where} GROUP BY perusahaan_id");
		//var_dump($this->db->last_query(), get_db_total_rows());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[3][$month]++;
			}
			unset($OL);
		}
		// #4
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where} GROUP BY posisi");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[4][$month]++;
			}
			unset($OL);
		}
		// #5
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where}");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[5][$month] = $performas[5][$month] + $r->jumlah_tenaga_kerja;
			}
			unset($OL);
		}
		// #6
		$list = OLowongan::get_list(0,0,'',"dt_added LIKE '".$tahun."%' {$lowongan_where}");
		//var_dump($this->db->last_query());
		if($list)
		{
			$OL = new OLowongan;
			foreach ($list as $r)
			{
				$OL->setup($r);
				$n_diterima = 0;
				$q = "SELECT count(*) as total
							FROM kandidat_lowongan_lamars
							WHERE lowongan_id = ".intval($OL->id)."
								AND diterima_flag = 1
							";
				$diterima_res = $this->db->query($q);
				if(!emptyres($diterima_res))
				{
					$n_diterima = intval($diterima_res->row()->total);
				}
				$month = intval(date("n", strtotime($r->dt_added)));
				$performas[6][$month] = $performas[6][$month] + $n_diterima;
			}
			//var_dump($performas[6]);
			unset($OL);
		}

		//die(var_dump($_POST));


		$data['performas'] = $performas;
		$list = OKandidat::get_list(0,1,'dt_added ASC');
		if($list):
			$data['tahun_awal'] = date("Y", strtotime($list[0]->dt_added));
		endif;
		

		
		$html = $this->load->view('tpl_performa',$data,TRUE);
        $this->_gen_pdf($html,'A4');
        
        /*$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/detail',$data);
		$this->load->view($this->footer,$data);*/
		//unset($O);
	}

	private function _gen_pdf($html,$paper='A4')
    {      
        $mpdf = new mPDF('utf-8',$paper);
        $stylesheet = file_get_contents(base_url('_assets/css/style.css'));
		$mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html);
        $mpdf->Output("Performa".date('Ymd').".pdf",'D'); //D
    }

}

/* End of file performa.php */
/* Location: ./application/controllers/performa.php */