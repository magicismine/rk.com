<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function index()
	{

		$get_params = get_params($_GET, array("page"));
		$page = (empty($_GET['page']) ? 0 : $_GET['page']);
		$perpage = 10;

		$data['O'] = new OArtikel;
		$data['list_artikel'] = OArtikel::get_list($page,$perpage,"id DESC");
		$total = get_db_total_rows();
		$url = "artikel?".$get_params;
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

		$this->load->view('header',$data);
		$this->load->view('artikel_listing',$data);
		$this->load->view('footer',$data);
	}

	public function detail($url_title)
	{
		$OA = $data['OA'] = new OArtikel($url_title,"url_title");

		$this->load->view('header',$data);
		$this->load->view('artikel_detail',$data);	
		$this->load->view('footer',$data);

		unset($OA);
	}

}

/* End of file artikel.php */
/* Location: ./application/controllers/artikel.php */