<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tmp extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}
	
	public function image_constructor()
	{
		$this->load->helper(array('directory'));
		$map = directory_map('./_assets/images/temp/', 1);
		//echo "<pre>";
		//var_dump($map);
		//echo "</pre>";
		
		if(count($map) > 0)
		{
			//$arr = NULL;
			$i = 0;
			foreach($map as $key => $val)
			{
				if(stristr($val, "resize_") === FALSE)
				{
					//$arr[] = $val;
					$filename = trim($val);
					auto_resize_photo($filename);
					echo "{$i}. Image {$filename} has been re-construction.<br />";
					sleep(1);
					$i++;
				}
			}
		}
		
		/*
		echo "<pre>";
		var_dump($arr);
		echo "</pre>";
		*/
	}

	public function rebuild_images($table_name, $field="photo")
	{
		if(empty($table_name))
		{
			$this->image_constructor();
		}
		else {
			$sql = "SELECT SQL_CALC_FOUND_ROWS {$field} as photo FROM {$table_name};";
			$query = $this->db->query($sql);
			if(emptyres($query)) die("No data found!");
			$res = $query->result();
			$n = get_db_total_rows();
			//var_dump($this->db->last_query());
			echo "<p>The images are going to be re-construction: <strong>".$n."</strong></p>";
			
			// ngetest
			/*
			$n = 0;
			foreach($res as $r)
			{
				$filename = trim($r->photo);
				$n++;
				$fpath = $_SERVER['DOCUMENT_ROOT']."/_assets/images/temp/";
				$size = filesize($fpath.$filename);
				$format_size = format_size($size);
				echo "{$n}. {$filename} ~ {$format_size}<br />";
				if($n==10000) { break; }
			}
			echo "<hr />";
			//*/
			
			// EXEC
			//*
			$i = 1;
			foreach($res as $r)
			{
				$filename = trim($r->photo);
				$fpath = $_SERVER['DOCUMENT_ROOT']."/".FPATH."_assets/images/temp/";
				$size = filesize($fpath.$filename);
				$format_size = format_size($size);
				if(is_file($fpath.$filename))
				{
					auto_resize_photo($filename);
					echo "{$i}. Image {$filename} ({$format_size}) has been re-build.<br />";
					sleep(1);
				}
				else {
					echo "{$i}. Image {$filename} ({$format_size}) has NOT been re-build.<br />";
				}
				$i++;
			}
			//*/
			echo "<p><strong>DONE</strong></p>";
		}
	}

	public function paypal_test($custom)
	{
		if($custom == "") return FALSE;
		else{
		$this->load->library('OCart');
		$q = "SELECT * FROM carts WHERE code = ? LIMIT 1";
		$res = $this->db->query($q,array($custom));
		$r = $res->row();

		$dc = new OCart();
		$dc->setup($r);
		$dc->process_order("PAYPAL",6);
		var_dump($dc->process_order("PAYPAL",6));
		}
	}
	
	public function generate_url_title($table="")
	{
		if(empty($table)) die("Please add table param.");
		if($table == "master_lokasi_kabupatens")
		{
			$list = OMaster_lokasi_kabupaten::get_list();
			$O = new OMaster_lokasi_kabupaten();
		}
		foreach($list as $r)
		{
			$O->setup($r);
			$O->update_url_title();
		}
		unset($O);
		echo "DONE";
	}

	public function test_local_email()
	{
		$to = "emaildua@localhost";
		$subject = "Hello World";
		$content = "Just want to testing more..";
		$from_name = "Email Satu";
		$from_mail = "emailsatu@localhost";
		//send_mail($from_name,$from_mail,$to,$subject,$content);
		noreply_mail($to,$subject,$content);

		/*
		//$kirim_email = mail($to,$subject,$content,"from: emailsatu@localhost");

		if($kirim_email)
		{
			echo "email sent";
		}
		else
		{
			echo "email failed sent";
		}*/
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */