<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	var $curpage;
	var $cu;
	public function __construct()
	{
		parent::__construct();	
		$this->curpage = "search";
		$this->cu = $cu = get_logged_in_user();

	}
	
	public function index()
	{
		$data['nav'] = "home";
		$OA = $data['OA'] = new OArtikel();
		$OLow = $data['OLow'] = new OLowongan();

		$this->load->view('header',$data);
		$this->load->view('index',$data);
		$this->load->view('footer',$data);

		unset($OA,$OLow);	
	}

	public function ajax($ajax_file,$page = 0)
	{
		if($ajax_file == "lowongan_pekerjaan")
		{
			//get kategori, bidang usaha, lokasi
			$data['OMK'] = $OMK = new OMaster_kategori();
			$data['OMBU'] = $OMBU = new OMaster_bidang_usaha();
			$data['OMLKab'] = $OMLKab = new OMaster_lokasi_kabupaten();

			$this->load->view('tpl_lowongan_kerja',$data);
			unset($OMK,$OMBU,$OMLKab,$data);
		}
		elseif ($ajax_file == "kandidat") 
		{
			$perpage = 9;
			// get all kandidat in users.status = aktif
			//$list = OKandidat::get_list(intval($page),$perpage,"id DESC");
			/*
			$this->db->select('kandidats.*')
					->from('kandidats')
					->join('users', 'users.id = kandidats.user_id','inner')
					->where(array('users.status' => 'aktif'));
			$total = $this->db->count_all_results();
			*/
			$this->db->select('SQL_CALC_FOUND_ROWS kandidats.*', FALSE)
					->from('kandidats')
					->join('users', 'users.id = kandidats.user_id','inner')
					->where(array('users.status' => 'aktif'))
					->limit($perpage,intval($page));
			$query = $this->db->get();
			//var_dump($this->db->last_query());
			$list = $query->result();
			$total = get_db_total_rows();
			//var_dump($total);
			$url = 'home/ajax/kandidat/';
			$pagination = genPagination($total, $perpage, $url, 4, 2, TRUE);
			$content = $this->load->view('tpl_kandidat',array('list' => $list, 'pagination' => $pagination),TRUE);
			echo $content;
			die;
		}
		elseif ($ajax_file == "list_perusahaan") 
		{
			// get all perusahaan in users.status = aktif
			$perpage = 15;
			//$list = OPerusahaan::get_list(intval($page),$perpage);
			/*
			$this->db->select('SQL_CALC_FOUND_ROWS perusahaans.*', FALSE)
					->from('perusahaans')
					->join('users', 'users.id = perusahaans.user_id','inner')
					->where(array('users.status' => 'aktif'))
					->order_by('perusahaans.ordering ASC, perusahaans.id DESC')
					->limit($perpage,intval($page));
			$query = $this->db->get();
			*/
			$q = "(
					SELECT perusahaans.* FROM perusahaans
					INNER JOIN users ON perusahaans.user_id = users.id
					WHERE users.status='aktif' AND perusahaans.logo <> ''
					ORDER BY perusahaans.ordering ASC, perusahaans.id DESC
				)
				UNION
				(
					SELECT perusahaans.* FROM perusahaans
					INNER JOIN users ON perusahaans.user_id = users.id
					WHERE users.status='aktif' AND perusahaans.logo = ''
					ORDER BY perusahaans.ordering ASC, perusahaans.id DESC
				)
				LIMIT ".intval($page).", ".intval($perpage)."
				";
			$query = $this->db->query($q);
			//var_dump($this->db->last_query());
			$list = $query->result();
			$total = count($list);
			//$total = get_db_total_rows();
			//var_dump($total);
			echo $this->load->view('tpl_list_perusahaan', array('list' => $list), TRUE);
			die();
		}else
		{
			//
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */