<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "kandidat/home";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();

		if($this->cu != FALSE)
		{
			if($this->cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}else
			{
				if($this->cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}else
		{
			redirect('account/login/kandidat');
		}
		
	}
	
	public function index()
	{
		$data['current'] = "home";
		$data['cu'] = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("kandidat");
		$O = new OKandidat();
		$data['O'] = $O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$perpage = 5;
		
		$data['list_perusahaan'] = $O->get_perusahaan_lihats($start,$perpage,"id DESC");
		$data['total_perusahaan'] = $total = get_db_total_rows();
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
				
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/index',$data);	
		$this->load->view($this->ffoot,$data);
	}
	
	public function perusahaan_lihat()
	{
		$data['current'] = "home";
		$data['cu'] = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("kandidat");
		$O = new OKandidat();
		$data['O'] = $O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$perpage = 0;
		
		$data['list_perusahaan'] = $O->get_perusahaan_lihats($start,$perpage,"id DESC");
		$data['total_perusahaan'] = $total = get_db_total_rows();
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
				
		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/perusahaan_lihat',$data);	
		$this->load->view($this->ffoot,$data);
	}

	public function logout()
	{
		unset_login_session("kandidat");
		redirect("");
	}

}

/* End of file home.php */
/* Location: ./application/controllers/kandidat/home.php */