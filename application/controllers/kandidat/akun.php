<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Akun extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "users/akun";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();

		if($this->cu != FALSE)
		{
			if($this->cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}else
			{
				if($this->cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}else
		{
			redirect('account/login/kandidat');
		}
	}
	
	public function index()
	{
		$data['current'] = "akun";
		$data['cu'] = $this->cu;
		$data['OUser'] = $OUser = new OUser($this->cu->id);
		$data['OKandidat'] = $OKandidat = new OKandidat($OUser->id,"user_id");

		if(sizeof($_POST) > 0)
		{
			extract($_POST);
			$arr['email'] = $email;
			$arr2['newsletter_flag'] = $newsletter_flag;
			$arr2['izin_lihat_cv_flag'] = $izin_lihat_cv_flag;
			$arr2['izin_lihat_dokumen_flag'] = $izin_lihat_dokumen_flag;

			if($password != ""){ $arr['password'] = $password;}
			if($conf_email != "") 
			{ 
				if($conf_email == $email)
				{
					$arr['email'] = $email;	
				}
				else
				{
					$this->session->set_flashdata("error", "Email dan Email Confirmation tidak sama.");
				}
				
			}
			if($conf_password != "")
			{ 
				if($conf_password == $password)
				{
					$arr['password'] = md5(md5($password));	
				}else
				{
					$this->session->set_flashdata("error", "Password dan Password Confirmation tidak sama");
				}
				
			}

			$res = $OUser->edit($arr);
			$res2 = $OKandidat->edit($arr2);	
			if($res) $this->session->set_flashdata("success", "Akun anda berhasil diupdate.");
			else{ $this->session->set_flashdata("error", "Akun anda tidak berhasil diupdate.");}
			redirect("kandidat/akun");
			exit();

		}

		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/pengaturan_akun/index',$data);	
		$this->load->view('footer',$data);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/kandidat/home.php */