<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resume extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "kandidat/resume";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}else
			{
				if($this->cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}else
		{
			redirect('account/login/kandidat');
		}
	}
	
	public function index()
	{
		$id = $this->cu->id;
		$O = $data['O'] = new OKandidat_resume($id,"kandidat_id");
		if($O->row == TRUE){ redirect("kandidat/resume/edit/".$O->id); }
		
		$this->form_validation->set_rules('judul','Judul','trim|required');
		$this->form_validation->set_rules('isi','Isi','trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-red">','</p>');
		if($this->form_validation->run() == TRUE)
		{
			extract($_POST);
			$arr = array
			(
				'kandidat_id' => $id,
				'judul'	 => $judul,
				'isi' => $isi
			);
			$res = OKandidat_resume::add($arr);
			$O->update_url_title();
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage);
            exit();

		}
		$data['current'] = "resume";
		
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/resume/index',$data);	
		$this->load->view('footer',$data);
		unset($O);
	}

	public function edit($id)
	{
		if($id == ""){ redirect($this->curpage);}
		$O = $data['O'] = new OKandidat_resume($id);
		if($O->id == ""){ redirect($this->curpage);}
		if($O->row->kandidat_id != $this->cu->id){ redirect($this->curpage."edit/".$O->id);}

		$this->form_validation->set_rules('judul','Judul','trim|required');
		$this->form_validation->set_rules('isi','Isi','trim|required');
		$this->form_validation->set_error_delimiters('<p class="text-red">','</p>');

		if($this->form_validation->run() == TRUE)
		{
			extract($_POST);
			$arr = array
				(
					'judul' => $judul,
					'isi' => $isi
				);
			$res = $O->edit($arr);
			$O->refresh();
            $O->update_url_title();
			if($res) warning("edit", "success");
            else warning("edit", "fail");
            redirect($this->curpage);
            exit();

		}

		$data['current'] = "resume";
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/resume/index',$data);	
		$this->load->view('footer',$data);
		unset($O);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/kandidat/home.php */