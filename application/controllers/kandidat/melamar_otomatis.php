<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Melamar_otomatis extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "kandidat/melamar_otomatis";
		$this->mainpage = "kandidat";
		//error_reporting(-1);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}else
			{
				if($this->cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}else
		{
			redirect('account/login/kandidat');
		}
	}
	
	public function index()
	{
		$id = $this->cu->id;
		$O = $data['O'] = new OKandidat($id,"user_id");

		if(empty($O->id)) { redirect($this->mainpage); exit; }

		if(sizeof($_POST) > 0)
		{
			extract($_POST);
			// kategori, lokasi, waktu_kerja, status_kerja, email_alert_active_flag
			$arr = array(
						'kategori_ids' => implode(',', $kategori).'',
						'lokasi_ids' => implode(',', $lokasi).'',
						'waktu_kerja_ids' => implode(',', $waktu_kerja).'',
						'status_kerja_ids' => implode(',', $status_kerja).'',
						'email_alert_active_flag' => $email_alert_active_flag.''
						);
			$O->set_auto_lamar($arr);
			redirect($this->curpage);
			exit;
		}

		$data['current'] = "melamar_otomatis";
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/melamar_otomatis/index',$data);	
		$this->load->view('footer',$data);
	}

}

/* End of file melamar_otomatis.php */
/* Location: ./application/controllers/kandidat/melamar_otomatis.php */