<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Curriculum_vitae extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "kandidat/curriculum_vitae";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();
		if($cu)
		{
			if($cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}
			else
			{
				if($cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}
		else
		{
			redirect('account/login/kandidat');
		}
	}
	
	public function index()
	{
		$id = $this->cu->id;
		$O = $data['O'] = new OKandidat($id,"user_id");

		$data['current'] = "curriculum_vitae";
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/curriculum_vitae/index',$data);	
		$this->load->view('footer',$data);
	}

	public function edit()
	{
		$id = $this->cu->id;
		$O = $data['O'] = new OKandidat($id,"user_id");

		extract($_POST);

		if($submit_identitas)
		{
			$arr = NULL;
			$exclude_arr = array("submit_identitas","url_title","");

			foreach($_POST as $key => $val)
			{
				if(!in_array($key, $exclude_arr) && !is_array($val))
				{	
					$arr[$key] = trim($val);
				}
			}
			$arr['tanggal_lahir'] = get_date_lang($tanggal_lahir,"EN");
			// check if alamat sekarang = alamat asal
			if(isset($skrg_asal_flag) && intval($skrg_asal_flag) == 1)
			{
				$arr['skrg_alamat'] = $asal_alamat;
				$arr['skrg_rt'] = $asal_rt;
				$arr['skrg_rw'] = $asal_rw;
				$arr['skrg_propinsi_id'] = $asal_propinsi_id;
				$arr['skrg_kabupaten_id'] = $asal_kabupaten_id;
				$arr['skrg_kawasan_id'] = $asal_kawasan_id;
				$arr['skrg_kecamatan'] = $asal_kecamatan;
				$arr['skrg_kelurahan'] = $asal_kelurahan;
				$arr['skrg_telepon'] = $asal_telepon;
			}
			$res = $O->edit($arr);
			foreach($_POST['image'] as $image)
			{
				$O->update_photo($image);
			}
			
			//var_dump($hah);
			
			$O->refresh();
			$O->update_url_title();
			redirect($this->curpage."/edit#identitas");
			exit;
		}
		elseif($submit_kesehatan)
		{
			$arr = NULL;
			$exclude_arr = array("submit_kesehatan","url_title","");
			$arr['kacamata_flag'] = 0;
            
			foreach($_POST as $key => $val)
			{
				if(!in_array($key, $exclude_arr) && !is_array($val))
				{	
					$arr[$key] = trim($val);
				}
			}
			$res = $O->edit_kesehatan($arr);
			redirect($this->curpage."/edit#kesehatan");
			exit;
		}
		elseif($submit_keluarga)
		{	
			// update keluarga
			
			$arr = NULL;
			$include_arr = array("status_kawin_id","nama_pasangan");
			foreach($_POST as $key => $val)
			{
				if(in_array($key, $include_arr) && !is_array($val))
				{	
					$arr[$key] = trim($val);
				}
			}
  		$O->edit($arr);
			
			foreach($keluarga_nama as $key => $val)
			{
				// check if empty
				if(empty($val)) continue;
				// check if duplicate
				$check = $O->get_keluarga(0,0,'',"nama = ".intval($keluarga_nama[$key])."");
				if(!emptyres($check)) continue;

				$keluarga_arr = array();
				$keluarga_arr['nama'] = $keluarga_nama[$key];
				$keluarga_arr['jenis_kelamin_id'] = $keluarga_jenis_kelamin_id[$key];
				$keluarga_arr['shdk_id'] = $keluarga_shdk_id[$key];
				$keluarga_arr['pendidikan_id'] = $keluarga_pendidikan_id[$key];
				$O->set_keluarga($keluarga_arr);
			}
			// edit keluarga
			foreach($edit_keluarga_nama as $key => $val)
			{
				if($edit_keluarga_delete[$key] == 1)
				{
					$O->delete_keluarga($key);
					continue;
				}
				$keluarga_arr = array();
				$keluarga_arr['nama'] = $edit_keluarga_nama[$key];
				$keluarga_arr['jenis_kelamin_id'] = $edit_keluarga_jenis_kelamin_id[$key];
				$keluarga_arr['shdk_id'] = $edit_keluarga_shdk_id[$key];
				$keluarga_arr['pendidikan_id'] = $edit_keluarga_pendidikan_id[$key];
				$O->edit_keluarga($key, $keluarga_arr);
			}
			redirect($this->curpage."/edit#keluarga");
			exit;
		}
		elseif($submit_pendidikan)
		{
			foreach($pendidikan_nama as $key => $val)
			{
				// check if empty
				if(empty($val)) continue;
				// check if duplicate
				$check = $O->get_pendidikan(0,0,'',"pendidikan_id = ".intval($pendidikan_id[$key])."");
				if(!emptyres($check)) continue;

				$pendidikan_arr = array();
				$pendidikan_arr['pendidikan_id'] = $pendidikan_id[$key];
				$pendidikan_arr['tahun_awal'] = $pendidikan_tahun_awal[$key];
				$pendidikan_arr['tahun_akhir'] = $pendidikan_tahun_akhir[$key];
				$pendidikan_arr['nama'] = $pendidikan_nama[$key];
				$pendidikan_arr['jurusan'] = $pendidikan_jurusan[$key];
				$pendidikan_arr['nilai'] = $pendidikan_nilai[$key];
				$O->set_pendidikan($pendidikan_arr);
			}
			// edit pendidikan
			foreach($edit_pendidikan_nama as $key => $val)
			{
				if($edit_pendidikan_delete[$key] == 1)
				{
					$O->delete_pendidikan($key);
					continue;
				}
				$pendidikan_arr = array();
				$pendidikan_arr['pendidikan_id'] = $edit_pendidikan_id[$key];
				$pendidikan_arr['tahun_awal'] = $edit_pendidikan_tahun_awal[$key];
				$pendidikan_arr['tahun_akhir'] = $edit_pendidikan_tahun_akhir[$key];
				$pendidikan_arr['nama'] = $edit_pendidikan_nama[$key];
				$pendidikan_arr['jurusan'] = $edit_pendidikan_jurusan[$key];
				$pendidikan_arr['nilai'] = $edit_pendidikan_nilai[$key];
				$O->edit_pendidikan($key, $pendidikan_arr);
			}
			redirect($this->curpage."/edit#pendidikan");
			exit;
		}
		elseif($submit_pengalaman_kerja)
		{
			// update pengalaman kerja
			foreach($pengalaman_perusahaan as $key => $val)
			{
				if(empty($val)) continue;
				$pengalaman_arr = array();
				if(strtotime($pengalaman_tanggal_awal[$key]) > strtotime($pengalaman_tanggal_akhir[$key]))
				{
					$tmp_awal = $pengalaman_tanggal_awal[$key];
					$tmp_akhir = $pengalaman_tanggal_akhir[$key];
					$pengalaman_tanggal_awal[$key] = $tmp_akhir;
					$pengalaman_tanggal_akhir[$key] = $tmp_awal;
				}
				$pengalaman_arr['tanggal_awal'] = get_date_lang($pengalaman_tanggal_awal[$key],"EN");
				$pengalaman_arr['tanggal_akhir'] = get_date_lang($pengalaman_tanggal_akhir[$key],"EN");
				$pengalaman_arr['perusahaan'] = $pengalaman_perusahaan[$key];
				$pengalaman_arr['bidang_usaha_id'] = $pengalaman_bidang_usaha_id[$key];
				$pengalaman_arr['jabatan'] = $pengalaman_jabatan[$key];
				$O->set_pengalaman($pengalaman_arr);
			}
			// edit pengalaman kerja
			foreach($edit_pengalaman_perusahaan as $key => $val)
			{
				if($edit_pengalaman_delete[$key] == 1)
				{
					$O->delete_pengalaman($key);
					continue;
				}
				$pengalaman_arr = array();
				if(strtotime($edit_pengalaman_tanggal_awal[$key]) > strtotime($edit_pengalaman_tanggal_akhir[$key]))
				{
					$tmp_awal = $edit_pengalaman_tanggal_awal[$key];
					$tmp_akhir = $edit_pengalaman_tanggal_akhir[$key];
					$edit_pengalaman_tanggal_awal[$key] = $tmp_akhir;
					$edit_pengalaman_tanggal_akhir[$key] = $tmp_awal;
				}
				$pengalaman_arr['tanggal_awal'] = get_date_lang($edit_pengalaman_tanggal_awal[$key],"EN");
				$pengalaman_arr['tanggal_akhir'] = get_date_lang($edit_pengalaman_tanggal_akhir[$key],"EN");
				$pengalaman_arr['perusahaan'] = $edit_pengalaman_perusahaan[$key];
				$pengalaman_arr['bidang_usaha_id'] = $edit_pengalaman_bidang_usaha_id[$key];
				$pengalaman_arr['jabatan'] = $edit_pengalaman_jabatan[$key];
				$O->edit_pengalaman($key, $pengalaman_arr);
			}
			redirect($this->curpage."/edit#pengalamankerja");
			exit;

		}
		elseif($submit_keterampilan)
		{
			// update keterampilan
			foreach($keterampilan_kategori_id as $key => $val)
			{
				// check if empty
				if(empty($val) || empty($keterampilan_nilai_skala[$key])) continue;
				// check if duplicate
				$check = $O->get_keterampilan(0,0,'',"kategori_id = ".intval($keterampilan_kategori_id[$key])."");
				if(!emptyres($check)) continue;

				$keterampilan_arr = array();
				$keterampilan_arr['kategori_id'] = $keterampilan_kategori_id[$key];
				$keterampilan_arr['nilai_skala'] = $keterampilan_nilai_skala[$key];
				$keterampilan_arr['sertifikasi'] = $keterampilan_sertifikasi[$key];
				$O->set_keterampilan($keterampilan_arr);
			}
			// edit keterampilan
			foreach($edit_keterampilan_kategori_id as $key => $val)
			{
				if($edit_keterampilan_delete[$key] == 1)
				{
					$O->delete_keterampilan($key);
					continue;
				}
				$keterampilan_arr = array();
				$keterampilan_arr['kategori_id'] = $edit_keterampilan_kategori_id[$key];
				$keterampilan_arr['nilai_skala'] = $edit_keterampilan_nilai_skala[$key];
				$keterampilan_arr['sertifikasi'] = $edit_keterampilan_sertifikasi[$key];
				$O->edit_keterampilan($key, $keterampilan_arr);
			}
			redirect($this->curpage."/edit#keterampilan");
			exit;
		}
		elseif($submit_bahasa)
		{
			// update bahasa
			foreach($bahasa_id as $key => $val)
			{
				// check if empty
				if(empty($val) || empty($bahasa_nilai_skala[$key])) continue;
				// check if duplicate
				$check = $O->get_bahasa(0,0,'',"bahasa_id = ".intval($bahasa_id[$key])."");
				if(!emptyres($check)) continue;

				$bahasa_arr = array();
				$bahasa_arr['bahasa_id'] = $bahasa_id[$key];
				$bahasa_arr['nilai_skala'] = $bahasa_nilai_skala[$key];
				$bahasa_arr['sertifikasi'] = $bahasa_sertifikasi[$key];
				$O->set_bahasa($bahasa_arr);
			}
			// edit bahasa
			foreach($edit_bahasa_id as $key => $val)
			{
				if($edit_bahasa_delete[$key] == 1)
				{
					$O->delete_bahasa($key);
					continue;
				}
				$bahasa_arr = array();
				$bahasa_arr['bahasa_id'] = $edit_bahasa_id[$key];
				$bahasa_arr['nilai_skala'] = $edit_bahasa_nilai_skala[$key];
				$bahasa_arr['sertifikasi'] = $edit_bahasa_sertifikasi[$key];
				$O->edit_bahasa($key, $bahasa_arr);
			}
			redirect($this->curpage."/edit#bahasa");
			exit;
		}
		elseif($submit_dokumen)
		{
			// update dokumens
			$O->set_dokumens($_FILES['dokumen'],array(1));
			// edit dokumens
			$O->edit_dokumens($_FILES['edit_dokumen'],$edit_dokumen_aktif_flag);
			foreach($edit_dokumen_aktif_flag as $key => $val)
			{
				if($edit_dokumen_delete[$key] == 1)
				{
					$O->delete_dokumen($key);
					continue;
				}
				$dokumen_arr = array();
				$dokumen_arr['aktif_flag'] = $edit_dokumen_aktif_flag[$key];
				$O->edit_dokumen($key, $dokumen_arr);
			}
			//handle delete if checked
			foreach($edit_dokumen_delete as $key => $val)
			{
				if($edit_dokumen_delete[$key] == 1)
				{
					$O->delete_dokumen($key);
					continue;
				}
			}
			redirect($this->curpage."/edit#uploadcv");
			exit;
		}

		$data['current'] = "curriculum_vitae";
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/curriculum_vitae/edit',$data);	
		$this->load->view('footer',$data);
	}

	public function ajax($action)	
	{
		$cu = $this->cu;
		if(!$cu) die('RESTRICTED!');
		$id = $cu->id;
		$O = $data['O'] = new OKandidat($id,"user_id");
		
		if($action == "get_identitas")
		{	
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_identitas',$data);
		}
		elseif($action == "get_kesehatan")
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_kesehatan',$data);
		}
		elseif ($action == "get_keluarga") 
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_keluarga',$data);	
		}
		elseif ($action == "get_pendidikan") 
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_pendidikan',$data);
		}
		elseif ($action == "get_pengalaman_kerja") 
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_pengalaman_kerja',$data);
		}
		elseif ($action == "get_keterampilan") 
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_keterampilan',$data);
		}
		elseif($action == "get_bahasa")
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_bahasa',$data);
		}
		elseif($action  == "get_dokumen")
		{
			$this->load->view($this->vpath.'/curriculum_vitae/tpl_dokumen',$data);
		}
		else
		{
			die("You cannot access this page.");
		}
	}

	public function set_dokumen($active = 1,$id)
	{
		$user_id = $this->cu->id;
		$OK = new OKandidat($user_id,"user_id");
		$kandidat_id = $OK->row->id;

		$O = new OKandidat_dokumen($id);

       	if($O->row->kandidat_id != $kandidat_id){ die("Tidak diperbolehkan mengakses halaman lain.");}      
       	$arr['aktif_flag'] = $active;
       	$res = $O->edit($arr);
       	if($res)
       	{
       		redirect($this->curpage."/edit#uploadcv");
			exit();	
       	}else
       	{
       		die("Gagal");
       	}
       	unset($O,$OK);
		
	}

}

/* End of file home.php */
/* Location: ./application/controllers/kandidat/home.php */