<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lowongan_terbaru extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "kandidat/lowongan_terbaru";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();

		if($this->cu != FALSE)
		{
			if($this->cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}else
			{
				if($this->cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}else
		{
			redirect('account/login/kandidat');
		}
	}
	
	public function index()
	{
		$data['current'] = "lowongan_terbaru";
		$data['cu'] = $cu = $this->cu;
		$OU = new OUser();
		$OU->setup($cu);
		
		$data['row'] = $row = $OU->get_info("kandidat");
		$O = new OKandidat();
		$data['O'] = $O->setup($row);
		
		extract($_GET);
		$page = intval($page);
		$perpage = 20;

		if(sizeof($_GET) > 0)
		{
			if($kategori_id != "")
			{
				$add_sql_arr[] = "kategori_id = {$kategori_id}";
				$data['kategori_id'] = $kategori_id;
			}
			if($bidang_usaha_id != "")
			{
				$add_sql_arr[] = "bidang_usaha_id = {$bidang_usaha_id}";
				$data['bidang_usaha_id'] = $bidang_usaha_id;
			}
			if($pendidikan_id != "")
			{
				$add_sql_arr[] = "pendidikan_id = {$pendidikan_id}";	
				$data['pendidikan_id'] = $pendidikan_id;
			}
			if($level_karir_id != "")
			{
				$add_sql_arr[] = "level_karir_id = {$level_karir_id}";
				$data['level_karir_id'] = $level_karir_id;
			}
			if($waktu_kerja_id != "")
			{
				$add_sql_arr[] = "waktu_kerja_id = {$waktu_kerja_id}";
				$data['waktu_kerja_id'] = $waktu_kerja_id;
			}
			if($lokasi_negara_id != "")
			{
				$add_sql_arr[] = "lokasi_negara_id = {$lokasi_negara_id}";
				$data['lokasi_negara_id'] = $lokasi_negara_id;
			}
			if($lokasi_propinsi_id != "")
			{
				$add_sql_arr[] = "lokasi_propinsi_id = {$lokasi_propinsi_id}";
				$data['lokasi_propinsi_id'] = $lokasi_propinsi_id;
			}
			if($lokasi_kabupaten_id != "")
			{
				$add_sql_arr[] = "lokasi_kabupaten_id = {$lokasi_kabupaten_id}";
				$data['lokasi_kabupaten_id'] = $lokasi_kabupaten_id;
			}
			if($keywords != "")
			{
				$add_sql_arr[] = "posisi LIKE '%{$keywords}%'";
				$data['keywords'] = $keywords;
			}
			$add_sql_arr[] = "dt_expired >= NOW()";
			$add_sql_arr[] = "active = 1";

			$add_sql_params .= implode(" AND ", $add_sql_arr);

			//$data['list'] = $O->get_lowongan_simpans($start,$perpage,"id DESC","{$add_sql_params}");
			$data['list'] = OLowongan::get_list($start,$perpage,"id DESC","{$add_sql_params}");
		}
		else
		{
			$data['list'] =  OLowongan::get_list($start,$perpage,"lowongans.id DESC","dt_expired >= NOW()");
		}
		//var_dump($this->db->last_query());

		$data['total'] = $total = get_db_total_rows();
		
		
		$url = $this->curpage."/index?".get_params($_GET, array("page"));
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);
				

		$this->load->view($this->fhead,$data);
		$this->load->view($this->vpath.'/lowongan/terbaru',$data);	
		$this->load->view($this->ffoot,$data);
	}

	public function simpan($url_title)
	{	
		$OKandidat = new OKandidat($this->cu->id,"user_id");
		$OLowongan = new OLowongan($url_title,"url_title");
		$simpan = $OKandidat->set_lowongan_simpan($OLowongan->id);

		if($OKandidat->id == "" || $OLowongan->id == "" || !$simpan)
		{
			$this->session->set_flashdata("error", "Lowongan gagal disimpan.");
		}
		
		if($simpan)
		{
			$this->session->set_flashdata("success", "Lowongan berhasil disimpan.");
		}
		
		redirect($OLowongan->get_link());
		unset($OKandidat,$OLowongan);
		exit;
	}

	public function hapus($url_title)
	{	
		$OKandidat = new OKandidat($this->cu->id,"user_id");
		$OLowongan = new OLowongan($url_title,"url_title");
		$simpan = $OKandidat->delete_lowongan_simpan($OLowongan->id);

		if($OKandidat->id == "" || $OLowongan->id == "" || !$simpan)
		{
			$this->session->set_flashdata("error", "Lowongan gagal dihapus.");
		}
		
		if($simpan)
		{
			$this->session->set_flashdata("success", "Lowongan berhasil dihapus.");
		}
		
		redirect($this->curpage);
		unset($OKandidat,$OLowongan);
		exit;
	}

}

/* End of file lowongan_terbaru.php */
/* Location: ./application/controllers/kandidat/lowongan_terbaru.php */