<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Export extends CI_Controller {

	var $cu;
	public function __construct()
	{
		parent::__construct();
		//error_reporting(-1);
		/*$this->vpath = "kandidat";
		$this->header = "header";
		$this->footer = "footer";
		$this->curpage = "user_kandidat";*/
		$this->vpath = "kandidat";
		$this->cu = $cu = get_logged_in_user();
		$this->load->library('parser');
	}

	public function index()
	{
		$cu = $this->cu;
		$O = $data['O'] = new OKandidat($cu->id,"user_id");
		if($O->id == ""){ die('Export Failed'); exit; }
		$OU = new OUser();
		$OU->setup($cu);
		$row = $OU->get_info("perusahaan");
		$data['OP'] = $OP = new OPerusahaan();
		$OP->setup($row);

		echo $this->load->view($this->vpath.'/to_pdf',$data,TRUE);
		//die('error');
	}
	
	public function to_pdf()
	{
		
		$cu = $this->cu;
		$O = $data['O'] = new OKandidat($cu->id,"user_id");
		if($O->id == ""){ die('Export Failed'); exit; }

			$OU = new OUser();
			$OU->setup($cu);
			$row = $OU->get_info("perusahaan");
			$data['OP'] = $OP = new OPerusahaan();
			$OP->setup($row);
		
		
		$html = $this->load->view($this->vpath.'/to_pdf',$data,TRUE);
        $this->_gen_pdf($html,'A4',$O->id);
        
        /*$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/detail',$data);
		$this->load->view($this->footer,$data);*/
		//unset($O);
	}

	private function _gen_pdf($html,$paper='A4',$id)
    {      
        $mpdf = new mPDF('utf-8',$paper);
        $stylesheet = file_get_contents(base_url('_assets/css/style.css'));
		$mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html);
        $mpdf->Output("CV{$id}.pdf",'D'); //D
    } 

    


}

/* End of file export.pdf */
/* Location: ./application/controllers/kandidat/export.php */