<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email_alert extends CI_Controller {

	var $cu;

	public function __construct()
	{
		parent::__construct();
		$this->fhead 	= "header";
		$this->ffoot 	= "footer";
		$this->vpath 	= "users/kandidat";
		$this->curpage 	= "kandidat/email_alert";
		$this->mainpage = "kandidat";
		//error_reporting(E_ALL);
		$this->cu = $cu = get_logged_in_user();
		if($this->cu != FALSE)
		{
			if($this->cu->role != "kandidat" )
			{	
				user_logout('kandidat');
			}else
			{
				if($this->cu->status != "aktif")
				{
					
					user_inactive();
				}
			}	
		}else
		{
			redirect('account/login/kandidat');
		}
	}
	
	public function index()
	{
		// inactive this page!!
		redirect($his->mainpage); exit;

		$id = $this->cu->id;
		$O = $data['O'] = new OKandidat($id,"user_id");

		$data['current'] = "email_alert";
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/email_alert/index',$data);	
		$this->load->view('footer',$data);
	}

}

/* End of file email_alert.php */
/* Location: ./application/controllers/kandidat/email_alert.php */