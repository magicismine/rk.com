<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function index()
	{
		show_404();
	}
	
	public function get_propinsi_ddl()
	{
		if(!$this->input->is_ajax_request()) exit('No direct script access allowed');
		extract($_POST);
		if(empty($default) && empty($no_default)) $default = "Pilih";
		if(empty($negara_id))
		{
			$empty_select = "<select {$option}><option>{$default}</option></select>";
			die($empty_select);
		}
		if(intval($negara_id) > 0)
		{
			$where = "master_lokasi_negara_id=".intval($negara_id);
		}
		$tmp = OMaster_lokasi_propinsi::drop_down_select($name,intval($propinsi_id),$option,$default,$where);
		die($tmp);
	}

	public function get_kabupaten_ddl()
	{
		if(!$this->input->is_ajax_request()) exit('No direct script access allowed');
		extract($_POST);
		if(empty($default) && empty($no_default)) $default = "Pilih";
		if(empty($propinsi_id))
		{
			$empty_select = "<select {$option}><option>{$default}</option></select>";
			die($empty_select);
		}
		if(intval($propinsi_id) > 0)
		{
			$where = "master_lokasi_propinsi_id=".intval($propinsi_id);
		}
		$tmp = OMaster_lokasi_kabupaten::drop_down_select($name,intval($kabupaten_id),$option,$default,$where);
		die($tmp);
	}
	
	public function get_kawasan_ddl()
	{
		if(!$this->input->is_ajax_request()) exit('No direct script access allowed');
		extract($_POST);
		if(empty($default) && empty($no_default)) $default = "Pilih";
		if(empty($kabupaten_id))
		{
			$empty_select = "<select {$option}><option>{$default}</option></select>";
			die($empty_select);
		}
		if(intval($kabupaten_id) > 0)
		{
			$where = "master_lokasi_kabupaten_id=".intval($kabupaten_id);
		}
		$tmp = OMaster_lokasi_kawasan::drop_down_select($name,intval($kawasan_id),$option,$default,$where);
		die($tmp);
	}
	
	public function get_subcategory_ddl()
	{
		if(!$this->input->is_ajax_request()) exit('No direct script access allowed');
		$default = "";
		extract($_POST);
		if(empty($category_id))
		{
			die('<select><option>Pilih</option></select>');
		}
		if(intval($category_id))
		{
			$where = "parent_id=".intval($category_id);
		}
		$tmp = OMaster_category::drop_down_select($name,intval($category_id),"",$default,$where);
		die($tmp);
	}
	
	public function upload($a = "")
	{
		if($a == "image")
		{
			$this->load->helper(array("image","string"));
			$this->config->load('image');
			$imagesizes = $this->config->item('image_sizes');
			
			$uploaddir = FCPATH.'/_assets/images/temp/';
			if(!is_dir($uploaddir)) mkdir($uploaddir,0777,TRUE);
			$filename = basename($_FILES['uploadfile']['name']);
			$filearr = explode(".", $filename);
			$random = time()."_".random_string("alnum", 20);
			$newfile =  $random. "." . $filearr[count($filearr)-1];
			$file = $uploaddir . $newfile;
			$size = $_FILES['uploadfile']['size'];
			$tmps = array();
			if($size>1048576)
			{
				//echo "error file size > 1 MB";
				echo 'error';
				if(file_exists($_FILES['uploadfile']['tmp_name'])) unlink($_FILES['uploadfile']['tmp_name']);
				exit;
			}
			if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
			{
				// resize the image to lower size
				$resize_newfile = "resize_".$newfile;
				resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
				echo $newfile; 
			} else {
				//die('error');
				//echo "error ".$_FILES['uploadfile']['error']." --- ".$_FILES['uploadfile']['tmp_name']." %%% ".$file."($size)";
			}
		}
		
		
		if($a == "all_files")
		{
			//$this->load->helper(array("image","string"));
			$uploaddir = FCPATH.'/_assets/all_downloads/temp/';
			if(!is_dir($uploaddir)) mkdir($uploaddir,0777,TRUE);
			$filename = basename($_FILES['uploadfile']['name']);
			$filearr = explode(".", $filename);
			/*$newfile = time()."_".random_string("alnum", 20) . "." . $filearr[count($filearr)-1];*/
			
			$newfile = $filename;
			$file = $uploaddir . $newfile;
			$size = $_FILES['uploadfile']['size'];
			$tmps = array();
			if($size>1048576)
			{
				//echo "error file size > 1 MB";
				echo 'error';
				if(file_exists($_FILES['uploadfile']['tmp_name'])) unlink($_FILES['uploadfile']['tmp_name']);
				exit;
			}
			if (move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file))
			{
				// resize the image to lower size
				//$resize_newfile = "resize_".$newfile;
				//resize_photo($file, $uploaddir.$resize_newfile, 300, "width");
				//crop_photo($file,$uploaddir.$resize_newfile,110,120);
				echo $newfile; 
			} else {
				//die('error');
				//echo "error ".$_FILES['uploadfile']['error']." --- ".$_FILES['uploadfile']['tmp_name']." %%% ".$file."($size)";
			}
		}

	}
	
}