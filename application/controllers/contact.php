<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//error_reporting(-1);
	}

	public function index()
	{
		$this->form_validation->set_rules('name', 'Nama', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'trim|required');
		$this->form_validation->set_rules('message', 'Message', 'trim|required');
		$this->form_validation->set_error_delimiters('', '<br />');


		if ($this->form_validation->run() != FALSE)
		{
			$to = get_setting('contact_email');
			$subject = 'New contact received at '.get_setting('sitename');
			$message = $this->load->view('tpl_contact',$_POST,TRUE);
			noreply_mail($to, $subject, $message);

			$this->session->set_flashdata('success', 'Pesan anda berhasil dikirim. Tim kami akan segera menanggapi pesan anda. Terima kasih.');
			redirect('contact');
			exit;
		}
		$this->load->view('header', $data);
		$this->load->view('contact_us', $data);
		$this->load->view('footer', $data);
	}

}

/* End of file contact.php */
/* Location: ./application/controllers/contact.php */