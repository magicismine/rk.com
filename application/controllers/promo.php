<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promo extends CI_Controller {

	public function index()
	{
		$get_params = get_params($_GET, array("page"));
		$page = (empty($_GET['page']) ? 0 : $_GET['page']);
		$perpage = 10;

		$data['O'] = $O = new OPerusahaan_promo;
		$data['list'] = $O->get_list($page,$perpage,"id DESC","active=1");
		$data['total'] = $total = get_db_total_rows();
		$url = current_url()."?".$get_params;
		$data['pagination'] = getPagination($total, $perpage, $url, 5, TRUE);

		$this->load->view('header',$data);
		$this->load->view('promo_kandidat/promo_kandidat_listing',$data);	
		$this->load->view('footer',$data);
	}

	public function kandidat($url_title)
	{
		$O = $data['O'] = new OPerusahaan_promo($url_title,"url_title");

		if(empty($O->id) || empty($O->row->active))
		{
			redirect('');
			exit;
		}

		$this->load->view('header',$data);
		$this->load->view('promo_kandidat/promo_kandidat_detail',$data);	
		$this->load->view('footer',$data);

		unset($OA);
	}

}

/* End of file promo.php */
/* Location: ./application/controllers/promo.php */