<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {

	var $curpage;
	var $cu;
	public function __construct()
	{
		parent::__construct();
		//error_reporting(-1);
		$this->vpath = "";
		$this->header = "header";
		$this->footer = "footer";
		$this->curpage = "search";
		$this->cu = $cu = get_logged_in_user();

	}

	public function index()
	{
		$this->load->view($this->header,$data);
		$this->load->view('search_lowongan',$data);
		$this->load->view($this->footer,$data);
	}

	public function lowongan($page=0)
	{
		$this->vpath = "lowongan";

		$O = $data['O'] = new OLowongan;
		$get_params = "?".get_params($_GET, array("page","input","keyword"));
		if(sizeof($_GET) > 0)
		{
			extract($_GET);

			/*if($kategori_id != "")
			{
				$add_sql_arr[] = "kategori_id = {$kategori_id}";
				$data['kategori_id'] = $kategori_id;
			}
			if($bidang_usaha_id != "")
			{
				$add_sql_arr[] = "bidang_usaha_id = {$bidang_usaha_id}";
				$data['bidang_usaha_id'] = $bidang_usaha_id;
			}
			if($pendidikan_id != "")
			{
				$add_sql_arr[] = "pendidikan_id = {$pendidikan_id}";	
				$data['pendidikan_id'] = $pendidikan_id;
			}
			if($level_karir_id != "")
			{
				$add_sql_arr[] = "level_karir_id = {$level_karir_id}";
				$data['level_karir_id'] = $level_karir_id;
			}
			if($waktu_kerja_id != "")
			{
				$add_sql_arr[] = "waktu_kerja_id = {$waktu_kerja_id}";
				$data['waktu_kerja_id'] = $waktu_kerja_id;
			}
			if($lokasi_negara_id != "")
			{
				$add_sql_arr[] = "lokasi_negara_id = {$lokasi_negara_id}";
				$data['lokasi_negara_id'] = $lokasi_negara_id;
			}
			if($lokasi_propinsi_id != "")
			{
				$add_sql_arr[] = "lokasi_propinsi_id = {$lokasi_propinsi_id}";
				$data['lokasi_propinsi_id'] = $lokasi_propinsi_id;
			}
			if($lokasi_kabupaten_id != "")
			{
				$add_sql_arr[] = "lokasi_kabupaten_id = {$lokasi_kabupaten_id}";
				$data['lokasi_kabupaten_id'] = $lokasi_kabupaten_id;
			}
			$data['keyword'] = $keyword = $_GET['keyword'];
			if($keyword != NULL) { $keywords = "posisi LIKE '{$keyword}%' AND ";  }
			*/
		}		
		//$add_sql_params .= implode(" AND ", $add_sql_arr);
		//var_dump($add_sql_params);die();

		$page = intval($_GET['page']);
		$perpage = 10;

		if(empty($keyword)) $orderby = "lowongans.id DESC";
		//$data['list'] = $list = OLowongan::get_list_filter($keyword,$page,$perpage,$orderby,"",$kategori_id,$bidang_usaha_id,$pendidikan_id,$level_karir_id,$waktu_kerja_id,$lokasi_propinsi_id,$lokasi_kabupaten_id);
		$filter_arr = NULL;
		$include_arr = array('waktu_kerja_id','level_karir_id','kategori_id','bidang_usaha_id','lokasi_negara_id','lokasi_propinsi_id','lokasi_kabupaten_id','lokasi_kawasan_id');
		foreach($_GET as $key => $val)
		{
			if(empty($val)) continue;
			if(in_array($key, $include_arr))
			{
				$filter_arr[$key] = $val;
			}
		}
		$data['list'] = $list = OLowongan::get_list_filter($keyword,$page,$perpage,$orderby,"",$filter_arr);
		//var_dump($this->db->last_query());
		$data['total'] = $total = get_db_total_rows();

		$url = $this->curpage."/lowongan{$get_params}";
		$data['pagination'] = getPaginationLowongan($total, $perpage, $url, 5, TRUE);

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/search',$data);
		$this->load->view($this->footer,$data);

		unset($O);
	}

	public function kandidat()
	{
		$this->vpath = "kandidat";

		$O = $data['O'] = new OKandidat;
		$get_params = "?".get_params($_GET, array("page","input","keyword"));
		
		if(sizeof($_GET) > 0)
		{
			extract($_GET);

			/*if($kategori_id != "")
			{
				$add_sql_arr[] = "kategori_id = {$kategori_id}";
				$data['kategori_id'] = $kategori_id;
			}
			if($bidang_usaha_id != "")
			{
				$add_sql_arr[] = "bidang_usaha_id = {$bidang_usaha_id}";
				$data['bidang_usaha_id'] = $bidang_usaha_id;
			}
			if($pendidikan_id != "")
			{
				$add_sql_arr[] = "pendidikan_id = {$pendidikan_id}";	
				$data['pendidikan_id'] = $pendidikan_id;
			}
			if($level_karir_id != "")
			{
				$add_sql_arr[] = "level_karir_id = {$level_karir_id}";
				$data['level_karir_id'] = $level_karir_id;
			}
			if($waktu_kerja_id != "")
			{
				$add_sql_arr[] = "waktu_kerja_id = {$waktu_kerja_id}";
				$data['waktu_kerja_id'] = $waktu_kerja_id;
			}
			if($lokasi_negara_id != "")
			{
				$add_sql_arr[] = "lokasi_negara_id = {$lokasi_negara_id}";
				$data['lokasi_negara_id'] = $lokasi_negara_id;
			}
			if($lokasi_propinsi_id != "")
			{
				$add_sql_arr[] = "asal_propinsi_id = {$lokasi_propinsi_id} OR skrg_propinsi_id = {$lokasi_propinsi_id}";
				$data['lokasi_propinsi_id'] = $lokasi_propinsi_id;
			}
			if($lokasi_kabupaten_id != "")
			{
				$add_sql_arr[] = "asal_kabupaten_id = {$lokasi_kabupaten_id} OR skrg_kabupaten_id = {$lokasi_kabupaten_id}";
				$data['lokasi_kabupaten_id'] = $lokasi_kabupaten_id;
			}
			$keyword = $this->db->escape_like_str($_GET['keyword']);
			if(empty($_GET['keyword']))
			{
				$keywords = "kandidats.nama_depan LIKE '%{$keyword}%' OR kandidats.nama_akhir LIKE '%{$keyword}%'";
			}*/
		}
		
		$add_sql_params .= implode(" AND ", $add_sql_arr);

		$page = intval($_GET['page']);
		$perpage = 10;
		if(empty($keyword)) $orderby = "kandidats.id DESC";
		//$data['list'] = $list = OKandidat::get_list_filter($keyword,$page,$perpage,$orderby,"",$kategori_id,$pendidikan_id,$level_karir_id,$waktu_kerja_id,$lokasi_propinsi_id,$lokasi_kabupaten_id);
		$filter_arr = NULL;
		$include_arr = array('kategori_id','jenis_kelamin_id','tinggi','pendidikan_id','level_karir_id','waktu_kerja_id','lokasi_propinsi_id','lokasi_kabupaten_id','lokasi_kawasan_id','umur','umur_mulai','umur_akhir');
		foreach($_GET as $key => $val)
		{
			if(empty($val)) continue;
			if(in_array($key, $include_arr) && !is_array($val))
			{
				$filter_arr[$key] = $val;
			}
		}
		//var_dump($filter_arr);
		$data['list'] = $list = OKandidat::get_list_filter($keyword,$page,$perpage,$orderby,"",$filter_arr);
		//var_dump($this->db->last_query());
		$data['total'] = $total = get_db_total_rows();
		$url = $this->curpage."/kandidat{$get_params}";
		$data['pagination'] = getPaginationLowongan($total, $perpage, $url, 5, TRUE);

		$this->load->view($this->header,$data);
		$this->load->view($this->vpath.'/search',$data);
		$this->load->view($this->footer,$data);

		unset($O);
	}



}

/* End of file search.php */
/* Location: ./application/controllers/search.php */