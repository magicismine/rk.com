<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_perusahaan extends CI_Controller {

	var $cu;
	public function __construct()
	{
		parent::__construct();	
		$this->vpath = "perusahaan";
		$this->header = "header";
		$this->footer = "footer";
		$this->curpage = "user_perusahaan";
		$this->cu = $cu = get_logged_in_user();
	}


	public function index()
	{
		redirect("");
	}

	public function details($url_name)
	{
		$O = $data['O'] = new OPerusahaan($url_name,"url_title");
		if($O->id == ""){ redirect("");}
		$this->load->view('header',$data);
		$this->load->view($this->vpath.'/detail',$data);
		$this->load->view('footer',$data);
		unset($O);
	}

}

/* End of file user_kandidat.php */
/* Location: ./application/controllers/user_kandidat.php */