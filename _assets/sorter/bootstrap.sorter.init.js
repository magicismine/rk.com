$("table").tablesorter({
    theme: "bootstrap",
    widthFixed: true,
    headerTemplate: '{content} {icon}',
    widgets: ["uitheme", "filter", "zebra"],
})
    .tablesorterPager({
    container: $(".ts-pager"),
    cssGoto: ".pagenum",
    output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'
});