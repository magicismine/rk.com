$(document).ajaxStop(function () {
    $("img.lazy").lazyload({
        skip_invisible: false,
        failure_limit: 10,
        effect: "fadeIn"
    }).removeClass("lazy");
	 $('input[type=text],input.text,input[type=password],textarea,select,input[type=file]').css({
        borderRadius: 4
    });
});
$(document).ready(function () {

    $('.dsopen').next('.tg-detail').text('Sembunyikan Pencarian Detail');
    $('.tg-detail').click(function () {
        var t = $(this);
        $('.detailSearch').slideToggle('slow', function () {
            t.toggleClass('rdown');
			$(this).toggleClass('dsopen');

            if ($(this).is(':hidden')) {

                t.text('Pencarian Detail');
            } else {
 
                t.text('Sembunyikan Pencarian Detail');
            }

        });
    });



    $('.hideList  .leftmenu').hide();
    $('input[type=text],input[type=password],textarea,select,.boxWhite,.advSearchBox,.btn_blue,.btn_Orange,.btn_Gray,.grayBox,.shortcutlist .col').css({
        borderRadius: 4
    });
    $("img.lazy").lazyload({
        skip_invisible: false,
        failure_limit: 10,
        effect: "fadeIn"
    }).removeClass("lazy");
    $('.hideList').click(function () {
        var _this = $(this);
        _this.find('.leftmenu').slideToggle('slow', function () {
            _this.find(" .arrow ").toggleClass('adown');
        });
    });

    $('#BigContTab').easytabs({
        tabActiveClass: 'on',
        transitionIn: 'slideDown',
        animationSpeed: 'slow'
    });
    $('#editcv-wrp').easytabs({
        tabActiveClass: 'on',
        transitionIn: 'slideDown',
        animationSpeed: 'slow'
    });


    $(" .wrp-companylist #paging_button li").click(function () {
        showLoader();
        $('.wrp-companylist  #paging_button li').removeClass('active');
        $(this).addClass('active');
        var loadLink = $(this).attr('data-link');
        $("#ajax-listperusahaan").load(loadLink, hideLoader);
        return false;
    });

    showLoader();
    var loadLink = $('#paging_button .active').data('link');
    $("#ajax-listperusahaan").load(loadLink);
    hideLoader();

    function showLoader() {
        $('body').append('<span class="loader">Loading </span>');
    }

    function hideLoader() {
        $('.loader').fadeOut(500);
    }

    $(".checkbox-wrap").each(function () {

        $(this).find(':checkbox').click(function () {
            var wrp = $(this).closest(".checkbox-wrap");
            var maxcheck = wrp.attr('max-check');
            //  alert(wrp.find(":checkbox:checked").length );
            if (maxcheck != "undifined") {
                if (wrp.find(":checkbox:checked").length >= maxcheck) {
                    wrp.find(":checkbox:not(:checked)").attr("disabled", "disabled");
                } else {
                    wrp.find(":checkbox").attr("disabled", false);
                }
            }

        });

    });

    $(document).on('click','.panel-container .tks-paging #paging_button .pagination a', function(e){
        showLoader();
        e.preventDefault(); e.stopPropagation();
        var t = $(this);
        var loadLink = t.attr('href');
        t.parents("#kandidat").load(loadLink, hideLoader);
    });

    get_propinsi_ddl("lokasi_");
    $(document).on('change', 'select#lokasi_negara_id', function(e){
        get_propinsi_ddl("lokasi_");
    });
    $(document).on('change', 'select#lokasi_propinsi_id', function(e){
        get_kabupaten_ddl("lokasi_");
    });
    $(document).on('change', 'select#lokasi_kabupaten_id', function(e){
        get_kawasan_ddl("lokasi_");
    });

    var chosen_config = {
    '.chosen-select' : {no_results_text:'Oops, nothing found!'},
    '.chosen-select-full' : {no_results_text:'Oops, nothing found!',width:"95%"}
    }
    for (var selector in chosen_config)
    {
        $(selector).chosen(chosen_config[selector]); 
    }
});

var get_propinsi_ddl = function(prefix)
{
    var t = $('#'+prefix+'propinsi_wrap');
    var p = {'negara_id': $('#'+prefix+'negara_id').val(), 'propinsi_id': t.data('value'), 'name': prefix+'propinsi_id', 'default': t.data('default'), 'option': t.data('option')}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: fpath+'ajax/get_propinsi_ddl',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
            get_kabupaten_ddl(prefix);
        }
    });
}

var get_kabupaten_ddl = function(prefix)
{
    var t = $('#'+prefix+'kabupaten_wrap');
    var p = {'propinsi_id': $('#'+prefix+'propinsi_id').val(), 'kabupaten_id': t.data('value'), 'name': prefix+'kabupaten_id', 'default': t.data('default'), 'option': t.data('option')}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: fpath+'ajax/get_kabupaten_ddl',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
            get_kawasan_ddl(prefix);
        }
    });
}

var get_kawasan_ddl = function(prefix)
{
    var t = $('#'+prefix+'kawasan_wrap');
    var p = {'kabupaten_id': $('#'+prefix+'kabupaten_id').val(), 'kawasan_id': t.data('value'), 'name': prefix+'kawasan_id', 'default': t.data('default'), 'option': t.data('option')}
    $.ajax({
        'type': 'POST',
        'async': false,
        url: fpath+'ajax/get_kawasan_ddl',
        data: p,
        complete: function(xhr, status)
        {
            var ret = xhr.responseText;
            t.html(ret);
        }
    });
}

var set_checkbox_flag = function(sel)
{
    var t = $(sel);
    var trgt = t.data('target');
    if(t.is(':checked') == true)
    {
        $(trgt).css('opacity', 0.4);
        $(trgt).find('input, select, textarea').attr('disabled', 'disabled');
    }
    else
    {
        $(trgt).css('opacity', 1);
        $(trgt).find('input, select, textarea').removeAttr('disabled');
    }
}