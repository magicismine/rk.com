var values = [];
var thead_tr = $('#performa-table thead tr');
var tbody_tr = $('#performa-table tbody tr');
values[0] = [$('#performa-table thead tr:eq(0) > td:eq(1)').html()];

//*
tbody_tr.each(function(){
	//values[0].push( $(this).find('td:eq(0)').data('value') );
	values[0].push( $(this).find('td:eq(0) strong').text() );
});
var i=1;
$('#performa-table thead tr:eq(1) > td').each(function(){
	values[i] = [$(this).data('value')];
	i++;
});
var i=1;
tbody_tr.each(function(){
	var j=0;
	$(this).find('td').each(function(){
		if(j>0)
		{
			values[j].push( parseInt($(this).html()) );
		}
		j++;
	});
	i++;
});
//*/
/*
$('#performa-table thead tr:eq(1) > td').each(function(){
	values[0].push( $(this).html() );
});

var i=1;
tbody_tr.each(function(){
	var j=0;
	values[i] = [];
	$(this).find('td').each(function(){
		if(j > 0)
		{
			values[i].push( parseInt($(this).html()) );
		}
		else values[i].push( $(this).data('value') );
		j++;
	});
	i++;
});
//*/
console.log(values);

// API EXEC
/* // LINE
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {

	var data = google.visualization.arrayToDataTable(values);

	var options = {
		title: 'PERFORMA ',
      	hAxis: {title: values[0][0], titleTextStyle: {color: 'orange'}, textPosition: 'out', slantedTextAngle: '45'},
      	pointSize: 5
	};

	var chart = new google.visualization.LineChart(document.getElementById('performa-chart'));
	chart.draw(data, options);
}
//*/

// BAR
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
  /*
  var data = google.visualization.arrayToDataTable([
    ['Bulan', 'Jumlah angkatan kerja pemula', 'Jumlah angkatan kerja akibat PHK','Jumlah perusahaan yang membuka kesempatan kerja','Jumlah posisi pekerjaan yang dibuka','Jumlah tenagakerja yang dibutuhkan','Jumlah pemenuhan tenagakerja'],
    ['Jan14',  1000,400,1000,400,1000,400],
    ['Feb14',  1170,460,1170,460,1170,460],
    ['Mar14',  660,1120,660,1120,660,1120],
    ['Apr14',  1030,540,1030,540,1030,540],
    ['Mei14',  311,532,983,388,852,100],
    ['Jun14',  741,321,32,321,61,531],
    ['Jul14',   521,321,74,1146,122,1028],
    ['Aug14',   321,351,32,100,122,12],
    ['Sep14',   781,321,775,431,122,531],
    ['Oct14',  781,321,1211,100,326,123],
    ['Nov14',  781,321,327,100,614,531],
    ['Des14',   781,321,32,100,734,4]
  ]);
	//*/
	var data = google.visualization.arrayToDataTable(values);
  var options = {
    title: 'PERFORMA ',
    hAxis: {title: values[0][0], titleTextStyle: {color: 'orange'}}
  };

  var chart = new google.visualization.ColumnChart(document.getElementById('performa-chart'));
  chart.draw(data, options);
}
