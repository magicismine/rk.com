$(document).ready(function() {

	var domain = "http://"+document.domain+"/index.php";

	$( ".ajax_popup" ).live('click', function(e) {
		e.preventDefault(); e.stopPropagation();
		var urlx = $(this).attr("href");
		POPUP.set_dimension(500,300);
		POPUP.set_title($(this).attr("title"));
		POPUP.ajax(urlx,'POST');
		return false;
	});
	
	$( ".ajax_popup_dimension_link" ).live('click',function(e)
	{
		e.preventDefault(); e.stopPropagation();
		var urlx = $(this).attr("href");
		var dim_arr = $(this).attr("dimension").split("x");
		var dim_len = dim_arr.length;
		var dim_w = dim_arr[dim_len-2];
		var dim_h = dim_arr[dim_len-1];
		if(dim_w == "" || dim_w == 0) dim_w = 500;
		if(dim_h == "" || dim_h == 0) dim_h = 300;
		POPUP.set_dimension(dim_w,dim_h);
		POPUP.set_title($(this).attr("title"));
		POPUP.ajax(urlx,'POST');
		return false;
	});
	
	$('form.ajax_form').live('submit',function(e)
	{
		e.preventDefault(); e.stopPropagation();
		var params = $(this).serialize();
		var urlx = $(this).attr("action");
		var selector = $(this);
		$.ajax({
				   'type': 'POST',
				   'async': false,
				   url: urlx,
				   data: params,
				   complete: function(xhr, status)
				   {
					   var data = xhr.responseText;
					   if(data == "success") location.href=window.location;
					   else {
					   		$("#dialog_error_string").html(data).show().delay(3000).fadeOut('slow');
					   }
				   }
			   });
		return false;
		
	});
	
	$('form.ajax_form_redirect').live('submit',function(e)
	{
		e.preventDefault(); e.stopPropagation();
		var params = $(this).serialize();
		var urlx = $(this).attr("action");
		var selector = $(this);
		$.ajax({
				   'type': 'POST',
				   'async': false,
				   url: urlx,
				   data: params,
				   complete: function(xhr, status)
				   {
					   var data = xhr.responseText;
					   var obj = $.parseJSON(data);
					   if(obj.status == "success") location.href = obj.redirect;
					   else {
					   		$("#dialog_error_string").html(obj.error_string).show().delay(3000).fadeOut('slow');
					   }
				   }
			   });
		return false;
		
	});
	
	//*
	var fullDate = new Date();
	$('body .datepicker').live('focus',function(){
		$(this).datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			gotoCurrent: true,
			yearRange: '-100:+30',
			/*maxDate: '+30Y',*/
			showOn:'focus'
		});
	});
	$('body .datepicker_future').live('focus',function(){
		$(this).datepicker({
			dateFormat: "dd-mm-yy",
			changeYear: true,
			changeMonth: true,
			gotoCurrent: true,
			minDate: new Date(),
			showOn:'focus'
		});
	});
	$('body .datepicker_dob').live('focus',function(){
		$(this).datepicker({
			dateFormat: "dd-mm-yy",
			yearRange: "-100:+0", // last hundred years
			changeYear: true,
			changeMonth: true,
			gotoCurrent: true,
			maxDate: new Date(),
			showOn:'focus'
		});
	});
	var dates = $( "#from, #to" ).datepicker({
		defaultDate: "+1w",
		changeMonth: true,
		minDate: new Date(),
		dateFormat: "yy-mm-dd",
		numberOfMonths: 3,
		onSelect: function( selectedDate ) {
			var option = this.id == "from" ? "minDate" : "maxDate",
				instance = $( this ).data( "datepicker" ),
				date = $.datepicker.parseDate(
					instance.settings.dateFormat ||
					$.datepicker._defaults.dateFormat,
					selectedDate, instance.settings );
			dates.not( this ).datepicker( "option", option, date );
		}
	});
	DATERANGE.init();
	//*/

});

function checkIfEmailInString(text)
{ 
    var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
}

var DATERANGE = {
	'destroy' : function()
	{
		$( ".start_dt, .end_dt" ).datepicker('destroy');
	},
	'refresh' : function()
	{
		$( ".start_dt, .end_dt" ).datepicker('refresh');
	},
	'init' : function()
	{
		//DATERANGE.destroy();
		DATERANGE.refresh();
		var cdates = $( ".start_dt, .end_dt" ).datepicker({
			//defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			minDate: new Date(),
			dateFormat: "dd-mm-yy",
			numberOfMonths: 3,
			onSelect: function( selectedDate ) {
				var option = ($(this).hasClass("start_dt") == true ? "minDate" : "maxDate"),
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				cdates.not( this ).datepicker( "option", option, date );
			}
		});
	}
}

var TABLEROW = {
	'init' : function(id)
	{
		this.id = id;
		this.par = $('table'+this.id);
		this.tbody = this.par.find('tbody');
		this.trlast = this.tbody.find('tr:last');
		if(this.par.find('tfoot.hide').length == 0)
		{
			this.par.append('<tfoot class="hide"></tfoot>');
		}
		this.tfoot = this.par.find('tfoot.hide');
		// Clone last tr on tbody to tfoot
		var trclone = this.trlast.clone();
		if(this.tfoot.find('tr').length == 0)
		{
			this.tfoot.html(trclone);
			this.tfoot.find('tr:last input, tr:last select, tr:last textarea').attr('disabled','disabled');
			this.tfoot.find('tr:last input').removeAttr('id').removeClass('hasDatepicker');
		}
	},
	'add' : function()
	{
		var trclone = this.tfoot.children().clone();
		this.tbody.append(trclone);
		this.tbody.find('tr:last input, tr:last select, tr:last textarea').removeAttr('disabled');
		this.tbody.find('tr:last input, tr:last textarea').val('');
		this.tbody.find('tr .remove_row').show();
		this.tbody.find('tr:not(:last) .add_row').hide();
		//DATERANGE.init();
	},
	'remove' : function(sel)
	{
		var tr_n = this.tbody.find('tr').length;
		var trsel = sel.parent().parent();
		if(tr_n > 1)
		{
			trsel.remove();
			this.tbody.find('tr:last .add_row').show();
			if((tr_n-1) == 1)
			{
				this.tbody.find('tr:last .remove_row').hide();
			}
		}
		//DATERANGE.init();
	}
}
var POPUP = {
	"w" : 400,
	"h" : 300,
	"title" : "Dialog",
	"modal" : true,
	"set_title" : function (title) 
	{
		this.title = title;
	},
	"set_dimension" : function(w,h)
	{
		this.w = w;
		this.h = h;
	},
	"content": function(text)
	{
		$('#dialog').html(text);
	},
	"ajax": function (url, type, data, callback)
	{
		$.ajax({
			   type: type,
			   url: url,
			   data: data,
			   async: false,
			   success: function (resp)
			   {
					POPUP.content(resp);
					POPUP.open();
			   }
			   });
	},
	"ajax_time": function (url, type, data, callback)
	{
		$.ajax({
			   type: type,
			   url: url,
			   data: data,
			   async: false,
			   success: function (resp)
			   {
					POPUP.content(resp);
					POPUP.open();
					POPUP.close_time();
			   }
			   });
	},
	"open": function()
	{
		$('#dialog').dialog({ title : this.title, resizable : false, width: this.w, height: this.h, modal: this.modal, draggable: false, show: "blind", hide: "explode" });
	},
	"close_time": function(time)
	{
		if(time == null || parseInt(time) <= 0) time = 5000;
		var popupTimeout = setTimeout("$('#dialog').dialog('close');", time);
	},
	"close": function()
	{
		clearTimeout(popupTimeout);
		$('#dialog').dialog('close');
	}
};